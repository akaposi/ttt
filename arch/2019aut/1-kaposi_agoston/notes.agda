module notes where

open import lib

{-
→ = "\r" or "\->"
← = "\l" or "\<-"
ℕ = "\bN"
λ = "\lambda" or "\Gl"
₁ = "\_1"
ₙ = "\_n"
≡ = "\=="
≥ = "\>="
≤ = "\<="
⊤ = "\top"
⊥ = "\bot"
× = "\x"
⊎ = "\u+"
↔ = "\<->"
¬ = "\neg"
∀ = "\forall" or "\all"
Σ = "\Sigma"
‌≠ = "\neq"
∧ = "\and"

Hotkeys:
C-c C-l     : load in agda compiler
C-c C-,     : agda shows what we have and what is the goal
C-c C-.     : agda shows the typed in expression (Goal and Have)
C-c C-space : accept a hole
C-u C-u C-c C-, : normalized C-c C-,
C-c C-c     : pattern matching for the variables in the hole

Creating a hole: just type ? and load it in agda

If you switched off the agda input mode, type C-\

-}
