open import Lib hiding (_≟ℕ_)
open import Lib.Containers.List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj m n eq = cong pred' eq

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m .m refl = refl

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj = {!!}

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl = {!!}

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr = {!!}

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 = {!!}

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false ()

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst (λ {true -> ⊤; false -> ⊥}) e tt

zero≠sucn : {n : ℕ} → zero ≢ ℕ.suc n
zero≠sucn e = subst (λ {zero -> ⊤; (suc n) -> ⊥}) e tt

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn zero e = zero≠sucn e
n≠sucn (suc n) e = n≠sucn n (cong pred' e)

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' n = {!!}

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node = {!!}

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons = {!!}

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 (m , hyp) = subst (λ {(suc _) -> ⊤; zero -> ⊥}) helper tt
  where
  helper : suc m ≡ 0
  helper = cong pred' (trans (comm+ 2 m) hyp)

{-

2 + m = 1 -- ilyet kéne csinálni, és akkor:
1 + m = 0
suc m = 0

    comm+
2 + m = m + 2 = 1
-}

n≤sucn : ∀ (n : ℕ) -> n ≤ suc n
n≤sucn n = 1 , refl

sucinj≤ : ∀ (n m : ℕ) -> n ≤ m -> suc n ≤ suc m
sucinj≤ n m (k , hyp) = k , trans (comm+ k (suc n))
                           (trans (cong suc (comm+ n k))
                                  (cong suc hyp))
{-
        comm+       comm+            hyp
k + suc n = suc (n + k) = suc (k + n) = suc m
-}

predinj≤ : ∀ (n m : ℕ) -> suc n ≤ suc m -> n ≤ m
predinj≤ = {!!}

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
false ≟Bool false = inl refl
false ≟Bool true = inr λ eq -> subst (λ {false -> ⊤; true -> ⊥}) eq tt
               -- inr (λ { () })
true ≟Bool false = {!!}
true ≟Bool true = inl refl

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')   --\?=
zero ≟ℕ zero = inl refl
zero ≟ℕ suc n' = inr λ { () }
suc n ≟ℕ zero = {!!}
suc n ≟ℕ suc n' = case (n ≟ℕ n') (λ eq -> inl (cong suc eq))
                                    λ neq -> inr λ sn=sn' -> neq (cong pred' sn=sn')

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
_≟BinTree_ = {!!}

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ = {!!}
