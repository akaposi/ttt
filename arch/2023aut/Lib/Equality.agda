{-# OPTIONS --safe #-}

module Lib.Equality where

open import Lib.Equality.Type public
open import Lib.Equality.Base public
open import Lib.Equality.Properties public

