{-# OPTIONS --safe #-}

module Lib.Fin where

open import Lib.Fin.Type public
open import Lib.Fin.Base public
open import Lib.Fin.Literals public
open import Lib.Fin.Properties public