{-# OPTIONS --safe #-}

module Lib.Nat.Instances where

open import Lib.Nat.Instances.DecidableEquality public
open import Lib.Nat.Instances.Eq public
