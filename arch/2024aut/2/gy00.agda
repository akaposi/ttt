module gy00 where

--import Lib

id : {A : Set} → A → A
id x = x

id' : {A : Set} → A → A
id' = λ x → x 
