module gy02 where

--open import Lib hiding (comm×; assoc×; flip; curry; uncurry)

open import Lib.Equality
open import Lib.Nat
open import Lib.Sigma hiding (comm×; assoc×; curry; uncurry)
open import Lib.Bool
open import Lib.Empty
open import Lib.Sum


f : ℕ → Set
f x = ℕ

g : (f 10) → ℕ
g x = 5

-- α-konverzió, renaming
id= : ∀{i}{A : Set i} → (λ (x : A) → x) ≡ (λ y → y)
id= = refl

-- Mesélni róla:
-- Függvények β-szabálya, η-szabálya -- ha nem volt még.
-- Esetleg konkrét példán megmutatni.

{-

f :: Int -> Int
--f x = x * 20
f = (*) 20

g x = h x ---> g = \ x -> h x
               g = h

k = (\ x -> x + 6) $ 10
k = 10 + 6

t :: (Int , String)
t = (36 , "körte")

h = fst t
h = 36
j = snd t
j = "körte"

-}

------------------------------------------------------
-- simple finite types
------------------------------------------------------

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flip : ℕ × Bool → Bool × ℕ
flip (fst₁ , snd₁) = snd₁ , fst₁

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flipback : Bool × ℕ → ℕ × Bool
flipback x = (snd x) , (fst x)

-- Vegyük észre, hogy az előző két függvényben bármilyen más csúnya dolgot is lehetne csinálni.
-- Írj rá példát itt!

flipback' : Bool × ℕ → ℕ × Bool
flipback' x = 9 , true

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
comm× : {A B : Set} → A × B → B × A
comm× x = (snd x) , (fst x)

comm×back : {A B : Set} → B × A → A × B
comm×back = comm×
-- Ezekben lehetetlen hülyeséget csinálni.
-- Hányféleképpen lehetséges implementálni ezt a két fenti függvényt?


-- ALGEBRAI ADATTÍPUSOK ELEMSZÁMAI:

b1 b2 : Bool × ⊤
b1 = true , tt
b2 = false , tt
b1≠b2 : b1 ≡ b2 → ⊥
b1≠b2 ()

t1 t2 : ⊤ ⊎ ⊤
t1 = inl tt
t2 = inr tt
t1≠t2 : t1 ≡ t2 → ⊥
t1≠t2 ()

bb1 bb2 bb3 : Bool ⊎ ⊤
bb1 = inr tt
bb2 = inl false
bb3 = inl true
bb1≠bb2 : bb1 ≡ bb2 → ⊥
bb1≠bb2 ()
bb1≠bb3 : bb1 ≡ bb3 → ⊥
bb1≠bb3 ()
bb2≠bb3 : bb2 ≡ bb3 → ⊥
bb2≠bb3 ()

{-

data Nev = Constr | Const

data Bot

-}

ee : (⊤ → ⊥) ⊎ (⊥ → ⊤)
ee = inr (λ {x → exfalso x})

d : (⊤ ⊎ (⊤ × ⊥)) × (⊤ ⊎ ⊥)
d = (inl tt) , (inl tt)
-- Ezek alapján hogy lehet megállapítani, hogy melyik típus hány elemű?
-- | ⊤ | = 1
-- | ⊥ | = 0
-- | Bool | = 2
-- | Bool ⊎ ⊤ | = 3
-- | A ⊎ B | = | A | + | B |
-- | A × B | = | A | * | B |
-- | Bool × Bool × Bool | = 8
-- | ⊤ → ⊥ | = | ⊥ | ^ | ⊤ | = 0 ^ 1 = 0
-- | ⊥ → ⊤ | = 1
-- | ⊥ → ⊥ | = 1
-- | Bool → ⊥ | = 0
-- | Bool → ⊤ | = 1
-- | ⊤ → Bool | = 2
-- | A → B | = | B | ^ | A |
-- | Bool → Bool → Bool | = | Bool → (Bool → Bool) | = |Bool → Bol| ^ |Bool| = (|Bool| ^ |Bool|) ^ |Bool| = (2 ^ 2 ) ^ 2 = 16


-- Ezek alapján milyen matematikai állítást mond ki és bizonyít a lenti állítás?
-- Válasz:
from' : {A : Set} → (A × A) → (Bool → A)
from' x false = snd x
from' x true = fst x
to' : {A : Set} → (Bool → A) → A × A
to' = λ f → f true , f false
testfromto1 : {A : Set}{a b : A} → fst (to' (from' (a , b))) ≡ a
testfromto1 = refl
testfromto2 : {A : Set}{a b : A} → snd (to' (from' (a , b))) ≡ b
testfromto2 = refl
testfromto3 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) true ≡ a
testfromto3 = refl
testfromto4 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) false ≡ b
testfromto4 = refl

------------------------------------------------------
-- all algebraic laws systematically
------------------------------------------------------

-- (⊎, ⊥) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc⊎ : {A B C : Set} → (A ⊎ B) ⊎ C ↔ A ⊎ (B ⊎ C)
assoc⊎ .fst (inl (inl a)) = inl a
assoc⊎ .fst (inl (inr b)) = inr (inl b)
assoc⊎ .fst (inr b) = inr (inr b)
assoc⊎ .snd (inl a) = inl (inl a)
assoc⊎ .snd (inr (inl a)) = inl (inr a)
assoc⊎ .snd (inr (inr b)) = inr b

idl⊎ : {A : Set} → ⊥ ⊎ A ↔ A
idl⊎ = {!!}

idr⊎ : {A : Set} → A ⊎ ⊥ ↔ A
idr⊎ = {!!}

comm⊎ : {A B : Set} → A ⊎ B ↔ B ⊎ A
comm⊎ = {!!}

-- (×, ⊤) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc× : {A B C : Set} → (A × B) × C ↔ A × (B × C)
assoc× = {!!}

idl× : {A : Set} → ⊤ × A ↔ A
idl× .fst x = snd x
idl× .snd x = tt , x

idr× : {A : Set} → A × ⊤ ↔ A
idr× = {!!}

-- ⊥ is a null element

null× : {A : Set} → A × ⊥ ↔ ⊥
null× .fst = snd
null× .snd x = exfalso x , x

-- distributivity of × and ⊎

dist : {A B C : Set} → A × (B ⊎ C) ↔ (A × B) ⊎ (A × C)
dist = {!!}

-- exponentiation laws

curry : ∀{A B C : Set} → (A × B → C) ↔ (A → B → C)
curry .fst x y z = x (y , z)
curry .snd x y = x (fst y) (snd y)

⊎×→ : {A B C D : Set} → ((A ⊎ B) → C) ↔ (A → C) × (B → C)
⊎×→ = {!!}

law^0 : {A : Set} → (⊥ → A) ↔ ⊤
law^0 = {!!}

law^1 : {A : Set} → (⊤ → A) ↔ A
law^1 .fst x = x tt
law^1 .snd x x₁ = x

law1^ : {A : Set} → (A → ⊤) ↔ ⊤
law1^ .fst x = tt
law1^ .snd x x₁ = x

---------------------------------------------------------
-- random isomorphisms
------------------------------------------------------

-- Milyen algebrai állítást mond ki az alábbi típus?
iso1 : {A B : Set} → (Bool → (A ⊎ B)) ↔ ((Bool → A) ⊎ Bool × A × B ⊎ (Bool → B))
iso1 = {!!}

iso2 : {A B : Set} → ((A ⊎ B) → ⊥) ↔ ((A → ⊥) × (B → ⊥))
iso2 = {!!}

iso3 : (⊤ ⊎ ⊤ ⊎ ⊤) ↔ Bool ⊎ ⊤
iso3 = {!!}
testiso3 : fst iso3 (inl tt) ≡ fst iso3 (inr (inl tt)) → ⊥
testiso3 ()
testiso3' : fst iso3 (inl tt) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3' ()
testiso3'' : fst iso3 (inr (inl tt)) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3'' ()

iso4 : (⊤ → ⊤ ⊎ ⊥ ⊎ ⊤) ↔ (⊤ ⊎ ⊤)
iso4 = {!!} , {!!}
testiso4 : fst iso4 (λ _ → inl tt) ≡ fst iso4 (λ _ → inr (inr tt)) → ⊥
testiso4 ()
testiso4' : snd iso4 (inl tt) tt ≡ snd iso4 (inr tt) tt → ⊥
testiso4' ()
  