module gy06 where

open import Lib hiding (K)


{-

ZH Megbeszlés...

-}

-- (x : A) → fv x

--------------------------------------------------------
-- Elmélet: Függőtípusok elemszámai
--------------------------------------------------------
{-
A függőtípusok is algebrai adattípusoknak számítanak, így természetesen a függőtípusok elemszámai is megadhatók könnyedén.
Nem véletlen, hogy a típusok nevei rendre Σ és Π.


Emlékeztetőül:
| A ⊎ B | = |A| + |B|
| A × B | = |A| ∙ |B|
| A → B | = |B| ^ |A|

Tfh:
P : Bool → Set
P true = Bool
P false = ⊥

Σ Bool P hány elemű lesz?
Ha a Bool-om true (1 konkrét érték), akkor Bool típusú eredményt kell a másik részbe írnom, tehát eddig 2¹ = 2
De ha a Bool-om false (1 konkrét érték), akkor ⊥ típusú eredmény kell, tehát 0¹ = 0

Tehát a nap végén | Σ Bool P | = |Bool| + |⊥| = 2, mert a P egy Bool-tól függő típust ad eredményül, tehát maga a típus vagy P true típusú értéket tartalmaz vagy P false-ot.
Az ilyen "vagy" kapcsolatról megbeszéltük korábban, hogy az az összeadást jelenti.

Legyen most:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Σ (Maybe Bool) P? Igazából a típus egyes elemei alapján csak meg kell nézni, hogy hány elemű típust adnak vissza.
| Σ (Maybe Bool) P | = | P nothing | + | P (just true) | + | P (just false) | = |⊤| + |Bool| + |Maybe Bool → Bool| = 1 + 2 + 8 = 11

Ez alapján az intuíció az lehet, hogy | Σ A B | = Σ (i : A) |B i|; tehát csak össze kell adni az egyes típusokból képzett új típusok elemszámát és ennyi.
(Nem véletlen, hogy Σ a típus neve. Ellenőrizhető, hogy A × B elemszáma könnyen ki fog jönni, hogy ha B nem függőtípus.)

Mi a helyzet, ha ugyanezt játszuk Π-vel? Hány elemű lesz Π A B?

Megint konkrét helyzetben, legyen:
P : Bool → Set
P true = Bool
P false = ⊥

| Π Bool P | =⟨ Agda szintaxissal ⟩= | (b : Bool) → P b | kell.
A függvényeknek totálisaknak kell lenniük, tehát ez azt jelenti, hogy MINDEN lehetséges b : Bool értékre P b-nek definiáltnak kell lennie, false-ra ÉS true-ra is.
Intuíció alapján | P true | ÉS | P false | kelleni fog, az ÉS kapcsolat matematikában a szorzást szokta jelenteni, tehát | P true | ∙ | P false | elemszámú lesz
ez a kifejezés.
| P true | ∙ | P false | = |Bool| ∙ |⊥| = 2 ∙ 0 = 0
Próbáljuk meg definiálni ezt a függvényt:
-}
P₁ : Bool → Set
P₁ true = Bool
P₁ false = ⊥

{-
ΠBoolP : Π Bool P₁
ΠBoolP false = {!   !}
ΠBoolP true = true
-}
-- Rájöhetünk, hogy a false ággal gondok lesznek.
{-
Következő példa, ez a P már ismerős:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Π (Maybe Bool) P?
A függvények továbbra is totálisak kell legyenek. Ez azt jelenti, hogy a függvény definiálva kell legyen (P nothing)-ra, (P (just true))-ra ÉS (P (just false))-ra is,
tehát lesz | P nothing | ∙ | P (just true) | ∙ | P (just false) | elemünk, |⊤| ∙ |Bool| ∙ |Maybe Bool → Bool| = 1 ∙ 2 ∙ 2³ = 16 elemű lesz Π (Maybe Bool) P.

Intuíció alapján általánosan | Π A B | = Π (i : A) | B i |, tehát csak az összes B-ből képezhető típus elemszámát össze kell szorozni.

Gyakorlás:
Adott a következő P:

P : Bool × Bool → Set
P (true , true) = ⊤
P (true , false) = Bool
P (false , true) = Bool → Bool ⊎ ⊤
P (false , false) = Bool ⊎ ⊤ → Bool

Hány elemű lesz Σ (Bool × Bool) P? = |⊤| + |Bool| + |Bool → Bool ⊎ ⊤| + |Bool ⊎ ⊤ → Bool| = 1 + 2 + 9 + 8 = 20
Hány elemű lesz Π (Bool × Bool) P? = |⊤| * |Bool| * |Bool → Bool ⊎ ⊤| * |Bool ⊎ ⊤ → Bool| = 1 * 2 * 9 * 8 = 144

Kicsit érdekesebb ezeket vegyíteni, de az elv ugyanaz marad.
Marad ugyanaz a P.

Hány elemű lesz Π (a : Bool) (Σ (b : Bool) (P (a , b)))? ( Agda szintaxissal (a : Bool) → Σ Bool (λ b → P (a , b)) ) = |Σ (b : Bool) (P (true , b))| * |Σ (b : Bool) (P (false , b))| = (|P (true, true)| + |P true false|) * (|P false true| +  |P false false|) = (|⊤| + |Bool|) * (|Bool → Bool ⊎ ⊤| + |Bool ⊎ ⊤ → Bool|) = (3) * (17) = 51
Hány elemű lesz Σ (a : Bool) (Π (b : Bool) (P (a , b)))? ( Agda szintaxissal Σ Bool λ a → (b : Bool) →  P (a , b)) )
-}

----------------------------------------------
-- Some Sigma types
----------------------------------------------

Σ=⊎ : {A B : Set} → Σ Bool (if_then A else B) ↔ A ⊎ B
fst Σ=⊎ (false , snd₁) = inr snd₁
fst Σ=⊎ (true , snd₁) = inl snd₁
snd Σ=⊎ (inl a) = true , a
snd Σ=⊎ (inr b) = false , b

Σ=× : {A B : Set} → Σ A (λ _ → B) ↔ A × B
fst Σ=× (fst₁ , snd₁) = fst₁ , snd₁
snd Σ=× (fst₁ , snd₁) = fst₁ , snd₁

-- Π A F is essentially (a : A) → F a
-- what does this mean?

                    -- Π A (λ _ → B)
Π=→ : {A B : Set} → ((a : A) → (λ _ → B) a) ≡ (A → B)
Π=→ = refl

                    -- Π Bool (if_then A else B)
→=× : {A B : Set} → ((b : Bool) → if b then A else B) ↔ A × B
fst →=× x = (x true) , (x false)
snd →=× x false = snd x
snd →=× x true = fst x

dependentCurry : {A : Set}{B : A → Set}{C : (a : A) → B a → Set} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
fst dependentCurry x w = x (fst w) (snd w)
snd dependentCurry x a b = x (a , b)

---------------------------------------------------------
-- propositional logic
------------------------------------------------------

-- Curry-Howard izomorfizmus
-- Elmélet:
--   ∙ átalakítani logikai állításokat típusokra.
--   ∙ formalizálni állításokat típusokkal.
--   × = ∧ = konjunkció
--   ⊎ = ∨ = diszjunkció
--   ¬ = ¬ =  = A → ⊥
--   ⊃ = → = implikáció

--------------------------------------------------
-- Formalisation
--------------------------------------------------

-- Formalizáljuk a mondatokat!

-- Az egyes formalizált alap mondatrészeket vegyük fel modul paraméterként, akkor szépen fog működni minden.
module Formalise (Sun : Set) (Rain : Set) (Umbrella : Set) (Rainbow : Set) where

  -- Nem süt a nap.
  form1 : Set
  form1 = ¬ Sun

  -- Esik az eső és süt a nap.
  form2 : Set
  form2 = Rain × Sun

  -- Nem kell az esernyő vagy esik az eső.
  form3 : Set
  form3 = ¬ Umbrella ⊎ Rain

  -- Ha esik az eső és süt a nap, akkor van szivárvány.
  form4 : Set
  form4 = Rain × Sun → Rainbow

  -- Van szivárvány.
  K : Set
  K = Rainbow

---- Következményfogalom (logika tárgy 1-3. gyakorlat)
  -- Agdában legegyszerűbben szintaktikus következményekkel lehet foglalkozni.

  -- Mondd ki, és bizonyítsd be, hogy a fenti állításokból következik a K.
  -- A típusban kell kimondani az állítást; az állítás kimondásához az eldöntésprobléma tételét kell használni.
  -- Két féleképpen lehet bizonyítani.

  Köv : Set
  {-
   form1 ⊃ form2 ⊃ form3 ⊃ from4 ⊃ K
   form1 ∧ form2 ∧ form3 ∧ from4 ∧ ¬ K
  -}
  Köv = form1 → form2 → form3 → form4 → K 

  Köv1 : Köv
  Köv1 x x₁ (inl a) x₃ = x₃ ((fst x₁) , snd x₁)
  Köv1 x x₁ (inr b) x₃ = x₃ ((fst x₁) , snd x₁)

  Köv2 : Köv
  Köv2 x x₁ x₂ x₃ = exfalso (x (snd x₁))

----------------------------------------------------------------------------

subt-prod : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
subt-prod x x₁ x₂ = (x (fst x₂)) , (x₁ (snd x₂))

subt-fun : {A A' B B' : Set} → (A → A') → (B → B') → (A' → B) → (A → B')
subt-fun x x₁ x₂ x₃ = x₁ (x₂ (x x₃))

anything : {X Y : Set} → ¬ X → X → Y
anything x x₁ = exfalso (x x₁)

ret : {X : Set} → X → ¬ ¬ X
ret x x₁ = x₁ x

{-
unret : {X : Set} → ¬ ¬ X → X
unret x = exfalso (x (λ x₁ → {!   !}))
-}

fun : {X Y : Set} → (¬ X) ⊎ Y → (X → Y)
fun (inl a) x₁ = exfalso (a x₁)
fun (inr b) x₁ = b

-- De Morgan

dm1 : {X Y : Set} →  ¬ (X ⊎ Y) ↔ ¬ X × ¬ Y
fst dm1 x = (λ x₁ → x (inl x₁)) , λ x₁ → x (inr x₁)
snd dm1 x (inl a) = fst x a
snd dm1 x (inr b) = snd x b

dm2 : {X Y : Set} → ¬ X ⊎ ¬ Y → ¬ (X × Y)
dm2 (inl a) x₁ = a (fst x₁)
dm2 (inr b) x₁ = b (snd x₁)

dm2b : {X Y : Set} → ¬ ¬ (¬ (X × Y) → ¬ X ⊎ ¬ Y)
dm2b x = x (λ y → inl λ k → x (λ _ → inr λ h → y (k , h)))

-- stuff

nocontra : {X : Set} → ¬ (X ↔ ¬ X)
nocontra (fst₁ , snd₁) = fst₁ (snd₁ (λ x → fst₁ x x)) ((snd₁ (λ x → fst₁ x x)))

¬¬invol₁ : {X : Set} → ¬ ¬ ¬ ¬ X ↔ ¬ ¬ X
fst ¬¬invol₁ x x₁ = x (λ y → y (λ k → x₁ k))
snd ¬¬invol₁ x x₁ = x₁ (λ y → x y)

¬¬invol₂ : {X : Set} → ¬ ¬ ¬ X ↔ ¬ X
fst ¬¬invol₂ x x₁ = x (λ y → y x₁)
snd ¬¬invol₂ x x₁ = x₁ (λ y → x y)

nnlem : {X : Set} → ¬ ¬ (X ⊎ ¬ X)
nnlem x = x (inr λ y → x (inl y))

nndnp : {X : Set} → ¬ ¬ (¬ ¬ X → X)
nndnp x = x (λ y → exfalso (y (λ k → x (λ _ → k))))

dec2stab : {X : Set} → (X ⊎ ¬ X) → (¬ ¬ X → X)
dec2stab (inl a) x = a
dec2stab (inr b) x = exfalso (x b)

-- you have to decide:
{-
Dec : Set → Set
Dec A = A ⊎ ¬ A
-}

open import Lib.Dec.PatternSynonym

ee1 : {X Y : Set} → Dec (X ⊎ Y → ¬ ¬ (Y ⊎ X))
ee1 = yes (λ x x₁ → x₁ (case x inr inl))

ee2 : {X : Set} → Dec (¬ (X ⊎ ¬ X))
ee2 = no λ x → x (no (λ k → x (yes k)))

e3 : {X : Set} → Dec (¬ (X → (¬ X → X)))
e3 = no (λ x → x (λ x₁ x₂ → x₁))

e4 : Dec ℕ
e4 = yes zero

e5 : Dec ⊥
e5 = no (λ x → x)

e6 : {X : Set} → Dec (⊥ → X ⊎ ¬ X)
e6 = {!!}

e7 : {X : Set} → Dec (X × ¬ X → ¬ X ⊎ X)
e7 = {!!}

e8 : {X : Set} → Dec ((X → X) → ⊥)
e8 = {!!}

f1 : {X Y : Set} → ¬ ¬ X ⊎ ¬ ¬ Y → ¬ ¬ (X ⊎ Y)
f1 = {!!}

f2 : ({X Y : Set} → ¬ (X × Y) → ¬ X ⊎ ¬ Y) → {X Y : Set} → ¬ ¬ (X ⊎ Y) → ¬ ¬ X ⊎ ¬ ¬ Y
f2 = {!!}

----------------------------------------------------------------------
-- Not exactly first order logic but kinda is and kinda isn't.

f3 : Dec ((X Y : Set) → X ⊎ Y → Y)
f3 = no (λ x → x ⊤ ⊥ (yes tt))

f4 : Dec ((X Y Z : Set) → (X → Z) ⊎ (Y → Z) → (X ⊎ Y → Z))
f4 = no λ x → x ⊤ ⊥ _ (no (λ x₁ → x₁)) (inl tt)

f5 : Dec ((X Y Z : Set) → (X → Z) × (Y → Z) → (X × Y → Z))
f5 = yes (λ X Y Z x x₁ → fst x (fst x₁))

f6 : Dec ((X Y Z : Set) → (X × Y → Z) → (X → Z) × (Y → Z))
f6 = no (λ x → fst (x ⊤ ⊥ _ snd) tt)

f7 : Dec ((X Y Z : Set) → (X ⊎ Y × Z) → (X ⊎ Y) × (X ⊎ Z))
f7 = {!!}

f8 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → ((X ⊎ Y) × Z))
f8 = {!!}

f9 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → (X ⊎ Y × Z))
f9 = {!!}
 