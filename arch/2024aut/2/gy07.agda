module gy07 where

open import Lib
open import Lib.Dec.PatternSynonym

------------------------------------------------------
-- statements as parameters
------------------------------------------------------

blowUp : ((A : Set) → ¬ A) → ⊥
blowUp f = f (¬ ⊥) (λ x → x)
-- what's the difference with this?
-- (A : Set) → ¬ A → ⊥

-- something like this may appear in the exam

---------------------------------------------------------
-- predicate (first order) logic example
---------------------------------------------------------

notExists↔noneOf : ∀{i}{A : Set i} → (P : A → Set) →
                        (∀ x → ¬ (P x)) ↔ ¬ (Σ A (λ x → P x))
notExists↔noneOf P .fst x x₁ = x (fst x₁) (snd x₁)
notExists↔noneOf P .snd x x₁ x₂ = x (x₁ , x₂)

module People
  (Person    : Set)
  (Ann       : Person)
  (Kate      : Person)
  (Peter     : Person)
  (_childOf_ : Person → Person → Set)
  (_sameAs_  : Person → Person → Set) -- ez most itt az emberek egyenlosege
  where

  -- Define the _hasChild predicate.
  _hasChild : Person → Set
  x hasChild = Σ Person λ p → p childOf x

  -- Formalise: Ann is not a child of Kate.
  ANK : Set
  ANK = ¬ (Ann childOf Kate)

  -- Formalise: there is someone with exactly one child.
  ONE : Set
  ONE = Σ Person λ p → Σ Person λ child → child childOf p × (∀ (x : Person) → x childOf p → x sameAs child)

  -- Define the relation _parentOf_.
  _parentOf_ : Person → Person → Set
  x parentOf y = y childOf x

  -- Formalise: No one is the parent of everyone.
  NOPE : Set
  NOPE = ¬ Σ Person λ person → ∀ (other : Person) → person parentOf other

  -- Prove that if Ann has no children then Kate is not the child of Ann.
  AK : ¬ (Σ Person λ y → y childOf Ann) → ¬ (Kate childOf Ann)
  AK x x₁ = x (Kate , x₁)

  -- Prove that if there is no person who is his own parent than no one is the parent of everyone.
  ¬xpopxthenNOPE : ¬ (Σ Person λ x → x parentOf x) → NOPE
  ¬xpopxthenNOPE x x₁ = x ((fst x₁) , (snd x₁ (fst x₁)))

---------------------------------------------------------
-- predicate (first order) logic laws
---------------------------------------------------------

∀×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a × Q a)  ↔ ((a : A) → P a) × ((a : A) → Q a)
∀×-distr A P Q .fst x = (λ a → fst (x a)) , λ a → snd (x a)
∀×-distr A P Q .snd x a = (fst x a) , (snd x a)

∀⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a) ⊎ ((a : A) → Q a) → ((a : A) → P a ⊎ Q a)
∀⊎-distr A P Q x a = case x (λ f → yes (f a)) λ f → no (f a)
-- ez miért csak odafelé megy?
-- miért nem ↔ van közte?

Σ×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a × Q a)  → Σ A P × Σ A Q
Σ×-distr A P Q x = ((fst x) , fst (snd x)) , ((fst x) , snd (snd x))

Σ⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a ⊎ Q a)  ↔ Σ A P ⊎ Σ A Q
Σ⊎-distr A P Q .fst (fs , sn) = case sn (λ x → yes (fs , x)) λ x → no (fs , x)
Σ⊎-distr A P Q .snd (yes a) = (fst a) , (yes (snd a))
Σ⊎-distr A P Q .snd (no b) = (fst b) , (no (snd b))

¬∀        :    (A : Set)(P : A → Set)              → (Σ A λ a → ¬ P a)      → ¬ ((a : A) → P a)
¬∀ A P x x₁ = snd x (x₁ (fst x))

-- Ugyanez van a fájl tetején is:
¬Σ        :    (A : Set)(P : A → Set)              → (¬ Σ A λ a → P a)      ↔ ((a : A) → ¬ P a)
¬Σ A P .fst x a x₁ = x (a , x₁)
¬Σ A P .snd x x₁ = x (fst x₁) (snd x₁)

¬¬∀-nat   :    (A : Set)(P : A → Set)              → ¬ ¬ ((x : A) → P x)    → (x : A) → ¬ ¬ (P x)
¬¬∀-nat A P x x₁ x₂ = x (λ g → x₂ (g x₁))

∀⊎-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (((a : A) → P a ⊎ Q a) → ((a : A) → P a) ⊎ ((a : A) → Q a)))
-- ∀a:(P a ∨ Q a) → (∀a: P a) ∨ (∀a: Q a)
∀⊎-distr' x = case (x Bool (if_then ⊤ else ⊥) (if_then ⊥ else ⊤) λ where false → inr tt
                                                                         true → inl tt) (λ f → f false) λ f → f true

Σ×-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (Σ A P × Σ A Q → Σ A λ a → P a × Q a))
-- (∃a: P a) ∧ (∃a: Q a) → ∃a : P a ∧ Q a 
Σ×-distr' w with w Bool (if_then ⊤ else ⊥) (if_then ⊥ else ⊤) ((true , tt) , (false , tt))
... | false , b = fst b
... | true , b = snd b
 
Σ∀       : (A B : Set)(R : A → B → Set)        → (Σ A λ x → (y : B) → R x y) → (y : B) → Σ A λ x → R x y
Σ∀ A B R x y = (fst x) , (snd x y)
AC       : (A B : Set)(R : A → B → Set)        → ((x : A) → Σ B λ y → R x y) → Σ (A → B) λ f → (x : A) → R x (f x)
AC A B R x = (λ a → fst (x a)) , λ a → snd (x a)
  