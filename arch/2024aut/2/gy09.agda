module gy09 where

-- open import Lib

open import Lib.Nat renaming (assoc+ to ass+)
open import Lib.Equality
open import Lib.Function
open import Lib.Empty
open import Lib.Sigma
open import Lib.Unit

---------------------------------------------------------
-- equational reasoning
------------------------------------------------------

_≡⟨_⟩_ : ∀{i}{A : Set i}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
x ≡⟨ x₁ ⟩ x₂ = trans { x = x } x₁ x₂

_◾ : ∀{i}{A : Set i}(x : A) → x ≡ x
x ◾ = refl

infixr 5 _≡⟨_⟩_
infixl 6 _◾

p4 : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4 x y = x + (y + zero) + x 
    ≡⟨ cong (λ e → x + e + x) (idr+ y) ⟩ 
  (x + y + x) 
    ≡⟨ (cong (_+ x) (comm+ x y)) ⟩
  y + x + x 
    ≡⟨ ass+ y x x ⟩
  (y + (x + x))
    ≡⟨ (comm+ y (x + x)) ⟩
  cong (_+ y) ((x + x) 
    ≡⟨ (cong (x +_) (sym (idr+ x))) ⟩
  (x + (x + zero)) ◾)

p3 : (a b : ℕ) → a + a + b + a * 0 ≡ 2 * a + b
p3 a b = trans (cong ((a + a + b +_)) (nullr* a))
  (trans (idr+ (a + a + b)) (cong (λ x → a + x + b) (sym (idr+ a))))

p2 : (a b c : ℕ) → c * (b + 1 + a) ≡ a * c + b * c + c
p2 a b c = trans (comm* c (b + 1 + a))
  (trans (dist+* (b + 1) a c) 
  (trans (comm+ ((b + 1) * c) (a * c)) 
  (trans (cong (a * c +_) 
      (trans (dist+* b 1 c) (cong (b * c +_) (idr+ c))))
  (sym (ass+ (a * c) (b * c) c)))))

p9' : 0 ≢ the ℕ 1
p9' ()

p9 : 2 * 2 ≢ 5 * 1
p9 x = subst prov x tt where
  prov : ℕ → Set
  prov zero = ℕ
  prov (suc zero) = ⊥
  prov (suc (suc zero)) = ℕ
  prov (suc (suc (suc zero))) = ℕ
  prov (suc (suc (suc (suc zero)))) = ⊤
  prov (suc (suc (suc (suc (suc x))))) = ⊥

-- Egyszerűbb, amikor mondani kell egy ellenpéldát:
p10 : ¬ ((n : ℕ) → n + 2 ≡ n + 1)
p10 x = subst prov (x 0) tt where
  prov : ℕ → Set
  prov zero = ℕ
  prov (suc zero) = ⊥
  prov (suc (suc x)) = ⊤

-- ...mintsem bizonyítani, hogy ez a kettő sosem lesz egyenlő:
p11 : (n : ℕ) → n + 2 ≢ n + 1
p11 (suc n) x = p11 n ((subst (λ k → n + 2 ≡ pred' k) x refl))

vanish-it : (a b c d e : ℕ) → b + c + d ≡ e → a + b + c + d ≡ a + e
vanish-it a b c d e eq = trans (trans (cong (_+ d) (ass+ a b c)) (ass+ a (b + c) d)) (cong (a +_) eq)

-- Mókásabb helyzet.
p11'' : ¬ Σ ℕ (λ n → n + 3 ≡ n + 1)
p11'' (fst₁ , snd₁) = subst {! p  !} (e snd₁) {!   !} where
  e : fst₁ + 3 ≡ fst₁ + 1 → 3 + fst₁ ≡ 1 + fst₁
  e x = trans (comm+ 3 fst₁) (trans x (comm+ fst₁ 1))
  p : ℕ → Set
  p = {!   !}

p12 : ¬ Σ ℕ (λ n → n + n ≡ 3)
p12 (suc (suc fs) , sn) rewrite comm+ fs (suc (suc fs)) = subst p sn tt where
  p : ℕ → Set
  p 3 = ⊥
  p _ = ⊤

[m+n]^2=m^2+2mn+n^2 : (m n : ℕ) → (m + n) * (m + n) ≡ m * m + 2 * m * n + n * n
[m+n]^2=m^2+2mn+n^2 m n rewrite (dist+* m n (m + n)) | comm* m (m + n) | comm* n (m + n) | dist+* m n m | dist+* m n n | idr+ m | ass+ (m * m) (n * m) (m * n + n * n) | sym (ass+ (n * m) (m * n) (n * n)) | comm* n m | sym (dist+* m m n) | sym (ass+ (m * m) ((m + m) * n) (n * n))  = refl

{-
infixr 8 _^_
_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n
-}

p1 : (a b : ℕ) → (a + b) ^ 2 ≡ a ^ 2 + 2 * a * b + b ^ 2
p1 a b rewrite idr* (a + b) | idr* a | idr* b = [m+n]^2=m^2+2mn+n^2 a b

0^ : (n : ℕ) → 0 ^ (suc n) ≡ 0
0^ n = refl

^0 : (a : ℕ) → a ^ 0 ≡ 1
^0 a = refl

1^ : (n : ℕ) → 1 ^ n ≡ 1
1^ zero = refl
1^ (suc n) = trans (idr+ (1 ^ n)) (1^ n)

^1 : (a : ℕ) → a ^ 1 ≡ a
^1 a = idr* a

^+ : (a m n : ℕ) → a ^ (m + n) ≡ a ^ m * a ^ n
^+ a zero n = sym (idr+ (a ^ n))
^+ a (suc m) n = trans (cong (a *_) (^+ a m n)) (sym (assoc* a (a ^ m) (a ^ n)))

^* : (a m n : ℕ) → a ^ (m * n) ≡ (a ^ m) ^ n
^* a m zero = cong (a ^_) (nullr* m)
^* a m (suc n) = trans (cong (a ^_) (comm* m (suc n))) (trans (^+ a m (n * m)) (cong (a ^ m *_) (trans (cong (a ^_) (comm* n m)) (^* a m n))))

*^ : (a b n : ℕ) → (a * b) ^ n ≡ a ^ n * b ^ n
*^ a b zero = refl
*^ a b (suc n) rewrite assoc* a (a ^ n) (b * b ^ n) | sym (assoc* (a ^ n) b (b ^ n)) | comm* (a ^ n) b | assoc* b (a ^ n) (b ^ n) | sym (assoc* a b (a ^ n * b ^ n))= cong (a * b *_) (*^ a b n)
       