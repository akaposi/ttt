module gy10 where

open import Lib hiding (_≟ℕ_)
open List

------------------------------------------------------
-- η-szabály / egyediség szabály
------------------------------------------------------
{-
Most már van egyenlőség típusunk, így tudunk beszélni az η-szabályról.
A félév elején meg volt említve a függvények η-szabálya (λ a → f a) ≡ f, ez a szabály garantálja,
hogy függvényre csakis egyféleképpen tudunk alkalmazni paramétert, magyarul csakis pontosan egy destruktorom/eliminátorom van.

Tehát az η-szabály azt mondja, hogy ha van egy másik eliminátorom,
ami pontosan ugyanúgy viselkedik, mint az eredeti eliminátor,
akkor az pontosan az elsőként meghatározott eliminátor kell legyen.

Példa Bool-okon:
Eliminátora: if_then_else_ : Bool → A → A → A
β-szabályok: if true  then x else y = x
             if false then x else y = y
η-szabály: {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ if b then x else y
           --------------------------------   ------------------------
             eliminátor dolgai                      β-szabályai
             paraméterek

Természetesen ez ugyanúgy generálható, hiszen minden része megadható csak a típusnak a definíciójából.

data 𝟛 : Set where
  a1 a2 a3 : 𝟛

ite𝟛 : A → A → A → 𝟛 → A
ite𝟛 x y z a1 = x
ite𝟛 x y z a2 = y
ite𝟛 x y z a3 = z

η-szabály: {A : Set}(a b c : A)(f : 𝟛 → A) → f a1 ≡ a → f a2 ≡ b → f a3 ≡ c → (k : 𝟛) → f k ≡ ite𝟛 a b c k

Az egyetlen trükkösebb helyzet, amikor a típusunk rekurzív. De ekkor sem kell megijedni, hiszen az η-szabály leírásakor
csak a β-szabályok viselkedését kell követni.

Például:

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

iteℕ : A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

η-szabály: {A : Set}(z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → (n : ℕ) → f n ≡ iteℕ z s n


data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : B → (A → B → B) → List A → B
iteList e c [] = e
iteList e c (x ∷ xs) = c x (iteList e c xs)

η-szabály: {A B : Set}(nil : A)(cons : B → A → A)(f : List B → A) → f [] ≡ nil → ((b : B) → (ls : List B) → f (b ∷ ls) ≡ cons b (f ls)) → (ls : List B) → f ls ≡ iteList nil cons ls

-}

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
-- sucinj m n refl = refl
sucinj m n x = subst (λ k → m ≡ pred' k) x refl

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = subst (λ k → m ≡ pred' k) e refl

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

destTree : ∀{i}{A : Set i} → Tree → A → ((ℕ → A) → A) → A
destTree leaf x₁ x₂ = x₁
destTree (node x) x₁ x₂ = x₂ (λ n → destTree (x  n) x₁ x₂)

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj refl = refl
-- nodeinj {f} {g} e = subst (λ x → destTree x (f ≡ g) λ h → f ≡ {! h  !}) e {!   !}

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
-- nodeinjl refl = refl
nodeinjl x = subst (λ {leaf → ⊤
                     ; (node b b₁) → _ ≡ b }) x refl

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
-- nodeinjr refl = refl
nodeinjr x = subst (λ {leaf → ⊥
                     ; (node b b₁) → _ ≡ b₁ }) x refl

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 = {! !}

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {! !}

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false = λ x → subst (if_then ⊤ else ⊥) x tt

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' ()

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn ()

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn n ()

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' zero x = subst (λ {zero → ⊤
                         ; (suc k) → ⊥}) x tt
n≠sucn' (suc n) x = n≠sucn' n (sucinj n (suc n) x)

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node ()

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' x = subst (λ {leaf → ⊤
                       ; (node k k₁) → ⊥ }) x tt

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons = {! !}

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : (suc zero) ≤ (suc (suc (suc zero)))
1≤3 .fst = (suc (suc zero))
1≤3 .snd = refl

¬2≤1 : ¬ ((suc (suc zero)) ≤ (suc zero))
¬2≤1 (suc zero , ())
¬2≤1 (suc (suc fst₁) , ())

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn n = (suc zero) , refl

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ n m x = (fst x) , trans (comm+ (fst x) (suc n)) (cong suc (trans (comm+ n (fst x)) (snd x)))

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ = {! !}

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
false ≟Bool false = inl refl
false ≟Bool true = inr (λ { () })
true ≟Bool false = inr (λ x → subst (if_then ⊤ else ⊥) x tt)
true ≟Bool true = inl refl

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
zero ≟ℕ zero = inl refl
zero ≟ℕ suc n' = inr λ where ()
suc n ≟ℕ zero = inr (λ x → subst (λ where zero → ⊥
                                          (suc x₁) → ⊤) x tt)
suc n ≟ℕ suc n' with n ≟ℕ n'
... | inl a = inl (cong suc a)
... | inr b = inr (λ x → b (sucinj _ _ x))

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
_≟BinTree_ = {! !}
 
_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ = {!  !}
    