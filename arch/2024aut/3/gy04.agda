module gy04 where

open import Lib hiding (_+∞_; coite-ℕ∞; ⊤η; ∞)

open List hiding (zipWith; head; tail; length; map; _++_)
open Stream hiding (zipWith; coiteStream; _++_; map)

---------------------------------------------------------
-- típusok η-szabályai
---------------------------------------------------------
{-
Az η-szabály azt mondja meg, hogy mit tegyünk, ha destruktorra alkalmazunk konstruktort.
Pl. függvények esetén (λ a → f a) ≡ f, ahol a függvényalkalmazás a destruktor és a λ a konstruktor.

Természetesen más típusoknak is ugyanúgy van η-szabálya.

Vegyük példaként a ⊤-ot:
Destruktora: ite⊤ : A → ⊤ → A

Ez alapján az η-szabály az alábbi lesz:
ite⊤ tt x ≡ x

Ez természetesen Agdában bizonyítható is.
-}

ite⊤ : ∀{i}{A : Set i} → A → ⊤ → A
ite⊤ x _ = x

⊤η : ∀{x} → ite⊤ tt x ≡ x
⊤η = refl

{-
Ahogy emlékeztek rá, a ⊤ η-szabálya úgy néz ki, hogy ∀ a → a ≡ tt,
tehát itt is igaz lesz, hogy egy típusnak több egymással ekvivalens η-szabálya lehet.

Nézzük újra példaként a Bool típust. A β-szabályai a következők voltak:
if true then u else v ≡ u
if false then u else v ≡ v

Mi lehet az η-szabály? Hogy lehet "destruktorra alkalmazni konstruktort" ilyen esetben?
Az if_then_else_ esetén a "then" és az "else" ágban lévő dolgok tetszőleges értékek lehetnek;
ide akár konstruktort is be lehet írni. Tehát úgy lehet felépíteni az η-szabályokat, hogy a destruktor megfelelő
helyeire beírom az azonos típus konstruktorait.
Bool esetén ez azt jelenti, hogy az if_then_else_-ben a második és harmadik helyre kell a Bool két konstruktorát írni.
Ezen felül úgy kell beírni a két konstruktort, hogy alapvetően az "identitás" függvényt kapjuk az adott típuson.
Bool esetén tehát úgy kell az if_then_else_-et felparaméterezni, hogy a false-ra false legyen az eredmény, true-ra pedig true.

Ez alapján mi lesz a Bool-oknak egy lehetséges η-szabálya?
Válasz: -- if_then_else_ : Bool → A → A → A;
        (b : Bool) → if b then true else false ≡ b

Ugyanezt az ismert 𝟛 típuson is el lehet játszani.
data 𝟛 : Set where
  a1 a2 a3 : 𝟛

Ismert a destruktor: ite𝟛 : A → A → A → 𝟛 → A

Mi lesz a 𝟛 η-szabálya?
Válasz: (x : 𝟛) → ite𝟛 a1 a2 a3 x ≡ x

Természetes számokon a helyzet szintén nem változik.
Ismert a destruktor: iteℕ : A → (A → A) → ℕ → A

Mi lesz ℕ η-szabálya?
Válasz: (n : ℕ) → iteℕ zero suc n ≡ n

-}

---------------------------------------------------------
-- positivity
---------------------------------------------------------

-- Miért nem enged agda bizonyos típusokat definiálni? Pl. alapesetben az alábbit sem.

{-# NO_POSITIVITY_CHECK #-}
data Tm : Set where
  lam : (Tm → Tm) → Tm

-- FELADAT: Tm-ből adjuk vissza a lam értékét.
app : Tm → (Tm → Tm)
app (lam x) = {!!} -- x

self-apply : Tm
self-apply = lam (λ t → app t t)

-- C-c C-n this:
Ω : Tm
Ω = app self-apply self-apply

{-# NO_POSITIVITY_CHECK #-}
data Weird : Set where
  foo : (Weird → ⊥) → Weird
  -- Hogy kell elolvasni magyarul a "foo" konstruktort?

unweird : Weird → ⊥
unweird (foo x) = x (foo x)

-- ⊥ típusú értéknek TILOS léteznie, ellenkező esetben a rendszer inkonzisztens, nem használható SEMMIRE.
bad : ⊥
bad = unweird (foo unweird)

---------------------------------------------------------
-- lists
---------------------------------------------------------

{-
data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A
infixr 5 _∷_
-}

-- FELADAT: Határozzuk meg egy lista elemszámát!
length : {A : Set} → List A → ℕ
length [] = 0
length (_ ∷ xs) = suc (length xs)

length-test1 : length {ℕ} (1 ∷ 2 ∷ 3 ∷ []) ≡ 3
length-test1 = refl
length-test2 : length {ℕ} (1 ∷ []) ≡ 1
length-test2 = refl

-- FELADAT: Adjuk össze egy lista számait.
sumList : List ℕ → ℕ
sumList [] = 0
sumList (x ∷ xs) = x + sumList xs

sumList-test : sumList (1 ∷ 2 ∷ 3 ∷ []) ≡ 6
sumList-test = refl

-- FELADAT: Fűzzünk össze két listát!
_++_ : {A : Set} → List A → List A → List A
[] ++ ys = ys
(x ∷ xs) ++ ys = x ∷ xs ++ ys
infixr 5 _++_

++-test : the (List ℕ) (3 ∷ 2 ∷ []) ++ 1 ∷ 4 ∷ [] ≡ 3 ∷ 2 ∷ 1 ∷ 4 ∷ []
++-test = refl

-- FELADAT: Alkalmazzunk egy függvényt egy lista minden elemén!
map : {A B : Set} → (A → B) → List A → List B
map f [] = []
map f (x ∷ xs) = f x ∷ map f xs

map-test : map (_+ 2) (3 ∷ 9 ∷ []) ≡ (5 ∷ 11 ∷ [])
map-test = refl

-- FELADAT: Definiáld a lista destruktorát! Dolgozzunk fel egy listát:
-- ha üres a lista, akkor csak adjunk vissza egy alapértéket
-- ha a listában van elem, akkor alkalmazzunk rá egy függvényt az alapértékkel úgy, hogy az kifejezés jobbra legyen zárójelezve.
-- Haskell-ben foldr
iteList : {A B : Set} → B → (A → B → B) → List A → B
iteList n c [] = n
iteList n c (a ∷ as) = c a (iteList n c as)

map-iteList : {A B : Set} → (A → B) → List A → List B
map-iteList f = iteList [] (λ x acc → f x ∷ acc)

iteList-test : iteList {ℕ} {List ℕ} [] _∷_ (1 ∷ 2 ∷ 3 ∷ []) ≡ 1 ∷ 2 ∷ 3 ∷ []
iteList-test = refl

-- FEL: add meg a fenti fuggvenyeket (length, ..., map) iteList segitsegevel!
length-iteList : {A : Set} → List A → ℕ
length-iteList = {!!}

sum-iteList : List ℕ → ℕ
sum-iteList = {!!}

_++-iteList_ : {A : Set} → List A → List A → List A
_++-iteList_ = {!!}

---------------------------------------------------------
-- coinductive types
---------------------------------------------------------

{-
record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A
open Stream
-}
-- check that the type of head : Stream A → A
--                        tail : Stream A → Stream A

-- Ez a típus lényegében a végtelen listákat kódolja el.
-- Ebben véges lista nincs benne, csak végtelen!


-- Copattern matching!
-- FELADAT: Add meg azt a végtelen listát, amely csak 0-kból áll.
zeroes : Stream ℕ
head zeroes = 0
tail zeroes = zeroes
-- Honnan tudja agda, hogy ez totális?
-- Termination checker nem tud futni, hiszen a lista végtelen.
-- Productivity checker

-- by pattern match on n
-- FELADAT: Add meg azt a listát, amely n-től 0-ig számol vissza egyesével.
countDownFrom : ℕ → List ℕ
countDownFrom zero = zero ∷ []
countDownFrom k@(suc n) = k ∷ countDownFrom n

-- from n is not by pattern match on n
-- copattern match on Stream
-- FELADAT: Adjuk meg azt a végtelen listát, amely n-től 1-esével felfelé számol!
from : ℕ → Stream ℕ
head (from n) = n
tail (from n) = from (suc n)

-- pointwise addition
zipWith : {A B C : Set} → (A → B → C) → Stream A → Stream B → Stream C
head (zipWith f xs ys) = f (head xs) (head ys)
tail (zipWith f xs ys) = zipWith f (tail xs) (tail ys)

-- Definiálható-e a filter sima listákon?
filterL : {A : Set} → (A → Bool) → List A → List A
filterL p [] = []
filterL p (x ∷ xs) = let rest = filterL p xs in if p x then x ∷ rest else rest

-- Definiálható-e a filter Stream-eken?
{-
filterS : {A : Set} → (A → Bool) → Stream A → Stream A
head (filterS p xs) = if p (head xs) then head xs else head (filterS p (tail xs))
tail (filterS p xs) = {!!}

Nem lehet filter-t Stream-eken. Ellenpélda: filter (λ _ → False) ...
-}

-- one element from the first stream, then from the second stream, then from the first, and so on
interleave : {A : Set} → Stream A → Stream A → Stream A
interleave = {!!}

-- get the n^th element of the stream
get : {A : Set} → ℕ → Stream A → A
get = {!!}

-- iteℕ : (A : Set) → A → (A → A)  → ℕ → A
--        \______________________/
--         ℕ - algebra

-- Mi lesz a Stream konstruktora?
coiteStream : {A B : Set} → (B → A) → (B → B) → B → Stream A
--               \_______________________________/
--                        Stream A - coalgebra
head (coiteStream f g b) = f b
tail (coiteStream f g b) = coiteStream f g (g b)

-- ex: redefine the above functions using coiteStream

-- ex: look at conatural numbers in Thorsten's book and do the exercises about them

-- Definiáljunk egy egyszerű számológépet:
-- Memóriájában tároljon egy számot, amit le lehet kérdezni (getNumber)
   -- ehhez lehet hozzáadni valamennyit (add)
   -- meg lehet szorozni valamennyivel (mul)
   -- vissza lehet állítani nullára (reset)
-- simple calculator (internally a number, you can ask for the number, add to that number, multiply that number, make it zero (reset))

record Machine : Set where
  coinductive
  field
    getNumber : ℕ
    add : ℕ → Machine
    mul : ℕ → Machine
    reset : Machine

open Machine

calculatorFrom : ℕ → Machine
getNumber (calculatorFrom n) = n
add (calculatorFrom n) k = calculatorFrom (n + k)
mul (calculatorFrom n) k = calculatorFrom (n * k)
reset (calculatorFrom n) = calculatorFrom 0

c0 c1 c2 c3 c4 c5 : Machine
c0 = calculatorFrom 0
c1 = add c0 3
c2 = add c1 5
c3 = mul c2 2
c4 = reset c3
c5 = add c4 2


--------------------
-- conatural numbers
--------------------
{-
record ℕ∞ : Set where
  coinductive
  field
    pred∞ : Maybe ℕ∞
open ℕ∞
-}
-- \inf = \infty = ∞

0∞ : ℕ∞
pred∞ 0∞ = nothing

1∞ : ℕ∞
pred∞ 1∞ = just 0∞

∞ : ℕ∞
pred∞ ∞ = just ∞

_+∞_ : ℕ∞ → ℕ∞ → ℕ∞
pred∞ (n +∞ k) with pred∞ n
pred∞ (n +∞ k) | nothing = pred∞ k
pred∞ (n +∞ k) | just x = just (x +∞ k)

-- Ez a függvény létezik, ezzel lehet megnézni
-- egy conat tényleges értékét.
-- Az első paraméter a fuel, maximum ezt a természetes számot tudja visszaadni.
-- Második paraméter a conat, amire kíváncsiak vagyunk.
-- Értelemszerűen ∞-re mindig nothing az eredmény.
{-
ℕ∞→ℕ : ℕ → ℕ∞ → Maybe ℕ
ℕ∞→ℕ zero _ = nothing
ℕ∞→ℕ (suc n) c with pred∞ c
... | zero∞ = just 0
... | suc∞ b with ℕ∞→ℕ n b
... | nothing = nothing
... | just x = just (suc x)
-}

coiteℕ∞ : {B : Set} → (B → Maybe B) → B → ℕ∞
pred∞ (coiteℕ∞ f b) with f b
... | nothing = nothing
... | just x = just (coiteℕ∞ f x)

-- TODO, further exercises: network protocols, simple machines: chocolate machine (input: coin, getChocolate, getBackCoins, output: error, chocolate, money back), some Turing machines, animations, IO, repl, shell
