module gy05 where

open import Lib hiding (fromℕ ; minMax; head; tail)
open Vec hiding (head; tail; map; length; _++_)
open List hiding (head; tail; map; _++_; filter)

-- Vec and Fin
{-
infixr 5 _∷_
data Vec (A : Set) : ℕ → Set where
  []  : Vec A zero
  _∷_ : {n : ℕ} → A → Vec A n → Vec A (suc n)

-}
head : {A : Set}{n : ℕ} → Vec A (suc n) → A
head (x ∷ xs) = x

tail : {A : Set}{n : ℕ} → Vec A (suc n) → Vec A n
tail (x ∷ xs) = xs

infixr 5 _++_
_++_ : {A : Set}{n k : ℕ} → Vec A n → Vec A k → Vec A (n + k)
[] ++ ys = ys
(x ∷ xs) ++ ys = x ∷ xs ++ ys

map : {A B : Set}{n : ℕ} → (A → B) → Vec A n → Vec B n
map f [] = []
map f (x ∷ xs) = f x ∷ map f xs

-- Melyik az a függvény, amit nem tudunk totálisan megírni (még)?
-- Indexelés! Kell hozzá új ötlet!

{-
data Fin : ℕ → Set where  -- Fin n = n-elemu halmaz
  fzero : {n : ℕ} → Fin (suc n)
  fsuc  : {n : ℕ} → Fin n → Fin (suc n)
-}

f0 : Fin 0 → ⊥
f0 ()

f1-0 : Fin 1
f1-0 = fzero {0}

f2-0 f2-1 : Fin 2
f2-0 = fzero {1}
f2-1 = fsuc {1} (fzero {0})

f3-0 f3-1 f3-2 : Fin 3
f3-0 = {!!}
f3-1 = {!!}
f3-2 = {!!}

f4-0 f4-1 f4-2 f4-3 : Fin 4
f4-0 = 0
f4-1 = 1
f4-2 = 2
f4-3 = 3

-- Lib-ben a unicode ‼ az indexelés.
infixl 9 _!!_
_!!_ : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Fin n → A
(x ∷ xs) !! fzero = x
(x ∷ xs) !! fsuc i = xs !! i

test-!! : (the ℕ 3 ∷ 4 ∷ 1 ∷ []) !! (fsuc (fsuc fzero)) ≡ 1
test-!! = refl

test2-!! : (the ℕ 3 ∷ 4 ∷ 1 ∷ 0 ∷ 10 ∷ []) !! 3 ≡ 0 -- 3-as literál a !! után valójában Fin 5 típusú.
test2-!! = refl

fromℕ : (n : ℕ) → Fin (suc n)
fromℕ = {!!}

test-fromℕ : fromℕ 3 ≡ fsuc (fsuc (fsuc fzero))
test-fromℕ = refl

{-
data List (A : Set) : Set where
  []  : List A
  _∷_ : A → List A → List A
-}

{-
length : {A : Set} → List A → ℕ
length [] = zero
length (x ∷ xs) = suc (length xs)
-}

fromList : {A : Set}(as : List A) → Vec A (length as)
fromList [] = []
fromList (a ∷ as) = a ∷ fromList as

tabulate : {n : ℕ}{A : Set} → (Fin n → A) → Vec A n
tabulate = {!!}

test-tabulate : tabulate (the (Fin 3 -> ℕ) (λ {fzero -> 6; (fsuc fzero) -> 9; (fsuc (fsuc fzero)) -> 2}))
                  ≡ 6 ∷ 9 ∷ 2 ∷ []
test-tabulate = refl

-- Sigma types

what : Σ ℕ (λ n → Vec Bool n)
what = 2 , true ∷ false ∷ []

filter : {A : Set}{n : ℕ}(f : A → Bool) → Vec A n → Σ ℕ (Vec A) -- ezen lehet pontosítani, hiszen n elemnél nem kéne legyen benne több elem soha.
filter p [] = 0 , []
filter p (x ∷ xs) = let r@(n , ys) = filter p xs in if p x then suc n , x ∷ ys else r

test-filter : filter {ℕ} (3 <ᵇ_) (4 ∷ 3 ∷ 2 ∷ 5 ∷ []) ≡ (2 , 4 ∷ 5 ∷ [])
test-filter = refl

-- \GS = Σ
-- \== = ≡
smarterLengthVec : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Σ ℕ (λ k → n ≡ℕ k)
smarterLengthVec [] = zero , tt
smarterLengthVec (x ∷ xs) = let (k , pr) = smarterLengthVec xs in suc k , pr

minMax' : ℕ → ℕ → ℕ × ℕ
minMax' n m = {!   !}

-- Ugyanez sokkal jobban, de leginkább pontosabban.
-- Az előző változatban vissza tudok adni csúnya dolgokat is.
-- Pl. konstans (0 , 0)-t.
minMax : (n m : ℕ) → Σ (ℕ × ℕ) (λ (a , b) → a ≤ℕ b × (n ≤ℕ m × n ≡ℕ a × m ≡ℕ b ⊎ m ≤ℕ n × n ≡ℕ b × m ≡ℕ a))
minMax n m = {!   !}
