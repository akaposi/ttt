module gy06 where

open import Lib hiding (module CoVec; CoVec)

-- Definiáld a programozás elmélet tárgyról ismert χ (\Gc) függvényt, amely Bool-t számra alakít, true-t 1-re, false-ot 0-ra.
χ : Bool → ℕ
χ false = 0
χ true = 1

χ-test1 : χ true ≡ 1
χ-test1 = refl

χ-test2 : χ false ≡ 0
χ-test2 = refl

{-
Definiáljuk az órán emlegetett pontosabb filtert.
Szükség van hozzá egy count függvényre, ami megszámolja, hogy hány elemre igaz egy predikátum.
-}
count : ∀{i}{A : Set i}{n : ℕ} → (A → Bool) → Vec A n → ℕ
count p [] = 0
count p (x ∷ xs) = χ (p x) + count p xs

count-test1 : count even (1 ∷ []) ≡ 0
count-test1 = refl

count-test2 : count even (3 ∷ 2 ∷ 4 ∷ 5 ∷ 112 ∷ []) ≡ 3
count-test2 = refl

count-test3 : count (_<ᵇ 10) (0 ∷ 7 ∷ 3 ∷ []) ≡ 3
count-test3 = refl

count-test4 : count (_<ᵇ 10) (0 ∷ 71 ∷ 3 ∷ 34 ∷ 5 ∷ 10 ∷ 10 ∷ 9 ∷ 8 ∷ []) ≡ 5
count-test4 = refl

count-test5 : count (9 <ᵇ_) (0 ∷ 71 ∷ 3 ∷ 34 ∷ 5 ∷ 10 ∷ 10 ∷ 9 ∷ 8 ∷ []) ≡ 4
count-test5 = refl

-- Add meg a filter típusát helyesen:
-- (A paraméterek helyesek, de lehet, hogy kell velük mást is csinálni)
filter : ∀{i}{A : Set i}{n : ℕ}(p : A → Bool)(xs : Vec A n) → Vec A (count p xs)
filter p [] = []
filter p (x ∷ xs) with p x
... | false = filter p xs
... | true = x ∷ filter p xs

filter-test1 : filter even (1 ∷ []) ≡ []
filter-test1 = refl

filter-test2 : filter even (3 ∷ 2 ∷ 4 ∷ 5 ∷ 112 ∷ []) ≡ 2 ∷ 4 ∷ 112 ∷ []
filter-test2 = refl

filter-test3 : filter (_<ᵇ 10) (0 ∷ 7 ∷ 3 ∷ []) ≡ 0 ∷ 7 ∷ 3 ∷ []
filter-test3 = refl

filter-test4 : filter (_<ᵇ 10) (0 ∷ 71 ∷ 3 ∷ 34 ∷ 5 ∷ 10 ∷ 10 ∷ 9 ∷ 8 ∷ []) ≡ 0 ∷ 3 ∷ 5 ∷ 9 ∷ 8 ∷ []
filter-test4 = refl

filter-test5 : filter (9 <ᵇ_) (0 ∷ 71 ∷ 3 ∷ 34 ∷ 5 ∷ 10 ∷ 10 ∷ 9 ∷ 8 ∷ []) ≡ 71 ∷ 34 ∷ 10 ∷ 10 ∷ []
filter-test5 = refl

-----------------------------------------------
-- Mókás intermission
-----------------------------------------------

predℕ∞′ : (n : ℕ∞) → .⦃ IsNotZero∞ n ⦄ → ℕ∞
predℕ∞′ n with pred∞ n
... | just x = x

infixr 5 _∷_
record CoVec {i}(A : Set i)(n : ℕ∞) : Set i where
  constructor _∷_
  coinductive
  field
    head : .⦃ IsNotZero∞ n ⦄ → A
    tail : .⦃ i : IsNotZero∞ n ⦄ → CoVec A (predℕ∞′ n)

open CoVec

[]V : ∀{i}{A : Set i} → CoVec A 0
head []V ⦃ ⦄
tail []V ⦃ ⦄

singleton : Bool → CoVec Bool 1
singleton b = b ∷ []V

replicate : ∀{i}{A : Set i}(n : ℕ∞) → A → CoVec A n
head (replicate n a) = a
tail (replicate n a) = replicate (predℕ∞′ n) a

repeat : ∀{i}{A : Set i} → A → CoVec A ∞
repeat = replicate ∞

--------------------------------------------------------
-- Elmélet: Függőtípusok elemszámai
--------------------------------------------------------
{-
A függőtípusok is algebrai adattípusoknak számítanak, így természetesen a függőtípusok elemszámai is megadhatók könnyedén.
Nem véletlen, hogy a típusok nevei rendre Σ és Π.

Emlékeztetőül:
| A ⊎ B | = |A| + |B|
| A × B | = |A| ∙ |B|
| A → B | = |B| ^ |A|

Tfh:
P : Bool → Set
P true = Bool
P false = ⊥

Σ Bool P hány elemű lesz?
Ha a Bool-om true (1 konkrét érték), akkor Bool típusú eredményt kell a másik részbe írnom, tehát eddig 2¹ = 2
De ha a Bool-om false (1 konkrét érték), akkor ⊥ típusú eredmény kell, tehát 0¹ = 0

Tehát a nap végén | Σ Bool P | = |Bool| + |⊥| = 2, mert a P egy Bool-tól függő típust ad eredményül, tehát maga a típus vagy P true típusú értéket tartalmaz vagy P false-ot.
Az ilyen "vagy" kapcsolatról megbeszéltük korábban, hogy az az összeadást jelenti.

Legyen most:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Σ (Maybe Bool) P? Igazából a típus egyes elemei alapján csak meg kell nézni, hogy hány elemű típust adnak vissza.
| Σ (Maybe Bool) P | = | P nothing | + | P (just true) | + | P (just false) | = |⊤| + |Bool| + |Maybe Bool → Bool| = 1 + 2 + 8 = 11

Ez alapján az intuíció az lehet, hogy | Σ A B | = Σ (i : A) |B i|; tehát csak össze kell adni az egyes típusokból képzett új típusok elemszámát és ennyi.
(Nem véletlen, hogy Σ a típus neve. Ellenőrizhető, hogy A × B elemszáma könnyen ki fog jönni, hogy ha B nem függőtípus.)

Mi a helyzet, ha ugyanezt játszuk Π-vel? Hány elemű lesz Π A B?

Megint konkrét helyzetben, legyen:
P : Bool → Set
P true = Bool
P false = ⊥

| Π Bool P | =⟨ Agda szintaxissal ⟩= | (b : Bool) → P b | kell.
A függvényeknek totálisaknak kell lenniük, tehát ez azt jelenti, hogy MINDEN lehetséges b : Bool értékre P b-nek definiáltnak kell lennie, false-ra ÉS true-ra is.
Intuíció alapján | P true | ÉS | P false | kelleni fog, az ÉS kapcsolat matematikában a szorzást szokta jelenteni, tehát | P true | ∙ | P false | elemszámú lesz
ez a kifejezés.
| P true | ∙ | P false | = |Bool| ∙ |⊥| = 2 ∙ 0 = 0
Próbáljuk meg definiálni ezt a függvényt:
-}
P₁ : Bool → Set
P₁ true = Bool
P₁ false = ⊥

P₂ : Maybe Bool → Set
P₂ nothing = Bool
P₂ (just false) = Maybe Bool
P₂ (just true) = ⊤

P₃ : Bool ⊎ Bool → Set
P₃ (inl false) = Fin 3
P₃ (inl true) = Fin 5
P₃ (inr false) = ⊤
P₃ (inr true) = Fin 10

ΠBoolP₁ : Π Bool P₁ -- (b : Bool) → P₁ b -- 0 elemű
ΠBoolP₁ false = {!!} -- ⊥ kéne, de olyan nincs.
ΠBoolP₁ true = false
-- Rájöhetünk, hogy a false ággal gondok lesznek.

ΠBoolP₂ : Π (Maybe Bool) P₂ -- 6 elemű
ΠBoolP₂ nothing = true
ΠBoolP₂ (just false) = just true
ΠBoolP₂ (just true) = tt

ΠBoolP₃ : Π (Bool ⊎ Bool) P₃ -- 150 elemű
ΠBoolP₃ b = {!!}

{-
Következő példa, ez a P már ismerős:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Π (Maybe Bool) P?
A függvények továbbra is totálisak kell legyenek. Ez azt jelenti, hogy a függvény definiálva kell legyen (P nothing)-ra, (P (just true))-ra ÉS (P (just false))-ra is,
tehát lesz | P nothing | ∙ | P (just true) | ∙ | P (just false) | elemünk, |⊤| ∙ |Bool| ∙ |Maybe Bool → Bool| = 1 ∙ 2 ∙ 2³ = 16 elemű lesz Π (Maybe Bool) P.

Intuíció alapján általánosan | Π A B | = Π (i : A) | B i |, tehát csak az összes B-ből képezhető típus elemszámát össze kell szorozni.

Gyakorlás:
Adott a következő P:

P : Bool × Bool → Set
P (true , true) = ⊤
P (true , false) = Bool
P (false , true) = Bool → Bool ⊎ ⊤
P (false , false) = Bool ⊎ ⊤ → Bool

Hány elemű lesz Σ (Bool × Bool) P?
Hány elemű lesz Π (Bool × Bool) P?

Kicsit érdekesebb ezeket vegyíteni, de az elv ugyanaz marad.
Marad ugyanaz a P.

Hány elemű lesz Π (a : Bool) (Σ (b : Bool) (P (a , b)))? ( Agda szintaxissal (a : Bool) → Σ Bool (λ b → P (a , b)) )
Hány elemű lesz Σ (a : Bool) (Π (b : Bool) (P (a , b)))? ( Agda szintaxissal Σ Bool λ a → (b : Bool) →  P (a , b)) )
-}

----------------------------------------------
-- Some Sigma types
----------------------------------------------

Σ=⊎ : {A B : Set} → Σ Bool (if_then A else B) ↔ A ⊎ B
Σ=⊎ = (λ { (false , r) → inr r ; (true , r) → inl r}) , λ { (inl a) → true , a ; (inr b) → false , b}

Σ=× : {A B : Set} → Σ A (λ _ → B) ↔ A × B
Σ=× = id , id

-- Π A F is essentially (a : A) → F a
-- what does this mean?

                    -- Π A (λ _ → B)
Π=→ : {A B : Set} → ((a : A) → (λ _ → B) a) ↔ (A → B)
Π=→ = id , id

                    -- Π Bool (if_then A else B)
Π=× : {A B : Set} → ((b : Bool) → if b then A else B) ↔ A × B
Π=× = (λ f → f true , f false) , λ { ab false → snd ab ; ab true → fst ab}

dependentCurry : {A : Set}{B : A → Set}{C : (a : A) → B a → Set} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
dependentCurry = (λ f ab → f (fst ab) (snd ab)) , λ f a b → let w = (a , b) in f w

-- Haskell-ben:
-- curry :: ((a,b) ->  c) -> a -> b -> c
-- uncurry :: (a -> b -> c) -> (a,b) -> c

---------------------------------------------------------
-- propositional logic
------------------------------------------------------

-- Curry-Howard izomorfizmus
-- Elmélet:
--   ∙ átalakítani logikai állításokat típusokra.
--   ∙ formalizálni állításokat típusokkal.
--   × = ∧ = konjunkció
--   ⊎ = ∨ = diszjunkció
--   ¬ = ¬ = negáció
--   ⊃ = → = implikáció

--------------------------------------------------
-- Formalisation
--------------------------------------------------

-- Formalizáljuk a mondatokat!

-- Az egyes formalizált alap mondatrészeket vegyük fel modul paraméterként, akkor szépen fog működni minden.
--
-- A : Süt a nap.
-- B : Esik az eső.
-- C : Kell az esernyő.
-- D : Van szivárvány.
module Formalise (A B C D : Set) where

  -- Süt a nap.
  form0 : Set
  form0 = A

  -- Nem süt a nap.
  form1 : Set
  form1 = ¬ A

  -- Esik az eső és süt a nap.
  form2 : Set
  form2 = B × A

  -- Nem kell az esernyő vagy esik az eső.
  form3 : Set
  form3 = ¬ C ⊎ B

  -- Ha esik az eső és süt a nap, akkor van szivárvány.
  form4 : Set
  form4 = B × A → D

  -- Van szivárvány.
  Köv : Set
  Köv = D

---- Következményfogalom (logika tárgy 1-3. gyakorlat)
  -- Agdában legegyszerűbben szintaktikus következményekkel lehet foglalkozni.

  -- Mondd ki, és bizonyítsd be, hogy a fenti állításokból következik a Köv.
  -- A típusban kell kimondani az állítást; az állítás kimondásához az eldöntésprobléma tételét kell használni.
  -- Két féleképpen lehet bizonyítani.

  Köv1 : Set
  Köv1 = form1 → form2 → form3 → form4 → Köv

  Proof1 : Köv1
  Proof1 F1 F2 F3 F4 = F4 F2

  Proof2 : Köv1
  Proof2 F1 (B , A) F3 F4 = exfalso (F1 A)

----------------------------------------------------------------------------

subt-prod : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
subt-prod f g (a , b) = (f a) , (g b)

subt-fun : {A A' B B' : Set} → (A → A') → (B → B') → (A' → B) → (A → B')
subt-fun f g h a = g (h (f a))

anything : {X Y : Set} → ¬ X → X → Y
anything ¬x x = exfalso (¬x x)

ret : {X : Set} → X → ¬ ¬ X
-- ¬ X = X → ⊥

-- ¬ ¬ X
-- ¬ X → ⊥
-- (X → ⊥) → ⊥
ret x ¬x = ¬x x

{-
-- Nem bizonyítható! (Konstruktív logikában vagyunk)
ter : {X : Set} → ¬ ¬ X → X
ter ¬¬x = exfalso (¬¬x (λ x → ¬¬x λ x2 → {!!}))
-}

fun : {X Y : Set} → (¬ X) ⊎ Y → (X → Y)
fun (inl ¬x) x = contradiction x ¬x
fun (inr y) x = y

-- De Morgan

dm1 : {X Y : Set} →  ¬ (X ⊎ Y) ↔ ¬ X × ¬ Y
dm1 = {!!}

dm2 : {X Y : Set} → ¬ X ⊎ ¬ Y → ¬ (X × Y)
dm2 = {!!}

dm2b : {X Y : Set} → ¬ ¬ (¬ (X × Y) → ¬ X ⊎ ¬ Y)
dm2b = {!!}

-- stuff

nocontra : {X : Set} → ¬ (X ↔ ¬ X)
nocontra = {!!}

¬¬invol₂ : {X : Set} → ¬ ¬ ¬ X ↔ ¬ X
¬¬invol₂ = (λ ¬¬¬x x → ¬¬¬x (ret x)) , ret

¬¬invol₁ : {X : Set} → ¬ ¬ ¬ ¬ X ↔ ¬ ¬ X
¬¬invol₁ {X} = ¬¬invol₂ {¬ X}

nnlem : {X : Set} → ¬ ¬ (X ⊎ ¬ X)
nnlem f = f (inl (exfalso (f (inr λ x → f (inl x)))))

nndnp : {X : Set} → ¬ ¬ (¬ ¬ X → X)
nndnp = {!!}

dec2stab : {X : Set} → (X ⊎ ¬ X) → (¬ ¬ X → X)
dec2stab = {!!}

-- you have to decide:
{-
Dec : Set → Set
Dec A = A ⊎ ¬ A
-}

open import Lib.Dec.PatternSynonym

ee1 : {X Y : Set} → Dec (X ⊎ Y → ¬ ¬ (Y ⊎ X))
ee1 = yes λ { (inl x) ¬y∨x → ¬y∨x (inr x) ; (inr y) ¬y∨x → ¬y∨x (inl y) }

ee2 : {X : Set} → Dec (¬ (X ⊎ ¬ X))
ee2 = {!!}

e3 : {X : Set} → Dec (¬ (X → (¬ X → X)))
e3 = no λ f → f λ x ¬x → x

e4 : Dec ℕ
e4 = yes zero

e5 : Dec ⊥
e5 = {!!}

e6 : {X : Set} → Dec (⊥ → X ⊎ ¬ X)
e6 = {!!}

e7 : {X : Set} → Dec (X × ¬ X → ¬ X ⊎ X)
e7 = {!!}

e8 : {X : Set} → Dec ((X → X) → ⊥)
e8 = {!!}

f1 : {X Y : Set} → ¬ ¬ X ⊎ ¬ ¬ Y → ¬ ¬ (X ⊎ Y)
f1 = {!!}

f2 : ({X Y : Set} → ¬ (X × Y) → ¬ X ⊎ ¬ Y) → {X Y : Set} → ¬ ¬ (X ⊎ Y) → ¬ ¬ X ⊎ ¬ ¬ Y
f2 = {!!}

----------------------------------------------------------------------
-- Not exactly first order logic but kinda is and kinda isn't.

f3 : Dec ((X Y : Set) → X ⊎ Y → Y)
f3 = no λ f → f ⊤ ⊥ (inl tt)

f4 : Dec ((X Y Z : Set) → (X → Z) ⊎ (Y → Z) → (X ⊎ Y → Z))
f4 = {!!}

f5 : Dec ((X Y Z : Set) → (X → Z) × (Y → Z) → (X × Y → Z))
f5 = yes λ _ _ _ → λ xzyz xy → snd xzyz (snd xy)

f6 : Dec ((X Y Z : Set) → (X × Y → Z) → (X → Z) × (Y → Z))
f6 = no λ f → fst (f ⊤ ⊥ ⊥ snd) tt

f7 : Dec ((X Y Z : Set) → (X ⊎ Y × Z) → (X ⊎ Y) × (X ⊎ Z))
f7 = {!!}

f8 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → ((X ⊎ Y) × Z))
f8 = {!!}

f9 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → (X ⊎ Y × Z))
f9 = {!!}
