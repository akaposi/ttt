module gy08 where

open import Lib
open import Lib.Dec.PatternSynonym

------------------------------------------------------
-- statements as parameters
------------------------------------------------------

blowUp : ((A : Set) → ¬ A) → ⊥
blowUp f = f Bool true
-- what's the difference with this?
-- (A : Set) → ¬ A → ⊥

---------------------------------------------------------
-- predicate (first order) logic example
---------------------------------------------------------

notExists↔noneOf : ∀{i}{A : Set i} → (P : A → Set) →
                        (∀ x → ¬ (P x)) ↔ ¬ (Σ A (λ x → P x))
notExists↔noneOf = {!!}

module People
  (Person    : Set)
  (Ann       : Person)
  (Kate      : Person)
  (Peter     : Person)
  (_childOf_ : Person → Person → Set)
  (_sameAs_  : Person → Person → Set) -- ez most itt az emberek egyenlosege
  where

  -- Define the _hasChild predicate.
  _hasChild : Person → Set
  x hasChild = Σ Person λ p → p childOf x

  -- Formalise: Ann is not a child of Kate.
  ANK : Set
  ANK = ¬ (Ann childOf Kate)

  -- Peter is a child of everyone.
  PET : Set
  PET = (P : Person) → Peter childOf P

  -- Formalise: there is someone with exactly one child.
  ONE : Set
  ONE = Σ Person λ p → Σ Person λ x → x childOf p × ((y : Person) → y childOf p → x sameAs y)

  -- Define the relation _parentOf_.
  _parentOf_ : Person → Person → Set
  x parentOf y = {!!}

  -- Formalise: No one is the parent of everyone.
  NOPE : Set
  NOPE = {!!}

  -- Prove that if Ann has no children then Kate is not the child of Ann.
  AK : ¬ (Σ Person λ y → y childOf Ann) → ¬ (Kate childOf Ann)
  AK = {!!}

  -- Prove that if there is no person who is his own parent than no one is the parent of everyone.
  ¬xpopxthenNOPE : ¬ (Σ Person λ x → x parentOf x) → NOPE
  ¬xpopxthenNOPE = {!!}

---------------------------------------------------------
-- predicate (first order) logic laws
---------------------------------------------------------
-- ∀a(P(a) ∧ Q(a)) ↔ ∀aP(a) ∧ ∀aQ(a)
∀×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a × Q a)  ↔ ((a : A) → P a) × ((a : A) → Q a)
∀×-distr _ _ _ = (λ f → (λ a → fst (f a)) , (λ a → snd (f a))) , λ (p , q) a → (p a) , (q a)

∀⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a) ⊎ ((a : A) → Q a) → ((a : A) → P a ⊎ Q a)
∀⊎-distr _ _ _ (inl p) a = inl (p a)
∀⊎-distr _ _ _ (inr q) a = inr (q a)
-- ez miért csak odafelé megy?
-- miért nem ↔ van közte?

Σ×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a × Q a)  → Σ A P × Σ A Q
Σ×-distr = {!!}

Σ⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a ⊎ Q a)  ↔ Σ A P ⊎ Σ A Q
Σ⊎-distr _ _ _ = (λ (a , pvq) → case pvq (λ pa → inl (a , pa)) λ qa → inr (a , qa))
               , λ pvq → case pvq (λ (a , pa) → a , inl pa) λ (a , qa) → a , inr qa

¬∀        :    (A : Set)(P : A → Set)              → (Σ A λ a → ¬ P a)      → ¬ ((a : A) → P a)
¬∀ = {!!}

-- Ugyanez van a fájl tetején is:
¬Σ        :    (A : Set)(P : A → Set)              → (¬ Σ A λ a → P a)      ↔ ((a : A) → ¬ P a)
¬Σ = {!!}

¬¬∀-nat   :    (A : Set)(P : A → Set)              → ¬ ¬ ((x : A) → P x)    → (x : A) → ¬ ¬ (P x)
¬¬∀-nat = {!!}

∀⊎-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (((a : A) → P a ⊎ Q a) → ((a : A) → P a) ⊎ ((a : A) → Q a)))
∀⊎-distr' f = case (f Bool P Q λ { false → inr tt ; true → inl tt}) (λ p → p false) λ q → q true where
  P : Bool → Set
  P false = ⊥
  P true = ⊤

  Q : Bool → Set
  Q false = ⊤
  Q true = ⊥

Σ×-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (Σ A P × Σ A Q → Σ A λ a → P a × Q a))
Σ×-distr' f with f Bool P Q ((true , tt) , (false , tt)) where
  P : Bool → Set
  P false = ⊥
  P true = ⊤

  Q : Bool → Set
  Q false = ⊤
  Q true = ⊥
Σ×-distr' f | false , p , q = p
Σ×-distr' f | true , p , q = q

Σ∀       : (A B : Set)(R : A → B → Set)        → (Σ A λ x → (y : B) → R x y) → (y : B) → Σ A λ x → R x y
Σ∀ = {!!}

AC       : (A B : Set)(R : A → B → Set)        → ((x : A) → Σ B λ y → R x y) → Σ (A → B) λ f → (x : A) → R x (f x)
AC = {!!}
