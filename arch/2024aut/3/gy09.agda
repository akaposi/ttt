module gy09 where

open import Lib hiding (sym; trans; cong; subst; idr+; sucr+; assoc+; comm+; dist+*; nullr*; idl*; idr*; sucr*; assoc*; comm*)

---------------------------------------------------------
-- equality
------------------------------------------------------
{-
data _≡_ {i}{A : Set i}(a : A) : A → Set i where
  refl : a ≡ a
-}

sym : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
sym refl = refl

trans : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans refl e2 = e2

{-
other notions of trans:
_◾_ = \sq5

\< = ⟨ ; \> = ⟩ ; \qed = ∎
_≡⟨_⟩_ + _∎

_≡⟨_⟩_
Λ  Λ ^-- proof
|  |
|  ⌞ proof
value

_∎ = basically reflexivity proof with an explicit value

Usual way of using this notion:

value1
  ≡⟨ proof1 ⟩
value2
  ≡⟨ proof2 ⟩
value3
  ≡⟨ proof3 ⟩
value4 ∎
-}

cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
cong f refl = refl

{-
-- Injektív függvények
cong⁻¹ : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → f x ≡ f y → x ≡ y
cong⁻¹ f e = {!e!} -- Nem tudtunk e-re mintailleszteni
-}

subst : ∀{i j}{A : Set i}(P : A → Set j){x y : A} → x ≡ y → P x → P y
subst P refl px = px

---------------------------------------------------------
-- properties of +,*
---------------------------------------------------------

idl+ : (n : ℕ) → zero + n ≡ n
idl+ n = refl

idr+ : (n : ℕ) → n + zero ≡ n
idr+ zero = refl
idr+ (suc n) = let ih = idr+ n in cong suc ih

sucr+ : (n m : ℕ) → n + suc m ≡ suc (n + m)
sucr+ zero m = refl
sucr+ (suc n) m = let ih = sucr+ n m in cong suc ih

assoc+ : (m n o : ℕ) → (m + n) + o ≡ m + (n + o)
assoc+ zero n o = refl
assoc+ (suc m) n o = let ih = assoc+ m n o in cong suc ih

comm+-helper : (n m : ℕ) → suc n + m ≡ n + suc m
comm+-helper n m = sym (sucr+ n m)

comm+ : (m n : ℕ) → m + n ≡ n + m
comm+ zero n = sym (idr+ n)
comm+ (suc m) n = let ih = comm+ m n in trans {y = suc (n + m)} (cong suc ih) (comm+-helper n m)

comm+' : (m n : ℕ) → m + n ≡ n + m
comm+' zero n = sym (idr+ n)
comm+' (suc m) n = let ih = comm+ m n in
  suc (m + n)
  ≡⟨ cong (λ x → suc x) ih ⟩
  suc (n + m)
  ≡⟨ comm+-helper n m ⟩
  n + suc m ∎

dist+* : (m n o : ℕ) → (n + o) * m ≡ n * m + o * m
dist+* m zero o = refl
dist+* m (suc n) o = let ih = dist+* m n o in
  m + (n + o) * m
  ≡⟨ cong (λ x → m + x) ih  ⟩
  m + (n * m + o * m)
  ≡⟨ sym (assoc+ m (n * m) (o * m)) ⟩
  m + n * m + o * m ∎

nullr* : (n : ℕ) → n * 0 ≡ 0
nullr* = {!!}

idl* : (n : ℕ) → 1 * n ≡ n
idl* = {!!}

idr* : (n : ℕ) → n * 1 ≡ n
idr* = {!!}

sucr* : (n m : ℕ) → n * suc m ≡ n + n * m
sucr* = {!!}

assoc* : (m n o : ℕ) → (m * n) * o ≡ m * (n * o)
assoc* = {!!}

comm*-helper : (n m : ℕ) → n + n * m ≡ n * suc m
comm*-helper = {!!}

comm* : (m n : ℕ) → m * n ≡ n * m
comm* = {!!}
