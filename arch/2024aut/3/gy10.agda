module gy10 where

open import Lib

------------------------------------------------------
-- η-szabály / egyediség szabály
------------------------------------------------------
{-
Most már van egyenlőség típusunk, így tudunk beszélni az η-szabályról.
A félév elején meg volt említve a függvények η-szabálya (λ a → f a) ≡ f, ez a szabály garantálja,
hogy függvényre csakis egyféleképpen tudunk alkalmazni paramétert, magyarul csakis pontosan egy destruktorom/eliminátorom van.

Tehát az η-szabály azt mondja, hogy ha van egy másik eliminátorom,
ami pontosan ugyanúgy viselkedik, mint az eredeti eliminátor,
akkor az pontosan az elsőként meghatározott eliminátor kell legyen.

Példa Bool-okon:
Eliminátora: if_then_else_ : Bool → A → A → A
β-szabályok: if true  then x else y = x
             if false then x else y = y
η-szabály: {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ if b then x else y
           --------------------------------   ------------------------
             eliminátor dolgai                      β-szabályai
             paraméterek

Természetesen ez ugyanúgy generálható, hiszen minden része megadható csak a típusnak a definíciójából.

data 𝟛 : Set where
  a1 a2 a3 : 𝟛

ite𝟛 : A → A → A → 𝟛 → A
ite𝟛 x y z a1 = x
ite𝟛 x y z a2 = y
ite𝟛 x y z a3 = z

η-szabály: (x y z : A)(f : 𝟛 → A) → f a1 ≡ x → f a2 ≡ y → f a3 ≡ z → (t : 𝟛) → f t ≡ ite𝟛 x y z t

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

iteℕ : A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

elimℕ : A 0 → ((n : ℕ) → A n → A (suc n)) → (n : ℕ) → A n
elimℕ z s zero = z
elimℕ z s (suc n) = s n (elimℕ z s n)

η-szabály: (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → (n : ℕ) → f n ≡ iteℕ z s n

data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : B → (A → B → B) → List A → B
iteList e c [] = e
iteList e c (x ∷ xs) = c x (iteList e c xs)

η-szabály: (e : B)(c : A → B → B)(f : List A → B) → f [] ≡ e → ((x : A)(xs : List A)
                                                  → f (x ∷ xs) ≡ c x (f xs))
                                                  → (xs : List A) → f xs ≡ iteList e c xs
-}

------------------------------------------------------
-- equational reasoning
------------------------------------------------------

p4 : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4 x y =
  x + (y + zero) + x
    ≡⟨ assoc+ x (y + 0) x ⟩
  x + ((y + 0) + x)
    ≡⟨ cong (x +_) (assoc+ y 0 x) ⟩
  x + (y + (0 + x))
    ≡⟨ cong (x +_) (comm+ y (0 + x)) ⟩
  x + ((0 + x) + y)
    ≡⟨ cong (λ a → x + (a + y)) (comm+ 0 x) ⟩
  x + (x + 0 + y)
    ≡⟨ sym (assoc+ x (x + 0) y) ⟩
  x + (x + zero) + y
    ≡⟨ refl ⟩
  2 * x + y ∎

p3 : (a b : ℕ) → a + a + b + a * 0 ≡ 2 * a + b
p3 = {!!}

p2 : (a b c : ℕ) → c * (b + 1 + a) ≡ a * c + b * c + c
p2 = {!!}

-- \==n = ≢
p9' : 0 ≢ the ℕ 1
p9' ()

p9 : 2 * 2 ≢ 5 * 1
p9 ()

-- Egyszerűbb, amikor mondani kell egy ellenpéldát:
p10 : ¬ ((n : ℕ) → n + 2 ≡ n + 1)
p10 f with f zero
... | ()

-- ...mintsem bizonyítani, hogy ez a kettő sosem lesz egyenlő:
p11 : (n : ℕ) → n + 2 ≢ n + 1
p11 (suc n) e = p11 n (cong p e)  where
  p : ℕ → ℕ
  p (suc n) = n
  p zero = zero
  -- pred'

-- Mókásabb helyzet.
p11'' : ¬ Σ ℕ (λ n → n + 3 ≡ n + 1)
p11'' (suc n , e) = p11' (suc n) e where
  p11' : (n : ℕ) → n + 3 ≢ n + 1
  p11' (suc n) e = p11' n (cong pred' e)

p12 : ¬ Σ ℕ (λ n → n + n ≡ 3)
p12 (suc (suc (suc zero)) , ())
p12 (suc (suc (suc (suc n))) , ())

{-
infixr 8 _^_
_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n
-}

p1 : (a b : ℕ) → (a + b) ^ 2 ≡ a ^ 2 + 2 * a * b + b ^ 2
p1 = {!!}

0^ : (n : ℕ) → 0 ^ (suc n) ≡ 0
0^ = {!!}

^0 : (a : ℕ) → a ^ 0 ≡ 1
^0 = {!!}

1^ : (n : ℕ) → 1 ^ n ≡ 1
1^ = {!!}

^1 : (a : ℕ) → a ^ 1 ≡ a
^1 = {!!}

^+ : (a m n : ℕ) → a ^ (m + n) ≡ a ^ m * a ^ n
^+ = {!!}

^* : (a m n : ℕ) → a ^ (m * n) ≡ (a ^ m) ^ n
^* = {!!}

*^ : (a b n : ℕ) → (a * b) ^ n ≡ a ^ n * b ^ n
*^ = {!!}
