module gy11 where

open import Lib hiding (_≟ℕ_)
open List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj m .m refl = refl

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = cong pred' e

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj = cong p where
  p : Tree → ℕ → Tree
  p leaf _ = leaf
  p (node f) = f

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl = cong p where
  p : BinTree → BinTree
  p leaf = leaf
  p (node l r) = l

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr = cong p where
  p : BinTree → BinTree
  p leaf = leaf
  p (node l r) = r

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 {x = x} = cong (p x) where
  p : {A : Set} → A → List A → A
  p a [] = a
  p _ (x ∷ xs) = x

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false ()

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst P e tt where
  P : Bool → Set
  P true = ⊤
  P false = ⊥

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn ()

zero≠sucn' : {n : ℕ} → zero ≢ suc n
zero≠sucn' e = subst P e tt where
  P : ℕ → Set
  P zero = ⊤
  P (suc _) = ⊥

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn n ()

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' zero = zero≠sucn
n≠sucn' (suc n) e = n≠sucn' n (sucinj n (suc n) e)

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node = {!!}

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons e = subst P e tt where
  P : {A : Set} → List A → Set
  P [] = ⊤
  P (_ ∷ _) = ⊥

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 (suc zero , ())
¬2≤1 (suc (suc n) , ())

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn n = 1 , refl

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ n m (k , e) = k , (sucr+ k n ◾ cong suc e)

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ n m (k , e) = k , sucinj (k + n) m (sym (sucr+ k n) ◾ e)

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
_≟Bool_ = {!!}

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
_≟ℕ_ = {!!}

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
_≟BinTree_ = {!!}

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ = {!!}
