module hf11 where

open import Lib hiding (_≟⊤_ ; _≟⊥_)

-----------------------------------------------
-- Bool műveletek
-----------------------------------------------

infixl 9 ¬ᵇ_
¬ᵇ_ : Bool → Bool
¬ᵇ false = true
¬ᵇ true  = false

task1 : Dec (∀ x y → (x ⊃ y) ≡ (¬ᵇ x ∨ ¬ᵇ y))
task1 = {!   !}

task1' : Dec (∀ x y → (x ⊃ y) ≡ (¬ᵇ x ∨ y))
task1' = {!   !}

task2 : ∀ x → x ≢ ¬ᵇ x
task2 = {!   !}

task3 : ∀ x → (x ∧ ¬ᵇ x) ≢ (x ∨ ¬ᵇ x)
task3 = {!   !}

LEM : ∀ x → T (x ∨ ¬ᵇ x)
LEM = {!!}

----------------------------------------------
-- Bool függvények injektivitása
----------------------------------------------

¬inj? : Dec (∀ a b → ¬ᵇ a ≡ ¬ᵇ b → a ≡ b)
¬inj? = {!   !}

∧≢ : ∀ a b c → (a ∧ b) ≢ (a ∧ c) → b ≢ c
∧≢ = {!   !}

∧injl? : Dec (∀ a b c → (a ∧ b) ≡ (a ∧ c) → b ≡ c)
∧injl? = {!   !}

∧injr? : Dec (∀ a b c → (a ∧ c) ≡ (b ∧ c) → a ≡ b)
∧injr? = {!   !}

∨injl? : Dec (∀ a b c → (a ∨ b) ≡ (a ∨ c) → b ≡ c)
∨injl? = {!   !}

∨injr? : Dec (∀ a b c → (a ∨ c) ≡ (b ∨ c) → a ≡ b)
∨injr? = {!   !}

⊃injl? : Dec (∀ a b c → (a ⊃ b) ≡ (a ⊃ c) → b ≡ c)
⊃injl? = {!   !}

⊃injr? : Dec (∀ a b c → (a ⊃ c) ≡ (b ⊃ c) → a ≡ b)
⊃injr? = {!   !}

¬¬involutive? : Dec (∀ a → ¬ᵇ ¬ᵇ a ≡ a)
¬¬involutive? = {!   !}

----------------------------------------------
-- ℕ függvények injektivitása
----------------------------------------------

+injl : {n m k : ℕ} → n + m ≡ n + k → m ≡ k
+injl = {!   !}

+injr : {n m k : ℕ} → n + m ≡ k + m → n ≡ k
+injr = {!   !}

*injr : {n m k : ℕ} → n * suc m ≡ k * suc m → n ≡ k
*injr = {!   !}

*injl : {n m k : ℕ} → suc n * m ≡ suc n * k → m ≡ k
*injl = {!   !}

----------------------------------------------
-- Bizonyítások injektivitása
----------------------------------------------

-- Írd fel a × injektivitását kimondó állítást, majd bizonyítsd!
-- f injektív <-> f a = f b -> a = b
-- Ez két paraméter esetén is így van:
-- f a b = f a' b' -> a = a' ÉS b = b'
-- Ez mindkét irányba működik, ezért így is írd fel.
×inj' : ∀{i j}{A : Set i}{B : Set j}{a a' : A}{b b' : B} →
  {!   !}
×inj' = {!   !}

-- Írd fel a Σ injektivitását kimondó állítást, majd bizonyítsd!
-- Ezt ténylegesen csak szétszedni lehet jól.
-- Összerakáshoz egy külön feladat szükséges (ez a következő lesz).
Σinj' : ∀{i j}{A : Set i}{B : A → Set j}{a b : Σ A B} →
  {!   !}
Σinj' = {!   !}

-- Egy Σ-ban lévő bizonyítás akkor fog teljesülni,
-- ha az első IS igaz és a második IS igaz.
-- Írd fel az állítást és bizonyítsd.
infixr 4 _,='_
_,='_ : ∀{i j}{A : Set i}{B : A → Set j}{a b : A}{x : B a}{y : B b} →
  {!   !}
_,='_ = {!   !}

------------------------------
-- ≢ tulajdonságok
------------------------------

≢notTrans : ∀{i} → ¬ ({A : Set i}{a b c : A} → a ≢ b → b ≢ c → a ≢ c)
≢notTrans = {!   !}

≢notReflexive : ∀{i}{A : Set i}{a : A} → ¬ (a ≢ a)
≢notReflexive = {!   !}

≢Symmetric : ∀{i}{A : Set i}{a b : A} → a ≢ b → b ≢ a
≢Symmetric = {!   !}

------------------------------
-- ≢ típusokon
------------------------------

-- Trükkös feladat, jó ötlet kell hozzá!
-- Arra kell gondolni, hogy ha két típus egyenlő, akkor
-- ebből milyen tulajdonságokat kapunk.
task10 : ⊤ ≢ (⊤ ⊎ ⊤)
task10 eq = {!   !}

-----------------------------
-- Eldönthetőség
-----------------------------

_≟⊤_ : (a b : ⊤) → Dec (a ≡ b)
_≟⊤_ = {!!}

_≟⊥_ : (a b : ⊥) → Dec (a ≡ b)
_≟⊥_ = {!!}

postulate
  funext : ∀{i j}{A : Set i}{B : Set j}{f g : A → B} → (∀ x → f x ≡ g x) → f ≡ g
  -- Ez MLTT-ben nem bizonyítható, de kubikális agdában igen.
{-
happly : ∀{i j}{A : Set i}{B : Set j}{f g : A → B} → f ≡ g → (∀ x → f x ≡ g x)
happly = {!!}
-}
-- funext és happly szükségesek!
-- Segítség: Érdemes definiálni egy segédfüggvényt, amely a négy lehetséges függvényt felismeri,
--           mindegyik ágon megkapva az adott ágon lévő igaz egyenlőséget.
_≟Bool→Bool_ : (b b' : Bool → Bool) → Dec (b ≡ b')
b ≟Bool→Bool b' = {!!}
