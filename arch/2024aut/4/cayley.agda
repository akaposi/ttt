open import Lib hiding (T)

module cayley where

{-

    Formalizáljuk és bizonyítjuk a Cayley tételét (Csoport elmélet) :

    Every group G is isomorphic to a subgroup of a symmetric group.

    Minden csoport G isomorfikus egy szimmetria csoport részcsoportjához. 

    De mi az a `csoport`, mi az a `szimmetria csoport` és mikor részcsoportja egy G csoport egy H-nak? 

    Próbáljuk meg formalizálni őket egyesével.

-}

-- Egy csoport egy A halmaz a következő megszabásokkal:
--   Van egy kitüntetett elem : e
--   Van egy bináris müvlet a halmaz felett : _∙_
--   Van egy unáris müvelet a halmaz felett : _⁻¹
--   Ezekre a müveletekre a következők igazak:

--   1| Bármely a ∈ A-ra : 
--      e ∙ a = a
--      a ∙ e = a
--   Ezt úgy mondjuk hogy e egységeleme _∙_-nek
--   2| Bármely a,b,c ∈ A-ra :
--      (a ∙ b) ∙ c = a ∙ (b ∙ c)
--   Vagyis _∙_ asszociatív
--   3| minden a ∈ A-ra :
--      a ⁻¹ ∙ a = e
--      a ∙ a ⁻¹ = e

-- Ezeket a megkötéseket tudjuk formalizálni a _≡_ típussal
-- Azt mondjuk hogy egy A akkor egy csoport ha tudja a fent kimondottakat


record Group (A : Set) : Set where
    infixl 10 _∙_
    field
        e : A
        _⁻¹ : A → A
        _∙_ : A → A → A

        idl : (a : A) → e ∙ a ≡ a
        idr : (a : A) → a ∙ e ≡ a

        invl : (a : A) → (a ⁻¹) ∙ a ≡ e
        invr : (a : A) → a ∙ (a ⁻¹) ≡ e
        assoc : (a b c : A) → (a ∙ b) ∙ c ≡ a ∙ (b ∙ c)

-- Nézzünk példákat csoportokra

OneElementGroup : Group ⊤ 
OneElementGroup = record
    { e = tt
    ; _⁻¹ = (λ _ → tt) 
    ; _∙_ = (λ _ _ → tt)
    ; idl = λ a → refl
    ; idr = λ a → refl
    ; invl = λ a → refl
    ; invr = λ a → refl
    ; assoc = λ a b c → refl
    }

-- TODO
-- A Bool true, _∧_, el csoport
--BoolGroup : Group Bool 
--BoolGroup = record
--    { idl = λ a → refl
--    ; idr = λ {true → refl; false → refl}
--    ; invl = λ a → {!   !}
--    ; invr = {!   !}
--    ; assoc = {!   !}
--    }

-- A következő lépés a szimmetria csoport definíciója
-- Egy A halmaz feletti szimmetria csoport elemei az A elemeinek permutációja, 
-- a _∙_ müvlet a a permutációk kompozíciója
-- a _⁻¹ müvelet pedig a permutációk megfordítása
-- Mi a permutációkat A → A bijektív függvényekkel formalizáljuk

record Bijection{A : Set}(f : A → A) : Set where
    field
        inj : ∀{a b} → f a ≡ f b → a ≡ b
        surj : ∀{a} → Σ (A → A) λ g → f (g a) ≡ a

symGroup : Set → Set
symGroup A = Group (Σ (A → A) (Bijection {A}))

-- A részcspoort tulajdonságot a következő képpen definiáljuk:
-- TODO explain

record _⊂_ {A B : Set}(G : Group A)(H : Group B) : Set where
    module G = Group G
    module H = Group H

    field
        -- f is an injective function such thatf is an injective function such that
        f : A → B
        inj : ∀{a b} → f a ≡ f b → a ≡ b
        -- f is a group homomorphism
        id'   : (f G.e) ≡ H.e
        comp' : (a b : A) → f (a G.∙ b) ≡ (f a) H.∙ (f b)
        inv'  : (a : A) → f (a G.⁻¹) ≡ ((f a) H.⁻¹)

-- Ekkor Caylay tétele azt mondja hogy minden G csoporthoz létezik H szimmetria csoport, hogy G ⊂ H
caylay-theorem : {A : Set} → (G : Group A) → Σ (symGroup A) (λ H → G ⊂ H)

-- A Bizonyítás a szokásos bizonyítást követi 

-- f(g , x) = g * x
--  

f : {A : Set} → (G : Group A)(g : A) → A → A
f G g x = g ∙ x
    where
        open Group G

f⁻¹ : {A : Set} → (G : Group A)(g : A) → A → A
f⁻¹ G g x = (f G (g ⁻¹) x)
    where
        open Group G

T : {A : Set}(G : Group A) → A → symGroup A
T G g = record
    { e = f G e , 
        (record 
            { inj = λ {a} {b} x → 
                trans (sym (idl a)) 
                (sym (trans (sym (idl b)) (sym x))) 
            ; surj = λ {a} → (f G e) , (trans (idl (e ∙ a)) (idl a)) })
    ; _⁻¹ = λ {(f , Bijf) → (λ x → f (x ⁻¹)) ,
        record 
            { inj = {!   !} 
            ; surj = {!   !} }}
    ; _∙_ = {!   !}
    ; idl = {!   !}
    ; idr = {!   !}
    ; invl = {!   !}
    ; invr = {!   !}
    ; assoc = {!   !}
    }
    where
        open Group G

caylay-theorem G = 
        (record
         { e = {!   !}
         ; _⁻¹ = {!   !}
         ; _∙_ = {!   !}
         ; idl = {!   !}
         ; idr = {!   !}
         ; invl = {!   !}
         ; invr = {!   !}
         ; assoc = {!   !}
         }) , {!   !}
    where
        module G = Group G
        open G