module gy06 where

open import Lib hiding (K)

--------------------------------------------------------
-- Elmélet: Függőtípusok elemszámai
--------------------------------------------------------
{-
A függőtípusok is algebrai adattípusoknak számítanak, így természetesen a függőtípusok elemszámai is megadhatók könnyedén.
Nem véletlen, hogy a típusok nevei rendre Σ és Π.

Emlékeztetőül:
| A ⊎ B | = |A| + |B|
| A × B | = |A| ∙ |B|
| A → B | = |B| ^ |A|

Tfh:
P : Bool → Set
P true = Bool
P false = ⊥

Σ Bool P hány elemű lesz?
Ha a Bool-om true (1 konkrét érték), akkor Bool típusú eredményt kell a másik részbe írnom, tehát eddig 2¹ = 2
De ha a Bool-om false (1 konkrét érték), akkor ⊥ típusú eredmény kell, tehát 0¹ = 0

Tehát a nap végén | Σ Bool P | = |Bool| + |⊥| = 2, mert a P egy Bool-tól függő típust ad eredményül, tehát maga a típus vagy P true típusú értéket tartalmaz vagy P false-ot.
Az ilyen "vagy" kapcsolatról megbeszéltük korábban, hogy az az összeadást jelenti.

Legyen most:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Σ (Maybe Bool) P? Igazából a típus egyes elemei alapján csak meg kell nézni, hogy hány elemű típust adnak vissza.
| Σ (Maybe Bool) P | = | P nothing | + | P (just true) | + | P (just false) | = |⊤| + |Bool| + |Maybe Bool → Bool| = 1 + 2 + 8 = 11

Ez alapján az intuíció az lehet, hogy | Σ A B | = Σ (i : A) |B i|; tehát csak össze kell adni az egyes típusokból képzett új típusok elemszámát és ennyi.
(Nem véletlen, hogy Σ a típus neve. Ellenőrizhető, hogy A × B elemszáma könnyen ki fog jönni, hogy ha B nem függőtípus.)

Mi a helyzet, ha ugyanezt játszuk Π-vel? Hány elemű lesz Π A B?

Megint konkrét helyzetben, legyen:
P : Bool → Set
P true = Bool
P false = ⊥

| Π Bool P | =⟨ Agda szintaxissal ⟩= | (b : Bool) → P b | kell.
A függvényeknek totálisaknak kell lenniük, tehát ez azt jelenti, hogy MINDEN lehetséges b : Bool értékre P b-nek definiáltnak kell lennie, false-ra ÉS true-ra is.
Intuíció alapján | P true | ÉS | P false | kelleni fog, az ÉS kapcsolat matematikában a szorzást szokta jelenteni, tehát | P true | ∙ | P false | elemszámú lesz
ez a kifejezés.
| P true | ∙ | P false | = |Bool| ∙ |⊥| = 2 ∙ 0 = 0
Próbáljuk meg definiálni ezt a függvényt:
-}
P₁ : Bool → Set
P₁ true = Bool
P₁ false = ⊥

ΠBoolP : Π Bool P₁
ΠBoolP b = {!!}

-- Rájöhetünk, hogy a false ággal gondok lesznek.
{-
Következő példa, ez a P már ismerős:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Π (Maybe Bool) P?
A függvények továbbra is totálisak kell legyenek. Ez azt jelenti, hogy a függvény definiálva kell legyen (P nothing)-ra, (P (just true))-ra ÉS (P (just false))-ra is,
tehát lesz | P nothing | ∙ | P (just true) | ∙ | P (just false) | elemünk, |⊤| ∙ |Bool| ∙ |Maybe Bool → Bool| = 1 ∙ 2 ∙ 2³ = 16 elemű lesz Π (Maybe Bool) P.

Intuíció alapján általánosan | Π A B | = Π (i : A) | B i |, tehát csak az összes B-ből képezhető típus elemszámát össze kell szorozni.

Gyakorlás:
Adott a következő P:

P : Bool × Bool → Set
P (true , true) = ⊤
P (true , false) = Bool
P (false , true) = Bool → Bool ⊎ ⊤
P (false , false) = Bool ⊎ ⊤ → Bool

Hány elemű lesz Σ (Bool × Bool) P?
Hány elemű lesz Π (Bool × Bool) P?

Kicsit érdekesebb ezeket vegyíteni, de az elv ugyanaz marad.
Marad ugyanaz a P.

Hány elemű lesz Π (a : Bool) (Σ (b : Bool) (P (a , b)))? ( Agda szintaxissal (a : Bool) → Σ Bool (λ b → P (a , b)) )
Hány elemű lesz Σ (a : Bool) (Π (b : Bool) (P (a , b)))? ( Agda szintaxissal Σ Bool λ a → (b : Bool) →  P (a , b)) )
-}

----------------------------------------------
-- Some Sigma types
----------------------------------------------

Σ=⊎ : {A B : Set} → Σ Bool (if_then A else B) ↔ A ⊎ B
fst Σ=⊎ (true , a) = inl a
fst Σ=⊎ (false , b) = inr b
snd Σ=⊎ (inl a) = true , a
snd Σ=⊎ (inr b) = false , b

Σ=× : {A B : Set} → Σ A (λ _ → B) ↔ A × B
fst Σ=× (a , b) = a , b
snd Σ=× (a , b) = a , b

-- Π A F is essentially (a : A) → F a
-- what does this mean?

                    -- Π A (λ _ → B)
Π=→ : {A B : Set} → ((a : A) → (λ _ → B) a) ≡ (A → B)
Π=→ = refl

                    -- Π Bool (if_then A else B)
→=× : {A B : Set} → ((b : Bool) → if b then A else B) ↔ A × B
fst →=× f = f true , f false
snd →=× x true = fst x
snd →=× x false = snd x

dependentCurry : {A : Set}{B : A → Set}{C : (a : A) → B a → Set} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
fst dependentCurry f (a , Ba) = f a Ba
snd dependentCurry f a Ba = f (a , Ba)

---------------------------------------------------------
-- propositional logic
------------------------------------------------------

-- Curry-Howard izomorfizmus
-- Elmélet:
--   ∙ átalakítani logikai állításokat típusokra.
--   ∙ formalizálni állításokat típusokkal.
--   × = ∧ = konjunkció
--   ⊎ = ∨ = diszjunkció
--   ¬ = ¬ = negáció
--   ⊃ = → = implikáció

--------------------------------------------------
-- Formalisation
--------------------------------------------------

-- Formalizáljuk a mondatokat!

-- Az egyes formalizált alap mondatrészeket vegyük fel modul paraméterként, akkor szépen fog működni minden.
module Formalise( SutANap EsikAzEso KellAzEsernyo VanSzivarvany : Set ) where

  -- Nem süt a nap.
  -- \Ű neg
  form1 : Set
  form1 = ¬ SutANap
-- 

  -- Esik az eső és süt a nap.
  form2 : Set
  form2 = EsikAzEso × SutANap

  -- Nem kell az esernyő vagy esik az eső.
  form3 : Set
  form3 = ¬ KellAzEsernyo ⊎ EsikAzEso

  -- Ha esik az eső és süt a nap, akkor van szivárvány.
  form4 : Set
  form4 = (EsikAzEso × SutANap) → VanSzivarvany

  -- Van szivárvány.
  K : Set
  K = VanSzivarvany

---- Következményfogalom (logika tárgy 1-3. gyakorlat)
  -- Agdában legegyszerűbben szintaktikus következményekkel lehet foglalkozni.

  -- Mondd ki, és bizonyítsd be, hogy a fenti állításokból következik a K.
  -- A típusban kell kimondani az állítást; az állítás kimondásához az eldöntésprobléma tételét kell használni.
  -- Két féleképpen lehet bizonyítani.

  Köv : Set
  Köv = ¬ (form1 × form2 × form3 × form4 × ¬ K)

  Köv1 : Köv
  Köv1 (f1 , f2 , f3 , f4 , nK) = nK (f4 f2)

  Köv2 : Köv
  Köv2 (f1 , f2 , f3 , f4 , nK) = f1 (snd f2)

----------------------------------------------------------------------------

subt-prod : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
subt-prod f g (a , b) = f a , g b

subt-fun : {A A' B B' : Set} → (A → A') → (B → B') → (A' → B) → (A → B')
subt-fun f g h a = g $ h $ f a -- g ( h (f a) )

anything : {X Y : Set} → ¬ X → X → Y
anything ¬x x = exfalso (¬x x)

ret : {X : Set} → X → ¬ ¬ X
ret x ¬x = exfalso (¬x x) 

postulate
  lem' : {X : Set} → X ⊎ ¬ X

fun : {X Y : Set} → (¬ X) ⊎ Y → (X → Y)
fun (inl a) x = exfalso (a x)
fun (inr b) x = b



-- De Morgan

-- (b : ⊥) → Y
-- exfalso b : Y
 
dm1 : {X Y : Set} →  ¬ (X ⊎ Y) ↔ ¬ X × ¬ Y
fst dm1 ¬x∨y = (λ x → ¬x∨y (inl x)) , λ y → ¬x∨y (inr y)
snd dm1 (¬x , ¬y) (inl x) = ¬x x
snd dm1 (¬x , ¬y) (inr y) = ¬y y

dm2 : {X Y : Set} → ¬ X ⊎ ¬ Y → ¬ (X × Y)
dm2 (inl ¬x) (x , y) = ¬x x
dm2 (inr ¬y) (x , y) = ¬y y

dm2b : {X Y : Set} → ¬ ¬ (¬ (X × Y) → ¬ X ⊎ ¬ Y)
dm2b c = c (λ f → inl λ x → {- Eddig felvettük az x et az x-es ágból -} c (λ f' → inr λ y → f (x , y)))

-- stuff

nocontra : {X : Set} → ¬ (X ↔ ¬ X)
nocontra (f , g) = 
  f 
    (g (λ x → f x x)) -- g-vel kapok egy x-et amit kétszer felhasználok 
    (g (λ x → f x x))

¬¬invol₁ : {X : Set} → ¬ ¬ ¬ ¬ X ↔ ¬ ¬ X
fst ¬¬invol₁ ¬¬¬¬x ¬x = ¬¬¬¬x (λ ¬¬x → ¬¬x ¬x)
snd ¬¬invol₁ ¬¬x ¬¬¬x = ¬¬¬x ¬¬x

¬¬invol₂ : {X : Set} → ¬ ¬ ¬ X ↔ ¬ X
fst ¬¬invol₂ ¬¬¬x x = ¬¬¬x (λ ¬x → ¬x x)
snd ¬¬invol₂ ¬x ¬¬x = ¬¬x ¬x

nnlem : {X : Set} → ¬ ¬ (X ⊎ ¬ X)
nnlem c = 
  c (inr {- először elmegyünk jobbra mert akkor kapunk egy x-et a ¬x-ből -} 
  λ x → 
  c (inl x))

nndnp : {X : Set} → ¬ ¬ (¬ ¬ X → X)
nndnp c = c (λ ¬¬x → exfalso (¬¬x (λ x → c (λ _ → x))))

-- HA van lem akkot van dne
dec2stab : {X : Set} → (X ⊎ ¬ X) → (¬ ¬ X → X)
dec2stab (inl x) ¬¬x = x
dec2stab (inr ¬x) ¬¬x = exfalso (¬¬x ¬x)

-- you have to decide:
{-
Dec : Set → Set
Dec A = A ⊎ ¬ A
-}

open import Lib.Dec.PatternSynonym

ee1 : {X Y : Set} → Dec (X ⊎ Y → ¬ ¬ (Y ⊎ X))
ee1 = yes (λ {(inl x) ¬y∨x → ¬y∨x (inr x) ; (inr y) ¬y∨x → ¬y∨x (inl y)})

ee2 : {X : Set} → Dec (¬ (X ⊎ ¬ X))
ee2 = no (λ f → f (inr λ x → f (inl x)))

e3 : {X : Set} → Dec (¬ (X → (¬ X → X)))
e3 = no (λ f → f (λ x ¬x → x))

e4 : Dec ℕ
e4 = yes zero

e5 : Dec ⊥
e5 = no (λ ())

e6 : {X : Set} → Dec (⊥ → X ⊎ ¬ X)
e6 = yes (λ b → inr (λ _ → b))
--e6 = yes (λ ())

e7 : {X : Set} → Dec (X × ¬ X → ¬ X ⊎ X)
e7 = yes (λ {(x , ¬x) → inl ¬x})
--e7 = yes (λ {(x , ¬x) → inr x})

e8 : {X : Set} → Dec ((X → X) → ⊥)
e8 = no λ f → f (λ z → z)

f1 : {X Y : Set} → ¬ ¬ X ⊎ ¬ ¬ Y → ¬ ¬ (X ⊎ Y)
f1 (inl a) f = a λ x → f (inl x)
f1 (inr b) f = b λ y → f (inr y)

f2 : ({X Y : Set} → ¬ (X × Y) → ¬ X ⊎ ¬ Y) → {X Y : Set} → ¬ ¬ (X ⊎ Y) → ¬ ¬ X ⊎ ¬ ¬ Y
f2 x x₁ = {!   !}

----------------------------------------------------------------------
-- Not exactly first order logic but kinda is and kinda isn't.

f3 : Dec ((X Y : Set) → X ⊎ Y → Y)
f3 = {!!}

f4 : Dec ((X Y Z : Set) → (X → Z) ⊎ (Y → Z) → (X ⊎ Y → Z))
f4 = {!!}

f5 : Dec ((X Y Z : Set) → (X → Z) × (Y → Z) → (X × Y → Z))
f5 = {!!}

f6 : Dec ((X Y Z : Set) → (X × Y → Z) → (X → Z) × (Y → Z))
f6 = {!!}

f7 : Dec ((X Y Z : Set) → (X ⊎ Y × Z) → (X ⊎ Y) × (X ⊎ Z))
f7 = {!!}

f8 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → ((X ⊎ Y) × Z))
f8 = {!!}

f9 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → (X ⊎ Y × Z))
f9 = {!!}
  