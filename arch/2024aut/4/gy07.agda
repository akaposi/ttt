module gy07 where

open import Lib
open import Lib.Dec.PatternSynonym

------------------------------------------------------
-- statements as parameters
------------------------------------------------------
--        Minden állításra, ha kapok egy bizonyítást hogy A-nem igaz (nincs eleme), akkor eljutok botomba
--blowUp' : (A : Set) → ¬ A → ⊥
--blowUp' A x = x {!   !}

-- Másik olvasat: (A : Set) → (¬ A → ⊥) = (A : Set) → (¬ ¬ A), ugye ez nem igaz mert mi van ha A = ⊥ 
-- A = ℕ-ra pl mükszik
blowupNat : ¬ ℕ → ⊥
blowupNat x = x zero

-- Itt ha kapok egy függvényt ami azt mondja hogy : Minden állításra ¬A, akkor ⊥
-- A lényeg hogy egy függvényt kapok, vagyis megválaszthatom az (A : Set)-et, vagyis az állítást 
blowUp : ((A : Set) → ¬ A) → ⊥
blowUp f = f ⊤ tt
-- what's the difference with this?
-- (A : Set) → ¬ A → ⊥

-- Első esetet úgy mondjuk : Minden A-ra, ...
-- Másodikat :               Létezik A hogy, ..

-- something like this may appear in the exam

---------------------------------------------------------
-- predicate (first order) logic example
---------------------------------------------------------

-- ∀ x → ¬ (P x) == (x : X) → ¬ (P x)

-- ∀ a → ? == (a : A) → ?
-- A külömbség hogy a baloldaliba megpróbálja kitalálni a típust agda
-- Σ A (λ x → P x) = Létezik x ∈ A hogy P x igaz
notExists↔noneOf : ∀{i}{A : Set i} → (P : A → Set) →
                        (∀ x → ¬ (P x)) ↔ ¬ (Σ A (λ x → P x))
fst (notExists↔noneOf P) f (a , Pa) = f a Pa
snd (notExists↔noneOf P) ¬f a Pa = ¬f (a , Pa)
-- A Sigma típus tárolja a x ∈ A-t és a tulajdonságot P x-et vagyis a bizonyítást is

module People
  (Person    : Set)
  (Ann       : Person)
  (Kate      : Person)
  (Peter     : Person)
  (_childOf_ : Person → Person → Set)
  (_sameAs_  : Person → Person → Set) -- ez most itt az emberek egyenlosege
  where

  -- Define the _hasChild predicate.
  _hasChild : Person → Set
  x hasChild = Σ Person λ y → y childOf x

  -- syntax Σ A (λ x → P) = ∃ x ∈ A , P

-- ∃ y ∈ Person :

  -- Formalise: Ann is not a child of Kate.
  ANK : Set
  ANK = ¬ (Ann childOf Kate)

  -- Formalise: there is someone with exactly one child.
  ONE : Set
  ONE = Σ Person (λ x → Σ Person (λ z → (z childOf x) × ((y : Person) → ¬ (y sameAs z) → ¬ (y childOf x))))

  -- Define the relation _parentOf_.
  _parentOf_ : Person → Person → Set
  x parentOf y = y childOf x

  -- Formalise: No one is the parent of everyone.
  NOPE : Set
  NOPE = ¬ (Σ Person (λ x → ∀ y → x parentOf y))

  -- Prove that if Ann has no children then Kate is not the child of Ann.
  AK : ¬ (Σ Person λ y → y childOf Ann) → ¬ (Kate childOf Ann)
  AK f kca = f (Kate  , kca)

  -- Prove that if there is no person who is his own parent than no one is the parent of everyone.
  ¬xpopxthenNOPE : ¬ (Σ Person λ x → x parentOf x) → NOPE
  ¬xpopxthenNOPE f (p , parentOfAll) = f (p , parentOfAll p)

---------------------------------------------------------
--        predicate (first order) logic laws           --
---------------------------------------------------------


∀×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a × Q a)  ↔ ((a : A) → P a) × ((a : A) → Q a)
fst (∀×-distr A P Q) x = (λ a → fst (x a)) , λ a → snd (x a)
snd (∀×-distr A P Q) (f , g) a = f a , g a

∀⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a) ⊎ ((a : A) → Q a) → ((a : A) → P a ⊎ Q a)
∀⊎-distr A P Q (inl Pa) a = inl (Pa a)
∀⊎-distr A P Q (inr Qa) a = inr (Qa a)
-- ez miért csak odafelé megy?
-- miért nem ↔ van közte?

Σ×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a × Q a)  → Σ A P × Σ A Q
Σ×-distr A P Q (a , Pa , Qa) = (a , Pa) , (a , Qa)

Σ⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a ⊎ Q a)  ↔ Σ A P ⊎ Σ A Q
fst (Σ⊎-distr A P Q) (a , inl Pa) = inl (a , Pa)
fst (Σ⊎-distr A P Q) (a , inr Qa) = inr (a , Qa)
snd (Σ⊎-distr A P Q) (inl (a , Pa)) = (a , inl Pa)
snd (Σ⊎-distr A P Q) (inr (a , Qa)) = (a , inr Qa)

¬∀        :    (A : Set)(P : A → Set)              → (Σ A λ a → ¬ P a)      → ¬ ((a : A) → P a)
¬∀ A P (a , ¬Pa) Pa = ¬Pa (Pa a)

-- Ugyanez van a fájl tetején is:
¬Σ        :    (A : Set)(P : A → Set)              → (¬ Σ A λ a → P a)      ↔ ((a : A) → ¬ P a)
fst (¬Σ A P) ¬∃Pa a Pa = ¬∃Pa ((a , Pa))
snd (¬Σ A P) ¬Pa (a , Pa) = ¬Pa a Pa

¬¬∀-nat   :    (A : Set)(P : A → Set)              → ¬ ¬ ((x : A) → P x)    → (x : A) → ¬ ¬ (P x)
¬¬∀-nat A P ¬¬Pa a ¬Pa = ¬¬Pa (λ Pa → ¬Pa (Pa a))

∀⊎-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (((a : A) → P a ⊎ Q a) → ((a : A) → P a) ⊎ ((a : A) → Q a)))
∀⊎-distr' w = cont ce 
  where
    P Q : Bool → Set

    P true = ⊤
    P false = ⊥

    Q true = ⊥
    Q false = ⊤

    p : (a : Bool) → P a ⊎ Q a
    p false = inr tt
    p true = inl tt

    ce : ((x : Bool) → P x) ⊎ ((x : Bool) → Q x)
    ce = (w Bool P Q p)
    
    cont : ((x : Bool) → P x) ⊎ ((x : Bool) → Q x) → ⊥
    cont (inl a) = a false
    cont (inr b) = b true

Σ×-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (Σ A P × Σ A Q → Σ A λ a → P a × Q a))
Σ×-distr' w = cont ce
  where
    P Q : Bool → Set

    P true = ⊤
    P false = ⊥

    Q true = ⊥
    Q false = ⊤

    ce : Σ Bool (λ a → (P a) × (Q a))
    ce = (w Bool P Q ( (true , tt) , (false , tt)))
    
    cont : Σ Bool (λ a → (P a) × (Q a)) → ⊥
    cont (false , (b , tt)) = b
    cont (true , (tt , b)) = b

Σ∀       : (A B : Set)(R : A → B → Set)        → (Σ A λ x → (y : B) → R x y) → (y : B) → Σ A λ x → R x y
Σ∀ A B R (a , ∀bRab) b  = a , ∀bRab b

AC       : (A B : Set)(R : A → B → Set)        → ((x : A) → Σ B λ y → R x y) → Σ (A → B) λ f → (x : A) → R x (f x)
AC A B R ∀a∃bRab = (λ a → fst (∀a∃bRab a)) , λ a → snd (∀a∃bRab a)