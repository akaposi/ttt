module gy08 where

open import Lib 
  hiding (sym; cong; subst; idr+; sucr+; assoc+; comm+; dist+*; nullr*; idl*; idr*; sucr*; assoc*; comm*)
  renaming (trans to transₗ)

---------------------------------------------------------
-- equality
------------------------------------------------------

data Eq' (A : Set) : A → A → Set where
  refl : (a : A) → Eq' A a a

sym : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
sym refl = refl

trans : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans refl b = b

{-
other notions of trans:
_◾_ = \sq5

\< = ⟨ ; \> = ⟩ ; \qed = ∎
_≡⟨_⟩_ + _∎

_≡⟨_⟩_
Λ  Λ ^-- proof
|  |
|  ⌞ proof
value

_∎ = basically reflexivity proof with an explicit value

Usual way of using this notion:

value1
  ≡⟨ proof1 ⟩
value2
  ≡⟨ proof2 ⟩
value3
  ≡⟨ proof3 ⟩
value4 ∎
-}

--_≡⟨_⟩_ : ∀{i}{A : Set i}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
--_ ≡⟨ p ⟩ q = transₗ p q
--
---- \qed = ∎
--_∎ : ∀{i}{A : Set i}(x : A) → x ≡ x
--_ ∎ = refl

--infixr 2 _≡⟨_⟩_
--infix 3 _∎

cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
cong f refl = refl

--                                  Ha x ≡ y és P igaz x-re ((P x)-nek van eleme) akkor P igaz y-ra is 
subst : ∀{i j}{A : Set i}(P : A → Set j){x y : A} → x ≡ y → P x → P y
subst P refl Px = Px

-- Mi történik amikor pattern matchelünk egy a≡b -re. 
-- subst a _≡_ típus "eleminátora", vagyis mindent meglehet írni vele    

cong' : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
cong' {i}{j}{A}{B} f {x}{y} eq = (subst (λ a → (f x ≡ f a)) eq refl)

sym' : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
sym' {i}{A}{x}{y} x≡y = 
  subst 
  (λ a → a ≡ x) 
  x≡y 
  refl

trans' : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans' {i}{A}{x}{y}{z} x≡y y≡z = {!   !}

---------------------------------------------------------
-- properties of +,*
---------------------------------------------------------

idl+ : (n : ℕ) → zero + n ≡ n
idl+ n = refl

idr+ : (n : ℕ) → n + zero ≡ n
idr+ zero = refl
idr+ (suc n) = cong suc (idr+ n)

sucr+ : (n m : ℕ) → n + suc m ≡ suc (n + m)
sucr+ zero m = refl
sucr+ (suc n) m = cong suc (sucr+ n m)

ass+ : (m n o : ℕ) → (m + n) + o ≡ m + (n + o)
ass+ zero n o = refl
ass+ (suc m) n o = cong suc (ass+ m n o)

comm+-helper : (n m : ℕ) → suc n + m ≡ n + suc m
comm+-helper zero m = refl
comm+-helper (suc n) m = cong suc (comm+-helper n m)

comm+ : (m n : ℕ) → m + n ≡ n + m
comm+ zero n = sym (idr+ n)
comm+ (suc m) n = sym (trans (sym (comm+-helper n m)) (cong suc (comm+ n m)))

-- Gergő megoldása
comm+' : (m n : ℕ) → m + n ≡ n + m
comm+' m zero = idr+ m
comm+' m (suc n) = trans (sucr+ m n) (cong suc (comm+' m n))

-- Dani megoldása
comm+'' : (m n : ℕ) → m + n ≡ n + m
comm+'' zero n = sym (idr+ n)
comm+'' (suc m) n = trans (comm+-helper m n) (trans (sucr+ m n) (sym (trans (sucr+ n m) (cong suc (comm+'' n m)))))

dist+* : (m n o : ℕ) → (n + o) * m ≡ n * m + o * m
dist+* m zero o = refl
dist+* m (suc n) o = sym (trans (ass+ m (n * m) (o * m)) (cong (m +_) (sym (dist+* m n o))))

nullr* : (n : ℕ) → n * 0 ≡ 0
nullr* zero = refl
nullr* (suc n) = nullr* n

idl* : (n : ℕ) → 1 * n ≡ n
idl* zero = refl
idl* (suc n) = cong suc (idl* n)

idr* : (n : ℕ) → n * 1 ≡ n
idr* zero = refl
idr* (suc n) = cong suc (idr* n)

sucr* : (n m : ℕ) → n * suc m ≡ n + n * m
sucr* zero m = refl
sucr* (suc n) m = 
  cong suc 
  (trans (cong (m +_) (sucr* n m)) 
  (trans (sym (ass+ m n (n * m))) 
  (trans (cong (_+ n * m) (comm+ m n)) (ass+ n m (n * m)))))

ass* : (m n o : ℕ) → (m * n) * o ≡ m * (n * o)
ass* zero n o = refl
ass* (suc m) n o = trans (dist+* o n (m * n)) (cong (λ z → n * o + z) (ass* m n o))

comm*-helper : (n m : ℕ) → n + n * m ≡ n * suc m
comm*-helper zero m = refl
comm*-helper (suc n) m = 
  cong suc (sym 
    (trans (cong (λ z → m + z) (sym (comm*-helper n m))) 
    (trans (sym (ass+ m n (n * m))) 
    (sym (trans (sym (ass+ n m (n * m))) (cong (_+ n * m) (comm+ n m))))))) 

comm* : (m n : ℕ) → m * n ≡ n * m
comm* zero n = sym (nullr* n)
comm* (suc m) n = sym (trans (sucr* n m) (cong (n +_) (comm* n m)))
