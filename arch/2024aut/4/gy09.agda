module gy09 where

open import Lib

------------------------------------------------------
-- η-szabály / egyediség szabály
------------------------------------------------------
{-
Most már van egyenlőség típusunk, így tudunk beszélni az η-szabályról.
A félév elején meg volt említve a függvények η-szabálya (λ a → f a) ≡ f, ez a szabály garantálja,
hogy függvényre csakis egyféleképpen tudunk alkalmazni paramétert, magyarul csakis pontosan egy destruktorom/eliminátorom van.

Tehát az η-szabály azt mondja, hogy ha van egy másik eliminátorom,
ami pontosan ugyanúgy viselkedik, mint az eredeti eliminátor,
akkor az pontosan az elsőként meghatározott eliminátor kell legyen.

Példa Bool-okon:
Eliminátora: if_then_else_ : Bool → A → A → A
β-szabályok: if true  then x else y = x
             if false then x else y = y
η-szabály: {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ if b then x else y
           --------------------------------   ------------------------
             eliminátor dolgai                      β-szabályai
             paraméterek

Természetesen ez ugyanúgy generálható, hiszen minden része megadható csak a típusnak a definíciójából.

data 𝟛 : Set where
  a1 a2 a3 : 𝟛

ite𝟛 : A → A → A → 𝟛 → A
ite𝟛 x y z a1 = x
ite𝟛 x y z a2 = y
ite𝟛 x y z a3 = z

η-szabály: ?

Az egyetlen trükkösebb helyzet, amikor a típusunk rekurzív. De ekkor sem kell megijedni, hiszen az η-szabály leírásakor
csak a β-szabályok viselkedését kell követni.

Például:

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

iteℕ : A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

η-szabály: (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → (n : ℕ) → f n ≡ iteℕ z s n


data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : B → (A → B → B) → List A → B
iteList e c [] = e
iteList e c (x ∷ xs) = c x (iteList e c xs)

η-szabály: ?

-}

---------------------------------------------------------
-- equational reasoning
------------------------------------------------------

--_≡⟨_⟩_ : ∀{i}{A : Set i}(x : A){y z : A} → x ≡ y → y ≡ z → x ≡ z
--_ ≡⟨ p ⟩ q = trans p q
--
---- \qed = ∎
--_∎ : ∀{i}{A : Set i}(x : A) → x ≡ x
--_ ∎ = refl


p4 : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4 x y = 
    (x + (y + zero) + x) 

        ≡⟨ cong (λ z → x + z + x) (idr+ y) ⟩ 

    ((x + y + x) 

        ≡⟨ assoc+ x y x ⟩ 

    ((x + (y + x)) 

        ≡⟨ cong (x +_) (comm+ y x) ⟩ 

    ((x + (x + y)) 

        ≡⟨ cong (λ z → x + (z + y)) (sym (idr+ x)) ⟩

    ((x + ((x + zero) + y)) 

        ≡⟨ sym (assoc+ x (x + zero) y) ⟩ 

    ((x + (x + zero) + y) 

        ∎)))))

p3 : (a b : ℕ) → a + a + b + a * 0 ≡ 2 * a + b
p3 = {!  !}

p2 : (a b c : ℕ) → c * (b + 1 + a) ≡ a * c + b * c + c
p2 = {!!}

p9' : 0 ≢ the ℕ 1
p9' ()

p9 : 2 * 2 ≢ 5 * 1
p9 ()

-- Egyszerűbb, amikor mondani kell egy ellenpéldát:
p10 : ¬ ((n : ℕ) → n + 2 ≡ n + 1)
--p10 ... -- másik ág
p10 f with (f zero)
... | ()


-- ≢
-- ...mintsem bizonyítani, hogy ez a kettő sosem lesz egyenlő:
p11 : (n : ℕ) → n + 2 ≢ n + 1
p11 (suc m) e = {!   !} -- p11 m (cong (pred') e)

-- Mókásabb helyzet.
{-# TERMINATING #-}
p11'' : ¬ Σ ℕ (λ n → n + 3 ≡ n + 1)
p11'' (zero , ())
p11'' (suc n , e) = p11'' (n , cong (pred') e)

p11''' : ¬ Σ ℕ (λ n → n + 3 ≡ n + 1)
p11'''  (n , e) = {!   !} -- s n e
  where
    s : (n : ℕ) → n + 3 ≢ n + 1 
    s (suc n) e = s n (cong pred' e)

p12 : ¬ Σ ℕ (λ n → n + n ≡ 3)
p12 (suc (suc (suc zero)) , ())
p12 (suc (suc (suc (suc n))) , ())

[m+n]^2=m^2+2mn+n^2 : (m n : ℕ) → (m + n) * (m + n) ≡ m * m + 2 * m * n + n * n
[m+n]^2=m^2+2mn+n^2 = {!!}

{-
infixr 8 _^_
_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n
-}

p1 : (a b : ℕ) → (a + b) ^ 2 ≡ a ^ 2 + 2 * a * b + b ^ 2
p1 = {!  !}

0^ : (n : ℕ) → 0 ^ (suc n) ≡ 0
0^ n = refl

^0 : (a : ℕ) → a ^ 0 ≡ 1
^0 a = refl

1^ : (n : ℕ) → 1 ^ n ≡ 1
1^ zero = refl
1^ (suc n) = trans (idr+ (1 ^ n)) (1^ n)

^1 : (a : ℕ) → a ^ 1 ≡ a
^1 zero = refl
^1 (suc a) = cong suc (^1 a)

^+ : (a m n : ℕ) → a ^ (m + n) ≡ a ^ m * a ^ n
^+ a zero n = sym (idr+ (a ^ n))
^+ a (suc m) n = trans (cong (a *_) (^+ a m n)) (sym (assoc* a (a ^ m) (a ^ n))) -- a * a ^ (m + n) ≡ (a * (a ^ m)) * (a ^ n)

*^ : (a b n : ℕ) → (a * b) ^ n ≡ a ^ n * b ^ n
*^ a b zero = refl
*^ a b (suc n) = 
    trans (assoc* a b ((a * b) ^ n)) 
    (trans (cong (a *_) (sym 
    (trans (sym (assoc* (a ^ n) b (b ^ n))) 
    (trans (cong (_* b ^ n) (comm* (a ^ n) b)) 
    (trans (assoc* b (a ^ n) (b ^ n)) (cong (b *_) (sym (*^ a b n)))))))) 
    (sym (assoc* a (a ^ n) (b * b ^ n))))

^* : (a m n : ℕ) → a ^ (m * n) ≡ (a ^ m) ^ n
^* a zero n = sym (1^ n)
^* a (suc m) n = trans (^+ a n  (m * n)) (sym (trans (*^ a (a ^ m) n) (cong (a ^ n *_) (sym (^* a m n))) ))

