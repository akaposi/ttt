module gy10 where

open import Lib hiding (_≟ℕ_)
open List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj m n refl = refl

-- prove it without pattern matching on e! (hint: use pred')
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = cong pred' e

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj {f} {g} e = subst (λ k → f ≡ remnode k) e refl -- cong remnode -- subst ? e refl -- seg : kell egy f => node f = f
  where
    remnode : Tree → (ℕ → Tree)
    remnode leaf _ = leaf
    remnode (node f) = f

-- sucinj m n x = subst (λ k → m ≡ pred' k) x refl


data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl e = cong getX e
  where
    getX : BinTree → BinTree
    getX leaf = leaf
    getX (node x y) = x

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr e = cong getY e
  where
    getY : BinTree → BinTree
    getY leaf = leaf
    getY (node x y) = y 

nodeinj' : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x' × y ≡ y' 
nodeinj' x = (nodeinjl x) , (nodeinjr x)

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 refl = refl

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 refl = refl

-- prove all of the above without pattern matching on equalities!


∷inj1' : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1' {A}{x} = cong (getHead x)
  where
    getHead : {A : Set} → A → List A → A
    getHead a [] = a
    getHead _ (x ∷ xs) = x

∷inj2' : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2'  = cong getTail
  where
    getTail : {A : Set} → List A → List A
    getTail [] = []
    getTail (x ∷ xs) = xs

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false ()

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst (λ x → if x then ⊤ else ⊥) e tt

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn e = subst P e tt
  where
    P : ℕ → Set
    P zero = ⊤
    P _ = ⊥

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn n ()

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' zero ()
n≠sucn' (suc n) e = n≠sucn' n (cong pred' e)

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node e = subst P e tt
  where
    P : Tree → Set
    P leaf = ⊤
    P (node x) = ⊥

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' e = subst P e tt
  where
    P : BinTree → Set
    P leaf = ⊤
    P (node x y) = ⊥

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons e = subst P e tt
  where
    P : {A : Set} → List A → Set
    P [] = ⊤
    P (x ∷ xs) = ⊥

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 (zero , ())
¬2≤1 (suc zero , ())
¬2≤1 (suc (suc k) , ())

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn n = 1 , refl

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ m .(k + m) (k , refl) = k , (sucr+ k m)

suc-monotonous≤' : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤' n m (k , e) = k , trans (sucr+ k n) (cong suc e)

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ n m (k , e) = k , (sucinj (k + n) m (trans (sym (sucr+ k n)) e)) -- 
     
---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

test : Bool → ⊤
test = λ { false → tt
         ; true → tt }

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
false ≟Bool false = inl refl
false ≟Bool true = inr (λ ())
true ≟Bool false = inr (λ ())
true ≟Bool true = inl (refl)

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
zero ≟ℕ zero = inl refl
zero ≟ℕ suc n' = inr (λ ())
suc n ≟ℕ zero = inr (λ ())
suc n ≟ℕ suc n' with (n ≟ℕ n') 
... | inl refl = inl refl
... | inr f = inr (λ e → f (cong pred' e))

-- is equality for Tree decidable?

_,='_ : {A B : Set}{a a' : A}{b b' : B} → a ≡ a' → b ≡ b' → (a , b) ≡ (a' , b')
refl ,=' refl = refl

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
leaf ≟BinTree leaf = inl refl
leaf ≟BinTree (node t' t'') = inr (λ ())
(node t t₁) ≟BinTree leaf = inr (λ ())
node t t₁ ≟BinTree node t' t'' with t ≟BinTree t' | t₁ ≟BinTree t''
... | inl a | inl a₁ = inl (cong (λ e → node (fst e) (snd e)) (a ,=' a₁))
... | inl a | inr b = inr λ x → b (nodeinjr x)
... | inr b | inl a = inr λ x → b (nodeinjl x)
... | inr b | inr b₁ = inr λ x → b (nodeinjl x) -- or nodeinjr x

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ {A} _ {[]} {[]} = inl refl
_≟List_ {A} _ {[]} {y ∷ ys} = inr (λ ())
_≟List_ {A} _ {x ∷ xs} {[]} = inr (λ ())
_≟List_ {A} ≟ {x ∷ xs} {y ∷ ys} with ≟ {x} {y} | (((_≟List_ {A}) ≟) {xs} {ys})
... | inl a | inl a₁ = inl (cong (λ z → fst z ∷ snd z) (a ,=' a₁))
... | inl a | inr b = inr λ f → b (∷inj2 f)
... | inr b | inl a = inr λ f → b (∷inj1 f)
... | inr b | inr b₁ = inr (λ f → b (∷inj1 f))