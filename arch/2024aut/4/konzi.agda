open import Lib.Nat
open import Lib.Containers.List using (_++_)
open import Lib

{-
    1. feladat

f : ℕ → ℕ
f = λ x → x + 1

g : (ℕ → ℕ) → ℕ
g = λ h → h (h 14)

g f = (λ h → h (h 14)) f                     (g definíciója)
    = (λ h → h (h 14)) (λ x → x + 1)         (f definíciója)
    = (λ h → h (h 14))[h := (λ x → x + 1)]
    = (λ x → x + 1) ((λ x → x + 1) 14)       (függvény β szabály)
    = (λ x → x + 1) ((λ x → x + 1)[x := 14]) 
    = (λ x → x + 1) (14 + 1)                 (függvény β szabály)
    = (λ x → x + 1)[x := (14 + 1)]           
    = (14 + 1) + 1                           (függvény β szabály)
    = 16                                     (általános iskolai tudás)


    2. feladat :

    (Bool → ⊤ ⊎ Bool) × (A → ⊥)
    
    | (Bool → ⊤ ⊎ Bool) | = |⊤ ⊎ Bool| ^ |Bool| = (|⊤| + |Bool|) ^ |Bool| = (1 + 2) ^ 2 = 3 ^ 2 = 9

    | (A → ⊥) | = |⊥| ^ |A| = 0 ^ |A| = 0 

    | (Bool → ⊤ ⊎ Bool) × (A → ⊥) | = 9 x 0 = 0

    | Bool | = 2
    | ⊤ |    = 1
    | ⊥ |    = 0
    | A |
    |A × B| = |A| × |B|
    |A ⊎ B| = |A| + |B|
    |A → B| = |B| ^ |A|

    A   --->   B
    a1         b1
    a2         b2
    a3         b3
    ...



-}

f : ℕ → ℕ
f = λ x → x + 1

g : (ℕ → ℕ) → ℕ
g = λ h → h (h 14)

test : {A : Set} → (Bool → ⊤ ⊎ Bool) × (A → ⊥)
test = (λ x → inl tt) , (λ x → {!   !})

-- 3. feladat

data X (A : Set) : Set where
    X1 : A → X A → X A
    X2 : ℕ → X A

iteX : {A C : Set} → (A → C → C) → (ℕ → C) → X A → C  -- destruktor/iterátor típusa
iteX f g (X1 a x) = f a (iteX f g x)   -- beta1
iteX f g (X2 n  ) = g n                -- beta2

{-

iterátor - destruktor

konstruktorok, csak a típusom 
helyett a tetszőleges típus van → típusom    → teszőleges másik típus
--        (A → C → C) → (ℕ → C) →   X A      → C

X1 : A → C → C
X2 : ℕ → C


iteX {A C : Set} → (A → C → C) → (ℕ → C) → X A → C
iteX f g (X1 a x) = f a (iteX f g x) 
iteX f g (X2 n  ) = g n

η szabály :
X1 : A → X A → X A 

iteX X1 X2 x = x   -- X η szabály

-}

-- 4. feladat

function' : ℕ → ℕ
function' zero = 2
function' (suc n) = 2 * n + (function' n) + 1

iteℕ : {A : Set} → A → (A → A) → ℕ → A
iteℕ b f zero = b
iteℕ b f (suc n) = f (iteℕ b f n)


funWithIte : ℕ → ℕ
funWithIte n = (rec n 2 (λ n acc → 2 * n + acc + 1))

{-

    5. feladat

data T : Set where

    c1 : (A₁ → T) → (A₂ → T) → (A₃ → T) → ... (Aₙ → T)
    ...

Minden konstruktor paramétereiben, igaz hogy Aᵢ-k ben nem szerepel T.

data T : Set where
    w : (T → ⊥) → T

Ebből be lehet ki tudom hogyni, hogy ⊥-nak van eleme

to : T → (T → ⊥) 
to w (f) = f

-}

{-# NO_POSITIVITY_CHECK #-}
data D : Set where
    w : (D → ⊥) → D

unweird : D → ⊥
unweird (w x) = x (w x)

bot' : ⊥
bot' = unweird (w unweird)


{-


-}
-- Ctrl + C  Ctrl + C
-- Ezt még órán elmondom 
-- 6. feladat
bij6 : (Bool → Bool) ↔ (Bool × Bool)
fst bij6 f     = f true , f false
snd bij6 bxb true = fst bxb
snd bij6 bxb false = snd bxb

tes : {b : Bool}{f : Bool → Bool} → ((snd bij6) ((fst bij6) f)) b ≡ (id f) b
tes {b} {f} = {!   !}

-- 7. feladat

data TriEither(A B C : Set) : Set where
    left   : A → TriEither A B C
    middle : B → TriEither A B C
    right  : C → TriEither A B C

{-

    left   : A → D
    middle : B → D
    right  : C → D

(A → D) → (B → D) → (C → D) → TriEither A B C → D
-}

iteTriEither : {A B C D : Set} → (A → D) → (B → D) → (C → D) → TriEither A B C → D
iteTriEither f g h (left   a) = f a
iteTriEither f g h (middle b) = g b
iteTriEither f g h (right  c) = h c

-- 8. feladat

f8 : ℕ → ℕ
f8 zero = 1 
f8 x@(suc n) = f8 n + 2 * n + 1 
-- f8 x == x^2+1 == (n+1)^2 + 1 == (n^2 + 2n + 1) + 1 == (n^2 + 2n + 2) == (n ^ 2 + 1) + 2*n + 1 = f8 + 2*n + 1

_ : f8 10 ≡ 101
_ = refl

_ : {n : ℕ} → f8 n ≡ n * n + 1
_ = {!   !}

-- 9. feladat
-- concat [[1,2,3] , [] , [4,5,6] , [7,8,9], []] == [1,2,3,4,5,6,7,8,9]
concat : {A : Set} → List (List A) → List A 
concat [] = [] 
concat (x ∷ xs) = x ++ concat xs 

_ : (concat {ℕ} ((1 ∷ 2 ∷ 3 ∷ []) ∷ ([]) ∷ (1 ∷ []) ∷ [])) ≡ (1 ∷ 2 ∷ 3 ∷ 1 ∷ [])
_ = refl
--  concat ([[1,2,3], [] , [1]]) == [1,2,3,1]
-- [A,l,m,a] ++ [, ,p,i,t,e,] == "Alma pite"