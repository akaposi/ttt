-- 1. 6 pont Vezesd le, hogy hány darab eleme van az alábbi típusnak!
-- | Σ N λ n → (k : N) → (k < n) → Fin n → Fin k | =
{-

-- | Σ N λ n → (k : N) → (k < n) → Fin n → Fin k | =
-- Σ (n = 0-tól ∞-ig) : | (k : N) → (k < n) → Fin n → Fin k | =
-- Σ (n = 0-tól ∞-ig) : Π (k = 0-tól ∞-ig) : |(k < n) → (Fin n → Fin k)| ==
-- Σ (n = 0-tól ∞-ig) : Π (k = 0-tól ∞-ig) : |(k < n) → (k ^ n)| =
-- Σ (n = 0-tól ∞-ig) : Π (k = 0-tól ∞-ig) : (k ^ n) ^ |(k < n)| = 
-- Σ (n = 0-tól ∞-ig) : Π (k = 0-tól ∞-ig) : k ^ (n * | k < n |) =
-- (Π (k = 0-tól ∞-ig) : k ^ (0 * | k < 0 |)) + Σ (n = 1-tól ∞-ig) : Π (k = 0-tól ∞-ig) : k ^ (n * | k < n |)
-- 1 + Σ (n = 1-tól ∞-ig) : Π (k = 0-tól ∞-ig) : k ^ (n * | k < n |)
-- 1 + Σ (n = 1-tól ∞-ig) : (0 ^ (n * | 0 < n |)) * (Π (k = 1-tól ∞-ig) : k ^ (n * | k < n |))
-- 1 + Σ (n = 1-tól ∞-ig) : 0 = 1 + 0 = 1

-- | Σ A B | Σ (a ∈ A) |B a|
-- | Π A B | Π (a ∈ A) |B a|
-- (a : A) → B a = Π A (λ a→ B a)

-- |(k < n)| = 1 ha k kissebb mint n, 0 ha n >= k 

-}

open import Lib
-- 2. feladat
data X : ℕ → Set where
    X1 : (n k : ℕ) → X n → X (n + k)
    X2 : (n : ℕ) → Fin n → X (suc n)

-- Írd le az X típus iterátorának típusát és a β-szabályait!
Xβ : {C : ℕ → Set}(i : ℕ) → 
    (∀(n)(k) → C n → C (n + k)) → -- X1
    (∀(n) → Fin n → C (suc n)) →  -- X2
    X i → C i
Xβ .(n + k) f g (X1 n k Xn) = f n k (Xβ n f g Xn)
Xβ .(suc n) f g (X2 n Finn) = g n Finn

-- Írd le az X típus η-szabályát!
-- Xη
-- w : másik iterátor
-- w : {C : ℕ → Set}(i : ℕ) → (∀(n)(k) → C n → C (n + k)) → (∀(n) → Fin n → C (suc n)) → X i → C i
-- és w i f g (X1 n k Xn) ≡ f n k (Xβ n f g Xn)
-- és w i f g (X2 n Finn) ≡ g n Finn
-- akkor ∀x → w i f g x ≡ Xβ i f g x

Xη : 
    {C : ℕ → Set}(i : ℕ) →
    (w : {C : ℕ → Set}(i : ℕ) → 
    X i → C i)
    → ((n k : ℕ)(Xn : X n)(f : (n k : ℕ) → C n → C (n + k)) → 
    w {C} (n + k) (X1 n k Xn) ≡ f n k (w {C} n Xn))
    → ((n : ℕ)(fin : Fin n)(g : (n : ℕ) → Fin n → C (suc n)) → 
    w {C} (suc n) (X2 n fin) ≡ g n fin)
    → ((f : (n k : ℕ) → C n → C (n + k))(g : (n : ℕ) → Fin n → C (suc n)) → ∀ x → w {C} i x ≡ Xβ i f g x)
Xη .(n + k) w e e₁ f g (X1 n k Xi) = trans (e n k Xi f) (cong (f n k) (Xη n w e e₁ f g Xi))
Xη .(suc n) w e e₁ f g (X2 n fn) = e₁ n fn g

{-
3. feladat
(a) Minden hallgató átmegy a ZH-n.
(b) Van olyan hallgató, aki nem megy át a ZH-n vagy konzultál.
(c) Van olyan hallgató, aki ha nem konzultál, akkor nem megy át a ZH-n.

Univerzum : hallgatók ⇒ Hallgatok : Set
(a) - ∀ h : Hallgatok → Atmegy h
(b) - ∃ h : Hallgatok → (¬ (Atmegy h)) ⊎ Konzoltal h
(c) - ∃ h : Hallgatok → ¬ (Konzultal h) × ¬ (Atmegy h)
-}

module Form
    (Hallgatok : Set) -- U = Hallgatók
    (Atmegy : Hallgatok → Set)
    (Barat : Hallgatok → Hallgatok)
    (Konzultal : Hallgatok → Set) where
    
    a : Set
    a = (h : Hallgatok) → Atmegy h

    b : Set
    b = Σ Hallgatok λ h → ¬ (Atmegy h) ⊎ Konzultal h

    c : Set
    c = Σ Hallgatok λ h → ¬ (Konzultal h) × ¬ (Atmegy h)

{-
4. feladat
Mit jelent az, hogy egy típus konstruktorai diszjunktak? Add meg egy tetszőleges kettő vagy több
konstruktorú típus konstruktorainak diszjunktságára vonatkozó szabályainak típusát! Hány ilyen szabálya
van egy olyan egyszerű típusnak, amelynek k darab konstruktora van? Válaszodat indokold!

Ha van egy X típus n db xᵢ kontruktorral:

konstruktorok diszjunktsága : Minden i,j ∈ [1..n]-re, úgy hogy i ≠ j : xᵢ a b c ... ≡ xⱼ a b c ... → ⊥
konstruktorok injektivitása : Minden i ∈ [1..n]-re, úgy hogy i : xᵢ a b c ... ≡ xᵢ a' b' c' ... → a ≡ a' × b ≡ b' → c ≡ c' → ...
-}

{-
5. feladat
Reprezentáljunk n · k méretű mátrixokat egy Vec (Vec A n) k típussal. Definiáljuk transpose
függvényt mátrixokra, amely a már ismert transzponálás műveletet végzi el!
-}

Matrix : ℕ → ℕ → Set → Set
Matrix n m A = Vec (Vec A n) m

toVec : {n : ℕ}{A : Set} → (Fin n → A) → Vec A n
toVec {zero} {A} xs = []
toVec {suc n} {A} xs = xs fzero ∷ toVec (λ x → xs (fsuc x))

genMatrix : ∀{n}{m}{A} → (Fin m → (Fin n → A)) → Matrix n m A
genMatrix x = toVec (λ sm → toVec (λ sn → x sm sn))

-- (1 ∷ 2 ∷ []) ∷ 
-- (3 ∷ 4 ∷ []) ∷ 
-- []
test : Matrix 2 2 ℕ
test = genMatrix f
    where
        f : Fin 2 → Fin 2 → ℕ
        f fzero fzero = 1
        f fzero (fsuc m) = 2
        f (fsuc n) fzero = 3
        f (fsuc n) (fsuc m) = 4

open import Lib.Containers.Vector

transpose' : ∀{n k A} → Matrix n k A → Matrix k n A
transpose' M = genMatrix (λ n' k' → (M ‼ k' ) ‼ n')
-- tmp[n][k] = m[k][n]

transpose'' : ∀ n k A → Matrix n k A → Matrix k n A
transpose'' n k A M = {! M [ ? ]%= ?  !}

zipWithVec : ∀{n}{A B C : Set} → (A → B → C) → Vec A n → Vec B n → Vec C n 
zipWithVec f [] [] = []
zipWithVec f (x ∷ xs) (y ∷ ys) = f x y ∷ (zipWithVec f xs ys)

transpose : ∀{n k}{A : Set} → Matrix n k A → Matrix k n A
transpose {n} [] = replicate n [] 
transpose (xs ∷ xss) = zipWithVec (_∷_) xs (transpose xss)

{-

 [[1,2] , [3,4]]
 
 [[1,3], [2,4]]

-}

-- (1 ∷ 3 ∷ []) ∷ 
-- (2 ∷ 4 ∷ []) ∷ []
test' : Matrix 2 2 ℕ
test' = transpose' test

-- 6. feladat
-- Formalizáljuk : ¬((A ∨ B ⊃ A) ∨ (A ∨ B ⊃ B))
form6 : {A B : Set} → Dec (¬ ((A ⊎ B → A) ⊎ (A ⊎ B → B)))
form6 = inr (λ x → x (inl (λ {(inl a) → a
                            ; (inr b) → exfalso (x (inr (λ _ → b)))})))

-- 7. feladat
form7a : Dec ((A : Set)(P : A → Set)(f : A → A) → ((∀ x → (P x)) → (∀ x → P (f x))))
form7a = inl (λ A P f ∀xPx a → ∀xPx (f a))

-- Keressünk ellenpéldát
-- olyan f ami nem injektív, vagyis ¬ (P (f a) → P a)
form7b : Dec ((A : Set)(P : A → Set)(f : A → A) → ((∀ x → P (f x)) → (∀ x → P x)))
form7b = inr (λ x → x Bool P f (λ x₁ → tt) false)
    where
        f : Bool → Bool
        f _ = true

        P : Bool → Set
        P false = ⊥ 
        P true = ⊤

-- 8. feladat
{-
    Hogy jött ki, mindkét oldalról elkezdtem kibontani a dolgokat aminnyire tudtam, mindig egyszerűsíteni. Lehet hosszú de ha kibontunk mindent ilyen "polinómos" alakra : x * a ^ n + y * a ^ (n-1) + ... + z. akkor biztos kijön, csak lehet sok lépés.  
-}
pr6 : (n : ℕ) → suc (n * (suc (suc n))) ≡ (n + suc zero) ^ (suc (suc zero)) + zero
pr6 n = 
    sym 
    (trans (idr+ ((n + 1) * ((n + 1) * 1))) 
    (trans (cong (λ z → (n + 1) * z) (idr* (n + 1))) 
    (trans (dist+* n 1 (n + 1)) 
    (trans (cong (n * (n + 1) +_) (idr+ (n + 1))) 
    (trans (cong (_+ (n + 1)) (dist*+ n n 1)) 
    (trans (cong (λ z → n * n + z + (n + 1)) (idr* n)) 
    (sym (trans (cong suc (trans (sucr* n (suc n)) (cong (n +_) (sucr* n n)))) 
    (sym (trans (comm+ (n * n + n) (n + 1)) 
    (trans (cong (_+ (n * n + n)) (comm+ n 1)) 
    (cong suc (cong (n +_) (comm+ (n * n) n))))))))))))))
