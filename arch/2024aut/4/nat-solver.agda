open import Data.Nat
open import Relation.Binary.PropositionalEquality
open import Data.Nat.Solver


module nat-solver where

open import Data.Nat.Solver using (module +-*-Solver)
open +-*-Solver using (solve; _:*_; _:+_; con; _:=_)

a+b^2≡a^2+2ab+b^2 : (a b : ℕ) → (a + b) * (a + b) ≡ a * a + 2 * a * b + b * b
a+b^2≡a^2+2ab+b^2 = solve 2 seg refl
    where
        seg = (λ a b → ((a :+ b) :* (a :+ b)) := ((a :* a) :+ (con 2 :* a :* b) :+ (b :* b))) 

-- Lets dig in

-- Írjunk saját solvert ℕ-ra
-- Nat solver a la 1lab
-- https://github.com/the1lab/1lab/blob/main/src/Cat/Solver.lagda.md

import Data.Fin
import Data.Vec

-- Ötlet : Normál formák + NbE
-- Normal forms : 
-- https://en.wikipedia.org/wiki/Normal_form
-- LOL
-- Okés, ötlet : 
-- 3 - 5 + 2 = 0
-- ((-2) + 10 - 2) * 2 - 12 = 0
-- 0 = 0
-- Ötlet : számoljuk ki mindent  

-- Sok külömböző dolog de ugyanaz a "jelentés"
-- Mi van akkor ha : 
-- m + 0 = m
-- m + n - n = m
-- Számoljunk ki mindent kb hasonló alakra

-- NbE : https://en.wikipedia.org/wiki/Normalisation_by_evaluation

-- Mini example
-- (ℕ,0,_+_) -> Monoid
-- 0 egység elem : n + 0 = 0 + n = n
-- + assoc : (a + b) + c = a + (b + c) 

record Mon(A : Set) : Set where
  field
    ε : A
    _∘_ : A → A → A 
    idl∘ : (a : A) → ε ∘ a ≡ a
    idr∘ : (a : A) → a ∘ ε ≡ a
    
    assoc∘ : (a b c : A) → (a ∘ b) ∘ c ≡ a ∘ (b ∘ c)

open import Data.Nat.Properties


ℕMon : Mon ℕ
ℕMon = 
  record 
    { ε = zero 
    ; _∘_ = _+_ 
    ; idl∘ = λ _ → refl 
    ; idr∘ = +-identityʳ
    ; assoc∘ = +-assoc }

-- Monad solver a la 1lab
module NbE1Lab(A : Set)(M : Mon A) where
    module N = Mon M
    open N
    infixl 6 _∘A_
    data NF : Set where
        idA : NF
        _∘A_ : NF → NF → NF
        [_]A : A → NF
        
    -- 0 + 3 + n

    --test' : ℕ → NF
    --test' n = (id ∘ [ 3 ]A) ∘ [ n ]A

    embed : NF → A
    embed idA = ε
    embed [ x ]A = x
    embed (n ∘A m) = embed n ∘ embed m

    eval : NF → (A → A)
    eval idA n = n
    eval [ x ]A n = x ∘ n
    eval (nf ∘A nf₁) n = eval nf (eval nf₁ n)

    nf : NF → A
    nf n = eval n ε

    --t : ℕ
    --t = embed (test' 3)

    
    eval-sound-k : (e : NF) (f : A) → eval e f ≡ (embed e) ∘ f
    eval-sound-k idA f = sym (idl∘ f)
    eval-sound-k (e ∘A e₁) f =
        trans (eval-sound-k e _) 
        (trans (cong (embed e ∘_) (eval-sound-k e₁ _)) (sym (assoc∘ (embed e) (embed e₁) f)))
    eval-sound-k [ x ]A f = refl

    eval-sound : (e : NF) → nf e ≡ embed e
    eval-sound e = trans (eval-sound-k e ε) (idr∘ (embed e))

    solve' : (f g : NF) → nf f ≡ nf g → embed f  ≡ embed g
    solve' f g p = trans (sym (eval-sound f)) (trans p (eval-sound g))

-- Csak (x : ℕ), _+_, 0

--module ℕSolver = NbE1Lab ℕ ℕMon 
--open ℕSolver

--sajat-test : (x y : ℕ) → (x + 1) + 0 + suc 6 + y + y ≡ (x + 8) + (y + y)
--sajat-test x y = 
--  solve' 
--    (([ x ]A ∘A [ 1 ]A) ∘A idA ∘A [ suc 6 ]A ∘A [ y ]A ∘A [ y ]A) 
--    (([ x ]A ∘A [ 8 ]A) ∘A ([ y ]A ∘A [ y ]A)) refl

--test'' : 1 + 1 ≡ 2
--test'' = solve' ([ 1 ]A ∘A [ 1 ]A) [ 2 ]A refl

module NbE(A : Set)(M : Mon A) where
  module N' = Mon M
  open N'
  
  infixl 6 _∘A_
  data Expr : Set where
    idA : Expr
    _∘A_ : Expr → Expr → Expr
    [_]A : A → Expr

  egy-fa : (a : A) → Expr
  egy-fa a = ([ a ]A ∘A [ a ]A) ∘A [ a ]A -- == [ a ]A ∘A ([ a ]A ∘A [ a ]A)

{-
--     ∘       ∘    
--    / \     / \   
--   ∘   a   a   ∘  
--  / \         / \ 
-- a   a       a   a
-}

-- Csináljunk egy típust ami csak a jobboldali vagy baloldali lehet

  NormalF : Set
  NormalF = (Expr → Expr)


  unlift : Expr → A
  unlift idA = ε
  unlift (a ∘A b) = (unlift a) ∘ (unlift b)
  unlift [ x ]A = x

  ⟦_⟧ : Expr → NormalF
  ⟦ idA ⟧ e = e
  ⟦ e ∘A e' ⟧ e'' =  ⟦ e ⟧ (⟦ e' ⟧ e'')
  ⟦ [ x ]A ⟧ e = [ x ]A ∘A e

  reify : NormalF → Expr
  reify f = f idA
  
  nbe : Expr → Expr
  nbe n = reify ⟦ n ⟧

  nbe-sound-k : (e f : Expr) →  unlift (⟦ e ⟧ f) ≡ (unlift e) ∘ unlift f
  nbe-sound-k idA f = sym (idl∘ (unlift f))
  nbe-sound-k (e ∘A e₁) f = 
    -- Ezt áríthatjuk _≡⟨_⟩_-ra
    trans (nbe-sound-k e _) 
    (trans (cong (unlift e ∘_) (nbe-sound-k e₁ f)) (sym (assoc∘ _ _ _)))
  nbe-sound-k [ x ]A _ =  refl

  nbe-sound : (e : Expr) → unlift (nbe e) ≡ unlift e
  nbe-sound e = trans (nbe-sound-k e idA) (idr∘ (unlift e))

  solve' : (e e' : Expr) → unlift (nbe e) ≡ unlift (nbe e') → unlift e ≡ unlift e'
  solve' e e' p = trans (sym (nbe-sound e)) (trans p (nbe-sound e'))

  egy-fa-nf : (a : A) → Expr
  egy-fa-nf a = nbe (egy-fa a)


module ℕSolver = NbE ℕ ℕMon
open ℕSolver

sajat-test : (x y : ℕ) → (x + 1) + 0 + suc 6 + y + y ≡ (x + 8) + (y + y)
sajat-test x y = 
  solve'
    (([ x ]A ∘A [ 1 ]A) ∘A idA ∘A [ suc 6 ]A ∘A [ y ]A ∘A [ y ]A) 
    (([ x ]A ∘A [ 8 ]A) ∘A ([ y ]A ∘A [ y ]A)) refl

module _ where

  ℕMon* : Mon ℕ
  ℕMon* = record 
      { ε = 1 
      ; _∘_ = _*_ 
      ; idl∘ = +-identityʳ 
      ; idr∘ = *-identityʳ 
      ; assoc∘ = *-assoc }

  module ℕSolver* = NbE ℕ ℕMon*
  open ℕSolver*

  test : {!   !}
  test = {!   !}

module NbEList(A : Set)(M : Mon A) where
  module N'' = Mon M
  open N''

  infixl 6 _∘A_
  data ExprL : Set where
    idA : ExprL
    _∘A_ : ExprL → ExprL → ExprL
    [_]A : A → ExprL

  open import Data.List

  NormalFL : Set
  NormalFL = List A

  ⟦_⟧L : ExprL → NormalFL
  ⟦ idA ⟧L = []
  ⟦ a ∘A a₁ ⟧L = ⟦ a ⟧L ++ ⟦ a₁ ⟧L
  ⟦ [ a ]A ⟧L = a ∷ []
  
  reifyL : NormalFL → ExprL
  reifyL [] = idA
  reifyL (x ∷ xs) = [ x ]A ∘A (reifyL xs)
  
  unliftL : ExprL → A
  unliftL idA = ε
  unliftL (e ∘A e₁) = (unliftL e) ∘ (unliftL e₁)
  unliftL [ x ]A = x

  nbeL : ExprL → ExprL
  nbeL n = reifyL ⟦ n ⟧L
  
  nbeL-sound-k : (e e₁ : ExprL) →  unliftL (reifyL (⟦ e ⟧L ++ ⟦ e₁ ⟧L)) ≡ (unliftL e) ∘ (unliftL e₁)
  nbeL-sound : (e : ExprL) → unliftL (nbeL e) ≡ unliftL e
  
  nbeL-resp-∘-++ : (l l₁ : List A) → unliftL (reifyL (l ++ l₁)) ≡ (unliftL (reifyL l)) ∘ (unliftL (reifyL l₁))
  nbeL-resp-∘-++ [] l₁ = sym (idl∘ _)
  nbeL-resp-∘-++ (x ∷ l) l₁ = trans (cong (x ∘_) (nbeL-resp-∘-++ l l₁)) (sym (assoc∘ _ _ _))

  nbeL-sound-k idA e₁ = (trans (nbeL-sound e₁) (sym (idl∘ (unliftL e₁))))
  nbeL-sound-k (e ∘A e₁) e₂ = 
    trans (nbeL-resp-∘-++  (⟦ e ⟧L ++ ⟦ e₁ ⟧L) ⟦ e₂ ⟧L) 
    (trans (cong (_∘ unliftL (reifyL ⟦ e₂ ⟧L)) (nbeL-resp-∘-++ ⟦ e ⟧L ⟦ e₁ ⟧L)) 
    (trans (cong ((unliftL (reifyL ⟦ e ⟧L) ∘ unliftL (reifyL ⟦ e₁ ⟧L)) ∘_) (nbeL-sound e₂)) 
    (cong (_∘ unliftL e₂) (trans (cong (unliftL (reifyL ⟦ e ⟧L) ∘_) (nbeL-sound e₁)) (cong (_∘ unliftL e₁) (nbeL-sound e))))))
  nbeL-sound-k [ x ]A e₁ = (cong (x ∘_) (nbeL-sound e₁)) 
  
  nbeL-sound idA = refl
  nbeL-sound (e ∘A e₁) = nbeL-sound-k  e e₁
  nbeL-sound [ x ]A = idr∘ x
  
  solveL : (e e' : ExprL) → unliftL (nbeL e) ≡ unliftL (nbeL e') → unliftL e ≡ unliftL e'
  solveL e e' p = trans (sym (nbeL-sound e)) (trans p (nbeL-sound e'))

-- Komplikáltabb példa :
-- https://agda.github.io/agda-stdlib/master/Algebra.Solver.Ring.html

-- Modellezzük az n változós polinómokat

data Muv : Set where
  [+] : Muv
  [*] : Muv

--          /-- Hány változónk van
--          V
data Poly (m : ℕ) : Set where
-- Két polinóm szorzata és összege is polinóm
  op   : (o : Muv) (pₗ : Poly m) (pᵣ : Poly m) → Poly m
-- Minden természetesszám polinóm
  con  : (c : ℕ) → Poly m
-- Válzotók : X, Y, Z
  var  : (x : Data.Fin.Fin m) → Poly m
-- Polinóm természetes szám hatványa is polinóm 
  _:^_ : (p : Poly m) (n : ℕ) → Poly m
-- Polinóm negáltja polinóm
  :-_  : (p : Poly m) → Poly m

sem : Muv → (ℕ → ℕ → ℕ)
sem [+] = _+_
sem [*] = _*_

--⟦_⟧ : ∀ {n} → Poly n → Data.Vec.Vec ℕ n → ℕ
--⟦ op o p₁ p₂ ⟧ ρ = sem o (⟦ p₁ ⟧ ρ) (⟦ p₂ ⟧ ρ)
--⟦ con c      ⟧ ρ = c
--⟦ var x      ⟧ ρ = Data.Vec.lookup ρ x
--⟦ p :^ n     ⟧ ρ = ⟦ p ⟧ ρ ^ n
--⟦ :- p       ⟧ ρ = ∣ zero - ( ⟦ p ⟧ ρ) ∣

------------------------------------------------------------------------
-- Normal forms of polynomials

-- A univariate polynomial of degree d,
--
--     p = a_d * x^d + a_{d-1} * x^{d-1} + … + a_0
--
-- is represented in Horner normal form by
--
--     p = ((a_d * x + a_{d-1}) * x + …)x + a_0.
--
-- Note that Horner normal forms can be represented as lists, with the
-- empty list standing for the zero polynomial of degree "-1".
--


mutual

  -- The polynomial representations are indexed by the polynomial's
  -- degree.

  data HNF' : ℕ → Set where
    ∅     : ∀ {n} → HNF' (suc n)
    _*x+_ : ∀ {n} → HNF' (suc n) → Normal' n → HNF' (suc n)

  data Normal' : ℕ → Set where
    con  : ℕ → Normal' zero
    poly : ∀ {n} → HNF' (suc n) → Normal' (suc n)

  -- Note that the data types above do /not/ ensure uniqueness of
  -- normal forms: the zero polynomial of degree one can be
  -- represented using both ∅ and ∅ * x + 0.


mutual

  -- Semantics.

  ⟦_⟧H : ∀ {n} → HNF' (suc n) → Data.Vec.Vec ℕ (suc n) → ℕ
  ⟦ ∅ ⟧H _ = 0
  ⟦ p *x+ c ⟧H (x Data.Vec.∷ ρ) = ⟦ p ⟧H (x Data.Vec.∷ ρ) * x + (⟦ c ⟧N ρ)
  
  ⟦_⟧N : ∀ {n} → Normal' n → Data.Vec.Vec ℕ n → ℕ
  ⟦ con c  ⟧N _ = c
  ⟦ poly p ⟧N ρ = ⟦ p ⟧H ρ

mutual

  -- Equality.

  data _≈H_ : ∀{n} → HNF' n → HNF' n → Set where
    ∅     : ∀{n} → _≈H_ {suc n} ∅ ∅
    _*x+_ : ∀{n} {p₁ p₂ : HNF' (suc n)} {c₁ c₂ : Normal' n} →
            p₁ ≈H p₂ → c₁ ≈N c₂ → (p₁ *x+ c₁) ≈H (p₂ *x+ c₂)

  data _≈N_ : ∀{n} → Normal' n  → Normal' n → Set where
    con  : ∀ {c₁ c₂} → c₁ ≡ c₂ → con c₁ ≈N con c₂
    poly : ∀ {n} {p₁ p₂ : HNF' (suc n)} → p₁ ≈H p₂ → poly p₁ ≈N poly p₂
    
-- Need : Normalisation 