module sort where


open import Lib
open import Lib.Containers.List using (map; length; take; drop; _++_; foldr)
open import Agda.Primitive
open import Lib.Nat.Instances.Ord
--open import Lib.Class.Ord
open import Lib.Class.Eq
open import Lib.Nat using (_div_)
-- Proposition that a list is sorted


Sorted : ∀{l}{A : Set l} → ⦃ Ord A ⦄ → List A → Set
Sorted [] = ⊤ 
Sorted (x ∷ []) = ⊤ 
Sorted (a ∷ b ∷ xs) = (compare a b ≡ LT) × Sorted (b ∷ xs)

_ : Sorted {lzero} {ℕ} (1 ∷ 2 ∷ 3 ∷ [])
_ = refl , refl , tt 

_ : Sorted {lzero} {ℕ} (2 ∷ 1 ∷ []) → ⊥
_ = λ ()

-- Do tree sort here
ls : List ℕ
ls = (3 ∷ 1 ∷ 2 ∷ [])

data Tree(A : Set) : Set where
    Leaf : Tree A
    Node : Tree A → A → Tree A → Tree A

insert : {A : Set} → ⦃ Ord A ⦄ → A → Tree A → Tree A
insert x Leaf = Node Leaf x Leaf
insert x (Node t y s) =
    if (x <= y)
        then Node (insert x t) y s
        else Node t y (insert x s)

flatten : {A : Set} → Tree A → List A
flatten Leaf = []
flatten (Node t x s) = flatten t ++ (x ∷ []) ++ flatten s

-- We insert the list into the tree, then we flatten it
treesort : {A : Set} → ⦃ Ord A ⦄ → List A → List A
treesort = flatten ∘ (foldr insert Leaf)

test : {A : Set} → ⦃ Ord A ⦄ → List A → Tree A
test = foldr insert Leaf

testList : List ℕ
testList = 2 ∷ 3 ∷ 4 ∷ 10 ∷ []

_ : Sorted {lzero} {ℕ} (treesort ls)
_ = refl , refl , tt

{-

    Proof outline:
        - We prove that inside the tree, the left subtree is always <= then the right

-}

-- TODO
treesortinv : {A : Set} → ⦃ i : Ord A ⦄ → (xs : List A) → (Sorted {lzero} {A} ⦃ i ⦄ (treesort xs))
treesortinv []           = tt
treesortinv (x ∷ [])     = tt
treesortinv (x ∷ y ∷ xs) = {!   !}

-- prove that 
-- prf : Sorted (heapsort xs )

-- (A → ℕ) → Ord A

-- Maybe its easier to just ignore the Eq, if u have _<=_ u already have _==_ 

--record Ord {i}(A : Set i) : Set (lsuc i) where
--  constructor OrdInstance
--  field
--    compare : A → A → Ordering
--    -- compare is refl; trans; and anti-sym
--    flippable : {x y : A} → compare x y ≡ LT ↔ compare y x ≡ GT
--    equality : {x : A} → compare x x ≡ EQ 
--open Ord {{...}} public



rankingToEq : ∀{i}{A : Set i} → (A → ℕ) → Eq A
(rankingToEq f Eq.≡ᵗ a) b = {!    !}
Eq.eqIsJust (rankingToEq f) = {!   !}

rankingToOrd : ∀{i}{A : Set i} → {{Eq A}} → (A → ℕ) → Ord A
Ord.eq (rankingToOrd {{a}} f) = a
Ord.compare (rankingToOrd f) = λ a b → if (a == b) then EQ else (compare (f a) (f b)) -- λ a₁ a₂ → compare (f a₁) (f a₂)
Ord.flippable (rankingToOrd f) {x} {y} = {!   !} -- flippableℕ {f x} {f y}
Ord.equality (rankingToOrd f) {x} = {!   !} -- equalityℕ {f x}   
Ord.consistencyWithEq (rankingToOrd f) {x} {y} = {!   !} -- consistencyWithEqℕ {f x} {f y} 