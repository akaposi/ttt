module absurdlambda where

open import Lib

task1 : Σ ℕ λ n → (k : ℕ) → (k <ℕ n) → Fin n → Fin k
task1 = 0 , λ k ()

task1b : ¬ (Σ ℕ λ n → (k : ℕ) → (k <ℕ suc n) → Fin (suc n) → Fin k)
task1b (n , hyp) = the (Fin 0 → ⊥) (λ ()) (hyp 0 tt fzero)
{-
task1b (n , hyp) = noFin0 fin0
  where
  fin0 : Fin 0
  fin0 = hyp 0 tt fzero
  noFin0 : Fin 0 → ⊥
  noFin0 ()         -- the (Fin 0 → ⊥) (λ ())
-}
