open import Lib

t : ℕ × ℕ
t = 1 , 2

data fr (A B : Set) : Set where
  f1 : A → B → fr A B
  f2 : A → fr A B
  f3 : B → fr A B
  f4 : fr A B → fr A B → fr A B

iteFr : {A B C : Set} → (A → B → C) → (A → C) → (B → C) → (C → C → C) → fr A B → C
iteFr f g h j (f1 x y) = f x y
iteFr f g h j (f2 x)   = g x
iteFr f g h j (f3 y)   = h y
iteFr f g h j (f4 x y) = j (iteFr f g h j x) (iteFr f g h j y)

recFr : {A B C : Set} → (A → B → C) → (A → C) → (B → C) → (fr A B → fr A B → C → C → C) → fr A B → C
recFr f g h j (f1 x y) = f x y
recFr f g h j (f2 x)   = g x
recFr f g h j (f3 y)   = h y
recFr f g h j (f4 x y) = j x y (recFr f g h j x) (recFr f g h j y)

{-
λ {true → true; false → true}
λ {true → true; false → false}
λ {true → false; false → true}
λ {true → false; false → false}

true , true
true , false
false , true
false , false
-}

bij6 : (Bool → Bool) ↔ (Bool × Bool)
fst bij6 f     = f true , f false
snd bij6 bxb   = λ {true → fst bxb; false → snd bxb}
{-
snd bij6 bxb true = fst bxb
snd bij6 bxb false = snd bxb
-}

bij6gen : {A : Set} → (Bool → A) ↔ (A × A)
fst bij6gen f     = f true , f false
snd bij6gen bxb   = λ {true → fst bxb; false → snd bxb}

{-
not strictly positive
record CoWeird : Set where
  coinductive
  field
    inside : CoWeird → ⊥
-}

data Y : Set where
  --     1     2    1       3    2     2     1      0
  cony : Y → (⊤ → Y) → ((Y → ⊤) → Y → Bool) → Y
-- positive: 0 és a páratlanok
-- strictly positive: csak 0 és 1

btob : ⊥ → ⊥
btob ()
-- btob bot = bot
-- de igazából ugyanaz a kettő, mert mindkettő nem rendel semmihez semmit

oneelement : (⊥ → ⊥) × ⊤
--oneelement = (λ {()}) , tt
oneelement = (λ bot → bot) , tt

record coindTree (A : Set) : Set where
  coinductive
  field
    left : Maybe (coindTree A)
    value : A
    right : Maybe (coindTree A)

coitTree : {C A : Set} → (C → Maybe C) → (C → A) → (C → Maybe C) → C → coindTree A
coindTree.left (coitTree l v r s) with l s
... | nothing = nothing
... | just ls = just (coitTree l v r ls)
coindTree.value (coitTree l v r s) = v s
coindTree.right (coitTree l v r s) with r s
... | nothing = nothing
... | just rs = just (coitTree l v r rs)

