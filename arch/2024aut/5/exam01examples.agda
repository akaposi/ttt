module exam01examples where

open import Lib

nonex : {A : Set} → A → ⊥
nonex a = {!!}
-- ha ennek lenne eleme:
b : ⊥
b = nonex 1

{-
-- reminder for the list:
data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : {A B : Set} → B → (A → B → B) → List A → B
iteList b f [] = b
iteList b f (a ∷ as) = f a (iteList b f as)
-}

-- β-rules:
-- actually, what we have written up there

data X (A : Set) : Set where
  X1 : A → X A → X A
  X2 : ℕ → X A

iteX : {A B : Set} → (A → B → B) → (ℕ → B) → X A → B
iteX f g (X1 a x) = f a (iteX f g x)
iteX f g (X2 n) = g n

iteNat : {A : Set} → A → (A → A) → ℕ → A
iteNat z s zero = z
iteNat z s (suc n) = s (iteNat z s n)

data TriEither (A B C : Set) : Set where
  left : A → TriEither A B C
  middle : B → TriEither A B C
  right : C → TriEither A B C

iteTriEither : {A B C S : Set} → (A → S) → (B → S) → (C → S) → TriEither A B C → S
iteTriEither f g h (left a) = f a
iteTriEither f g h (middle b) = g b
iteTriEither f g h (right c) = h c

{-
ismerjük:
  x² + 1 =: f x
  x
kéne:
  f (x + 1) = (x + 1)² + 1 =
  = x² + 2x + 1 + 1 = (x² + 1) + 2x + 1 = f x + 2x + 1

f(0) = 1
f(n+1) = f(n) + 2n + 1

f(n) = n² + 1
indukciós feltevés: tfh. n-re már bizonyítottuk (f(n) = n² + 1)
és ebből f(n+1)-re belátni
-}

concat : {A : Set} → List (List A) → List A
concat [] = []
concat ([] ∷ ls₁) = concat ls₁
concat ((x ∷ ls) ∷ ls₁) = x ∷ (concat (ls ∷ ls₁))

intersperse : {A : Set} → A → Stream A → Stream A
head (intersperse x xs) = head xs
head (tail (intersperse x xs)) = x
tail (tail (intersperse x xs)) = intersperse x (tail xs)
{-
head (intersperse x xs) = head xs
tail (intersperse x xs) = x ∷ intersperse x (tail xs)
-}

fn : ℕ → ℕ
fn zero = 2
fn (suc n) = 3 + fn n

fn' : ℕ → ℕ
fn' = iteNat 2 (3 +_)

fnproof : (n : ℕ) → fn n ≡ fn' n
fnproof zero = refl
fnproof (suc n) = cong (3 +_) (fnproof n)

-- még ami a régebbi mintafájlban volt:
-- A legyen (ℕ × ℕ)
oldfn : ℕ → ℕ
-- oldfn zero = 2
-- oldfn (suc n) = 2 * n + oldfn n + 1
oldfn n = fst (iteNat (2 , 0) (λ (rec , i) → (2 * i + rec + 1 , suc i)) n)
