module gy02 where

open import Lib hiding (comm×; assoc×; flip; curry; uncurry)

-- α-konverzió: renaming
-- ez az, hogy a paramétert a λ-n belül szabadon átnevezheted
id= : ∀{i}{A : Set i} → (λ (x : A) → x) ≡ (λ y → y)
id= = refl
-- miért jó? hogy ne ütközzenek a nevek
-- pl. λ x → (λ y → (λ x → ...)) esetén abban a ...-ben a külső x-et nem tudom használni

-- β-redukció: substitution
-- ami volt is
-- pl. (λ x → x + 2 * x) 5 = (x + 2 * x)[x := 5] = 5 + 2 * 5

-- η-redukció: simplification
-- az, hogy a λ x → f x igazából simán f-re átírható;
-- _feltéve_, hogy az f nem függ x-től
η= : ∀{i j}{A : Set i}{B : Set j}(f : A → B) → (λ x → f x) ≡ f
η= _ = refl

------------------------------------------------------
-- simple finite types
------------------------------------------------------

-- _×_ : tuple (_,_ a konstruktora, fst és snd a destruktorai)
-- pl.
tuple1 : ℕ × Bool
tuple1 = 5 , true
num1 : ℕ
num1 = fst tuple1
bool1 : Bool
bool1 = snd tuple1

-- _⊎_ : union/either (inl és inr a konstruktorai)
-- pl.
either1 : ℕ ⊎ Bool
either1 = inl 5
-- ennek a felbontása:
destructeither : ℕ ⊎ Bool → String
destructeither (inl a) = "valami szám"
destructeither (inr b) = if b then "igaz" else "hamis"
-- vagy:
destructeither' : ℕ ⊎ Bool → String
destructeither' avb = case avb {!!} {!!}

-- _→_ : függvény
-- kinda a λ a konstruktora (kap egy paraméternevet meg egy kifejezést)
-- és a függvényalkalmazás a destruktora
fun1 : ℕ → ℕ
fun1 = λ n → 2 * n
-- destruálva:
destfun : ℕ
destfun = fun1 5

-- _↔_ : simán egy rövidítés
-- A ↔ B = (A → B) × (B → A)

-- Bool : true és false

-- ⊤ (\top) : az egyelemű típus (tt az egyetlen eleme)
top1 : ⊤
top1 = tt

-- ⊥ (\bot, bottom) : az üres típus
b : ⊥
b = {!!}
-- de van egy érdekes szabály:
exfalso' : ∀ {i}{A : Set i} → ⊥ → A
exfalso' b = {!!}

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flip : ℕ × Bool → Bool × ℕ
flip = {!!}

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flipback : Bool × ℕ → ℕ × Bool
flipback = {!!}

-- Vegyük észre, hogy az előző két függvényben bármilyen más csúnya dolgot is lehetne csinálni.
-- Írj rá példát itt!



-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
comm× : {A B : Set} → A × B → B × A
comm× = {!!}

comm×back : {A B : Set} → B × A → A × B
comm×back = {!!}
-- Ezekben lehetetlen hülyeséget csinálni.
-- Hányféleképpen lehetséges implementálni ezt a két fenti függvényt?

-- ALGEBRAI ADATTÍPUSOK ELEMSZÁMAI:

b1 b2 : Bool × ⊤
b1 = {!!}
b2 = {!!}
-- ez azt ellenőrzi, hogy tényleg két különbözőről van szó
b1≠b2 : b1 ≡ b2 → ⊥
b1≠b2 ()

t1 t2 : ⊤ ⊎ ⊤
t1 = {!!}
t2 = {!!}
t1≠t2 : t1 ≡ t2 → ⊥
t1≠t2 ()

bb1 bb2 bb3 : Bool ⊎ ⊤
bb1 = {!!}
bb2 = {!!}
bb3 = {!!}
bb1≠bb2 : bb1 ≡ bb2 → ⊥
bb1≠bb2 ()
bb1≠bb3 : bb1 ≡ bb3 → ⊥
bb1≠bb3 ()
bb2≠bb3 : bb2 ≡ bb3 → ⊥
bb2≠bb3 ()

ee : (⊤ → ⊥) ⊎ (⊥ → ⊤)
ee = {!!}

d : (⊤ ⊎ (⊤ × ⊥)) × (⊤ ⊎ ⊥)
d = {!!}
-- Ezek alapján hogy lehet megállapítani, hogy melyik típus hány elemű?
-- | ⊤ | =
-- | ⊥ | =
-- | Bool | =
-- | Bool ⊎ ⊤ | =
-- | A ⊎ B | =
-- | A × B | =
-- | Bool × Bool × Bool | =
-- | ⊤ → ⊥ | =
-- | ⊥ → ⊤ | =
-- | ⊥ → ⊥ | =
-- | Bool → ⊥ | =
-- | Bool → ⊤ | =
-- | ⊤ → Bool | =
-- | A → B | =
-- | Bool → Bool → Bool | =


-- Ezek alapján milyen matematikai állítást mond ki és bizonyít a lenti állítás?
-- Válasz:
from' : {A : Set} → A × A → (Bool → A)
from' = {!!}
to' : {A : Set} → (Bool → A) → A × A
to' = λ f → f true , f false
testfromto1 : {A : Set}{a b : A} → fst (to' (from' (a , b))) ≡ a
testfromto1 = refl
testfromto2 : {A : Set}{a b : A} → snd (to' (from' (a , b))) ≡ b
testfromto2 = refl
testfromto3 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) true ≡ a
testfromto3 = refl
testfromto4 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) false ≡ b
testfromto4 = refl

-- röviden:
iso0 : {A : Set} → A × A ↔ (Bool → A)
iso0 = from' , to'

-- de vigyázz: egy _↔_ nem feltétlen bijekció!
-- pl.
badiso : ⊤ ↔ Bool
fst badiso = λ _ → true
snd badiso = λ _ → tt

------------------------------------------------------
-- all algebraic laws systematically
------------------------------------------------------

-- (⊎, ⊥) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc⊎ : {A B C : Set} → (A ⊎ B) ⊎ C ↔ A ⊎ (B ⊎ C)
assoc⊎ = {!!}

idl⊎ : {A : Set} → ⊥ ⊎ A ↔ A
idl⊎ = {!!}

idr⊎ : {A : Set} → A ⊎ ⊥ ↔ A
idr⊎ = {!!}

comm⊎ : {A B : Set} → A ⊎ B ↔ B ⊎ A
comm⊎ = {!!}

-- (×, ⊤) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc× : {A B C : Set} → (A × B) × C ↔ A × (B × C)
assoc× = {!!}

idl× : {A : Set} → ⊤ × A ↔ A
idl× = {!!}

idr× : {A : Set} → A × ⊤ ↔ A
idr× = {!!}

-- ⊥ is a null element

null× : {A : Set} → A × ⊥ ↔ ⊥
null× = {!!}

-- distributivity of × and ⊎

dist : {A B C : Set} → A × (B ⊎ C) ↔ (A × B) ⊎ (A × C)
dist = {!!}

-- exponentiation laws

curry : ∀{A B C : Set} → (A × B → C) ↔ (A → B → C)
curry = {!!}

⊎×→ : {A B C D : Set} → ((A ⊎ B) → C) ↔ (A → C) × (B → C)
⊎×→ = {!!}

law^0 : {A : Set} → (⊥ → A) ↔ ⊤
law^0 = {!!}

law^1 : {A : Set} → (⊤ → A) ↔ A
law^1 = {!!}

law1^ : {A : Set} → (A → ⊤) ↔ ⊤
law1^ = {!!}

---------------------------------------------------------
-- random isomorphisms
------------------------------------------------------

-- Milyen algebrai állítást mond ki az alábbi típus?
iso1 : {A B : Set} → (Bool → (A ⊎ B)) ↔ ((Bool → A) ⊎ Bool × A × B ⊎ (Bool → B))
iso1 = {!!}

iso2 : {A B : Set} → ((A ⊎ B) → ⊥) ↔ ((A → ⊥) × (B → ⊥))
iso2 = {!!}

iso3 : (⊤ ⊎ ⊤ ⊎ ⊤) ↔ Bool ⊎ ⊤
iso3 = {!!}
testiso3 : fst iso3 (inl tt) ≡ fst iso3 (inr (inl tt)) → ⊥
testiso3 ()
testiso3' : fst iso3 (inl tt) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3' ()
testiso3'' : fst iso3 (inr (inl tt)) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3'' ()

iso4 : (⊤ → ⊤ ⊎ ⊥ ⊎ ⊤) ↔ (⊤ ⊎ ⊤)
iso4 = {!!} , {!!}
testiso4 : fst iso4 (λ _ → inl tt) ≡ fst iso4 (λ _ → inr (inr tt)) → ⊥
testiso4 ()
testiso4' : snd iso4 (inl tt) tt ≡ snd iso4 (inr tt) tt → ⊥
testiso4' ()
