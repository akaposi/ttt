module gy02sol where

open import Lib hiding (comm×; assoc×; flip; curry; uncurry)

-- α-konverzió: renaming
-- ez az, hogy a paramétert a λ-n belül szabadon átnevezheted
-- (λ x → x) ≡ (λ y → y)
id= : ∀{i}{A : Set i} → (λ (x : A) → x) ≡ (λ y → y)
id= = refl
-- miért jó? hogy ne ütközzenek a nevek
-- pl. λ x → (λ y → (λ x → ...)) esetén abban a ...-ben a külső x-et nem tudom használni

-- β-redukció: substitution
-- ami volt is
-- pl. (λ x → x + 2 * x) 5 = (x + 2 * x)[x := 5] = 5 + 2 * 5
β= : ∀{i j}{A : Set i}{B : Set j}(f : A → B)(y : A) → (λ x → f x) y ≡ f y
β= _ _ = refl

-- η-redukció: simplification
-- az, hogy a λ x → f x igazából simán f-re átírható;
-- _feltéve_, hogy az f nem függ x-től
η= : ∀{i j}{A : Set i}{B : Set j}(f : A → B) → (λ x → f x) ≡ f
η= _ = refl

{-
add3 x = (3 +_) x
add3 = (3 +_)

add3 = λ x -> (3 +_) x
add3 = (3 +_)
-}

-- ha az f függ x-től, akkor nem
-- pl. (λ x -> (λ y -> x + y) x)
--   = (λ x -> (x + y)[y := x])
--   = (λ x -> x + x)

------------------------------------------------------
-- simple finite types
------------------------------------------------------

-- _×_ : tuple (_,_ a konstruktora, fst és snd a destruktorai)
-- pl.
tuple1 : ℕ × Bool       -- Haskell: (Natural,Bool)
tuple1 = 5 , true        -- Haskell: (5,true)
num1 : ℕ
num1 = fst tuple1
bool1 : Bool
bool1 = snd tuple1

-- _⊎_ : union/either (inl és inr a konstruktorai)
-- pl.
either1 : ℕ ⊎ Bool             -- Haskell: Either Natural Bool
either1 = inr false             -- Haskell: Right False
-- ennek a felbontása:
destructeither : ℕ ⊎ Bool → String
destructeither (inl n) = "ez egy szám"
destructeither (inr false) = "hamis"
destructeither (inr true) = "igaz"

-- vagy:
destructeither' : ℕ ⊎ Bool → String
destructeither' nvb = case nvb
                           (λ n -> "ez egy szám")
                           (λ {true -> "igaz"; false -> "hamis"})

-- _→_ : függvény
-- kinda a λ a konstruktora (kap egy paraméternevet meg egy kifejezést)
-- és a függvényalkalmazás a destruktora
fun1 : ℕ → ℕ
fun1 = λ n → 2 * n
-- destruálva:
destfun : ℕ
destfun = fun1 5

-- \lr
-- _↔_ : simán egy rövidítés
-- A ↔ B = (A → B) × (B → A)

-- Bool : true és false

-- ⊤ (\top) : az egyelemű típus (tt az egyetlen eleme)
top1 : ⊤
top1 = tt

-- ⊥ (\bot, bottom) : az üres típus
b : ⊥
b = {!!}
-- de van egy érdekes szabály:
exfalso' : ∀ {i}{A : Set i} → ⊥ → A
exfalso' ()

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flip : ℕ × Bool → Bool × ℕ
flip (n , b) = b , n

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flipback : Bool × ℕ → ℕ × Bool
flipback = {!!}

-- Vegyük észre, hogy az előző két függvényben bármilyen más csúnya dolgot is lehetne csinálni.
-- Írj rá példát itt!

badflip : ℕ × Bool → Bool × ℕ
badflip (n , b) = true , 42


-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
comm× : {A B : Set} → A × B → B × A
comm× (a , b) = b , a

comm×back : {A B : Set} → B × A → A × B
comm×back = comm×
-- Ezekben lehetetlen hülyeséget csinálni.
-- Hányféleképpen lehetséges implementálni ezt a két fenti függvényt?

-- ALGEBRAI ADATTÍPUSOK ELEMSZÁMAI:

b1 b2 : Bool × ⊤
b1 = true , tt
b2 = false , tt
-- ez azt ellenőrzi, hogy tényleg két különbözőről van szó
b1≠b2 : b1 ≡ b2 → ⊥
b1≠b2 ()

t1 t2 : ⊤ ⊎ ⊤
t1 = inl tt
t2 = inr tt    -- nem ugyanaz!
t1≠t2 : t1 ≡ t2 → ⊥
t1≠t2 ()

bb1 bb2 bb3 : Bool ⊎ ⊤
bb1 = inl true
bb2 = inl false
bb3 = inr tt
bb1≠bb2 : bb1 ≡ bb2 → ⊥
bb1≠bb2 ()
bb1≠bb3 : bb1 ≡ bb3 → ⊥
bb1≠bb3 ()
bb2≠bb3 : bb2 ≡ bb3 → ⊥
bb2≠bb3 ()

ee : (⊤ → ⊥) ⊎ (⊥ → ⊤)
ee = {!!}

{-
-- kis unrelated ökörködés
_←_ : Set -> Set -> Set
B ← A = A → B

wh : ⊤ ← Bool
wh false = tt
wh true = tt
-}

--  (1  + (1  * 0)) * (1 + 0)
d : (⊤ ⊎ (⊤ × ⊥)) × (⊤ ⊎ ⊥)
d = inl tt , inl tt
-- Ezek alapján hogy lehet megállapítani, hogy melyik típus hány elemű?
-- | ⊤ | =
-- | ⊥ | =
-- | Bool | =
-- | Bool ⊎ ⊤ | =
-- | A ⊎ B | =
-- | A × B | =
-- | Bool × Bool × Bool | =
-- | ⊤ → ⊥ | =
-- | ⊥ → ⊤ | =
-- | ⊥ → ⊥ | =
-- | Bool → ⊥ | = 0 ^ 2
-- | Bool → ⊤ | = 1 ^ 2
-- | ⊤ → Bool | = 2 ^ 1
-- | A → B | =
-- | Bool → Bool → Bool | =

{-
miért hatványozás?

alma körte barack : Gyümölcs
true false: Bool

f1 : Gyümölcs → Bool
f1 alma = false
f1 körte = false
f1 barack = false

f2 : Gyümölcs → Bool
f2 alma = true
f2 körte = false
f2 barack = false

f3 : Gyümölcs → Bool
f3 alma = false
f3 körte = true
f3 barack = false

...

-}

-- Ezek alapján milyen matematikai állítást mond ki és bizonyít a lenti állítás?
-- Válasz:
{-
alma , alma
alma , körte
alma , barack
körte , alma
...

λ {true -> alma; false -> alma}
λ {true -> alma; false -> körte}
λ {true -> alma; false -> barack}
λ {true -> körte; false -> alma}
-}
from' : {A : Set} → A × A → (Bool → A)
from' (a1 , a2) = λ {true -> a1; false -> a2}
to' : {A : Set} → (Bool → A) → A × A
to' f = f true , f false
testfromto1 : {A : Set}{a b : A} → fst (to' (from' (a , b))) ≡ a
testfromto1 = refl
testfromto2 : {A : Set}{a b : A} → snd (to' (from' (a , b))) ≡ b
testfromto2 = refl
testfromto3 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) true ≡ a
testfromto3 = refl
testfromto4 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) false ≡ b
testfromto4 = refl

-- röviden:
-- A × A ↔ (Bool → A) ≡ (A × A → (Bool → A)) × ((Bool → A) → A × A)
iso0 : {A : Set} → A × A ↔ (Bool → A)
fst iso0 = from'
snd iso0 = to'

-- de vigyázz: egy _↔_ nem feltétlen bijekció!
-- pl.
badiso : ⊤ ↔ Bool
fst badiso = λ _ → true
snd badiso = λ _ → tt

------------------------------------------------------
-- all algebraic laws systematically
------------------------------------------------------

-- (⊎, ⊥) form a commutative monoid (kommutativ egysegelemes felcsoport)

                      --  (|A| + |B|) + |C| = |A| + (|B| + |C|)
assoc⊎ : {A B C : Set} → (A ⊎ B) ⊎ C ↔ A ⊎ (B ⊎ C)
fst assoc⊎ (inl (inl a)) = inl a
fst assoc⊎ (inl (inr b)) = inr (inl b)
fst assoc⊎ (inr c) = inr (inr c)
snd assoc⊎ (inl a) = (inl (inl a))
snd assoc⊎ (inr (inl b)) = (inl (inr b))
snd assoc⊎ (inr (inr c)) = (inr c)
{-
fst assoc⊎ = λ {(inl (inl a)) -> inl a; (inl (inr b)) -> inr (inl b); (inr c) -> inr (inr c)}
snd assoc⊎ = {!!}
-}

idl⊎ : {A : Set} → ⊥ ⊎ A ↔ A
-- fst idl⊎ (inl ())
fst idl⊎ (inr a) = a
snd idl⊎ a = inr a

idr⊎ : {A : Set} → A ⊎ ⊥ ↔ A
idr⊎ = {!!}

comm⊎ : {A B : Set} → A ⊎ B ↔ B ⊎ A
fst comm⊎ (inl a) = inr a
fst comm⊎ (inr b) = inl b
snd comm⊎ = {!!}

-- (×, ⊤) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc× : {A B C : Set} → (A × B) × C ↔ A × (B × C)
fst assoc× ((a , b) , c) = a , (b , c)
snd assoc× = {!!}

idl× : {A : Set} → ⊤ × A ↔ A
fst idl× (_ , a) = a
snd idl× a = tt , a

idr× : {A : Set} → A × ⊤ ↔ A
idr× = {!!}

-- ⊥ is a null element

null× : {A : Set} → A × ⊥ ↔ ⊥
fst null× ()
snd null× ()

-- distributivity of × and ⊎

dist : {A B C : Set} → A × (B ⊎ C) ↔ (A × B) ⊎ (A × C)
fst dist (a , inl b) = inl (a , b)
fst dist (a , inr c) = inr (a , c)
snd dist (inl (a , b)) = a , inl b
snd dist (inr (a , c)) = a , inr c

-- exponentiation laws

curry : ∀{A B C : Set} → (A × B → C) ↔ (A → B → C)
curry = {!!}

--                         |C| ^ (|A| + |B|) = (|C| ^ |A|) * (|C| ^ |B|)

⊎×→ : {A B C D : Set} → ((A ⊎ B) → C) ↔ (A → C) × (B → C)
fst ⊎×→ f = (λ a -> f (inl a)) , λ b -> f (inr b)
snd ⊎×→ (g , h) = λ {(inl a) → g a; (inr b) → h b}

law^0 : {A : Set} → (⊥ → A) ↔ ⊤
law^0 = {!!}

law^1 : {A : Set} → (⊤ → A) ↔ A
law^1 = {!!}

law1^ : {A : Set} → (A → ⊤) ↔ ⊤
law1^ = {!!}

---------------------------------------------------------
-- random isomorphisms
------------------------------------------------------

-- Milyen algebrai állítást mond ki az alábbi típus?
{-
példa:
a1 a2 : A
b1 : B

λ {true -> inl a1; false -> inl a1}
λ {true -> inl a1; false -> inl a2}
λ {true -> inl a2; false -> inl a1}
λ {true -> inl a2; false -> inl a2}
λ {true -> inl a1; false -> inr b1}
λ {true -> inl a2; false -> inr b1}
λ {true -> inr b1; false -> inl a1}
λ {true -> inr b1; false -> inl a2}
λ {true -> inr b1; false -> inr b1}

inl (λ {true -> a1; false -> a1})
inl (λ {true -> a1; false -> a2})
...
inr (inl (true , a1 , b1))
inr (inl (true , a2 , b1))
inr (inl (false , a1 , b1))
inr (inl (false , a2 , b1))
inr (inr (λ {true -> b1; false -> b1}))

λ {true -> inl a; false -> inl a})   ↔      inl (λ {true -> a; false -> a})
λ {true -> inl a; false -> inr b})   ↔ inr (inl (true  , a , b))
λ {true -> inr b; false -> inl a})   ↔ inr (inl (false , a , b))
λ {true -> inr b; false -> inr b})   ↔ inr (inr (λ {true -> b; false -> b})

-}

{-
λ {(inl a) → ?; (inr b) -> ?}   avb     ≡
case avb (λ a -> ?) (λ b -> ?)
-}

iso1 : {A B : Set} →
  (Bool → (A ⊎ B)) ↔ ((Bool → A) ⊎ Bool × A × B ⊎ (Bool → B))
fst iso1 f = case (f true)
               (λ a -> case (f false)
                         (λ a2 -> inl λ {true -> a; false -> a2})
                          λ b -> inr (inl (true , a , b)))
               λ b -> case (f false)
                         (λ a -> inr (inl (false , a , b)))
                         λ b2 -> inr (inr λ {true -> b; false -> b2})
snd iso1 (inl f) = λ {true -> inl (f true); false -> inl (f false)}
snd iso1 (inr (inl (false , a , b))) = λ {true -> inr b; false -> inl a}
snd iso1 (inr (inl (true , a , b))) = λ {true -> inl a; false -> inr b}
snd iso1 (inr (inr g)) = λ {true -> inr (g true); false -> inr (g false)}


iso2 : {A B : Set} → ((A ⊎ B) → ⊥) ↔ ((A → ⊥) × (B → ⊥))
fst iso2 f = (λ a -> f (inl a)) , λ b -> f (inr b)
snd iso2 (g , h) (inl a) = g a
snd iso2 (g , h) (inr b) = h b

{-
inl tt
inr (inl tt)
inr (inr tt)

inl true
inl false
inr tt

-}
iso3 : (⊤ ⊎ ⊤ ⊎ ⊤) ↔ Bool ⊎ ⊤
fst iso3 (inl tt) = inl true
fst iso3 (inr (inl tt)) = inl false
fst iso3 (inr (inr tt)) = inr tt
snd iso3 (inl false) = inr (inl tt)
snd iso3 (inl true) = inl tt
snd iso3 (inr tt) = inr (inr tt)
testiso3 : fst iso3 (inl tt) ≡ fst iso3 (inr (inl tt)) → ⊥
testiso3 ()
testiso3' : fst iso3 (inl tt) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3' ()
testiso3'' : fst iso3 (inr (inl tt)) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3'' ()

{-
λ {tt -> inl tt}
λ {tt -> inr (inr tt)}

inl tt
inr tt
-}

iso4 : (⊤ → ⊤ ⊎ ⊥ ⊎ ⊤) ↔ (⊤ ⊎ ⊤)
fst iso4 f = case (f tt) (λ _ -> inl tt) (λ _ -> inr tt)
snd iso4 (inl a) = λ _ -> inl tt
snd iso4 (inr b₁) = λ _ -> inr (inr tt)
testiso4 : fst iso4 (λ _ → inl tt) ≡ fst iso4 (λ _ → inr (inr tt)) → ⊥
testiso4 ()
testiso4' : snd iso4 (inl tt) tt ≡ snd iso4 (inr tt) tt → ⊥
testiso4' ()
