module gy02solfull where

open import Lib hiding (comm×; assoc×; flip; curry; uncurry)

-- α-konverzió: renaming
-- ez az, hogy a paramétert a λ-n belül szabadon átnevezheted
id= : ∀{i}{A : Set i} → (λ (x : A) → x) ≡ (λ y → y)
id= = refl
-- miért jó? hogy ne ütközzenek a nevek
-- pl. λ x → (λ y → (λ x → ...)) esetén abban a ...-ben a külső x-et nem tudom használni

-- β-redukció: substitution
-- ami volt is
-- pl. (λ x → x + 2 * x) 5 = (x + 2 * x)[x := 5] = 5 + 2 * 5

-- η-redukció: simplification
-- az, hogy a λ x → f x igazából simán f-re átírható;
-- _feltéve_, hogy az f nem függ x-től
η= : ∀{i j}{A : Set i}{B : Set j}(f : A → B) → (λ x → f x) ≡ f
η= _ = refl

{-
add3 x = (3 +_) x
add3 = (3 +_)

add3 = λ x -> (3 +_) x
add3 = (3 +_)
-}

-- ha az f függ x-től, akkor nem
-- pl. (λ x -> (λ y -> x + y) x)
--   = (λ x -> (x + y)[y := x])
--   = (λ x -> x + x)

------------------------------------------------------
-- simple finite types
------------------------------------------------------

-- _×_ : tuple (_,_ a konstruktora, fst és snd a destruktorai)
-- pl.
tuple1 : ℕ × Bool       -- Haskell: (Natural,Bool)
tuple1 = 5 , true        -- Haskell: (5,true)
num1 : ℕ
num1 = fst tuple1
bool1 : Bool
bool1 = snd tuple1

-- _⊎_ : union/either (inl és inr a konstruktorai)
-- pl.
either1 : ℕ ⊎ Bool             -- Haskell: Either Natural Bool
either1 = inr false             -- Haskell: Right False
-- ennek a felbontása:
destructeither : ℕ ⊎ Bool → String
destructeither (inl n) = "ez egy szám"
destructeither (inr false) = "hamis"
destructeither (inr true) = "igaz"

-- vagy:
destructeither' : ℕ ⊎ Bool → String
destructeither' nvb = case nvb
                           (λ n -> "ez egy szám")
                           (λ {true -> "igaz"; false -> "hamis"})

-- _→_ : függvény
-- kinda a λ a konstruktora (kap egy paraméternevet meg egy kifejezést)
-- és a függvényalkalmazás a destruktora
fun1 : ℕ → ℕ
fun1 = λ n → 2 * n
-- destruálva:
destfun : ℕ
destfun = fun1 5

-- \lr
-- _↔_ : simán egy rövidítés
-- A ↔ B = (A → B) × (B → A)

-- Bool : true és false

-- ⊤ (\top) : az egyelemű típus (tt az egyetlen eleme)
top1 : ⊤
top1 = tt

-- ⊥ (\bot, bottom) : az üres típus
b : ⊥
b = {!!}
-- de van egy érdekes szabály:
exfalso' : ∀ {i}{A : Set i} → ⊥ → A
exfalso' ()

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flip : ℕ × Bool → Bool × ℕ
flip (n , b) = b , n

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flipback : Bool × ℕ → ℕ × Bool
flipback (b , n) = n , b

-- Vegyük észre, hogy az előző két függvényben bármilyen más csúnya dolgot is lehetne csinálni.
-- Írj rá példát itt!

badflip : ℕ × Bool → Bool × ℕ
badflip (n , b) = true , 42


-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
comm× : {A B : Set} → A × B → B × A
comm× (a , b) = b , a

comm×back : {A B : Set} → B × A → A × B
comm×back = comm×
-- Ezekben lehetetlen hülyeséget csinálni.
-- Hányféleképpen lehetséges implementálni ezt a két fenti függvényt?

-- ALGEBRAI ADATTÍPUSOK ELEMSZÁMAI:

b1 b2 : Bool × ⊤
b1 = true , tt
b2 = false , tt
-- ez azt ellenőrzi, hogy tényleg két különbözőről van szó
b1≠b2 : b1 ≡ b2 → ⊥
b1≠b2 ()

t1 t2 : ⊤ ⊎ ⊤
t1 = inl tt
t2 = inr tt    -- nem ugyanaz!
t1≠t2 : t1 ≡ t2 → ⊥
t1≠t2 ()

bb1 bb2 bb3 : Bool ⊎ ⊤
bb1 = inl true
bb2 = inl false
bb3 = inr tt
bb1≠bb2 : bb1 ≡ bb2 → ⊥
bb1≠bb2 ()
bb1≠bb3 : bb1 ≡ bb3 → ⊥
bb1≠bb3 ()
bb2≠bb3 : bb2 ≡ bb3 → ⊥
bb2≠bb3 ()

ee : (⊤ → ⊥) ⊎ (⊥ → ⊤)
ee = inr exfalso  -- más ilyen nincs

{-
-- kis unrelated ökörködés
_←_ : Set -> Set -> Set
B ← A = A → B

wh : ⊤ ← Bool
wh false = tt
wh true = tt
-}

d : (⊤ ⊎ (⊤ × ⊥)) × (⊤ ⊎ ⊥)
d = inl tt , inl tt        -- más ilyen nincs

-- Ezek alapján hogy lehet megállapítani, hogy melyik típus hány elemű?
-- | ⊤ | = 1
-- | ⊥ | = 0
-- | Bool | = 2
-- | Bool ⊎ ⊤ | = 3
-- | A ⊎ B | = | A | + | B |
-- | A × B | = | A | * | B |
-- | Bool × Bool × Bool | = 8
-- | ⊤ → ⊥ | = 0
-- | ⊥ → ⊤ | = 1
-- | ⊥ → ⊥ | = 1
-- | Bool → ⊥ | = 0 ^ 2
-- | Bool → ⊤ | = 1 ^ 2
-- | ⊤ → Bool | = 2 ^ 1
-- | A → B | = | B | ^ | A |
-- | Bool → Bool → Bool | = 2 ^ 4 = 16 (a bemenet lehet 4-féle, a kimenet 2-féle)


-- Ezek alapján milyen matematikai állítást mond ki és bizonyít a lenti állítás?
-- Válasz: Azt, hogy az A × A-nak ugyanannyi eleme van, mint a Bool → A-nak.
-- Kölcsönösen meg lehet feleltetni őket egymásnak:
-- ami a tuple-ben az első elem, az lesz a true-ra a visszatérési érték;
-- ami a második, az a false-ra.
from' : {A : Set} → A × A → (Bool → A)
from' (a1 , a2) = λ {true -> a1; false -> a2}
to' : {A : Set} → (Bool → A) → A × A
to' f = f true , f false
-- ezek bizonyítások, hogy ez tényleg bijekció
testfromto1 : {A : Set}{a b : A} → fst (to' (from' (a , b))) ≡ a
testfromto1 = refl
testfromto2 : {A : Set}{a b : A} → snd (to' (from' (a , b))) ≡ b
testfromto2 = refl
testfromto3 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) true ≡ a
testfromto3 = refl
testfromto4 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) false ≡ b
testfromto4 = refl

-- röviden:
iso0 : {A : Set} → A × A ↔ (Bool → A)
iso0 = from' , to'

-- de vigyázz: egy _↔_ nem feltétlen bijekció!
-- pl.
badiso : ⊤ ↔ Bool
fst badiso = λ _ → true
snd badiso = λ _ → tt
-- de nyilván nem ugyanannyi elemük van

------------------------------------------------------
-- all algebraic laws systematically
------------------------------------------------------

-- (⊎, ⊥) form a commutative monoid (kommutativ egysegelemes felcsoport)
-- most tekintsünk el a bizonyításoktól
-- megjegyzés: az _⊎_, _×_ jobbasszociatív;
-- szóval például az A ⊎ B ⊎ C az A ⊎ (B ⊎ C)-t jelenti

assoc⊎ : {A B C : Set} → (A ⊎ B) ⊎ C ↔ A ⊎ (B ⊎ C)
fst assoc⊎ (inl (inl a)) = inl a
fst assoc⊎ (inl (inr b)) = inr (inl b)
fst assoc⊎ (inr c) = inr (inr c)
snd assoc⊎ (inl a) = inl (inl a)
snd assoc⊎ (inr (inl b)) = inl (inr b)
snd assoc⊎ (inr (inr c)) = inr c

idl⊎ : {A : Set} → ⊥ ⊎ A ↔ A
fst idl⊎ (inl ())        -- de ezt el is lehet hagyni
fst idl⊎ (inr a) = a
snd idl⊎ a = inr a

idr⊎ : {A : Set} → A ⊎ ⊥ ↔ A
fst idr⊎ (inl a) = a
snd idr⊎ a = inl a

comm⊎ : {A B : Set} → A ⊎ B ↔ B ⊎ A
fst comm⊎ (inl a) = inr a
fst comm⊎ (inr b) = inl b
snd comm⊎ = fst comm⊎       -- igazából az első, csak felcserélve a típusváltozók

-- (×, ⊤) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc× : {A B C : Set} → (A × B) × C ↔ A × (B × C)
fst assoc× ((a , b) , c) = a , (b , c)
snd assoc× (a , (b , c)) = (a , b) , c

idl× : {A : Set} → ⊤ × A ↔ A
fst idl× (_ , a) = a      -- mehet aláhúzás, mert ott úgyis tudjuk, hogy tt van
snd idl× a = tt , a

idr× : {A : Set} → A × ⊤ ↔ A
fst idr× (a , _) = a
snd idr× a = a , tt

-- ⊥ is a null element

null× : {A : Set} → A × ⊥ ↔ ⊥
fst null× ()         -- úgysem lehet egyiknek sem eleme
snd null× ()

-- de ha nagyon ki akarjuk bontani, ki lehet:
null×' : {A : Set} → A × ⊥ ↔ ⊥
fst null×' (_ , bot) = bot
snd null×' bot = exfalso bot

-- distributivity of × and ⊎

dist : {A B C : Set} → A × (B ⊎ C) ↔ (A × B) ⊎ (A × C)
fst dist (a , inl b) = inl (a , b)
fst dist (a , inr c) = inr (a , c)
snd dist (inl (a , b)) = a , inl b
snd dist (inr (a , c)) = a , inr c

-- exponentiation laws

curry : ∀{A B C : Set} → (A × B → C) ↔ (A → B → C)
-- igazából az a-t, b-t is felvehetem a bal oldalra paraméternek
-- de talán lambdával átláthatóbb, mit jelent
fst curry f = λ a b -> f (a , b)
snd curry f = λ (a , b) -> f a b

⊎×→ : {A B C D : Set} → ((A ⊎ B) → C) ↔ (A → C) × (B → C)
fst ⊎×→ f = (λ a -> f (inl a)) , (λ b -> f (inr b))
snd ⊎×→ (f1 , f2) = λ {(inl a) -> f1 a; (inr b) -> f2 b}
-- vagy másképp:
-- snd ⊎×→ (f1 , f2) = λ avb -> case avb f1 f2

law^0 : {A : Set} → (⊥ → A) ↔ ⊤
-- mivel úgyis tudjuk, mik a paraméterek,
-- elég az aláhúzás
fst law^0 _ = tt
snd law^0 _ = exfalso

law^1 : {A : Set} → (⊤ → A) ↔ A
fst law^1 f = f tt
snd law^1 a = λ _ -> a

law1^ : {A : Set} → (A → ⊤) ↔ ⊤
fst law1^ _ = tt
snd law1^ _ = λ _ -> tt

---------------------------------------------------------
-- random isomorphisms
------------------------------------------------------

-- Milyen algebrai állítást mond ki az alábbi típus?
iso1 : {A B : Set} → (Bool → (A ⊎ B)) ↔ ((Bool → A) ⊎ Bool × A × B ⊎ (Bool → B))
iso1 = {!!}

iso2 : {A B : Set} → ((A ⊎ B) → ⊥) ↔ ((A → ⊥) × (B → ⊥))
iso2 = {!!}

iso3 : (⊤ ⊎ ⊤ ⊎ ⊤) ↔ Bool ⊎ ⊤
iso3 = {!!}
testiso3 : fst iso3 (inl tt) ≡ fst iso3 (inr (inl tt)) → ⊥
testiso3 ()
testiso3' : fst iso3 (inl tt) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3' ()
testiso3'' : fst iso3 (inr (inl tt)) ≡ fst iso3 (inr (inr tt)) → ⊥
testiso3'' ()

iso4 : (⊤ → ⊤ ⊎ ⊥ ⊎ ⊤) ↔ (⊤ ⊎ ⊤)
iso4 = {!!} , {!!}
testiso4 : fst iso4 (λ _ → inl tt) ≡ fst iso4 (λ _ → inr (inr tt)) → ⊥
testiso4 ()
testiso4' : snd iso4 (inl tt) tt ≡ snd iso4 (inr tt) tt → ⊥
testiso4' ()
