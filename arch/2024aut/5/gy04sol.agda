module gy04sol where

open import Lib hiding (_+∞_; coite-ℕ∞; ⊤η)

open import Lib.Containers.List hiding (zipWith; head; tail)
open import Lib.Containers.Stream hiding (zipWith; coiteStream)

---------------------------------------------------------
-- típusok η-szabályai
---------------------------------------------------------
{-
Emlékeztető: Az η-szabály azt mondja meg, hogy mit tegyünk, ha destruktorra alkalmazunk konstruktort.
Pl. függvények esetén (λ a → f a) ≡ f, ahol a függvényalkalmazás a destruktor és a λ a konstruktor.

Természetesen más típusoknak is ugyanúgy van η-szabálya.

Vegyük példaként a ⊤-ot:
Destruktora: ite⊤ : A → ⊤ → A

Ez alapján az η-szabály az alábbi lesz:
ite⊤ tt x ≡ x

Ez természetesen Agdában bizonyítható is.
-}

ite⊤ : ∀{i}{A : Set i} → A → ⊤ → A
ite⊤ x _ = x

⊤η : ∀{x} → ite⊤ tt x ≡ x
⊤η = refl

{-
Ahogy emlékeztek rá, a ⊤ η-szabálya úgy néz ki, hogy ∀ a → a ≡ tt,
tehát itt is igaz lesz, hogy egy típusnak több egymással ekvivalens η-szabálya lehet.

Nézzük újra példaként a Bool típust. A β-szabályai a következők voltak:
if true then u else v ≡ u
if false then u else v ≡ v

Mi lehet az η-szabály? Hogy lehet "destruktorra alkalmazni konstruktort" ilyen esetben?
Az if_then_else_ esetén a "then" és az "else" ágban lévő dolgok tetszőleges értékek lehetnek;
ide akár konstruktort is be lehet írni. Tehát úgy lehet felépíteni az η-szabályokat, hogy a destruktor megfelelő
helyeire beírom az azonos típus konstruktorait.
Bool esetén ez azt jelenti, hogy az if_then_else_-ben a második és harmadik helyre kell a Bool két konstruktorát írni.
Ezen felül úgy kell beírni a két konstruktort, hogy alapvetően az "identitás" függvényt kapjuk az adott típuson.
Bool esetén tehát úgy kell az if_then_else_-et felparaméterezni, hogy a false-ra false legyen az eredmény, true-ra pedig true.

Ez alapján mi lesz a Bool-oknak egy lehetséges η-szabálya?
Válasz:

if b then true else false ≡ b

Ugyanezt az ismert 𝟛 típuson is el lehet játszani.
data 𝟛 : Set where
  a1 a2 a3 : 𝟛

Ismert a destruktor: ite𝟛 : A → A → A → 𝟛 → A

Mi lesz a 𝟛 η-szabálya?
Válasz:
ite𝟛 a1 a2 a3 x ≡ x

Természetes számokon a helyzet szintén nem változik.
Ismert a destruktor: iteℕ : A → (A → A) → ℕ → A

Mi lesz ℕ η-szabálya?
Válasz:
iteℕ zero suc n ≡ n

-}

---------------------------------------------------------
-- positivity
---------------------------------------------------------

-- Miért nem enged agda bizonyos típusokat definiálni? Pl. alapesetben az alábbit sem.

{-# NO_POSITIVITY_CHECK #-}
data Tm : Set where
  lam : (Tm → Tm) → Tm

-- FELADAT: Tm-ből adjuk vissza a lam értékét.
app : Tm → (Tm → Tm)
app (lam f) = f

self-apply : Tm
self-apply = lam (λ t → app t t)
          -- (λ t → t t)

-- C-c C-n this:
Ω : Tm
Ω = app self-apply self-apply
{-
(λ t → t t) (λ t → t t) =
= (t t)[t := (λ t → t t)] =
= (λ t → t t) (λ t → t t)
-}

{-# NO_POSITIVITY_CHECK #-}
data Weird : Set where
  foo : (Weird → ⊥) → Weird
  -- Hogy kell elolvasni magyarul a "foo" konstruktort?

noweird : Weird → ⊥
noweird (foo x) = x (foo x)


weird : Weird
weird = foo noweird

bottom : ⊥
bottom = noweird weird

{-
foo:
  (Weird → ⊥)
     -      +

-> Weird

lam :
  (Tm → Tm)
   -     +
→ Tm
-}


-- a positive but not strictly positive type
{-# NO_POSITIVITY_CHECK #-}
data Huh : Set where
  bar : ((Huh → ⊤) -> Huh) -> Huh
--         +     -      +
{-
bar :
    ((Huh → ⊤) -> Huh)
       +      -     +
-> Huh 
-}

-- ez miért jó?
data Tree : Set where
  leaf : Tree
  node : Tree → Tree → Tree

{-
node:
    Tree
    Tree
→ Tree
-}

---------------------------------------------------------
-- coinductive types
---------------------------------------------------------


-- végtelen lista?
{-
repeatL : {A : Set} → A → List A
repeatL x = x ∷ repeatL x
-}

{-
record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A
open Stream
-}
-- check that the type of head : Stream A → A
--                        tail : Stream A → Stream A

-- Ez a típus lényegében a végtelen listákat kódolja el.
-- Ebben véges lista nincs benne, csak végtelen!


-- Copattern matching!
-- FELADAT: Add meg azt a végtelen listát, amely csak 0-kból áll.
zeroes : Stream ℕ
head zeroes = 0
tail zeroes = zeroes
-- Honnan tudja agda, hogy ez totális?
-- Termination checker nem tud futni, hiszen a lista végtelen.
-- Productivity checker

-- by pattern match on n
-- FELADAT: Add meg azt a listát, amely n-től 0-ig számol vissza egyesével.
countDownFrom : ℕ → List ℕ
countDownFrom n = {!!}

-- from n is not by pattern match on n
-- copattern match on Stream
-- FELADAT: Adjuk meg azt a végtelen listát, amely n-től 1-esével felfelé számol!
from : ℕ → Stream ℕ
head (from n) = n
tail (from n) = from (suc n)

-- pointwise addition
zipWith : {A B C : Set} → (A → B → C) → Stream A → Stream B → Stream C
zipWith = {!!}

filterL : {A : Set} → (A → Bool) → List A → List A
filterL p [] = []
filterL p (x ∷ xs) = if p x then x ∷ filterL p xs else filterL p xs

{-
filterS : {A : Set} → (A → Bool) → Stream A → Stream A
head (filterS p xs) = if p (head xs)
                      then head xs
                      else head (filterS p (tail xs))
tail (filterS p xs) = if p (head xs)
                       then (filterS p (tail xs))
                       else tail (filterS p (tail xs))
-}

-- one element from the first stream, then from the second stream, then from the first, and so on
interleave : {A : Set} → Stream A → Stream A → Stream A
interleave = {!!}

-- get the n^th element of the stream
get : {A : Set} → ℕ → Stream A → A
get = {!!}

-- byIndices [0,2,3,2,...] [1,2,3,4,5,...] = [1,3,4,2,...]
byIndices : {A : Set} → Stream ℕ → Stream A → Stream A
byIndices = {!!}

-- iteℕ : (A : Set) → A → (A → A)  → ℕ → A
--        \______________________/
--         ℕ - algebra

-- Mi lesz a Stream konstruktora?
             -- head : Stream A → A lesz (C → A)
                               -- tail : Stream A → Stream A lesz (C → C)
coiteStream : {C A : Set} → (C → A) → (C → C) → C → Stream A
--               \_______________________________/
--                        Stream A - coalgebra
head (coiteStream gen next seed) = gen seed
tail (coiteStream gen next seed) = coiteStream gen next (next seed)

streamId : {A : Set} → Stream A → Stream A
streamId xs = coiteStream head tail xs

-- do this with coiteStream:
-- it's true if the index is dividable by 3; otherwise false
allThirdTrue : Stream Bool
allThirdTrue = coiteStream isDivBy3 suc 0
  where
  isDivBy3 : ℕ → Bool
  isDivBy3 zero = true
  isDivBy3 (suc zero) = false
  isDivBy3 (suc (suc zero)) = false
  isDivBy3 (suc (suc (suc n))) = isDivBy3 n


-- ex: redefine the above functions using coiteStream

-- ex: look at conatural numbers in Thorsten's book and do the exercises about them

-- simple calculator (internally a number, you can ask for the number, add to that number, multiply that number, make it zero (reset))
record Machine (A : Set) : Set where
  coinductive
  field
    getNumber : A
    add       : A → Machine A
    mul       : A → Machine A
    reset     : Machine A
open Machine

calculatorFrom : ℕ → Machine ℕ
getNumber (calculatorFrom n) = n
add (calculatorFrom n) m = calculatorFrom (n + m)
mul (calculatorFrom n) m = calculatorFrom (n * m)
reset (calculatorFrom n) = calculatorFrom 0

c0 c1 c2 c3 c4 c5 : Machine ℕ
c0 = calculatorFrom 0
c1 = add c0 3
c2 = add c1 5
c3 = mul c2 2
c4 = reset c3
c5 = add c4 2

-- FELADAT: Készítsünk egy csokiautomatát.
-- A gépbe tudunk pénzt dobálni (ez legyen ℕ, ennyit adjunk hozzá a jelenlegi kreditünhöz).
-- A tranzakciót meg tudjuk szakítani, a kredit 0 lesz és visszaadja a pénzünket.
-- Legyen 3 termékünk, ezek egyenként kerülnek valamennyibe és van belőlük a gépben valamennyi.
-- + Twix: 350-be kerül, kezdetben van a gépben 50 darab.
-- + Croissant: 400-ba kerül, kezdetben van 75 darab.
-- + Snickers: 375-be kerül, kezdetben van 60 darab.
-- Tudunk 1 terméket vásárolni, ha van elég bedobott pénzünk, ekkor a darabszámból vonjunk le egyet (ha lehet) és adjuk vissza a visszajárót, a kreditet nullázzuk le.
-- A gép tartalmát újra tudjuk tölteni, ekkor twix-ből legyen újra 50 darab, croissant-ból 75, snickers-ből pedig 60.

-- conatural numbers
{-
record ℕ∞ : Set where
  coinductive
  field
    pred∞ : Maybe ℕ∞
open ℕ∞
-}

ℕtoℕ∞ : ℕ → ℕ∞         -- \inf
pred∞ (ℕtoℕ∞ zero) = nothing
pred∞ (ℕtoℕ∞ (suc n)) = just (ℕtoℕ∞ n)

∞' : ℕ∞
pred∞ ∞' = just ∞'

_+∞_ : ℕ∞ → ℕ∞ → ℕ∞
pred∞ (m +∞ n) with pred∞ m
... | nothing = pred∞ n
... | just m-1 = just (m-1 +∞ n)
{- vagy:
m +∞ n with pred∞ m
... | nothing = n
... | just m-1 = λ where .pred∞ → just (m-1 +∞ n)
              -- ez az a szám, aminek a predje just (m-1 +∞ n)
-}

-- Ez a függvény létezik, ezzel lehet megnézni
-- egy conat tényleges értékét.
-- Az első paraméter a fuel, maximum ezt a természetes számot tudja visszaadni.
-- Második paraméter a conat, amire kíváncsiak vagyunk.
-- Értelemszerűen ∞-re mindig nothing az eredmény.
ℕ∞→ℕ' : ℕ → ℕ∞ → Maybe ℕ
ℕ∞→ℕ' fuel x with pred∞ x
ℕ∞→ℕ' fuel x | nothing = just zero
ℕ∞→ℕ' zero x | just px = nothing
ℕ∞→ℕ' (suc fuel) x | just px with ℕ∞→ℕ' fuel px
...                            | nothing = nothing
...                            | just rec = just (suc rec)

testConv1 : ℕ∞→ℕ' 5 (the ℕ∞ 5) ≡ just (the ℕ 5)
testConv1 = refl
testConv2 : ℕ∞→ℕ' 6 (the ℕ∞ 5) ≡ just (the ℕ 5)
testConv2 = refl
testConv : ℕ∞→ℕ' 4 (the ℕ∞ 5) ≡ nothing        -- the Int 5 is the equivalent of (5 :: Int)
testConv = refl

{-
5 -> just 4
4 -> just 3
...
0 -> nothing
-}

coiteℕ∞ : {B : Set} → (B → Maybe B) → B → ℕ∞
pred∞ (coiteℕ∞ f s) with f s
... | nothing = nothing
... | just news = just (coiteℕ∞ f news)

safePred : ℕ → Maybe ℕ
safePred zero = nothing
safePred (suc n) = just n

ℕ→ℕ∞' : ℕ → ℕ∞
ℕ→ℕ∞' n = coiteℕ∞ safePred n

{-
5  ->  just 4  ->  just 3  -> just 2 -> just 1 -> just 0 -> nothing
[.]->  just [.]->   just [.] -> ... -> nothing

-}

-- TODO, further exercises: network protocols, simple machines: chocolate machine (input: coin, getChocolate, getBackCoins, output: error, chocolate, money back), some Turing machines, animations, IO, repl, shell
