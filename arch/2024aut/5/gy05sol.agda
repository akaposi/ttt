module gy05sol where

open import Lib hiding (fromℕ ; minMax; head; tail)
open import Lib.Containers.Vector hiding (head; tail; map; length; _++_)
open import Lib.Containers.List hiding (head; tail; map; _++_; filter)

-- Vec and Fin
{-
infixr 5 _∷_
data Vec (A : Set) : ℕ → Set where
  []  : Vec A 0
  _∷_ : {n : ℕ} → A → Vec A n → Vec A (suc n)
-}

threevec : Vec Bool 3
threevec = true ∷ false ∷ false ∷ []

onevec : Vec Bool 1
onevec = _∷_ {n = 0} true []

head : {A : Set}{n : ℕ} → Vec A (suc n) → A
head (x ∷ _) = x

-- már fordításkor hiba:)
-- what0 : Bool
-- what0 = head []

tail : {A : Set}{n : ℕ} → Vec A (suc n) → Vec A n
tail (_ ∷ xs) = xs

countDownFrom : (n : ℕ) → Vec ℕ n
countDownFrom zero = []
countDownFrom (suc n) = suc n ∷ countDownFrom n

test-countDownFrom : countDownFrom 3 ≡ 3 ∷ 2 ∷ 1 ∷ []
test-countDownFrom = refl

_++_ : {A : Set}{m n : ℕ} → Vec A m → Vec A n → Vec A (m + n)
[] ++ ys = ys
(x ∷ xs) ++ ys = x ∷ (xs ++ ys)

map : {A B : Set}{n : ℕ} → (A → B) → Vec A n → Vec B n
map f xs = {!!}

{-
ezt így nem lehet
_!!'_ : {A : Set}{n : ℕ} → Vec A n → ℕ → A
[] !!' zero = {!!}    -- mert ide mi jönne?
(x ∷ xs) !!' zero = {!!}
xs !!' suc n = {!!}
-}

{-
data Fin : ℕ → Set where  -- Fin n = n-elemu halmaz
  fzero : {n : ℕ} → Fin (suc n)
  fsuc  : {n : ℕ} → Fin n → Fin (suc n)
-}

f0 : Fin 0 → ⊥
f0 ()

f1-0 : Fin 1
f1-0 = fzero {0}

f2-0 f2-1 : Fin 2
f2-0 = fzero {1}
f2-1 = fsuc {1} (fzero {0})

f3-0 f3-1 f3-2 : Fin 3
f3-0 = fzero {2}
f3-1 = fsuc {2} (fzero {1})
f3-2 = fsuc {2} (fsuc {1} (fzero {0}))

f4-0 f4-1 f4-2 f4-3 : Fin 4
f4-0 = {!!}
f4-1 = {!!}
f4-2 = {!!}
f4-3 = {!!}

-- Lib-ben a unicode ‼ az indexelés.
infixl 9 _!!_
_!!_ : {A : Set}{n : ℕ} → Vec A n → Fin n → A
[] !! ()
(x ∷ xs) !! fzero = x
(x ∷ xs) !! fsuc k = xs !! k

      -- Haskell: (3 :: Natural)   Agda: the ℕ 3
test-!! : (the ℕ 3 ∷ 4 ∷ 1 ∷ []) !! (fsuc (fsuc fzero)) ≡ 1
test-!! = refl

test2-!! : (the ℕ 3 ∷ 4 ∷ 1 ∷ 0 ∷ 10 ∷ []) !! 3 ≡ 0 -- 3-as literál a !! után valójában Fin 5 típusú.
test2-!! = refl

fromℕ : (n : ℕ) → Fin (suc n)
fromℕ zero = fzero
fromℕ (suc n) = fsuc (fromℕ n)
-- de mi van, ha nekem a Fin 120-beli 5 kell?

test-fromℕ : fromℕ 3 ≡ the (Fin 4) (fsuc (fsuc (fsuc fzero)))
test-fromℕ = refl

{-
data List (A : Set) : Set where
  []  : List A
  _∷_ : A → List A → List A
-}

{-
length : {A : Set} → List A → ℕ
length [] = zero
length (x ∷ xs) = suc (length xs)
-}

fromList : {A : Set}(as : List A) → Vec A (length as)
fromList [] = []
fromList (x ∷ xs) = x ∷ fromList xs       -- itt [], _∷_ túl vannak terhelve

{-
t : Fin 2 → ℕ
t fzero = 59
t (fsuc fzero) = 62

v : Vec ℕ 2
v = 59 ∷ 62 ∷ []

meg lehet feleltetni
-}

tabulate : {n : ℕ}{A : Set} → (Fin n → A) → Vec A n
tabulate {zero} f = []
tabulate {suc n} f = f fzero ∷ tabulate {n} (λ k → f (fsuc k))

test-tabulate : tabulate (the (Fin 3 -> ℕ) (λ {fzero -> 6; (fsuc fzero) -> 9; (fsuc (fsuc fzero)) -> 2}))
                  ≡ 6 ∷ 9 ∷ 2 ∷ []
test-tabulate = refl

-- Sigma types

what : Σ ℕ (Vec Bool)
what = 0 , []

-- (if_then ℕ else Bool) ≡ (λ b -> if b then ℕ else Bool)
what2 what3 : Σ Bool (if_then ℕ else Bool)
what2 = true , 5
what3 = false , true

filter : {A : Set}{n : ℕ}(p : A → Bool) → Vec A n → Σ ℕ (Vec A) -- ezen lehet pontosítani, hiszen n elemnél nem kéne legyen benne több elem soha.
filter p [] = 0 , []
filter {A} p (x ∷ xs) = if p x then (suc (fst rec) , x ∷ snd rec) else rec
  where
  rec : Σ ℕ (Vec A)
  rec = filter p xs

test-filter : filter {ℕ} (3 <ᵇ_) (4 ∷ 3 ∷ 2 ∷ 5 ∷ []) ≡ (2 , 4 ∷ 5 ∷ [])
test-filter = refl

smarterLengthVec : ∀{i}{A : Set i}{n : ℕ} → Vec A n → ℕ
smarterLengthVec {n = n} _ = n

minMax' : ℕ → ℕ → ℕ × ℕ
minMax' n m = if n < m then n , m else m , n

-- Ugyanez sokkal jobban, de leginkább pontosabban.
-- Az előző változatban vissza tudok adni csúnya dolgokat is.
-- Pl. konstans (0 , 0)-t.
minMax : (n m : ℕ) → Σ (ℕ × ℕ) (λ (a , b) → a ≤ℕ b × ((n ≤ℕ m × n ≡ a × m ≡ b) ⊎ (m ≤ℕ n × n ≡ b × m ≡ a)))
minMax zero m = (zero , m) , tt , inl (tt , refl , refl)
minMax (suc n) zero = (zero , (suc n)) , tt , inr (tt , refl , refl)
minMax (suc n) (suc m) = (suc (fst (fst rec)) , suc (snd (fst rec))) , fst (snd rec) , {!!}
  where
  rec = minMax n m
  proof : suc n ≤ℕ suc m ×
      suc n ≡ suc (fst (fst rec)) × suc m ≡ suc (snd (fst rec))
      ⊎
      suc m ≤ℕ suc n ×
      suc n ≡ suc (snd (fst rec)) × suc m ≡ suc (fst (fst rec))
  proof with snd (snd rec)
  proof | inl p = inl ({!!} , {!!} , {!!})
  proof | inr p = {!!}
