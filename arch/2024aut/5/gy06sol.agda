module gy06sol where

open import Lib hiding (K)

--------------------------------------------------------
-- Elmélet: Függőtípusok elemszámai
--------------------------------------------------------
{-
A függőtípusok is algebrai adattípusoknak számítanak, így természetesen a függőtípusok elemszámai is megadhatók könnyedén.
Nem véletlen, hogy a típusok nevei rendre Σ és Π.

Emlékeztetőül:
| A ⊎ B | = |A| + |B|
| A × B | = |A| ∙ |B|
| A → B | = |B| ^ |A|

Tfh:
-}
P : Bool → Set
P true = Bool
P false = ⊥
{-
Σ Bool P hány elemű lesz?
Ha a Bool-om true (1 konkrét érték), akkor Bool típusú eredményt kell a másik részbe írnom, tehát eddig 2¹ = 2
De ha a Bool-om false (1 konkrét érték), akkor ⊥ típusú eredmény kell, tehát 0¹ = 0

Tehát a nap végén | Σ Bool P | = |Bool| + |⊥| = 2, mert a P egy Bool-tól függő típust ad eredményül, tehát maga a típus vagy P true típusú értéket tartalmaz vagy P false-ot.
Az ilyen "vagy" kapcsolatról megbeszéltük korábban, hogy az az összeadást jelenti.
-}

w1 w2 : Σ Bool P
w1 = true , true
w2 = true , false

{-
Legyen most:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Σ (Maybe Bool) P? Igazából a típus egyes elemei alapján csak meg kell nézni, hogy hány elemű típust adnak vissza.
| Σ (Maybe Bool) P | = | P nothing | + | P (just true) | + | P (just false) | = |⊤| + |Bool| + |Maybe Bool → Bool| = 1 + 2 + 8 = 11

-}

P' : Maybe Bool → Set
P' nothing = ⊤
P' (just true) = Bool
P' (just false) = Maybe Bool → Bool

a1 a2 a3 a4 {-...-} : Σ (Maybe Bool) P'
a1 = {!!}
a2 = {!!}
a3 = {!!}
a4 = {!!}

{-

Ez alapján az intuíció az lehet, hogy | Σ A B | = Σ (i : A) |B i|; tehát csak össze kell adni az egyes típusokból képzett új típusok elemszámát és ennyi.
(Nem véletlen, hogy Σ a típus neve. Ellenőrizhető, hogy A × B elemszáma könnyen ki fog jönni, hogy ha B nem függőtípus.)

Mi a helyzet, ha ugyanezt játszuk Π-vel? Hány elemű lesz Π A B?

Megint konkrét helyzetben, legyen:
P : Bool → Set
P true = Bool
P false = ⊥

| Π Bool P | =⟨ Agda szintaxissal ⟩= | (b : Bool) → P b | kell.
A függvényeknek totálisaknak kell lenniük, tehát ez azt jelenti, hogy MINDEN lehetséges b : Bool értékre P b-nek definiáltnak kell lennie, false-ra ÉS true-ra is.
Intuíció alapján | P true | ÉS | P false | kelleni fog, az ÉS kapcsolat matematikában a szorzást szokta jelenteni, tehát | P true | ∙ | P false | elemszámú lesz
ez a kifejezés.
| P true | ∙ | P false | = |Bool| ∙ |⊥| = 2 ∙ 0 = 0
Próbáljuk meg definiálni ezt a függvényt:
-}

ΠBoolP : Π Bool P
ΠBoolP false = {!!}
ΠBoolP true = true

-- Rájöhetünk, hogy a false ággal gondok lesznek.
{-
Következő példa, ez a P már ismerős:
P : Maybe Bool → Set
P nothing = ⊤
P (just true) = Bool
P (just false) = Maybe Bool → Bool

Hány elemű lesz Π (Maybe Bool) P?
A függvények továbbra is totálisak kell legyenek. Ez azt jelenti, hogy a függvény definiálva kell legyen (P nothing)-ra, (P (just true))-ra ÉS (P (just false))-ra is,
tehát lesz | P nothing | ∙ | P (just true) | ∙ | P (just false) | elemünk, |⊤| ∙ |Bool| ∙ |Maybe Bool → Bool| = 1 ∙ 2 ∙ 2³ = 16 elemű lesz Π (Maybe Bool) P.

-}

t1 : Π (Maybe Bool) P'
t1 nothing = tt          -- 1-féle
t1 (just false) = {!!}   -- 2^3-féle
t1 (just true) = {!!}    -- 2-féle

{-

Intuíció alapján általánosan | Π A B | = Π (i : A) | B i |, tehát csak az összes B-ből képezhető típus elemszámát össze kell szorozni.

Gyakorlás:
Adott a következő P:

P : Bool × Bool → Set
P (true , true) = ⊤
P (true , false) = Bool
P (false , true) = Bool → Bool ⊎ ⊤
P (false , false) = Bool ⊎ ⊤ → Bool

Hány elemű lesz Σ (Bool × Bool) P?
Hány elemű lesz Π (Bool × Bool) P?

Kicsit érdekesebb ezeket vegyíteni, de az elv ugyanaz marad.
Marad ugyanaz a P.

                                                                             Π Bool λ a → Σ Bool (λ b → P (a , b))
Hány elemű lesz Π (a : Bool) (Σ (b : Bool) (P (a , b)))? ( Agda szintaxissal (a : Bool) → Σ Bool (λ b → P (a , b)) )
∏_{a∈Bool} (Σ_{b∈Bool} |P(a,b)|) = (|P(true,true)| + |P(true,false)|) * (|P(false,true)| + |P(false,false)|)
                                                 a=true-ra a szigma                  a=false-ra a szigma


                                                                             Σ Bool λ a → Π Bool (λ b → P (a , b))
Hány elemű lesz Σ (a : Bool) (Π (b : Bool) (P (a , b)))? ( Agda szintaxissal Σ Bool λ a → (b : Bool) →  P (a , b)) )
∑_{a∈Bool} (∏_{b∈Bool} |P(a,b)|) = (|P(true,true)| * |P(true,false)|) + (|P(false,true)| * |P(false,false)|)
                                                 a=true-ra a szorzat                 a=false-ra a szorzat

-}



----------------------------------------------
-- Some Sigma types
----------------------------------------------

{-
true , a
false , b

inl a
inr b
-}
Σ=⊎ : {A B : Set} → Σ Bool (if_then A else B) ↔ A ⊎ B
fst Σ=⊎ (false , b) = inr b
fst Σ=⊎ (true , a) = inl a
snd Σ=⊎ (inl a) = true , a
snd Σ=⊎ (inr b) = false , b

{-
λ {true → a; false → b}

a , b
-}
                    -- Π Bool (if_then A else B)
→=× : {A B : Set} → ((b : Bool) → if b then A else B) ↔ A × B
fst →=× f = f true , f false
snd →=× (a , b) = λ {true → a; false → b}

Σ=× : {A B : Set} → Σ A (λ _ → B) ↔ A × B
fst Σ=× (a , b) = a , b
snd Σ=× (a , b) = a , b

-- there is _≡_, not _↔_
-- what does this mean?

                    -- Π A (λ _ → B)
Π=→ : {A B : Set} → ((a : A) → (λ _ → B) a) ≡ (A → B)
Π=→ = refl

{-
--  A  |  B a   C a b
λ   a      b    → c
λ  (a  ,   b)   → c
-}
dependentCurry : {A : Set}{B : A → Set}{C : (a : A) → B a → Set} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
fst dependentCurry f (a , b) = f a b
snd dependentCurry g a b = g (a , b)

---------------------------------------------------------
-- propositional logic
------------------------------------------------------

-- Curry-Howard izomorfizmus
-- Elmélet:
--   ∙ átalakítani logikai állításokat típusokra.
--   ∙ formalizálni állításokat típusokkal.
--   × = ∧ = konjunkció
--   ⊎ = ∨ = diszjunkció
--   ¬ = ¬ = negáció
--   ⊃ = → = implikáció

--------------------------------------------------
-- Formalisation
--------------------------------------------------

-- Formalizáljuk a mondatokat!

-- Az egyes formalizált alap mondatrészeket vegyük fel modul paraméterként, akkor szépen fog működni minden.
module Formalise (SunShining RainFalling UmbrellaNeeded RainbowSeen : Set) where

  -- Nem süt a nap.
  Form1 : Set
  Form1 = ¬ SunShining        -- \neg

  -- Esik az eső és süt a nap.
  Form2 : Set
  Form2 = RainFalling × SunShining

  -- Nem kell az esernyő vagy esik az eső.
  Form3 : Set
  Form3 = ¬ UmbrellaNeeded ⊎ RainFalling

  -- Ha esik az eső és süt a nap, akkor van szivárvány.
  Form4 : Set
  Form4 = (RainFalling × SunShining) → RainbowSeen

  -- Van szivárvány.
  K : Set
  K = RainbowSeen

---- Következményfogalom (logika tárgy 1-3. gyakorlat)
  -- Agdában legegyszerűbben szintaktikus következményekkel lehet foglalkozni.

  -- Mondd ki, és bizonyítsd be, hogy a fenti állításokból következik a K.
  -- Kétféleképpen lehet bizonyítani.

  Theorem : Set
  Theorem = Form1 → Form2 → Form3 → Form4 → K

  -- C-u C-u C-c C-,
  proof1 : Theorem
  proof1 f1 f2 f3 f4 = f4 f2

  proof2 : Theorem
  proof2 f1 f2 f3 f4 = exfalso (f1 (snd f2))

----------------------------------------------------------------------------

subt-prod : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
subt-prod = {!!}

subt-fun : {A A' B B' : Set} → (A → A') → (B → B') → (A' → B) → (A → B')
subt-fun ata' btb' a'tb a = btb' (a'tb (ata' a))

anything : {X Y : Set} → ¬ X → X → Y
anything nx x = exfalso (nx x)

ret : {X : Set} → X → ¬ ¬ X
ret x nx = nx x

-- ez nem bizonyítható:
-- lem' : {X : Set} → X ⊎ ¬ X
-- lem' = {!!}

nnlem : {X : Set} → ¬ ¬ (X ⊎ ¬ X)
nnlem nlem = nlem (inr λ x → nlem (inl x))

old-impl : {X Y : Set} → (¬ X) ⊎ Y → (X → Y)
old-impl (inl nx) x = exfalso (nx x)
old-impl (inr y) x = y

-- de ezt megint nem lehet:
-- old-impl⁻¹ : {X Y : Set} → (X → Y) → (¬ X) ⊎ Y
-- old-impl⁻¹ = {!!}

-- De Morgan

dm1 : {X Y : Set} →  ¬ (X ⊎ Y) ↔ ¬ X × ¬ Y
fst dm1 nxvy = (λ x → nxvy (inl x)) , λ y → nxvy (inr y)
snd dm1 (nx , ny) (inl x) = nx x
snd dm1 (nx , ny) (inr y) = ny y

-- házi
dm2 : {X Y : Set} → ¬ X ⊎ ¬ Y → ¬ (X × Y)
dm2 = {!!}

dm2b : {X Y : Set} → ¬ ¬ (¬ (X × Y) → ¬ X ⊎ ¬ Y)
dm2b f = f λ nxay → inl λ x → f λ _ → inr λ y → nxay (x , y)

-- stuff

-- házi
nocontra : {X : Set} → ¬ (X ↔ ¬ X)
nocontra = {!!}

¬¬invol₁ : {X : Set} → ¬ ¬ ¬ ¬ X ↔ ¬ ¬ X
fst ¬¬invol₁ nnnnx nx = nnnnx λ nnx → nnx nx
snd ¬¬invol₁ = ret

-- házi
¬¬invol₂ : {X : Set} → ¬ ¬ ¬ X ↔ ¬ X
¬¬invol₂ = {!!}

¬¬invol₃ : {X : Set} → ¬ ¬ X ↔ X
fst ¬¬invol₃ = {!!}  -- ezt nem lehet
snd ¬¬invol₃ = ret

-- házi
nndnp : {X : Set} → ¬ ¬ (¬ ¬ X → X)
nndnp ndnp = ndnp λ nnx → exfalso (nnx λ x → ndnp λ _ → x)

lem2dnp : {X : Set} → (X ⊎ ¬ X) → (¬ ¬ X → X)
lem2dnp (inl x) nnx = x
lem2dnp (inr nx) nnx = exfalso (nnx nx)

-- you have to decide:
{-
Dec : Set → Set
Dec A = A ⊎ ¬ A
-}

open import Lib.Dec.PatternSynonym

ee1 : {X Y : Set} → Dec (X ⊎ Y → ¬ ¬ (Y ⊎ X))
ee1 = inl λ xvy → λ nyvx → nyvx (case xvy (λ x → inr x) λ y → inl y)

ee2 : {X : Set} → Dec (¬ (X ⊎ ¬ X))
ee2 = {!!}

e3 : {X : Set} → Dec (¬ (X → (¬ X → X)))
e3 = {!!}

e4 : Dec ℕ
e4 = inl 0

e5 : Dec ⊥
e5 = inr exfalso

e6 : {X : Set} → Dec (⊥ → X ⊎ ¬ X)
e6 = {!!}

e7 : {X : Set} → Dec (X × ¬ X → ¬ X ⊎ X)
e7 = {!!}

e8 : {X : Set} → Dec ((X → X) → ⊥)
e8 = {!!}

f1 : {X Y : Set} → ¬ ¬ X ⊎ ¬ ¬ Y → ¬ ¬ (X ⊎ Y)
f1 = {!!}

f2 : ({X Y : Set} → ¬ (X × Y) → ¬ X ⊎ ¬ Y) → {X Y : Set} → ¬ ¬ (X ⊎ Y) → ¬ ¬ X ⊎ ¬ ¬ Y
f2 hyp {X} {Y} nnxvy = hyp {¬ X} {¬ Y} λ (nx , ny) → nnxvy λ {(inl x) → nx x; (inr y) → ny y}

------------------------------------------------------
-- statements as parameters
------------------------------------------------------

blowUp : ((A : Set) → ¬ A) → ⊥
blowUp f = f ℕ 0
-- what's the difference with this?
-- (A : Set) → ¬ A → ⊥   -- ez nem is igaz

-- something like this may appear in the exam

----------------------------------------------------------------------
-- Not exactly first order logic but kinda is and kinda isn't.

-- igaz az, hogy ∀ X Y-ra igaz, hogy     ↔  ∀ X Y-ra igaz, hogy
-- hamis az, hogy ∀ X Y-ra igaz, hogy    ≠  ∀ X Y-ra hamis, hogy

f3 : Dec ((X Y : Set) → X ⊎ Y → Y)
f3 = inr λ f → f ⊤ ⊥ (inl tt)

f4 : Dec ((X Y Z : Set) → (X → Z) ⊎ (Y → Z) → (X ⊎ Y → Z))
f4 = inr λ hyp → hyp ⊤ ⊥ ⊥ (inr (λ b → b)) (inl tt)

f5 : Dec ((X Y Z : Set) → (X → Z) × (Y → Z) → (X × Y → Z))
f5 = inl λ _ _ _ (f , g) (x , y) → f x

f6 : Dec ((X Y Z : Set) → (X × Y → Z) → (X → Z) × (Y → Z))
f6 = inr λ f → fst (f ⊤ ⊥ ⊥ snd) tt

f7 : Dec ((X Y Z : Set) → (X ⊎ Y × Z) → (X ⊎ Y) × (X ⊎ Z))
f7 = {!!}

f8 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → ((X ⊎ Y) × Z))
f8 = {!!}

f9 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → (X ⊎ Y × Z))
f9 = {!!}
