module gy07sol where

open import Lib
open import Lib.Dec.PatternSynonym

---------------------------------------------------------
-- predicate (first order) logic example
---------------------------------------------------------

exists : Σ ℕ (λ n → n ≡ 5)
exists = 5 , refl

all : ∀ n → 0 ≤ℕ n     -- (n : ℕ) → 0 ≤ℕ n
all n = tt

-- lemma:


notExists↔noneOf : ∀{A : Set} → (P : A → Set) →
                        (∀ x → ¬ (P x)) ↔ ¬ (Σ A (λ x → P x))
fst (notExists↔noneOf P) f (x , px) = f x px
snd (notExists↔noneOf P) g x px = g (x , px)

-- actually, we have a friend:
dependentCurry : ∀{i j k} {A : Set i}{B : A → Set j}{C : (a : A) → B a → Set k} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
fst dependentCurry f (a , b) = f a b
snd dependentCurry g a b = g (a , b)

notExists↔noneOf' : ∀{A : Set} → (P : A → Set) →
                        (∀ x → ¬ (P x)) ↔ ¬ (Σ A (λ x → P x))
notExists↔noneOf' P = dependentCurry

-- then:

module People
  (Person    : Set)
  (Ann       : Person)
  (Kate      : Person)
  (Peter     : Person)
  (_childOf_ : Person → Person → Set)
  (_sameAs_  : Person → Person → Set) -- ez most itt az emberek egyenlosege
  where

  -- Define the _hasChild predicate.
  _hasChild : Person → Set
  x hasChild = Σ Person (λ child → child childOf x)

  -- Formalise: Ann is not a child of Kate.
  ANK : Set
  ANK = ¬ (Ann childOf Kate)

  -- Formalise: there is someone with exactly one child.
  ONE : Set
  ONE = Σ Person (λ p → p hasChild × (∀ (c1 c2 : Person) → c1 childOf p → c2 childOf p → c1 sameAs c2))

  -- Define the relation _parentOf_.
  _parentOf_ : Person → Person → Set
  x parentOf y = y childOf x

  -- Formalise: No one is the parent of everyone.
  NOPE : Set
  NOPE = ∀ x → ¬ (∀ y → y childOf x)
  -- NOPE = ¬ (Σ Person (λ x → ∀ y → y childOf x))  -- ez ekvivalens
  -- NOPE = ∀ x → Σ Person (λ y → ¬ (y childOf x))  -- ez valójában erősebb

  -- Prove that if Ann has no children then Kate is not the child of Ann.
  AK : ¬ (Σ Person λ y → y childOf Ann) → ¬ (Kate childOf Ann)
  AK hyp kca = hyp (Kate , kca)

  -- Prove that if there is no person who is his own parent than no one is the parent of everyone.
  ¬xpopxthenNOPE : ¬ (Σ Person λ x → x parentOf x) → NOPE
  ¬xpopxthenNOPE nxpopx x allycx = nxpopx (x , allycx x)

---------------------------------------------------------
-- predicate (first order) logic laws
---------------------------------------------------------

∀×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a × Q a)  ↔ ((a : A) → P a) × ((a : A) → Q a)
fst (∀×-distr A P Q) f = (λ a → fst (f a)) , λ a → snd (f a)
snd (∀×-distr A P Q) (g , h) = λ a → (g a , h a)

∀⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → ((a : A) → P a) ⊎ ((a : A) → Q a) → ((a : A) → P a ⊎ Q a)
∀⊎-distr A P Q (inl allapa) a = inl (allapa a)
∀⊎-distr A P Q (inr allaqa) a = inr (allaqa a)
-- ez miért csak odafelé megy?
-- miért nem ↔ van közte?

Σ×-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a × Q a)  → Σ A P × Σ A Q
Σ×-distr = {!!}

Σ⊎-distr  :    (A : Set)(P : A → Set)(Q : A → Set) → (Σ A λ a → P a ⊎ Q a)  ↔ Σ A P ⊎ Σ A Q
fst (Σ⊎-distr A P Q) (a , inl pa) = inl (a , pa)
fst (Σ⊎-distr A P Q) (a , inr qa) = inr (a , qa)
snd (Σ⊎-distr A P Q) (inl (a , pa)) = a , inl pa
snd (Σ⊎-distr A P Q) (inr (a , qa)) = a , inr qa

¬∀        :    (A : Set)(P : A → Set)              → (Σ A λ a → ¬ P a)      → ¬ ((a : A) → P a)
¬∀ A P (a , npa) f = npa (f a)

{-
-- miért nem megy fordítva?
¬∀-inv        :    (A : Set)(P : A → Set)          → ¬ ((a : A) → P a)      → (Σ A λ a → ¬ P a)
¬∀-inv A P nforallpa = {!!} , {!!}
-}

-- Ugyanez van a fájl tetején is:
¬Σ        :    (A : Set)(P : A → Set)              → (¬ Σ A λ a → P a)      ↔ ((a : A) → ¬ P a)
¬Σ A = {!!}

¬¬∀-nat   :    (A : Set)(P : A → Set)              → ¬ ¬ ((x : A) → P x)    → (x : A) → ¬ ¬ (P x)
¬¬∀-nat = {!!}

{-
0 → nulla
1 → nem nulla
2 → nem nulla
...

szóval mindre igaz, hogy vagy nulla, vagy nem nulla

de az nem igaz, hogy mind nulla
és az sem igaz, hogy mind nem nulla
-}

∀⊎-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (((a : A) → P a ⊎ Q a) → ((a : A) → P a) ⊎ ((a : A) → Q a)))
∀⊎-distr' hyp = case lemma (λ allzero → allzero 1) λ allnotzero → allnotzero 0
  where
  P : ℕ → Set
  P zero = ⊤
  P (suc _) = ⊥

  Q : ℕ → Set
  Q zero = ⊥
  Q (suc _) = ⊤

  lemma : ((a : ℕ) → P a) ⊎ ((a : ℕ) → Q a)
  lemma = hyp ℕ P Q λ {zero → inl tt; (suc _) → inr tt}

Σ×-distr' : ¬ ((A : Set)(P : A → Set)(Q : A → Set) → (Σ A P × Σ A Q → Σ A λ a → P a × Q a))
Σ×-distr' hyp = cont lemma
  where
  lemma : Σ ℕ (λ Q → IsZero Q × IsNotZero Q)
  lemma = hyp ℕ IsZero IsNotZero ((0 , tt) , 1 , tt)

  cont : Σ ℕ (λ Q → IsZero Q × IsNotZero Q) → ⊥
  cont (zero , proof) = snd proof
  cont (suc n , proof) = fst proof

-- kulcsok és ajtók
Σ∀       : (A B : Set)(R : A → B → Set)        → (Σ A λ x → (y : B) → R x y) → (y : B) → Σ A λ x → R x y
Σ∀ A B R (a , allyray) y = a , allyray y

-- axiom of choice
-- itt igazából tétel
-- képzeld el, hogy R x y = y ∈ x
-- és akkor visszakapjuk a klasszikus megfogalmazást
AC       : (A B : Set)(R : A → B → Set)        → ((x : A) → Σ B λ y → R x y) → Σ (A → B) λ f → (x : A) → R x (f x)
AC A B R hyp = (λ x → fst (hyp x)) , λ x → snd (hyp x)
