module gy08sol where

open import Lib hiding (sym; trans; cong; subst; idr+; sucr+; assoc+; comm+; dist+*; nullr*; idl*; idr*; sucr*; assoc*; comm*)

---------------------------------------------------------
-- equality
------------------------------------------------------

sym : ∀{i}{A : Set i}{x y : A} → x ≡ y → y ≡ x
sym {x = x} {y = .x} refl = refl

trans : ∀{i}{A : Set i}{x y z : A} → x ≡ y → y ≡ z → x ≡ z
trans {x = x} {y = .x} {z = .x} refl refl = refl

-- ha két dolog egyenlő, akkor akármi függvényt ráküldesz, az is egyenlő
cong : ∀{i j}{A : Set i}{B : Set j}(f : A → B){x y : A} → x ≡ y → f x ≡ f y
cong f refl = refl

-- ha két dolog egyenlő, akkor minden, ami az egyikre igaz, az a másikra is igaz lesz
subst : ∀{i j}{A : Set i}(P : A → Set j){x y : A} → x ≡ y → P x → P y
subst P refl px = px

---------------------------------------------------------
-- properties of +,*
---------------------------------------------------------

idl+ : (n : ℕ) → zero + n ≡ n
idl+ n = refl

idr+ : (n : ℕ) → n + zero ≡ n
idr+ zero = refl
idr+ (suc n) = cong suc (idr+ n)

sucr+ : (n m : ℕ) → n + suc m ≡ suc (n + m)
sucr+ zero m = refl
sucr+ (suc n) m = cong suc (sucr+ n m)

ass+ : (m n o : ℕ) → (m + n) + o ≡ m + (n + o)
ass+ zero n o = refl
ass+ (suc m) n o = cong suc (ass+ m n o)

comm+-helper : (n m : ℕ) → suc n + m ≡ n + suc m
comm+-helper n m = sym (sucr+ n m)

comm+ : (m n : ℕ) → m + n ≡ n + m
comm+ zero n = sym (idr+ n)
comm+ (suc m) n = trans {y = suc n + m} (cong suc (comm+ m n)) (comm+-helper n m)
{-
          comm+      comm+-helper
suc (m + n) = suc n + m = n + suc m
-}

dist+* : (m n o : ℕ) → (n + o) * m ≡ n * m + o * m
dist+* m zero o = refl
dist+* m (suc n) o = trans {y = m + (n * m + o * m)}
                           (cong (m +_) (dist+* m n o))
                           (sym (ass+ m (n * m) (o * m)))
{-
m + n * m + o * m igazából (m + n * m) + o * m!
              dist+*                 ass+
m + (n + o) * m = m + (n * m + o * m) = m + n * m + o * m
-}

nullr* : (n : ℕ) → n * 0 ≡ 0
nullr* zero = refl
nullr* (suc n) = nullr* n

-- házi
idl* : (n : ℕ) → 1 * n ≡ n
idl* = {!!}

-- házi
idr* : (n : ℕ) → n * 1 ≡ n
idr* = {!!}

sucr* : (n m : ℕ) → n * suc m ≡ n + n * m
sucr* zero m = refl
sucr* (suc n) m = cong suc (trans {y = m + (n + n * m)}
                                  (cong (m +_) (sucr* n m))
                           (trans {y = (m + n) + n * m}
                                  (sym (ass+ m n (n * m)))
                           (trans {y = (n + m) + n * m}
                                  (cong (_+ n * m) (comm+ m n))
                                  (ass+ n m (n * m)))))
{-
m + n * suc m = [sucr*]
= m + (n + n * m) = [ass+]
= (m + n) + n * m = [comm+]
= (n + m) + n * m = [ass+]
= n + (m + n * m)
-}

-- házi
ass* : (m n o : ℕ) → (m * n) * o ≡ m * (n * o)
ass* = {!!}

comm*-helper : (n m : ℕ) → n + n * m ≡ n * suc m
comm*-helper n m = sym (sucr* n m)

comm* : (m n : ℕ) → m * n ≡ n * m
comm* zero n = sym (nullr* n)
comm* (suc m) n = {!!} -- házi
{-
       comm*        comm*-helper
n + m * n = n + n * m = n * suc m
-}

{-
other notions of trans:
_◾_ = \sq5

\< = ⟨ ; \> = ⟩ ; \qed = ∎
_≡⟨_⟩_ + _∎

_≡⟨_⟩_
Λ  Λ ^-- proof
|  |
|  ⌞ proof
value

_∎ = basically reflexivity proof with an explicit value

Usual way of using this notion:

value1
  ≡⟨ proof1 ⟩
value2
  ≡⟨ proof2 ⟩
value3
  ≡⟨ proof3 ⟩
value4 ∎
-}

open import Lib.Relation.Notation {A = ℕ} _≡_ trans refl

sucr*' : (n m : ℕ) → n * suc m ≡ n + n * m
sucr*' zero m = refl
sucr*' (suc n) m = cong suc
  (m + n * suc m
    ≡⟨ cong (m +_) (sucr*' n m) ⟩
  m + (n + n * m)
    ≡⟨ sym (ass+ m n (n * m)) ⟩
  (m + n) + n * m
    ≡⟨ cong (_+ n * m) (comm+ m n) ⟩
  (n + m) + n * m
    ≡⟨ ass+ n m (n * m) ⟩
  n + (m + n * m) ∎)
{-
m + n * suc m = [sucr*]
= m + (n + n * m) = [ass+]
= (m + n) + n * m = [comm+]
= (n + m) + n * m = [ass+]
= n + (m + n * m)
-}

-- rewrite the above proofs with this notation, if you like it
