module gy09sol where

-- open import Lib
open import Agda.Builtin.Sigma
open import Lib.Empty
open import Lib.Equality
open import Lib.Nat
open import Lib.Function
open import Lib.Sigma

---------------------------------------------------------
-- equational reasoning
------------------------------------------------------

p4 : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4 x y = trans (cong (λ t → x + t + x) (idr+ y))
        (trans (assoc+ x y x)
        (trans (cong (x +_) (comm+ y x))
        (trans (sym (assoc+ x x y))
               (cong (λ t → x + t + y) (sym (idr+ x))))))
{-
x + (y + zero) + x = [idr+]
= (x + y) + x = [assoc+]
= x + (y + x) = [comm+]
= x + (x + y) = [assoc+]
= x + x + y = [idr+]
= x + (x + zero) + y
-}

p4' : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4' x y rewrite idr+ y | idr+ x | assoc+ x y x | comm+ y x | assoc+ x x y = refl

-- házi
p3 : (a b : ℕ) → a + a + b + a * 0 ≡ 2 * a + b
p3 = {!!}

-- házi
p2 : (a b c : ℕ) → c * (b + 1 + a) ≡ a * c + b * c + c
p2 = {!!}

p9' : 0 ≢ the ℕ 1    -- 0 ≡ the ℕ 1 → ⊥
p9' ()
-- 0 ≡ 1 igazából zero ≡ suc zero
-- és mivel zero nem ua., mint a suc, ezért az Agda látja

-- és mintaillesztés nélkül?
p9'' : 0 ≢ the ℕ 1
p9'' = {!!}

p9 : 2 * 2 ≢ 5 * 1
p9 = {!!}

-- Egyszerűbb, amikor mondani kell egy ellenpéldát:
p10 : ¬ ((n : ℕ) → n + 2 ≡ n + 1)      -- nem minden n-re igaz
p10 hyp = subst (λ {(suc (suc zero)) → ⊤; _ → ⊥}) (hyp 0) tt   -- a 0 az ellenpélda

-- ...mintsem bizonyítani, hogy ez a kettő sosem lesz egyenlő:
p11 : (n : ℕ) → n + 2 ≢ n + 1         -- minden n-re hamis
p11 (suc n) e = p11 n (cong pred' e)

-- Mókásabb helyzet.
p11'' : ¬ Σ ℕ (λ n → n + 2 ≡ n + 1)   -- nincs n, amire igaz
-- p11'' (suc n , e) = p11'' (n , cong pred' e)         -- **termination checking failed**
p11'' = uncurry p11

p12 : ¬ Σ ℕ (λ n → n + n ≡ 3)
p12 (suc (suc (suc zero)) , ())
p12 (suc (suc (suc (suc n))) , ())

-- Most mintaillesztés nélkül:
p12' : ¬ Σ ℕ (λ n → n + n ≡ 3)
p12' (suc (suc n) , e) = subst (λ {(suc (suc (suc (suc _)))) → ⊥; _ → ⊤}) strangeeq tt
  where
  strangeeq : 3 ≡ suc (suc (suc (suc (n + n))))
  strangeeq = trans (sym e) (cong (λ t → suc (suc t)) (comm+ n (suc (suc n))))

leftwards : (a b c d e : ℕ) → b + c + d ≡ e → a + b + c + d ≡ a + e
leftwards a b c d e eq = trans {y = (a + (b + c)) + d}
                              (cong (_+ d) (assoc+ a b c))
                         (trans {y = (a + ((b + c) + d))}
                              (assoc+ a (b + c) d)
                              (cong (a +_) eq))
{-
  ((a + b) + c) + d = [assoc+ a b c]
= (a + (b + c)) + d = [assoc+ a (b + c) d]
= (a + ((b + c) + d))
   a +  (b + c  + d)
-}
leftwards' : (a b c d e : ℕ) → b + c + d ≡ e → a + b + c + d ≡ a + e
leftwards' a b c d e eq rewrite assoc+ a b c | assoc+ a (b + c) d | eq = refl

[m+n]^2=m^2+2mn+n^2 : (m n : ℕ) → (m + n) * (m + n) ≡ m * m + 2 * m * n + n * n
[m+n]^2=m^2+2mn+n^2 m n rewrite dist+* m n (m + n) | idr+ m | dist*+ m m n | dist*+ n m n | dist+* m m n | comm* n m | sym (assoc+ (m * m +  m * n) (m * n) (n * n)) | assoc+ (m * m) (m * n) (m * n) = refl
{-
  (m * m +  m * n) + (m * n  + n * n)  =
 ((m * m +  m * n) +  m * n) + n * n   =
   m * m + (m * n  +  m * n) + n * n
-}


{-
infixr 8 _^_
_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n
-}

p1 : (a b : ℕ) → (a + b) ^ 2 ≡ a ^ 2 + 2 * a * b + b ^ 2
p1 a b rewrite idr* (a + b) | [m+n]^2=m^2+2mn+n^2 a b | idr* a | idr* b = refl

0^ : (n : ℕ) → 0 ^ (suc n) ≡ 0
0^ _ = refl

^0 : (a : ℕ) → a ^ 0 ≡ 1
^0 _ = refl

1^ : (n : ℕ) → 1 ^ n ≡ 1
1^ zero = refl
1^ (suc n) rewrite idr+ (1 ^ n) = 1^ n

^1 : (a : ℕ) → a ^ 1 ≡ a
^1 = idr*

^+ : (a m n : ℕ) → a ^ (m + n) ≡ a ^ m * a ^ n
^+ a zero n = sym (idr+ (a ^ n))
^+ a (suc m) n rewrite ^+ a m n = sym (assoc* a (a ^ m) (a ^ n))

*^ : (a b n : ℕ) → (a * b) ^ n ≡ a ^ n * b ^ n
*^ a b zero = refl
*^ a b (suc n) rewrite *^ a b n | assoc* a b (a ^ n * b ^ n) | sym (assoc* b (a ^ n) (b ^ n)) | comm* b (a ^ n) | assoc* a (a ^ n) (b * b ^ n) | assoc* (a ^ n) b (b ^ n) = refl

^* : (a m n : ℕ) → a ^ (m * n) ≡ (a ^ m) ^ n
^* a zero n = sym (1^ n)
^* a (suc m) n rewrite ^+ a n (m * n) | ^* a m n = sym (*^ a (a ^ m) n)
