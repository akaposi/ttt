module gy10 where

open import Lib hiding (_≟ℕ_)
open List

-- injektív függvény: különböző dolgokhoz különböző dolgokat rendel
-- másképp: ha f x = f y, az csak úgy lehet, ha x = y

IsInjective : ∀{i j}{A : Set i}{B : Set j}
              → (A → B) → Set (i ⊔ j)
IsInjective {A = A} f = ∀ (x y : A) → f x ≡ f y → x ≡ y

-- nem minden függvény ilyen!
-- pl.
counterexample : ¬ IsInjective (λ (_ : ℕ) → zero)
counterexample = {!!}

-- de a konstruktorok mind ilyenek

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj = {!!}

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = {!!}

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj = {!!}

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl = {!!}

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr = {!!}

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 = {!!}

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false = {!!}

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = {!!}

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn = {!!}

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn = {!!}

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node = {!!}

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons = {!!}

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' = {!!}

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = {!!}

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 = {!!}

-- most egyenlőségre mintaillesztés nélkül
¬2≤1' : ¬ (2 ≤ 1)
¬2≤1' (n , eq) = {!!}

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn = {!!}

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ = {!!}

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ = {!!}

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
_≟Bool_ = {!!}

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
_≟ℕ_ = {!!}

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
_≟BinTree_ = {!!}

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ = {!!}

------------------------------------------------------
-- η-szabály / egyediség szabály
------------------------------------------------------
{-
Most már van egyenlőség típusunk, így tudunk beszélni az η-szabályról.
A félév elején meg volt említve a függvények η-szabálya (λ a → f a) ≡ f, ez a szabály garantálja,
hogy függvényre csakis egyféleképpen tudunk alkalmazni paramétert, magyarul csakis pontosan egy destruktorom/eliminátorom van.

Tehát az η-szabály azt mondja, hogy ha van egy másik eliminátorom,
ami pontosan ugyanúgy viselkedik, mint az eredeti eliminátor,
akkor az pontosan az elsőként meghatározott eliminátor kell legyen.

Példa Bool-okon:
Eliminátora: if_then_else_ : Bool → A → A → A
β-szabályok: if true  then x else y = x
             if false then x else y = y
η-szabály: {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ if b then x else y
           --------------------------------   ------------------------
             eliminátor dolgai                      β-szabályai
             paraméterek

Természetesen ez ugyanúgy generálható, hiszen minden része megadható csak a típusnak a definíciójából.
-}

η-Bool : {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ (if b then x else y)
η-Bool a a' f eq1 eq2 false = eq2
η-Bool a a' f eq1 eq2 true = eq1

{-
data 𝟛 : Set where
  a1 a2 a3 : 𝟛

ite𝟛 : A → A → A → 𝟛 → A
ite𝟛 x y z a1 = x
ite𝟛 x y z a2 = y
ite𝟛 x y z a3 = z

-- házi
η-szabály: ?

Az egyetlen trükkösebb helyzet, amikor a típusunk rekurzív. De ekkor sem kell megijedni, hiszen az η-szabály leírásakor
csak a β-szabályok viselkedését kell követni.

Például:

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

-}
iteℕ : {A : Set} → A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

-- η-szabály:
η-ℕ' : {A : Set} (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → f ≡ iteℕ z s
η-ℕ' z s f eqz eqs = {!!}  -- itt semmi támpontot nem kapunk

η-ℕ : {A : Set} (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → ∀ k → f k ≡ iteℕ z s k
η-ℕ z s f eqz eqs zero = eqz
η-ℕ z s f eqz eqs (suc k) rewrite eqs k | η-ℕ z s f eqz eqs k = refl

{-

data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : B → (A → B → B) → List A → B
iteList e c [] = e
iteList e c (x ∷ xs) = c x (iteList e c xs)

η-szabály: ?

-}
