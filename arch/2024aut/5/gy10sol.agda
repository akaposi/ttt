module gy10sol where

open import Lib.Containers.List
open import Agda.Primitive
open import Agda.Builtin.Sigma
open import Lib.Equality
open import Lib.Bool
open import Lib.Dec
open import Lib.Empty
open import Lib.Equality
open import Lib.Nat
open import Lib.Function
open import Lib.Sum

-- injektív függvény: különböző dolgokhoz különböző dolgokat rendel
--           másképp: azonos dolgokat csak azonos dolgokhoz rendel
-- ha f x = f y, az csak úgy lehet, ha x = y

IsInjective : ∀{i j}{A : Set i}{B : Set j}
              → (A → B) → Set (i ⊔ j)
IsInjective {A = A} f = ∀ (x y : A) → f x ≡ f y → x ≡ y

-- nem minden függvény ilyen!
-- pl.
counterexample : ¬ IsInjective (λ (_ : ℕ) → zero)
counterexample hyp = subst IsZero (hyp 0 1 refl) tt

-- de a konstruktorok mind ilyenek

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
-- sucinj m .m refl = refl
-- sucinj m n sm=sn = cong (λ {(suc t) → t; _ → 42}) sm=sn
sucinj m n sm=sn = cong pred' sm=sn



data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj eq = cong (λ {(node t) → t; _ → (λ _ → leaf)}) eq
                                       -- ^ itt akármi lehet

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl eq = cong (λ {(node x _) → x; _ → leaf}) eq

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr eq = cong (λ {(node _ y) → y; _ → leaf}) eq

-- házi
∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 = {!!}

-- házi
∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

AreDisjoint : ∀{i j}{A : Set i}{B : Set j}
              → (A → B) → (A → B) → Set (i ⊔ j)
AreDisjoint {A = A} f g = ∀ (x y : A) → f x ≢ g y  -- akkor sem, ha x ≡ y

true≠false : true ≢ false    -- AreDisjoint (λ _ → true) (λ _ → false)
true≠false ()

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst (λ {true → ⊤; false → ⊥}) e tt

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn e = subst (λ {zero → ⊤; (suc _) → ⊥}) e tt

n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' n ()

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node = {!!}

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons = {!!}

-- prove this using induction on n!
n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn zero ()
n≠sucn (suc n) e = n≠sucn n (cong pred' e)

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 (d , eq) = butno sucsucd=1
  where
  sucsucd=1 : suc (suc d) ≡ 1
  sucsucd=1 = (trans (comm+ 2 d) eq)

  butno : suc (suc d) ≢ 1     -- \nequiv
  butno ()
{-
d + 2 = [comm+]
= 2 + d
igazából suc (suc d)
az 1 az pedig suc zero

-}

-- most egyenlőségre mintaillesztés nélkül
¬2≤1' : ¬ (2 ≤ 1)
¬2≤1' (d , eq) = butno sucsucd=1
  where
  sucsucd=1 : suc (suc d) ≡ 1
  sucsucd=1 = (trans (comm+ 2 d) eq)

  butno : suc (suc d) ≢ 1
  butno eq' = subst (λ {(suc (suc _)) → ⊤; _ → ⊥}) eq' tt

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn n = 1 , refl

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ n m (d , eq) = d , proof
  where
  proof : d + suc n ≡ suc m
  proof rewrite comm+ d (suc n) | comm+ n d = cong suc eq

{-
d + suc n = [comm+]
suc (n + d) = [comm+]
suc (d + n) = [eq]
suc m
-}

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ n m (d , eq) = d , cong pred' sucd+n=sucm
  where
  sucd+n=sucm : suc (d + n) ≡ suc m
  sucd+n=sucm rewrite comm+ d n | comm+ (suc n) d = eq
{-
suc (d + n) = [comm+]
suc n + d   = [comm+]
d + suc n   = [eq]
suc m
-}

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')      -- \?=
false ≟Bool false = inl refl
false ≟Bool true = inr (λ eq → subst (λ {false → ⊤; true → ⊥}) eq tt)
true ≟Bool false = inr (λ eq → subst (λ {true → ⊤; false → ⊥}) eq tt)
true ≟Bool true = inl refl

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
zero ≟ℕ zero = inl refl
zero ≟ℕ suc n' = inr λ ()
suc n ≟ℕ zero = inr λ ()
suc n ≟ℕ suc n' with n ≟ℕ n'
... | inl eq = inl (cong suc eq)
... | inr neq = inr λ seq → neq (cong pred' seq)

_≟Tree_ : (t t' : Tree) → Dec (t ≡ t')
leaf ≟Tree leaf = inl refl
leaf ≟Tree node x = inr λ ()
node x ≟Tree leaf = inr λ ()
node x ≟Tree node x' = {!!}   -- ezt nem lehet

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
leaf ≟BinTree leaf = {!!}
leaf ≟BinTree node t' t'' = {!!}
node t t₁ ≟BinTree leaf = {!!}
node t1 t2 ≟BinTree node t1' t2' with t1 ≟BinTree t1'
... | inl eq with t2 ≟BinTree t2'
...             | inl eq' rewrite eq | eq' = inl refl
...             | inr neq' = inr λ hyp → neq' (cong (λ {(node t1 t2) → t2; leaf → leaf}) hyp)
node t1 t2 ≟BinTree node t1' t2' | inr neq = inr λ hyp → {!!}   -- ezt nézzétek meg

-- ≡-dec-List a Libben
decList : {A : Set} → ((x y : A) → Dec (x ≡ y)) → (xs ys : List A) → Dec (xs ≡ ys)
decList decA [] [] = inl refl
decList decA [] (x ∷ ys) = {!!}
decList decA (x ∷ xs) [] = {!!}
decList decA (x ∷ xs) (y ∷ ys) with decA x y
... | inl eq with decList decA xs ys
...    | inl eq' rewrite eq | eq' = inl refl
...    | inr neq' = inr λ hyp → neq' (∷-injectiveʳ hyp)
decList decA (x ∷ xs) (y ∷ ys) | inr neq = inr λ hyp → neq (∷-injectiveˡ hyp)

------------------------------------------------------
-- η-szabály / egyediség szabály
------------------------------------------------------
{-
Most már van egyenlőség típusunk, így tudunk beszélni az η-szabályról.
A félév elején meg volt említve a függvények η-szabálya (λ a → f a) ≡ f, ez a szabály garantálja,
hogy függvényre csakis egyféleképpen tudunk alkalmazni paramétert, magyarul csakis pontosan egy destruktorom/eliminátorom van.

Tehát az η-szabály azt mondja, hogy ha van egy másik eliminátorom,
ami pontosan ugyanúgy viselkedik, mint az eredeti eliminátor,
akkor az pontosan az elsőként meghatározott eliminátor kell legyen.

Példa Bool-okon:
Eliminátora: if_then_else_ : Bool → A → A → A
β-szabályok: if true  then x else y = x
             if false then x else y = y
η-szabály: {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ if b then x else y
           --------------------------------   ------------------------
             eliminátor dolgai                      β-szabályai
             paraméterek

Természetesen ez ugyanúgy generálható, hiszen minden része megadható csak a típusnak a definíciójából.
-}

η-Bool : {A : Set}(x y : A)(f : Bool → A) → f true ≡ x → f false ≡ y → (b : Bool) → f b ≡ (if b then x else y)
η-Bool a a' f eq1 eq2 false = eq2
η-Bool a a' f eq1 eq2 true = eq1

{-
data 𝟛 : Set where
  a1 a2 a3 : 𝟛

ite𝟛 : A → A → A → 𝟛 → A
ite𝟛 x y z a1 = x
ite𝟛 x y z a2 = y
ite𝟛 x y z a3 = z

-- házi
η-szabály: ?

Az egyetlen trükkösebb helyzet, amikor a típusunk rekurzív. De ekkor sem kell megijedni, hiszen az η-szabály leírásakor
csak a β-szabályok viselkedését kell követni.

Például:

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

-}
iteℕ : {A : Set} → A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

-- η-szabály:
η-ℕ' : {A : Set} (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → f ≡ iteℕ z s
η-ℕ' z s f eqz eqs = {!!}  -- itt semmi támpontot nem kapunk

η-ℕ : {A : Set} (z : A)(s : A → A)(f : ℕ → A) → f zero ≡ z → ((n : ℕ) → f (suc n) ≡ s (f n)) → ∀ k → f k ≡ iteℕ z s k
η-ℕ z s f eqz eqs zero = eqz
η-ℕ z s f eqz eqs (suc k) rewrite eqs k | η-ℕ z s f eqz eqs k = refl

{-

data List (A : Set) : Set where
  [] : List A
  _∷_ : A → List A → List A

iteList : B → (A → B → B) → List A → B
iteList e c [] = e
iteList e c (x ∷ xs) = c x (iteList e c xs)

η-szabály: ?

-}
