open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Agda.Builtin.Equality

--------------------------------------------------
-- 1. először fejben/papíron átgondolandó emlékeztető kérdések

-- Melyik billentyűkombinációval kell az Emacsben egy fájlt:
  -- megnyitni,
  -- menteni,
  -- más néven menteni?
-- Hogyan illesztünk be Emacsben a vágólapról?

-- Melyik billentyűkombinációval kell egy Agda-fájlt betölteni (ezt fogjuk sokszor nyomogatni)?
-- Hogyan kell egy lyukat létrehozni?
-- Melyik billentyűkombinációval kell egy lyukra:
  -- megnézni, hogy milyen típusú kifejezést vár a rendszer;
  -- összehasonlítani ezt az éppen beírt dolog típusával;
  -- elfogadtatni a beírt kifejezést ("betölteni" a lyukat)?
-- Hogyan kell kiértékelni egy tetszőleges kifejezést?
-- Hogyan kell megnézni egy tetszőleges kifejezés típusát?

-- 2. Írd le újra az alábbi szövegeket (a \-es bevitel gyakorlása):
-- Βασιλεια (Basileia)
-- 𝕄𝕖𝕥𝕣𝕠𝟚
-- abs : ℤ → ℕ
-- λ ε 0<ε → ball (ε , 0<ε) x y

-- Nézd meg Emacsen belül, mi a kódja az alábbi karakternek:
-- ⋊


--------------------------------------------------
-- 2. pár gyakorlati feladat

-- Ez a következő feladathoz kell:
add3 : ℕ → ℕ
add3 x = x + 3

-- Állítsd elő a 7-et az add3 függvény kétszeri alkalmazásával; fogadtasd el az eredményt az Agdával is.
seven : ℕ
seven = {!!}
-- ez egy teszt; piros lesz, ha nem 7
-- még nem kell érteni, hogy működik;
-- de talán látszik, hogy menő:)
sevenTest : seven ≡ 7
sevenTest = refl

-- C-c C-n-nel értékeld ki, hogy ℕ → (ℕ → ℕ)
-- Mire következtetsz ebből?

-- Kommenteld ki az alábbi két sort és hozz létre egy-egy lyukat a + előtt és a + után:
--nat : ℕ
--nat = {-ide egy lyuk-} + {-ide egy lyuk-}

-- Töltsd ki a lyukakat természetes számokkal; ellenőrizd, hogy a beírt dolgok típushelyesek-e és fogadtasd el őket az Agdával.

-- Írd le kommentben, hogyan értékelődik ki az alábbi kifejezés:
toEvaluate : ℕ
toEvaluate = add3 ((λ x -> x + x) 5)

-- Írj függvényt a következő típusokkal (kis Haskell-ismétlés):
tr1 : ℕ → (ℕ → ℕ) → ℕ
tr1 = {!!}
tr2 : ℕ → ℕ → (ℕ → ℕ)
tr2 = {!!}

-- Mi ennek a típusa? és a jelentése?
weird = (λ n m -> n + 2 * m) ((λ x -> 1 + x) (add3 5))
