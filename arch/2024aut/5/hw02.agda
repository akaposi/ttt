module hw02 where

open import Lib

-- Hány eleme van az alábbi típusoknak? (gy02-ből másoltam ide)
-- | ⊤ | =
-- | ⊥ | =
-- | Bool | =
-- | Bool ⊎ ⊤ | =
-- | A ⊎ B | =
-- | A × B | =
-- | Bool × Bool × Bool | =
-- | ⊤ → ⊥ | =
-- | ⊥ → ⊤ | =
-- | ⊥ → ⊥ | =
-- | Bool → ⊥ | = 0 ^ 2
-- | Bool → ⊤ | = 1 ^ 2
-- | ⊤ → Bool | = 2 ^ 1
-- | A → B | =
-- | Bool → Bool → Bool | =

-- Add meg az alábbi típusok összes elemét
-- (mindenhova 2 lyukat raktam; lehet több és lehet kevesebb is –
-- ha több van, csinálj még; ha kevesebb, töröld ki a feleslegeseket).
-- Segítség: 10-nél több egyikből sincs; a legtöbb 5-nél kevesebb.
-- És lehet 0 is.
exa1 exa2 : Bool ⊎ ⊤
exa1 = {!!}
exa2 = {!!}
exb1 exb2 : ⊤ × ⊤
exb1 = {!!}
exb2 = {!!}
exc1 exc2 : ⊤ → Bool
exc1 = {!!}
exc2 = {!!}
exd1 exd2 : ⊥ ⊎ ⊤
exd1 = {!!}
exd2 = {!!}
exe1 exe2 : ⊤ → ⊥
exe1 = {!!}
exe2 = {!!}
exf1 exf2 : (⊤ ⊎ ⊤) → ⊤
exf1 = {!!}
exf2 = {!!}
-- segítség: a _×_ erősebben köt, mint az _⊎_
-- még segítség: először számold ki, hány van
exg1 exg2 : (⊤ ⊎ Bool × ⊤) → (⊤ × Bool)
exg1 = {!!}
exg2 = {!!}

-- Írj meg egy függvényt az alábbi típusszignatúrával.
-- (Az elején a fura ∀ meg ilyesmi csak azért van,
-- hogy működjön Settel, Set₁-gyel, Set₂-vel stb. is.)
comm⊎ : ∀{i j}{A : Set i}{B : Set j}
        → A ⊎ B → B ⊎ A
comm⊎ = {!!}
-- Van más ilyen függvény? Ha igen, adj meg egyet. Ha nem, miért nem?
-- (Most azt tekintem különbözőnek,
-- aminél van olyan bemenet, amire más kimenetet ad.)

-- Írd meg ezután a lehető legrövidebben ezt a függvényt:
comm⊎back : ∀{i j}{A : Set i}{B : Set j}
        → B ⊎ A → A ⊎ B
comm⊎back = {!!}

-- Emlékeztető: próbáld meg fejből leírni, mi az η-redukció.
