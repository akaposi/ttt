module hw03 where

open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Agda.Builtin.Equality

{-
Kis elméleti emlékeztető:

Mi a ℕ destruktora? Mik a β-szabályai?
Mi a _×_ destruktora (vagy destruktorai)? Mik a β-szabályai? (Több válasz is jó.)
Mi a lista destruktora? Mik a β-szabályai?
-}

-- Írd meg a ↑↑ függvényt, ami annyiszor hatványoz egy számot saját magával, amennyiszer odaírod.
-- Pl. 5 ↑↑ 5 = 5 ^ (5 ^ (5 ^ (5 ^ 5))).
-- (Definíció szerint n ↑↑ 0 legyen 1.)

_↑↑_ : ℕ -> ℕ -> ℕ     -- ↑ kódja: \u
_↑↑_ = {!!}

test1 : 6 ↑↑ 0 ≡ 1
test1 = refl
test2 : 6 ↑↑ 1 ≡ 6
test2 = refl
test3 : 6 ↑↑ 2 ≡ 46656
test3 = refl
test4 : 2 ↑↑ 2 ≡ 4
test4 = refl
test5 : 2 ↑↑ 3 ≡ 16
test5 = refl

-- Írd meg a logfloor függvényt, ami veszi egy egész szám 2 alapú logaritmusát,
-- majd azt lefelé kerekíti.
-- 0-ra adjon vissza 42-t (ez megint borzasztóan ronda, de tudunk majd jobbat).
-- Lásd a teszteket példákért.
-- Segítség: használd fel a half függvényt.
-- És lazacszín nem ér!

half : ℕ -> ℕ
half zero = zero
half (suc zero) = zero
half (suc (suc n)) = suc (half n)

logfloor : ℕ -> ℕ
logfloor = {!!}

test'1 : logfloor 1 ≡ 0
test'1 = refl
test'2 : logfloor 2 ≡ 1
test'2 = refl
test'3 : logfloor 3 ≡ 1
test'3 = refl
test'4 : logfloor 4 ≡ 2
test'4 = refl
test'7 : logfloor 7 ≡ 2
test'7 = refl
test'8 : logfloor 8 ≡ 3
test'8 = refl
test'63 : logfloor 63 ≡ 5
test'63 = refl
test'64 : logfloor 64 ≡ 6
test'64 = refl
test'65 : logfloor 65 ≡ 6
test'65 = refl

{-
És akkor amit gy03-ból javaslok:
- Egyszerű természetes rekurzív függvények (szorzás, even?, fib, eq?).
- Maradékos osztás.
- Iterátorral-rekurzorral újraírni pár ilyen egyszerűbb függvényt.
- Haskelles típusokból ha valami nem ismerős, akkor azt gyakorolni.
- TreeInf (annyira nem fontos, de izgalmas).
-}
