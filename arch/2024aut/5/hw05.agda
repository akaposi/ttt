module hw05 where

open import Lib hiding (_*∞_)

{-
Mi lesz a lista egy lehetséges η-szabálya? (Segít, ha a ℕ-éra visszaemlékszel.)
És a Stream egy lehetséges η-szabálya? (Ezt meg lehet adni 1 vagy 2 szabályként is.)

Mit jelent, hogy egy típus szigorúan pozitív?

Szigorúan pozitívak-e az alábbi típusok?

data First : Set where
  const : ℕ → First
  twist : (First → ℕ) → First
data Second : Set where
  wrap : ((Second → Bool) → ℕ) → Second
data Third : Set where
  leaf : Third
  twine : Third → ℕ → Third → Third
-}

-- Definiálj egy streamet, ami x-től előrefelé felsorolja az összes ℕ∞-értéket.
-- Ne használj _+∞_-et.
from : ℕ∞ → Stream ℕ∞
head (from n) = n
tail (from n) = from (λ where .pred∞ → just n)

-- És akkor amiket meghagytunk:
-- - a listás/streames gyakorlóból ami maradt (meg coiteStreammel újraírni őket esetleg);
-- - ha bevállalósak vagytok, a csokiautomata;
-- - meg a végén a két feladattal is megpróbálkozhattok, ami megmaradt.
