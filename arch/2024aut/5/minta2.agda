module dec6 where

open import Lib

t1a : Σ ℕ λ n → (k : ℕ) → (k <ℕ n) → Fin n → Fin k
t1a = 0 , λ _ ()

t1b : ¬ (Σ ℕ λ n → IsNotZero n × ((k : ℕ) → (k <ℕ n) → Fin n → Fin k))
t1b (suc n , tt , hyp) = the (Fin zero → ⊥) (λ ()) (hyp 0 tt fzero)

data X : ℕ → Set where
  X1 : (n k : ℕ) → X n → X (n + k)
  X2 : (n : ℕ) → Fin n → X (suc n)

iteX : ∀{i}{A : ℕ → Set i}
        → ((n k : ℕ) → A n → A (n + k))
        → ((n : ℕ) → Fin n → A (suc n))
        → {n : ℕ} → X n → A n
iteX f g (X1 n k x) = f n k (iteX f g x)
iteX f g (X2 n fn) = g n fn

η-X : ∀{i}{A : ℕ → Set i}
        (f : ((n k : ℕ) → A n → A (n + k)))
        (g : ((n : ℕ) → Fin n → A (suc n)))
        (otherIte : {n : ℕ} → X n → A n)
        → (∀(n k : ℕ) (x : X n) → otherIte (X1 n k x) ≡ f n k (otherIte x))
        → (∀(n : ℕ)(fn : Fin n) → otherIte (X2 n fn)  ≡ g n fn)
      → ∀ {n : ℕ} (x : X n) → otherIte x ≡ iteX f g x
η-X f g otherIte hyp1 hyp2 (X1 n k x) rewrite hyp1 n k x | η-X f g otherIte hyp1 hyp2 x = refl
η-X f g otherIte hyp1 hyp2 (X2 n x) = hyp2 n x

module ZH
    (Student : Set)
    (_passes : Student → Set)
    (_consults : Student → Set)
  where

  a : Set
  a = ∀(s : Student) → s passes
  b : Set
  b = Σ Student (λ s → (¬ (s passes)) ⊎ s consults)
  c : Set
  c = Σ Student (λ s → ¬ (s consults) → ¬ (s passes))

disjointℕ : ∀ (n : ℕ) → suc n ≢ zero
disjointℕ n ()

disjoint⊎ : ∀{i j}{A : Set i}{B : Set j} →
            ∀ (a : A)(b : B) → inl a ≢ inr b
disjoint⊎ _ _ ()

private
  splitFirstRow : ∀{i}{A : Set i}{n-1 k : ℕ} →
             Vec (Vec A (suc n-1)) k → (Vec A k × Vec (Vec A n-1) k)
  splitFirstRow [] = [] , []
  splitFirstRow ((x ∷ vs) ∷ vs₁) with splitFirstRow vs₁
  ... | (fr , rem) = x ∷ fr , vs ∷ rem

transpose : ∀{i}{A : Set i}{n k : ℕ} →
              Vec (Vec A n) k → Vec (Vec A k) n
transpose {n = zero} {k = k} m = []
transpose {n = suc n} {k = k} m with splitFirstRow m
... | (fr , rem) = fr ∷ transpose rem

{-
x₁₁ x₁₂ ⋯ x₁ₖ
...
xₙ₁ xₙ₂ ⋯ xₙₖ
-}

{-
A B után van a Dec
igaz: ∀ A B-re igaz, hogy    (ami ugyanaz, mint az "igaz, hogy minden A-ra B-re")
hamis: ∀ A B-re hamis, hogy  (ami _nem_, mint a "hamis, hogy minden A-ra B-re")
-}

t6 : ∀{A B : Set} → Dec (¬ ((A ⊎ B → A) ⊎ (A ⊎ B → B)))
t6 {A = A} {B = B} = inr λ hyp → nnlem {A} λ {(inl a) → hyp (inl λ _ → a);
                                               (inr na) → hyp (inr λ {(inl a) → exfalso (na a); (inr b) → b})}
  where
  nnlem : ∀ {P : Set} → ¬ ¬ (P ⊎ ¬ P)
  nnlem nlem = nlem (inr λ p → nlem (inl p))

{-
nem mindegy:
∀x → P x → Q x
(∀x → P x) → (∀ x → Q x)
-}

t7a : Dec (∀{A : Set} (P : A → Set)(f : A → A)
                         → (∀ x → P x) → (∀ x → P (f x)))
t7a = inl λ P f hyp x → hyp (f x)

t7b : Dec (∀{A : Set} → (P : A → Set)(f : A → A)
             → (∀ x → P (f x)) → (∀ x → P x))
t7b = inr λ hyp → hyp {ℕ} IsZero (λ _ → zero) (λ _ → tt) 1

t8 : (n : ℕ) → suc (n * (suc (suc n))) ≡ (n + suc zero) ^ (suc (suc zero)) + zero
t8 n rewrite comm* n (suc (suc n)) | comm+ n 1 | comm* n 1 | idr+ n | idr+ (n + n * suc n) | comm* n (suc n) = refl

{-
suc (n * (suc (suc n))) = [comm* n (suc (suc n))]
= suc ( suc (suc n) * n)
  suc ( n + suc n * n)
  suc ( n + (n + n * n))

(n + 1) * ((n + 1) * 1) + zero = [comm+ n 1]
= suc n * (suc n * 1) + zero = [comm* (suc n) 1]
= suc n * (1 * suc n) + zero
  suc n * (suc n + 0 * suc n) + zero
  suc n * (suc n + zero) + zero
  (suc n + zero) + n * (suc n + zero) + zero = [idr+ ((suc n + zero) + n * (suc n + zero))]
= (suc n + zero) + n * (suc n + zero) = [idr+ (suc n)]
= suc n + n * suc n = [comm* n (suc n)]
= suc n + suc n * n
  suc n + (n + n * n)
  suc (n + (n + n * n))
-}

exampleForWith : {A A : Set} → A ⊎ A → A
exampleForWith ava with ava
... | inl a = {!!}
... | inr a = {!!}
