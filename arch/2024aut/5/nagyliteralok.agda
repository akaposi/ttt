module nagyliteralok where

open import Lib

-- Lib.Nat.Properties-ben vannak a tételek.

{-
natPattern : ℕ → Bool
natPattern 21 = true
natPattern _  = false
-}

private
  lemma : (a b : ℕ) → (a + b) ^ 2 ≡ a ^ 2 + 2 * a * b + b ^ 2
  lemma a b rewrite idr* (a + b) | idr* a | idr* b | idr+ a | dist+* a b (a + b) | dist*+ a a b | dist*+ b a b | dist+* a a b | sym (assoc+ (a * a) (a * b) (a * b)) | assoc+ (a * a + a * b) (a * b) (b * b) | comm* a b = refl

-- Gonosz feladat, a 100 miatt túl hosszú lenne a kifejezés lépésenként,
-- ha hagynánk agdát is dolgozni.
p5 : (a : ℕ) → (10 * a + 5) ^ 2 ≡ a * (suc a) * 100 + 25
-- p5 a rewrite comm* (a * (suc a)) 100 = {!!} -- huh
p5 a rewrite lemma (10 * a) 5 | dist*^ 10 a 2 | comm* (2 * (10 * a)) 5 | comm* 100 (a ^ 2) | sym (assoc* 5 2 (10 * a)) | sym (assoc* 10 10 a) | comm* 100 a | idr* a | comm* a (suc a) | comm+ a (a * a) | dist+* (a * a) a 100 = refl

{-
(10 * a + 5) ^ 2 = [lemma (10 * a) 5]
= (10 * a) ^ 2 + 2 * (10 * a) * 5 + 25 =
= 100 * a ^ 2 + 2 * (10 * a) * 5 + 25 = [comm* (2 * (10 * a)) 5]
= 100 * a ^ 2 + 5 * (2 * (10 * a)) + 25 = [sym (assoc* 5 2 (10 * a))]
= 100 * a ^ 2 + 10 * (10 * a) + 25 = [sym (assoc* 10 10 a)]
= 100 * a ^ 2 + 100 * a + 25


a * (suc a) * 100 + 25 = [comm* a (suc a)]
= (a + a * a) * 100 + 25 = [dist+*]
= a * 100 + a * a * 100 + 25 = [comm+]
= a * a * 100 + a * 100 + 25 = [comm*]
= 100 * (a * a) + 100 * a + 25 = [vhogy a * a ≡ a ^ 2]
= 100 * a ^ 2 + 100 * a + 25
-}
