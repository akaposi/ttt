0:00
  házival volt gond? akkor megbeszéljük
0:05
  ez a ℕ
  valójában a ℕ sincs a nyelvbe égetve, amíg nem importálod
  nézzük meg M-.-tyel a definíciót
  jó, itt ez a {-# BUILTIN NATURAL Nat #-} azért feketemágia, mert Haskell-Integerekre optimalizálja ki
0:08
  predekkel játszunk
  de ezeknél majd szebb predet lehet:
  olyat, amit fizikailag nem tudsz meghívni 0-ra
  de most muszáj 0-ra is adni valamit, mert ugye csak totális függvények vannak
0:12
  double: minden sucot két sucra
  half: na, itt két alapeset van
  és a _+_: ott az elsőre elég mintát illeszteni
  szorzás házi e mentén
0:18
  _^_-on hagyhatjuk őket picit gondolkodni
0:21
  megoldás
  _! házi feladat (de nézd, ilyen posztfix operátor; menő)
0:24
  _-_-ból is egyelőre csak ilyen csúnyát tudunk, de lesz jobb
0:26
  _≥_-t valaki mondja gyorsan
  utána: hogy lehet _>_-et egy sorban?
  hogy lehet _<_-at egy sorban?
  min hf.
0:30
  comp: lehetne az előzőekkel, de akkor kétszer rekurzálnánk végig; az lassú
  csináljuk meg eggyel
  az így fog kinézni; négy eset
  a változónevek furák, de ennyi
0:33
  gcd: lassú lesz, mert még nincs maradékos osztásunk, de ez van
  btw, a 0-t külön kell kezelni, mert azt aztán vonogathatod ítéletnapig
  de: anyázik
  de: ha a terminatinget odarakom, akkor nem
  mese
0:35
  gcd-helper: ha odarakod a fuelt, akkor jó
  persze ez borzasztóan ronda, és elvben bizonyítani is kéne, hogy ez ugyanazt adja
  (ugye a fuel elég lesz mindig, mert minden lépésben legalább 1-gyel csökken az összeg)
  even?, fib, eq? hf.; eddigiek alapján egyszerű
0:38
  remhez a let, mint konstrukció
  sőt akkor már hívjuk b-1-nek
  és megint nem terminál... fuel
  vagy másik opció: amit az Agda beépítettje csinál; nézzük meg
  div házi feladat; annyi segítség, hogy növelgetni kell menetközben
0:46
  iteNat: ez lesz a ℕ destruktora
  (szóval belemászunk, szétszedjük és aszerint döntünk)
  és igazából ez az iterátor (újra és újra alkalmazzuk az s függvényt)
  egy double-t meg egy összeadást megnézhetünk gyorsan
0:50
  de faktoriálist hogyan?
  ahhoz kéne az is, hogy éppen hanyadik iterációnál tartunk
  erre van a recNat
  de igazából iteNattal definiálható, csak tuple kell hozzá
  hf.: nézzetek meg párat ezekkel is
0:55
  listát felteszem, Haskellből tudjátok; ha nem, segítek esetleg külön
  egyet nézzünk meg: iteList
  ez lesz a destruktor
  igazából a foldr
  lent meg már látszik az, ami az η-szabály lesz
1:00
  a többiből csak kettőt nézzünk meg
  a RoseTree-nél egy node-nak tetszőleges véges sok gyereke lehet (és ha nincs neki, akkor llevél)
  TreeInfnél pedig vannak levelek, meg vannak node-ok, amiknek viszont MV sok gyereke van
  lehet ilyen vicceseket (picit rajzoljunk)
  és mondjuk itt is az index csúnya, mert leafre nem szabadna értéket visszaadnia
1:05
  és most megint elmélet
  ehhez picit azért magyarázok
  eleve van egy értéked, azt szétszeded, és utána újra összerakod, és ugyanazt kell kapnod
  Boolnál egyszerűbb megérteni
  ∀ (b : Bool) -> if b then true else false ≡ b
  akkor ⊤-nál:
  ∀ (t : ⊤) -> ite⊤ tt t ≡ t
    mert ugye csak tt lehet az is
  ℕ-ra:
  ∀ (n : ℕ) -> iteℕ zero suc n ≡ n
1:13
  pozitivitás
  van egy ilyen izgalmas dolog
  ugye definiáljuk ezt
  és akkor próbáljuk meg ezt levezetni
  ha a lamoktól eltekintünk:
  (λ x -> x x) (λ x -> x x)
  amúgy ezért is nem szeretik a típustalan lambda-kalkulust
  na, de ez típusos
  igen, de nem lehetnek akármilyenek a típusok
1:18
  nézd: Weird
  vmi olyasmi, hogy ha tudunk a Weirdből egy ⊥-ot csinálni, akkor van egy Weirdünk
  ezzel mi lesz a probléma?
  hát csinálunk egy ⊥-ot; dobpergés...
1:23
  a szabály az, hogy csak _szigorúan pozitív_ típusokat fogadunk el
  ez azt jelenti, hogy a konstruktor argumentumai A → X alakúak lehetnek legfeljebb
  szóval az argumentum argumentuma nem lehet X például
  és ha beágyazzuk?
  ```
  Huh is not strictly positive, because it occurs
  to the left of an arrow
  in the type of the constructor bar
  in the definition of Huh.
  ```
  ez pozitív, de ezt sem szereti;
  nem feltétlen lenne baj, de például a kizárt harmadik elvét agyonvágná
  szóval inkább nem
1:28
  akkor összegyűjtöm majd, miket adtam fel
  meg találok ki elméleti kérdéseket
  kérdés?
