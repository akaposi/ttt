0:00
  házival volt gond? akkor megbeszéljük
0:05
  és most megint elmélet
  ehhez picit azért magyarázok
  eleve van egy értéked, azt szétszeded, és utána újra összerakod, és ugyanazt kell kapnod
  Boolnál egyszerűbb megérteni
  ∀ (b : Bool) -> if b then true else false ≡ b
  akkor ⊤-nál:
  ∀ (t : ⊤) -> ite⊤ tt t ≡ t
    mert ugye csak tt lehet az is
  ℕ-ra:
  ∀ (n : ℕ) -> iteℕ zero suc n ≡ n
0:13
  pozitivitás
  van egy ilyen izgalmas dolog
  ugye definiáljuk ezt
  és akkor próbáljuk meg ezt levezetni
  ha a lamoktól eltekintünk:
  (λ x -> x x) (λ x -> x x)
  amúgy ezért is nem szeretik a típustalan lambda-kalkulust
  na, de ez típusos
  igen, de nem lehetnek akármilyenek a típusok
0:18
  nézd: Weird
  vmi olyasmi, hogy ha tudunk a Weirdből egy ⊥-ot csinálni, akkor van egy Weirdünk
  ezzel mi lesz a probléma?
  hát csinálunk egy ⊥-ot; dobpergés...
0:23
  a szabály az, hogy csak _szigorúan pozitív_ típusokat fogadunk el
  ez azt jelenti, hogy a konstruktor argumentumai A → X alakúak lehetnek legfeljebb
  szóval az argumentum argumentuma nem lehet X például
  és ha beágyazzuk?
  ```
  Huh is not strictly positive, because it occurs
  to the left of an arrow
  in the type of the constructor bar
  in the definition of Huh.
  ```
  ez pozitív, de ezt sem szereti;
  nem feltétlen lenne baj, de például a kizárt harmadik elvét agyonvágná
  szóval inkább nem
0:26
  koinduktív
  próbáljunk meg végtelen listát: a termination checker beszól
  mert ugye Agdában nincsenek nem termináló függvények
  de valahogy meg kéne oldani
0:27
  --guardedness kell hozzá
  és a destruktoraival definiáljuk, mint egy rekordot
  (amúgy lehet konstruktort is adni neki, de ilyen cigarettásdoboz-barna lesz és nem túl ügyesen kezeli; inkább ne)
  amúgy miért koinduktív:
    rekurzív sum vs. rekurzív product
    konstruktorok helyett destruktorokkal definiáljuk
    (egyébként nem a lista duálisa; ahhoz az kéne, hogy véges is lehessen)
  miért terminál:
    kis rajzocska
0:37
  zeroes közösen
  countDownFrom vs. from
  zipWith szerintem mehet házinak, és kérdezzenek, ha nem stimmel
0:40
  filter listán
  filter streamen
  huh? miért nem jó?
  hát mert ki garantálja, hogy végtelen lesz az is
    fixen meg kell tudni mondani, mi lesz a headje
  ha végesíted, már jó erre
  csak picit bonyolult, mert sok esetszétválasztás
0:46
  getet még együtt
0:48
  coiteStream meg így; ez a konstruktor
  mit jelent ez?
    van egy seeded
    van egy függvény, ami megcsinálja a következőt
    és van egy függvény, ami konkrétan az elemeket legenerálja
    (A → B) → (A → A) → A → Stream B
  próbáljunk hárommal oszthatót természetes seeddel
  maradék lehet házi
0:56
  hogy picit jobban érthető legyen: Machine
  bár ez inkább jobban össze szokta zavarni, de ha megérted, akkor tényleg megérted
  mi az add típusa? Machine → ℕ → Machine
  hát itt végtelen sok lehetőség van... ezt induktívba már nem pakolnád bele
  és itt csak akkor generálja le, ha kell
  pl. ez egy ilyen
  és tulajdonképpen a műveleteket is te definiálod
  aztán az A helyére mehetne racionális szám, és akkor azokkal a műveletekkel
  vagy véges test
  gyakorlásnak lehet a csokiautomata; bár az hosszas
  és igazából ha picit bonyolultabb, akkor egy operációs rendszert kapsz
1:02
  ez a konat
  ez már tényleg a ℕ duálisa
  van egy megelőzője, de nem biztos, hogy van
    különben sok értelme nem is lenne
  véges természetes lehet, vagy végtelen
  ismerkedjünk vele
1:07
  összeadás; huh
  hát itt a legegyszerűbb a with lesz
  megnézzük, hogy nothing-e a predje avagy nem
  így felírjuk, és aztán px-re már lehet mintát illeszteni
  ha meg a predjével kell megadni, akkor λ where .pred∞ → ?
  tudom, borzasztó
1:10
  ℕ∞→ℕ'
  gonna be a bumpy ride...
  itt a nothingba, justba bele lehet zavarodni
1:15
  és koiterátor
  de ez már egyszerűbb, mint a streamé
1:18
  úúú, és vektor
  na, itt jön, hogy miért kellett ennyit szenvedni eddig
  meg hogy milyen lesz a szép head, szép tail
  bekerülhetnek az elemek a típus leírásába
  C++-ban array<int, 4>? na, de ott legenerálja; itt meg a nyelv szerves része
  itt vannak a konstruktorok; a _∷_ mindig egy eggyel hosszabbat csinál
1:23
  szép head: {A : Set}{n : ℕ} → Vec A (suc n) → A
  ez garantáltan nemüreset kap
  de: ahhoz, hogy az n-et használhassuk később, ahhoz a függvénynek meg kell kapnia paraméterként
  ez a rejtett
  kapcsos zárójelben oda is írhatom
  de ha már megkapja a konkrét vektort, akkor kikövetkezteti maga
1:26
  tail : {A : Set}{n : ℕ} → Vec A (suc n) → Vec A n
  még azt is megmondhatom, hogy az eredmény eggyel rövidebb
  és szinte nem tudom elrontani; nem tudok üreset beírni, ilyesmi
1:28
  házit akkor a kihagyottakon kívül szerintem adok még
  kérdés?
