0:00
  van kérdés, amit meg akartok beszélni?
  + konzultáció; holnap mikor?
0:02
  technikai kérdésekre válaszokat kiírtam, de itt is mondom:
  1. Agda-typecheckert lehet használni az elméleti részhez is.
  2. Felosztva nincs az idő a két rész között; az időtöket úgy használjátok, ahogy szeretnétek. A 90 perces időtartamot nem tudni még, de annyi minimum lesz.
  3. Saját gépet eddig úgy néz ki, hogy nem lehet használni. De a géptermieken van Agda, Emacs meg VS Code-kiegészítő is.
0:04
  lett frissítve a mintazh
  és vannak ilyen megjegyzések
  meg van egy koinduktívos feladat; azt nézzük meg azért közösen
0:08
  és akkor folytatjuk a vektorokkal
  szép head: {A : Set}{n : ℕ} → Vec A (suc n) → A
  ez garantáltan nemüreset kap
  de: ahhoz, hogy az n-et használhassuk később, ahhoz a függvénynek meg kell kapnia paraméterként
  ez a rejtett
  kapcsos zárójelben oda is írhatom
  de ha már megkapja a konkrét vektort, akkor kikövetkezteti maga
0:11
  tail : {A : Set}{n : ℕ} → Vec A (suc n) → Vec A n
  még azt is megmondhatom, hogy az eredmény eggyel rövidebb
  és szinte nem tudom elrontani; nem tudok üreset beírni, ilyesmi
0:13
  countDownFrom: a hosszát itt sem tudom elrontani; legfeljebb az elemeket
0:14
  _++_: típusa mi lesz?
  de utána a definíciója tök ugyanaz
  map legyen házi
0:16
  _!!'_: elkezdjük
  üresre mit adunk vissza? hátööö...
  hibát ugye nem dobhatunk
  szóval vagy Maybe, vagy...
0:19
  Fin
  röviden: a Fin n egy n-elemű véges (finite) halmaz
  hogyan oldod meg?
  fzero és fsuc a két konstruktor
  de: oké, hogy az fsuc fzero az az 1, de most a Fin 2-beli 1 vagy a Fin 3-beli 1 vagy a Fin hány?
  nézzük meg a táblázatot meg a definíciót
  szóval rejtett paraméterek; ha gondolod, oda is írhatod kapcsos zárójelben
  Fin 0 miért üres?
0:24
  picit akkor itt is próbáljuk ki kapcsos zárójelekkel
0:26
  és akkor indexelés
  fel is ismeri, hogy nem lehet üres, mert akkor Fin 0-t kéne kapnia, ami nincs
  és innentől tök ugyanaz
  mi van amúgy a kapcsos zárójelekben, ha kitöltjük?
0:30
  fromℕ: itt mi lenne a kapcsos zárójelekben?
  másik gond: ez a Fin 4-beli 3; de ha én a Fin 12-beli 3-at szeretném?
  na; majd később
0:33
  fromList: meglepően logikus
  és a hosszát is kitalálja
  de figyelem: a _∷_ és a [] túlterheltek; ez a [] nem az a []
  (the amúgy itt érdekes)
0:36
  tabulate: igazából Fin n → A-ból ugyanannyi van, mint Vec A n-ből
  (|A|^n)
  ugyanaz az információtartalmuk
  rajzoljuk fel
  és hogyan implementáljuk? mintaillesztéssel, de mire?
0:41
  szigma
  ezt azért magyarázni kell
  szóval egy tuple, ahol a második elem _típusa_ függ az első elem _értékétől_
  pl. ez a Σ ℕ (Vec Bool)
  miért jobb a listánál? mert ott van egy tuple-ben a hossza, ami garantáltan a hossza
0:46
  filter: ez erősebb lesz azért
  lent a teszt segít, hogy mit akar tőlünk a feladat
  segédfüggvénnyel a legegyszerűbb talán
0:51
  smarterLengthVec: simán a rejtett paramétert kivesszük
0:53
  minMax
  hát ez ütős lesz
  mit jelent?
  mi az a _≤ℕ_? egy típus, ami vagy ⊤, vagy ⊥
  mi az _≡_? egy másik típus, aminek refl a konstruktora, ha az Agda magától ki tudja találni, vagy levezetni tudod
  arra vigyázz, hogy a sucokat rakd rá a rekurzív eredményre
  na, és itt nem baj, ha most még nem értetek mindent, amit csinálok
1:03
  mesélünk akkor a szigmák meg a Π-k elemszámairól
  meg még a Π-t kéne elmesélni
  a Π nem adattípus külön, hanem igazából egy shortcut erre
  adjunk meg egy ilyet; úgy egyszerűbb elképzelni
  de a nagyon részletes magyarázat le van írva; szóval miután ezt elmutogattuk, lehet házi az elolvasása
1:08
  izomorfizmusok again
  boolos szigma ugye az ⊎; boolos Π az a tuple
  aztán az, hogy ha a Σ-ba meg Π-be nem függő típust rakunk, akkor igazából visszakapjuk a régit
  de a Π-nél konkrétan ≡
  (haladóknak: valójában a Libben a tuple is szigmaként van definiálva; szóval az is)
1:13
  meseeeee
  szóval:
    konstruktivizmus
    Brouwer, Hilbert
    eldönthetetlen problémák (és akkor arra is igaz, hogy vagy igaz, vagy nem?)
    és Curry–Howard
    lesz haszna, mert minden egzisztenciabizonyítás egy program is
    és Brouwer–Heyting–Kolmogorov
1:18
  ha van időnk, elkezdjük a Formalise modult
  ezek a modulparaméterek... hát, most maradjunk annyiban, hogy ilyen állítások, amiknek nem tudjuk, van-e elemük
  szóval az Isten ledobta őket és vannak
  és akkor formalizáció vs. bizonyítás
1:23
  ha még több időnk van, a subt-prod egy példaként
  subt-fun házi
  és az anything még együtt
1:28
  kérdés?
  és akkor konzultáció ekkor meg akkor
  és holnap Loviba ekkor meg akkor
