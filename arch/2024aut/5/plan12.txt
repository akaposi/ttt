0:00
  when2meetre nagy érdeklődés nem volt
  de amondó vagyok, ma leadok mindent, ami még kell
  és akkor jövő héten, 1 nappal előtte már nem adnék le új anyagot,
    hanem akinek van igénye kérdezni, az jöjjön
  kértek még ezen kívül konzultációt?
  és akkor zh jövő hét kedd előadás sávjában;
  pótzh meg huszadika (első vizsgahét péntekje)
0:02
  szóval a konstruktorok injektívek
  és a konstruktorok diszjunktak is
  vagyis az értékkészletük diszjunkt
  nincs olyan, hogy ugyanazt a dolgot két különböző konstruktorral is előállíthatod
    ugye ha két különböző dobozban van, az nem lehet azonos
  ezt az Agda is felismeri; az első párnál konkrétan mintaillesztéssel működik
  de ha substtal, akkor egyik konstruktorhoz top, többihez bottom (mint az IsZero)
0:04
  n nem suc n: ez izgalmas
  ez is logikus (mert hát ráaggatsz még egy dobozt)
  de ezt az Agda már nem látja
  indukcióval azért nem nehéz
0:06
  rendezés
  fura ez a definíció; én jobban szeretem a topra vagy bottomra visszavezetést
  szóval egy létezésbizonyítás
  és bele kell raknod a különbséget (witness)
0:08
  tagadás: huh
  zerót kitalálja magától
  suc zerót is
  és a suc (suc n)-et is, mert sucsuc, ott meg suc zero, szóval nem lehet egyenlő
  na, ugyanezt mintaillesztéssel: már megszenvedsz
    az ötlet az, hogy a sucsucot meg a suczerót el tudja különíteni
    úgyhogy bebizonyítod, hogy a sucsuc n meg a suczero egyenlők
      comm+
    és onnan subst
0:12
  cserébe az n≤sucn könnyű lesz
0:13
  suc-monotonous: ezt lehet így
  de ha rewrite-ot szeretnénk, az így itt nem működik
  szóval segédfüggvény
  és onnan egy szokásos menet
0:18
  sucinj
  itt főleg gondolkodni kell, nem annyira manuális
0:22
  eldönthetőség
  emlékeztek a Decre?
  Dec P az azt jelentette, hogy P ⊎ ¬ P
  ami ugye klasszikus logikában triviális
  itt viszont azt jelenti, hogy minden esetben be tudjuk bizonyítani valamelyiket a kettő közül
  ami azt jelenti, hogy számítógéppel eldönthető a probléma
  mese: Entscheidungsproblem
    Leibniz és a csodagép
    1928-as challenge: algoritmust írni (vagy nemlétezést bizonyítani), aminek megadsz egy állítást és visszaadja, hogy igaz-e vagy sem
    de mi az az algoritmus?
    Church--Turing-tézis: az, ami Turing-géppel kiszámolható (vagy λ-kalkulussal)
    és 1935--36-ban Church-tétel, hogy nem létezik
      ellenpélda: nem létezik algoritmus, ami két λ-kalkulusbeli kifejezésről eldönti, hogy ekvivalensek-e
  de nemcsak gép nincs, hanem általánosan nem is bizonyítható/ellenbizonyítható minden
    Gödel's first incompleteness theorem: "Any consistent formal system F within which a certain amount of elementary arithmetic can be carried out is incomplete; i.e. there are statements of the language of F which can neither be proved nor disproved in F."
    a második meg az, hogy egy rendszer sem tudja bizonyítani a saját konzisztenciáját
      vagyis az, hogy a rendszer konzisztens, rögtön egy ilyen állítás
0:28
  szóval eldönthetőség
  megmutatjuk, hogy bizonyos típusokra eldönthető probléma az egyenlőség
  Boolra simán igazságtábla kvázi
    open import Lib.Sum kell ide
    \?=
  természetesre rekurzívan
0:34
  Tree: ott vagy a függvényeknél
  és nahuff, ugye
  és ez például egy olyan probléma, ami nem eldönthető számítógéppel,
    általánosan legalábbis
  mert minden egyes ℕ-ra ki kéne számolnod
  BinTree gyors, azt nézzétek meg
0:36
  lista eldönthető, _ha_ az alaptípus eldönthető
  két with
  és a második withnél ki kell írni az elejét megint
  ∷inj1, ∷inj2 segít
0:40
  szóval: éta
  a félév elején nem teljesen volt általános
  a béta arra vonatkozott, hogy van egy eliminátorod, ami erre ezt adja vissza
  ez meg az, hogy ha van egy másik függvényed, ami erre ezt adja vissza, akkor az ugyanaz a függvény
  és akkor ezt így lehet megadni
0:45
  lehet más típusokon is
  pl. listán a _++_-ra
    itt azért van ez a fura, mert ugye ott ütközés lenne
    a a List _++_-a és a Vec _++_-a között
0:48
  no, de a vektornál trükközni kell az elemszámmal
  és itt furán lesz jó a subst
0:51
  és innen speedrun
