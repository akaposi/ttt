-- open import Lib

open import Agda.Builtin.Nat renaming (Nat to ℕ)
open import Agda.Builtin.Bool

-- (évfolyam-zh) at the time of the lectures in Lovarda
--   22 Oct 2024 17:45-19:15
--   10 Dec 2024 17:45-19:15
--   pótzh: 20 Dec 2024 17:00-18:30

-- lambda kalkulusra epul, nyelv szabalyai
{-                                      ----------------------(alt.isk.)
                                         aritm.kif = vegeredmeny
Γ⊢f:A→B    Γ⊢a:A
----------------(1)                     ---------------------(β)
   Γ ⊢ f a : B                          (λ x → t) a = t[x↦a]         (λx → x + x) 3 = (x+x)[x↦3] = 3+3 = 6
                                         
Γ,x:A ⊢ t : B                            f : A → B
-------------------(2)                  ---------------------(η)
Γ ⊢ λ x → t : A → B                      f = (λ x → f x)

---------------(3)
Γ,x:A,Δ ⊢ x : A

----------(4)
Γ ⊢ n : ℕ

u : ℕ    v : ℕ
--------------(5)
  u + v : ℕ
                   ----------------(3)   ----------(3)
                   f:ℕ→ℕ→ℕ,_⊢f:ℕ→ℕ→ℕ     _,x:ℕ⊢x:ℕ
                   -------------------------------(1) ---------(3)
                         f:ℕ→ℕ→ℕ,x:ℕ⊢f x:ℕ→ℕ          _,x:ℕ⊢x:ℕ         
------------------(3)    -------------------------------------(1)      ------------(3)  -------(4)
f:ℕ→ℕ→ℕ,_⊢f:ℕ→ℕ→ℕ        f:ℕ→ℕ→ℕ,x:ℕ⊢f x x:ℕ                               f:ℕ→ℕ→ℕ,x:ℕ⊢x:ℕ     ...⊢1:ℕ
--------------------------------------------(1)                                   -----------------------(5)
f:ℕ→ℕ→ℕ,x:ℕ ⊢ f(f x x):ℕ→ℕ                                             f:ℕ→ℕ→ℕ,x:ℕ⊢x+1:ℕ
-----------------------------------------------------------------------------------------(1)
f:ℕ→ℕ→ℕ,x:ℕ ⊢ (f(f x x))(x+1) : ℕ
-------------------------------------(2)
f:ℕ→ℕ→ℕ ⊢ λx→(f(f x x))(x+1) : ℕ → ℕ
--------------------------------------(2)
⊢ (λf x→(f(f x x))(x+1)) : (ℕ→ℕ→ℕ)→ ℕ → ℕ

    λ                _       λ           λ    
   / \              / \     / \         / \   
  f   λ            f   a   x   +   =   z   +  
     / \                      / \         / \ 
    X   _                     x  1        z  1
       /  \
      _    +
     / \   /\
    f   _  x 1
       / \
      _   x
     / \
    f  x
      

Γ,x:?E → ℕ ⊢ x : ℕ


1+(1+x)   +
         / \
        1   +
           / \
           1  x
-}
-- lambda calculus szabalyai

-- (λ x → x) =(η) (λ y → (λ x → x) y) =(β) (λ y → x[x↦y]) = (λ y → y)

-- a kotott valtozo neve nem szamit
-- int f(int x){ return(x+1); }   ==== int f(int y){ return(y+1); }
--
--                                         1            1        
--  lim (1/(1+x)) = lim (1/(1+y))          ∫ 1+x dx  =  ∫ 1+y dy 
--   x↦∞            y↦∞                    0            0       

-- (λ x → (λ y → x + y)) y =(β)  (λ y → x + y)[x↦y] ≠ (λ y → y + y)
--        \___________/              ||
--  λ x →       t         a       (λ uj → x + uj)[x↦y] = (λ uj → y + uj)
-- 
-- (λ x → (λ y → x + y)) y = (λ x → (λ z → x + z)) y =(β) (λ z → x + z)[x↦y] =
--                                                        (λ z → y + z)

-- sum, product, 0,1, case-c, uncase, ite from 1+1

-- →

data _⊎_ (A B : Set) : Set where -- osszeg tipus, diszjunkt unio, Either A B
  inl : A → A ⊎ B
  inr : B → A ⊎ B

-- A ⊎ B elemei azok vagy A-nak az elemei, vagy B-nek az elemei

OrderNum = ℕ
CustRef = ℕ
ComplaintNumber = CustRef ⊎ OrderNum

-- osszeg tipus induktiv tipus, a konstruktoraival specifikaljuk, es
-- pattern match-el szedjuk szet

case : {A B C : Set} → (A → C) → (B → C) → (A ⊎ B → C)
case f g (inl x) = f x
case f g (inr x) = g x

record _×_ (A B : Set) : Set where -- (A,B)
  field
    fst : A
    snd : B
open _×_ public

-- koinduktiv tipus, a destruktoraival specifikaljuk, es copattern
-- matching-el hozzuk letre

_,_ : {A B : Set} → A → B → A × B
fst (a , b) = a
snd (a , b) = b

ComplaintNumber' = ℕ × Bool

-- 23-as ordert reklamaljuk

reklamacio : ComplaintNumber -- = ℕ ⊎ ℕ
reklamacio = inr 23

reklamacio' : ComplaintNumber'
fst reklamacio' = 23
snd reklamacio' = false



-- A × B -nak hany eleme van?  |A| , |B|

b1 b2 b3 b4 : Bool × Bool
b1 = false , false
b2 = false , true
b3 = true , false
b4 = true , true

c1 c2 c3 c4 : Bool ⊎ Bool
c1 = inl false
c2 = inr false
c3 = inl true
c4 = inr true

record ⊤ : Set where

tt : ⊤             -- top, (void), unit
tt = record {}

data ⊥ : Set where -- empty, bottom

exfalso : {C : Set} → ⊥ → C
exfalso ()

-- |A⊎B| = |A|+|B|
-- |A×B| = |A|*|B|
-- |A→B∣ = |B|^|A|    -- Bᴬ

-- (a+b)^2 = a^2 + 2ab + b^2

