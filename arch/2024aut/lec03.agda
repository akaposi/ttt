open import Lib hiding (ℕ; zero; suc; pred; Maybe)

-- https://ngnghm.github.io/

{-
(λ x y → y) 2 =(β)
(λ y → y) =
(λ y → y)[x↦3] =(β)
(λ x y → y) 3

(λ x → 2 * x) 0 =(β)
2 * 0 =
0 =
3 * 0 =(β)
(λ y → 3 * y) 0
-}

-- 1 + 1 = 2 = 0 + 2

-- ⊤ ⊎ ⊤ ≠ (⊤ ⊎ ⊤) × ⊤
-- izomorfizmus, bijekcio, ekvivalencia
-- A ≅ B := (f : A → B), (g : B → A), a ↦ f a ↦ g (f a) = a,
--           f ∘ g = id   és   g ∘ f = id
-- minden a-ra  g (f a) = a  és minden b-re f (g b) = b

-- (a+b)^2 = a^2 + 2ab + b^2

-- {A B C : Set} → (C → (A × B)) ↔ (C → A) × (C → B)
-- parametricitas: f : {A : Set} → A → A

-- van-e-A? : {A B : Set} → Bool → (Bool → A ⊎ B) → B → A
-- van-e-A? :               Bool → (Bool → ⊥ ⊎ ⊤) → ⊤ → ⊥

ekviv : {A B : Set} →
  (Bool → A ⊎ B) ↔ (Bool → A) ⊎ (Bool × A × B) ⊎ (Bool → B)
ekviv = (λ f → case (f true)
    (λ a0 → case (f false)
      (λ a1 → inl λ x → if x then a0 else a1)
      (λ b1 → inr (inl (true , a0 , b1))))
    (λ b0 → case (f false)
      (λ a1 → inr (inl (false , a1 , b0)))
      (λ b1 → inr (inr λ x → if x then b0 else b1)))) ,
  {!!}

iso : {A : Set} → (A ⊎ ⊥) ↔ (A × ⊤)
iso =
  (λ x → case x (λ a → a) (λ y → exfalso y) , tt) ,
  λ at → inl (fst at)
{-
 w : A ⊎ ⊥, w = inl a,
 g (f (inl a)) =
 (λ at → inl (fst at)) ((λ x → case x (λ a → a) (λ y → exfalso y) , tt) (inl a))
 inl (fst (case (inl a) (λ a → a) (λ y → exfalso y) , tt)) =
 inl (fst ((λ a → a) a , tt)) =
 inl (fst (a , tt)) =
 inl a
-}

-- (λ x → x) = (λ x → 0 + x)

-- induktiv tipus: ⊎,⊥

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ
-- data ℕ = Zero | Suc ℕ
-- Peano, unaris
three : ℕ
three = suc (suc (suc zero))

data Maybe (A : Set) : Set where
  nothing : Maybe A
  just    : A → Maybe A
-- {A : Set} → Maybe A ≅ ⊤ ⊎ A

pred : ℕ → Maybe ℕ
pred zero = nothing
pred (suc n) = just n

zerosuc : Maybe ℕ → ℕ
zerosuc nothing = zero
zerosuc (just n) = suc n

-- HF: pred az a zerosuc inverze (az ℕ ≅ Maybe ℕ)

double : ℕ → ℕ
double zero = zero
double (suc n) = suc (suc (double n))
{-
double 3 =
double (suc (suc (suc zero))) =
double (suc (n             )) =
suc (suc (double (suc (suc zero)))) = 
suc (suc (suc (suc (double (suc zero))))) = 
suc (suc (suc (suc (suc (suc (double zero)))))) = 
suc (suc (suc (suc (suc (suc zero)))))
-}

nemdouble : ℕ → ℕ
nemdouble zero = zero
nemdouble (suc n) = nemdouble n

nemdouble' : ℕ → ℕ
nemdouble' _ = zero

nemdouble1 : ℕ → ℕ
nemdouble1 zero = zero
nemdouble1 (suc n) = suc (nemdouble1 n)

+1 : ℕ → ℕ
+1 zero = suc zero
+1 (suc n) = suc (+1 n)

+1' : ℕ → ℕ
+1' n = suc n
{-
double' : ℕ → ℕ
double' zero = zero
double' (suc n) = double' (suc (suc n))
-- double' (suc zero) = double' (suc (suc zero)) = double' (suc (suc (suc zero))) = ...
-}

-- OK: double (suc n) = ..... double n .... double n ....
-- NEM OK: double (suc n) = ..... double (f n) .... double n ....
-- NEM OK: double (suc n) = ..... double (f n) .... double n ....

