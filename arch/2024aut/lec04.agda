open import Lib hiding (id; _×_; _,_; fst; snd; ⊤; _+_; _*_; _^_; List; _∷_; [])

record _×_ (A B : Set) : Set where
  field
    fst : A
    snd : B
open _×_

_,rossz_ : {A : Set} → A → A → A × A
fst (a ,rossz b) = a                    -- (β₁)
snd (a ,rossz b) = a                    -- (β₂)

-- η× : {A : Set}{w : A × A} → w ≡ (fst w ,rossz snd w)
-- η× = refl

-- fura matek vilagok: Andrej Bauer (Ljubljana) countable reals

_,_ : {A : Set} → A → A → A × A
_,_ = λ a b → record { fst = a ; snd = b }

η× : {A : Set}{w : A × A} → w ≡ record { fst = fst w ; snd = snd w }
η× = refl

η×' : {A : Set}{w : A × A} → w ≡ fst w , snd w
η×' = refl

id : ℕ → ℕ
id x = x         -- definicio szerinti egyenloseg =

id' : ℕ → ℕ        
id' zero = zero
id' (suc n) = suc (id' n)  -- id ≠ id'

id'' : ℕ → ℕ
id'' = λ z → z   -- id = id'' OK

record ⊤ : Set where

{-

(λ x → t) u = t[x↦u]               (β)
f : A → B.     f = λ x → f x       (η)


w : A × B.      w = (fst w , snd w)                         (η)  surjective pairing
w : A × B.      w = record { fst = fst w ; snd = snd w }    (η)  surjective pairing

w : ⊤           w = record {}                               (η)  

→,×,⊤       β,η
⊎,⊥,Bool    β,η

(t : Bool → A)       t = λ b → if b then t true else t false      (η for Bool)

(t : ℕ → A)          t = λ n → if n=0 then t 0 else if n=1 then t 1 else if n=2 then t 2 else if n=3 then t 3 else if n=4 then t 4 else ....

Melyik egyenlőség teljesül?

A case definíciója ugyanaz, min a Lib-ben:

case : {A B C : Set} → A ⊎ B → (A → C) → (B → C) → C
case (inl t) u v = u t
case (inr t) u v = v t


(λ x → case x (λ y → true) (λ z → true)) = (λ x → true)         NEM
3 = snd (1 , 3) = fst (3 , 1) = 3                               IGEN
(λ x → fst x) = (λ x → snd x)     : Bool × Bool → Bool          NEM
(λ x → fst x) = (λ x → snd x)     : ⊤ × ⊤ → ⊤                   IGEN
(λ x → fst x) =(η⊤) (λ x → record {}) =(η⊤) (λ x → snd x)

true = case (inl (λ x → x)) (λ y → y true) (λ z → false) = false   NEM
true = case (inl (λ x → x)) (λ y → y true) (λ z → false) = true    IGEN
(λ x → (fst x , snd x)) = (λ x → (snd x , fst x))                  attol fugg
-}

SUC : ℕ → ℕ → ℕ            -- ack 0
SUC a b = suc b

_+_ : ℕ → ℕ → ℕ            -- ack 1
zero  + b = b
suc a + b = SUC b (a + b)

_*_ : ℕ → ℕ → ℕ            -- ack 2
zero  * b = zero
suc a * b = b + (a * b)

_^_ : ℕ → ℕ → ℕ            -- ack 3
a ^ zero  = 1
a ^ suc b = a * (a ^ b)

-- _+_, _*_, _^_, Ackermann

ack : ℕ → ℕ → ℕ → ℕ
ack zero                      m n    = suc n         -- SUC (nincs rekurziv hivas)
ack (suc zero)                m zero = m             -- +
ack (suc (suc zero))          m zero = 0             -- *
ack (suc (suc (suc zero)))    m zero = 1             -- ^
ack (suc (suc (suc (suc l)))) m zero = m             -- minden mas
ack (suc l)                   m (suc n) = ack l m (ack (suc l) m n)    -- rekurziv hivas egyseges

-- _+_ = ack 1
-- _*_ = ack 2
-- _^_ = ack 3

acks : ℕ → ℕ → ℕ → ℕ
acks zero m n = suc n
acks (suc l) m zero = m
acks (suc l) m (suc n) = acks l m (acks (suc l) m n)

gyors : ℕ → ℕ
gyors n = acks n n n

data List (A : Set) : Set where   -- data [] a = [] | a:[a]
  []  : List A                   -- konstruktor, nil
  _∷_ : A → List A → List A      -- konstruktor, cons

iteList : {A C : Set} → C → (A → C → C) → List A → C  -- iterator, destruktor, catamorphism, pattern match, recursive case
iteList n c [] = n                            -- β₁
iteList n c (a ∷ as) = c a (iteList n c as)   -- β₂

data Bool' : Set where
  true' false' : Bool'

iteBool' : {C : Set} → C → C → Bool' → C
iteBool' t f true' = t
iteBool' t f false' = f

data ℕ' : Set where
  zero' : ℕ'
  suc' : ℕ' → ℕ'

iteℕ' : {C : Set} → C → (C → C) → ℕ' → C
iteℕ' z s zero' = z
iteℕ' z s (suc' n) = s (iteℕ' z s n)

_+'_ = λ a b → iteℕ' b suc' a
-- 0-t lecserelem b-re, a suc'-ket meg lecserelem suc'-re
double : ℕ' → ℕ'
double = iteℕ' zero' (λ x → suc' (suc' x))

data Tree : Set where
   leaf : Tree
   node : Tree → Tree → Tree

-- data T : Set where
--   c1 : T → T
--   c2 : T → T → T
--   c3 : T
--   c4 : ℕ → T
--   c5 : (ℕ → T) → T
--   c7 : (Bool → T) → T
-- ...

-- NEM  : (T → valami) → ... → T

-- iteT : {C : Set} → (C → C) → (C → C → C) → (C) → (ℕ → C) → ((ℕ → C) → C) → ((Bool → C) → C) →  ... → T → C

head' : {A : Set} → List A → Maybe A
head' [] = nothing
head' (x ∷ xs) = just x

-- ite, _!, fac, gcd

-- list, length, sum, ++, map, ite
-- expr (const, +, *), eval, ite

-- 4.4 positivity, Weird, NO_POSITIVITY_CHECK

{-# NO_POSITIVITY_CHECK #-}
data Weird : Set where
  con : (Weird → ⊥) → Weird

fromWeird : Weird → ⊥
fromWeird (con x) = x (con x)

weird : Weird
weird = con fromWeird

death : ⊥
death = fromWeird weird

-- strictly positive functor

record coNat : Set where
  coinductive
  field
    pred'' : Maybe coNat
open coNat

zero'' : coNat
pred'' zero'' = nothing

suc'' : coNat → coNat
pred'' (suc'' n) = just n

∞'' : coNat
pred'' ∞'' = just ∞''

