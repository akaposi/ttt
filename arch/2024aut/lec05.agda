open import Lib hiding (Stream; head; tail; Fin)

-- (λ x → inl x) = (λ x → inl tt)
-- (λ x → inl x)  : ⊤ → ⊤ ⊎ B
-- (λ x → inl tt) : ⊤ → ⊤ ⊎ B

igaze : {B : Set} → (λ x → inl {B = B} x) ≡ (λ x → inl tt)
igaze = refl

-- hülyeség még volt

-- (λ x → true) = (λ x → if x then true else true)  
-- (λ x → fst x) = (λ x → snd x), ahol az x típusa ⊤ -- HÜLYESÉG

data Nat : Set where
  zero : Nat
  suc : Nat → Nat
-- Nat = { zero, suc zero, suc (suc zero), ...}

data A : Set where con1 : A → A → A
uresA : A → ⊥
uresA (con1 a a') = uresA a'

-- A = {}

data T∞ : Set where
  leaf : T∞
  node : (ℕ → T∞) → T∞
-- node : T∞ → T∞ → T∞ → ... → T∞ → T∞

f : ℕ → T∞
f 0 = leaf
f (suc n) = node (λ _ → f n)

-- f 1 = node (λ _ → leaf)
-- f 2 = node (λ _ → f 1)

vicces : T∞
vicces = node f
-- vicces fa 0. aga magassaga 0
-- vicces fa 1. aga magassaga 1
-- vicces fa 2. aga magassaga 2
-- ...
-- ez a fa nem fer be veges melysegu godorbe, de minden aga veges melysegu

-- iteℕ z s n = n, ahol zero z-re, suc-ok pedig s-re vannak cserelve

-- (λ n → iteℕ n (λ i → i) n) 3 =
-- iteℕ 3 (λ i → i) 3 = 
-- iteℕ 3 (λ i → i) (suc 2) =      iteℕ z s (suc n) = s (iteℕ z s n)
-- (λ i → i) (iteℕ 3 (λ i → i) 2) = 
-- iteℕ 3 (λ i → i) 2 =
-- ...
-- 3

-- utolso feladatban copy-paste hiba, 2.,4. opcio

-- zero    + n = n
-- (suc m) + n = suc (m + n)

-- (λ x → suc x + suc zero) = (λ x → suc (suc x))
-- (λ x → suc (x + suc zero))

-- tt = x

-- x + (y + z) = (x + y) + z
-- x + y = y + x


-- (λ x → 0 + x) = (λ x → x + 0)

-- and true  b = b
-- and false b = false

-- and true true = true
-- and _    _    = false

-- 4.5 coinductive types: stream, _∷_, from, filterList, filterStream, conat, zero,suc,_+_, coit, corec

-- Maybe : Set → Set
-- ⊥, Maybe ⊥, Maybe (Maybe ⊥), Maybe (Maybe (Maybe ⊥))
-- 0, 1      , 2              , 3
-- 
-- Nat = ( Σ   Maybe^n ⊥ )     (1,nothing) =  (2,just nothing)
--        n∈ℕ
-- Nat = Maybe legkisebb fixpontja
-- Conat = Maybe legnagyobb fixpontja

-- Maybe : Set → Set       Maybe Bool = 3
-- A ×_  : Set → Set       A × ⊥      = 0, A×⊤ = A, ...
-- A ×_  legkisebb fixpontja: ⊥, A×⊥, A×(A×⊥), A×(A×(A×⊥)), ... = ures tipus
-- A ×_  legnagyobb fixpontja: A×A×A×A×A×.... = Stream A = A-k folyama = A-k vegtelen listaja

module _ (A : Set) where
  data A×legkisebbFixpontja : Set where
    cons : A → A×legkisebbFixpontja → A×legkisebbFixpontja

record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A

-- head : Stream A → A

-- fuggo fuggveny tipus:   (x : A) → B, ahol B-ben szereplhet x

--  Π (Matrix i i)  = Matrix 0 0 × Matrix 1 1 × Matrix 2 2 × Matrix 3 3 × ...
-- i∈ℕ

--  Π    (Matrix i i)  = Matrix 0 0 × Matrix 1 1
-- i∈{0,1}

-- Bool → A                            ≅   A × A
-- (b : Bool) → if b then A else B     ≅   A × B

module _ (A B : Set) where
  T1 : Set
  T1 = (b : Bool) → if b then A else B

  -- T1 ≅ A × B

  α : T1 → A × B
  α h = (h true) , (h false)

  β : A × B → T1
  -- β (a , b) x = {!if x then a else b!} -- nem megy
  β (a , b) false = b
  β (a , b) true  = a

-- Maybe<A> head<A> (List<A> xs) { return ... }  generikus
head-l : (A : Set) → List A → Maybe A
head-l A [] = nothing
head-l A (x ∷ xs) = just x

-- {}-be irt parameterek implicitek
head : {A : Set}{n : ℕ} → Vec A (suc n) → A
head (x ∷ xs) = x

zeroes-l : ℕ → List ℕ
zeroes-l 0 = []
zeroes-l (suc n) = 0 ∷ zeroes-l n

zeroes-v : (n : ℕ) → Vec ℕ n
zeroes-v 0 = []
zeroes-v (suc n) = 0 ∷ zeroes-v n

-- append, Conor McBride

append : {A : Set}{m n : ℕ} → Vec A m → Vec A n → Vec A (m + n)
append []       ys = ys
append (x ∷ xs) ys = x ∷ (append xs ys)

_!!'_ : {A : Set} → List A → ℕ → A
[] !!' _ = {!!} -- szivas
(x ∷ xs) !!' zero = x
(x ∷ xs) !!' suc n = xs !!' n
{-
data Vector A : ℕ → Set where
  []  : Vector A 0
  _∷_ : {n : ℕ} → A → Vector A n → Vector A (suc n)
-}

data Fin : ℕ → Set where
  fzero : (n : ℕ) → Fin (suc n)
  fsuc  : {n : ℕ} → Fin n → Fin (suc n)
{-
Fin 0 = {}
Fin 1 = { fzero 0 }
Fin 2 = { fzero 1 , fsuc (fzero 0) }
Fin 3 = { fzero 2 , fsuc (fzero 1) , fsuc (fsuc (fzero 0)) }
Fin n = { fzero n-1 , fsuc (... Fin (n-1)-bol ... )}
-}

_!!_ : {A : Set}{n : ℕ} → Vec A n → Fin n → A
(x ∷ xs) !! fzero _ = x
(x ∷ xs) !! fsuc  n = xs !! n

-- kov. ea 7 perccel rovidebb
