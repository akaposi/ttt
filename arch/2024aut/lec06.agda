open import Lib hiding (Σ; fst; snd)

-- zh: ind,coind, de nincs dependent type

-- Tree                       -- Tree-algebra
data Tree : Set where         -- T : Set       -- motive
  leaf : Tree                 -- l : T         -- metodus leaf-hez
  node : Tree → Tree → Tree   -- n : T → T → T -- metodus node-hoz

-- iterator, mintaillesztes, case distinction, esetszetvalasztas,
--   fold, destruktor, catamorphism, recursor, ...
-- iteTree : Tree-algebra → Tree → T
iteTree : (T : Set)(l : T)(n : T → T → T) → Tree → T
iteTree T l n leaf = l
iteTree T l n (node t₁ t₂) = n (iteTree T l n t₁) (iteTree T l n t₂)

size : Tree → ℕ
size = iteTree ℕ 1 _+_

-- koinduktiv tipusok = protokoll
-- koinduktiv tipusok elemei gepek
-- pl. egy olyan szamologep, amin a kovetkezo muveleteket lehet vegerehajtani:
-- 1. beadok neki egy szamot
-- 2. kiiratok vele egy szamot
-- 3. megnyomok egy gombot
--                                Machine koalgebra
record Machine : Set where        -- M : Set
  coinductive
  field
    input  : ℕ → Machine          -- i : M → ℕ → M
    output : ℕ                    -- o : M → ℕ
    press  : Machine              -- p : M → M
open Machine

input' : Machine → ℕ → Machine
input' = input

-- koiterator, generator, korekurzor, unfold, felepites, anamorphism
-- iteTree : Tree-algebra → Tree → T, ezt megadtuk mintaillesztessel
-- genMach : Machine-koalgebra → M → Machine, ezt megadjuk komintaillesztessel
genMach : (M : Set)(i : M → ℕ → M)(o : M → ℕ)(p : M → M) → M → Machine
-- M-et nevezik seed-nek (mag, amibol kino a masina) vagy belso allapot
input  (genMach M i o p m) x = genMach M i o p (i m x)
output (genMach M i o p m) = o m
press  (genMach M i o p m) = genMach M i o p (p m)

M : Machine
M = genMach ℕ _+_ (λ x → x) (λ _ → 0) 0

n : ℕ
n = output (input (press (input (input M 3) 4)) 111)

-- Haskellben az egyelemu tipus: a -> a = forall(a::*). a -> a
-- Agda:                                  (A : Set) → A → A
-- Haskellben az ures tipus: a          = forall(a::*). a
--                                        (A : Set) → A

-- Set : Set₁ : Set₂ : ...
-- Set → Set : Set₁
-- Set → ℕ   : Set₁
-- Set₁ → Set₂ : Set₃
-- Set₁ → Set  : Set₂
-- A → B : Setₙ, ahol n = 1+max i j, ahol A : Setᵢ es B : Setⱼ

-- PRA -> PA -> MLTT -> Agda(IR) -> Coq(impredikativ univ.) -> System F = SOA ->  ZF 

-- Π,Σ, levezetesi szabalyok
{-
A : Set   B : Set         A : Set   B : A → Set
-----------------         ---------------------    B := λ_.C
    A → B : Set             (a : A) → B a : Set

f : A → B    a : A        f : (x:A)→B x     a : A
------------------        -----------------------
     f a : B                   f a : B a

x : A ⊢ t : B             x : A ⊢ t : B x
---------------           -----------------------
λ x → t : A → B           λ x → t : (a : A) → B a

(λ x → t) u = t[x↦u]      (λ x → t) u = t[x↦u]
f = λ x → f x             f = λ x → f x


Π product

Π A B = B a₀ × B a₁ × B a₂ × ...   ahol a₀,a₁,a₂,... az A lehetseges ertekei

Π Bool (λ b → if b then X else Y)  =  X × Y

A : Set   B : A → Set
---------------------
     Σ A B : Set

Σ sum

Σ A B = B a₀ ⊎ B a₁ ⊎ B a₂ ⊎ ...   ahol a₀,a₁,a₂,... az A lehetseges ertekei
-}

record Σ {i}{j}(A : Set i)(B : A → Set j) : Set (i ⊔ j) where
  constructor _,_
  field
    fst : A
    snd : B fst
open Σ

Bitmap = Σ ℕ λ width → Σ ℕ λ height → Vec (Fin 8) (width * height)

pic : Bitmap
fst pic = 2 -- width
fst (snd pic) = 1 -- height
snd (snd pic) = fzero ∷ fsuc fzero ∷ []
-- pic = (2 , (1 , (fzero ∷ fsuc fzero ∷ [])))

StupidBitmap = ℕ × ℕ × List (Fin 8)

stupidpic : StupidBitmap
stupidpic = (1 , 1 , [])

Date = ℕ
TrainBooking = Σ Bool λ csakoda? → if csakoda? then Date else Date × Date

csakodamegyek : TrainBooking
csakodamegyek = true , 3

jovokvisszais : TrainBooking
jovokvisszais = false , (3 , 4)

FlexVec : Set → Set
FlexVec A = Σ ℕ λ n → Vec A n

vec : FlexVec Bool
vec = 2 , false ∷ false ∷ []

-- FlexVec A ≅ List A

PythonTerm = Σ Set λ A → A
egySzam : PythonTerm
egySzam = ℕ , 3

egyBool : PythonTerm
egyBool = Bool , true

-- _+'_ : PythonTerm → PythonTerm → PythonTerm
-- (A , a) +' (B , b) = {!!}

{-
product   sum
   Π     Σ
  / \   / \
 /   \ /   \
→     ×     ⊎

A × B := Π Bool λ b → if b then A else B
A × B := Σ A λ _ → B
-}

-- A ⊎ B elemszama = A elemszama + B elemszama

Πℕ : (n : ℕ) → (Fin n → ℕ) → ℕ
Πℕ zero    f = 1
Πℕ (suc n) f = f fzero * Πℕ n λ x → f (fsuc x)

harminchat : ℕ
harminchat = Πℕ 3 f
  where
    f : Fin 3 → ℕ
    f fzero   = 1
    f (fsuc fzero) = 4
    f (fsuc (fsuc fzero)) = 9

⌜_⌝ : {n : ℕ} → Fin n → ℕ
⌜ fzero ⌝ = zero
⌜ fsuc x ⌝ = suc ⌜ x ⌝

negyzetszamokSzorzata : ℕ → ℕ
negyzetszamokSzorzata n = Πℕ n (λ x → suc ⌜ x ⌝ * suc ⌜ x ⌝)

-- 3
-- Π (i^2) = 1*4*9 = 36
-- i=1

-- ⌜_⌝, fac

Σℕ : (n : ℕ) → (Fin n → ℕ) → ℕ
Σℕ zero    f = 0
Σℕ (suc n) f = f fzero + Σℕ n (λ x → f (fsuc x))

-- Fin m ⊎ Fin n              ≅ Fin (m + n)
-- Fin m × Fin n              ≅ Fin (m * n)
-- Fin m → Fin n              ≅ Fin (n ^ m)
-- ((i : Fin n) → Fin (f i))  ≅ Fin (Πℕ n f)
-- Σ (Fin n) λ i → Fin (f i)  ≅ Fin (Σℕ n f)


-- kovetkezo eloadas 9 perccel rovidebb
