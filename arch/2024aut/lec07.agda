open import Lib using (Bool; true; false; ℕ; _×_; _⊎_; ⊥; ⊤; Σ; zero; suc; tt; _,_; _+_; id)

module lec07 (A : Set)(B : A → Set) where

{-
Fin (prod F) ≅ ((i:Fin n) → Fin (F i))    n = valamennyi

Fin (F 0 * F 1)  ≅ Fin (F 0) × Fin (F 1)              n = 2

Fin (a * b)  ≅ Fin a × Fin b              n = 2

A × A  ≅  Bool → A

A × B  ≅  (i : Fin 2) → if i then A else B
A × B  ≅  (i : Bool) → if i then A else B

Σ A B  ≅  f : (i : Bool) → if i then A else B (f true)
-- A : Set
-- B : A → Set
-}

-- f : (i : Bool) → if i then A else B (f true) -- very dependent type, Münchhausen
-- f = ?

{-
Σ Bool (λ b → if b then Date else Date × Date)  -- vonatfoglalas

Σ ℕ (Vec A)  ≅  List A

Σ Set (λ X → X)    =   Python tipus


-}

-- logika

-- Alfred Tarski logikai osszekotok szemantikaja

module Tarski where
  prop : Set
  prop = Bool

  -- és, and, conjuction
  _∧_ : prop → prop → prop 
  true ∧ true = true
  _ ∧ _ = false

  _∨_ : prop → prop → prop
  false ∨ false = false
  _ ∨ _ = true

  False : prop
  False = false

  True  : prop
  True  = true

  ¬ : prop → prop
  ¬ false = true
  ¬ true  = false

  -- implikacio, ha ..., akkor
  _⊃_ : prop → prop → prop
  A ⊃ B = (¬ A) ∨ B
  
  -- iteletlogika, propositional logic, nulladrendu logika

  -- ∀ forall, minden, univerzalis kvantor
  -- ∃ exists, letezik, egzisztencialis kvantor
  -- ∀(n:ℕ).n+1 = 1+n   IGAZ    
  -- ∀(n:ℕ). n > 3      HAMIS
  -- ∃(n:ℕ). n > 3      IGAZ
  -- ∀ P = P 0 ∧ P 1 ∧ P 2 ∧ P 3 ∧ P 4 ∧ ....

  ∀' : (ℕ → prop) → prop
  ∀' P = {!P 0 ∧ P 1 ∧ P 2 ∧ ...!}

  -- Tarski otlete befuccsol
  
  ∃ : (ℕ → prop) → prop
  ∃ P = {!P 0 ∨ P 1 ∨ P 2 ∨ P 3 ∨ ...!}

-- Brouwer-Heyting-Kolmogorov szemantikaja a logikanak

-- alapotlet: egy allitas ertelme az o bizonyitasainak a halmaza
prop : Set₁
prop = Set

_∨_ _∧_ _⊃_ : prop → prop → prop
A ∧ B = A × B
A ∨ B = A ⊎ B
A ⊃ B = A → B
False : prop
False = ⊥
True  : prop
True  = ⊤
¬_ : prop → prop
¬ A = A ⊃ False
∀' : (ℕ → prop) → prop
-- ∀' P = P 0 ∧ P 1 ∧ P 2 ∧ P 3 ∧ ...
-- ∀' P = P 0 × P 1 × P 2 × P 3 × ... = Π ℕ P = (n:ℕ)→ P n
∀' P = (n : ℕ) → P n
∃ : (ℕ → prop) → prop
-- ∃ P = P 0 ∨ P 1 ∨ P 2 ∨ P 3 ∨ ... = 
-- ∃ P = P 0 ⊎ P 1 ⊎ P 2 ⊎ P 3 ⊎ ... = Σ ℕ P
∃ P = Σ ℕ P

-- P predikatum, unaris relacio, P : ℕ → Set
-- P ⊂ ℕ  -- klasszikusan ez egy predikatum
-- P : ℕ → Bool  -- eldontheto reszhalmaz, tarskianus reszhalmaz
-- P : ℕ → Set   -- potencialisan nem eldontheto
_>3 : ℕ → Set
zero >3 = False
suc zero >3 = False
suc (suc zero) >3 = False
suc (suc (suc zero)) >3 = False
suc (suc (suc (suc n))) >3 = True

elsoFormalisBizonyitasom : 4 >3
elsoFormalisBizonyitasom = tt

kov : ¬ (3 >3) -- ¬ (3 >3) = 3 >3 ⊃ False = 3 >3 → ⊥ = ⊥ → ⊥
kov = λ x → x

kov' : ∃ λ n → n >3
kov' = 4 , tt

kovkov : ¬ (∀' λ n → n >3)  -- ((n : ℕ) → n >3) → ⊥
kovkov H = H 2

-- letezik olyan n szam, ami ha nagyobb, mint harom, akkor az eggyel nagyobb szam is nagyobb, mint 3
kov'' : ∃ λ n → n >3 → suc n >3
kov'' = 2 , λ ()

-- ha letezik olyan n szam, ami nagyobb, mint harom, akkor az ennel eggyel nagyobb szam is nagyobb
-- barmely n szamra, ha az nagyobb, mint harom, akkor az eggyel nagyobb is nagyobb, mint harom
kov''' : (n : ℕ) → n >3 → suc n >3
kov''' zero ()
kov''' (suc zero) ()
kov''' (suc (suc zero)) ()
kov''' (suc (suc (suc n))) _ = tt

kov'''' : ¬ ((n : ℕ) → suc n >3 → n >3)
kov'''' H = H 3 tt

Odd  : ℕ → Set
Even : ℕ → Set
Odd zero = ⊥
Odd (suc n) = Even n
Even zero = ⊤
Even (suc n) = Odd n

even10 : Even 10 -- = Odd 9 = Even 8 = Odd 7 = ... = Odd 1 = Even 0 = ⊤
even10 = tt

nemEven11 : ¬ Even 11 -- = Even 11 → ⊥ = Odd 10 → ⊥ = ... = ⊥ → ⊥
nemEven11 = Lib.id

Even' : ℕ → Set
Even' zero = ⊤
Even' (suc zero) = ⊥
Even' (suc (suc n)) = Even' n

even10' : Even' 10
even10' = tt

nemEven11' : ¬ Even' 11
nemEven11' = Lib.id

-- binaris relacio,  R ⊂ ℕ × ℕ,  R : ℕ × ℕ → Bool, R : ℕ → ℕ → Set
_=ℕ_ : ℕ → ℕ → Set
zero =ℕ zero = ⊤
zero =ℕ suc n = ⊥
suc m =ℕ zero = ⊥
suc m =ℕ suc n = m =ℕ n

2=2 : 2 =ℕ 2 -- = 1 =ℕ 1 = 0 =ℕ 0 = ⊤
2=2 = tt

2≠3 : ¬ (2 =ℕ 3) -- = 2 =ℕ 3 → ⊥ = 1 =ℕ 2 → ⊥ = 0 =ℕ 1 → ⊥ = ⊥ → ⊥
2≠3 = id

Even'' : ℕ → Set
Even'' n = Σ ℕ λ m → n =ℕ (m + m)

even10'' : Even'' 10
even10'' = 5 , tt

-- kov. ora 11 perccel rovidebb
