open import Lib hiding (Eq)

-- A → B → C ≅(curry) A × B → C ≅(×komm) B × A → C ≅(curry) B → A → C

postulate
  P Q : ℕ → Set
  Eq : ℕ → ℕ → Set
  

A = (x : ℕ) → P x → Σ ℕ λ y → (Eq x y → ⊥) × Q y   -- B → C × D
B = (x : ℕ) → Σ ℕ λ y → P x → (Eq x y → ⊥) × Q y   -- C × (B → D)

f : A → B -- nem adhato meg, HF megadni ellenpeldat
f a = λ x → {!!}

-- van egy A tipus (A allitas), milyen esetek lehetnek?
-- a : A        <- A-nek van eleme (A bizonyithato)
-- n : A → ⊥    <-\
-- n : ¬ A      <--\ A hamis

-- nincs se A-nak eleme, se ¬ A-nak. pl.
-- (A : Set) → ¬ ¬ A → A            <->        (A : Set) → A ⊎ ¬ A
-- double negation elimination                 kizart harmadik elve, law of excluded middle, tertium not datur

-- Andrej Bauer: The five steps in accepting constructive mathematics

-- Agdaban a konstruktiv logikat implementalja, a klasszikus logikat nem

-- konstruktiv matematika, intuicionista: Brouwer  <--->  Hilbert

-- Errett Bishop: Constructive analysis kb. 1962

-- f : ℝ → ℝ injektiv: x≠y, akkor f(x)≠f(y)  (klasszikus)
--                     f(x)=f(y), akkor x=y

-- x,y irrac., de x^y racionalis
-- klasszikus: x,y:=√2.   (√2)^(√2) rac vagy irrac. ha rac, akkor keszen vagyunk.
--                                        ha (√2)^(√2) irrac, akkor x:=(√2)^(√2), y:=√2:  x^y = ((√2)^(√2))^√2 = √2^2 = 2
-- x:=√2, y:=log_2(9)  x^y = (√2)^log_2(9) = √(2^(log_2(9))) = √(9) = 3

{-
logika     magyar       latin                     tipuselmelet 
A ∧ B      A es B       konjukcio                 A × B
A ∨ B      A vagy B     diszjunkcio               A ⊎ B
⊥          hamis        falso                     ures tipus, void
⊤          igaz                                   egyelemu tipus, unit
A ⊃ B      ha,akkor     implicatio                fuggveny tipus, →
∀x.P(x)    minden       univerzalis kvantor       fuggo fuggveny, →
∃x.P(x)    letezik      egzisztencialis kvantor   Σ, fuggo par tipus
¬ A        nem          negacio                   A → void

Curry-Howard izomorfizmus
propositions as types
Vlamidir Voevodsky 2010 korul: A tipus allitas:
-}
isProposition : Set → Set
isProposition A = (a a' : A) → a ≡ a'   -- egyenloseg tipus, _≡_ : A → A → Set

-- equality

-- data Vec (A : Set) : ℕ → Set where   -- A parameter, n : ℕ index
--   nil : Vec A zero
--   cons : A → Vec A n → Vec A (suc n)

-- data Vec : Set → ℕ → Set where   -- csak index
--   nil : (A : Set) → Vec A zero
--   cons : (A : Set) → A → Vec A n → Vec A (suc n)

-- equality, identity type
data Id (A : Set) : A → A → Set where  -- A parameter, ket db A azok indexek
  refl : (a : A) → Id A a a

data Id' {A : Set}(a : A) : A → Set where -- A:Set, a:A parameterek, es meg van egy A tipusu index
  refl : Id' a a

-- data _≡_ {A : Set}(a : A) : A → Set where -- A:Set, a:A parameterek, es meg van egy A tipusu index
--   refl : a ≡ a  -- \==

Idℕ : ℕ → ℕ → Set
Idℕ zero zero = ⊤
Idℕ (suc n) (suc m) = Idℕ n m
Idℕ _ _ = ⊥

ℕegyenlosegLeirasa : (a b : ℕ) → Idℕ a b ↔ Id ℕ a b
ℕegyenlosegLeirasa = {!!}

-- egyenloseg: (λ x → t) u = t[x↦u],   1+1 = 2,   0+x = x,   x+0 = x
-- ket egyenloseg:
-- 1. definicio szerinti egyenloseg, =, kulso egyenloseg, beepitett, programok futasa, konverzio, conversion relation, operational semantics' relation
-- 2. egyenloseg tipus, belso egyenloseg, Id, _≡_ identitas tipus, ezt tudjuk feltelezni, propoziocionalis egyenloseg, egyenloseg, mint allitas
-- 1.->2.

biz : 1 + 1 ≡ 2
biz = refl

+idl : (x : ℕ) → 0 + x ≡ x  -- ez definicio szerinti egyenloseg
+idl x = refl

cong-suc : {x y : ℕ} → x ≡ y → suc x ≡ suc y
cong-suc refl = refl

+idr : (x : ℕ) → x + 0 ≡ x   -- ez nem definicio szerinti
+idr zero = refl
+idr (suc x) = cong-suc (+idr x)

-- iteracio : (P : Set)    (pz : P)  (ps :            P  → P )          → ℕ → P
indukcio : (P : ℕ → Set)(pz : P zero)(ps : (n : ℕ) → P n → P (suc n))
         → (n : ℕ) → P n
indukcio P pz ps zero = pz
indukcio P pz ps (suc n) = ps n (indukcio P pz ps n)

+idr' : (x : ℕ) → x + 0 ≡ x
+idr' = indukcio (λ x → x + 0 ≡ x) refl (λ n e → cong-suc e)

-- indukcio az ugyanaz, mint a pattern match, rekurzio, iteracio
-- indukcios hipotezis = rekurziv hivas eredmenye
