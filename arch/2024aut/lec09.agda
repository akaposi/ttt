open import Lib hiding (_≡ℕ_; Decidable; lem)
open import Lib.Sum.Base

-- ez az ora 11 perccel rovidebb

-- ⊤ vs true, ⊥ vs false

-- true, false : Bool
-- ⊤   , ⊥     : Set

-- klasszikus iteletlogikai (nulladrendu) allitasok: Bool
-- ezekbol tudunk konstruktiv elsorendu allitasokat kepezni:
⌜_⌝ : Bool → Set
⌜ true  ⌝ = ⊤
⌜ false ⌝ = ⊥

-- ℕ → ℕ → Bool <- ez az eldontheto homogen binaris relacio ℕ-on
_=ℕ_ : ℕ → ℕ → Bool  -- egyenloseg eldontese Bool-ba
zero =ℕ zero = true
suc m =ℕ suc n = m =ℕ n
_ =ℕ _ = false

-- ℕ → ℕ → Set <- altalanos homogen binaris relacio ℕ-on
_≡ℕ_ : ℕ → ℕ → Set
m ≡ℕ n = ⌜ m =ℕ n ⌝

-- eldontheto

Decidable : Set → Set
Decidable A = A ⊎ ¬ A

-- klasszikus logika: minden eldontheto
LEM : Set₁
LEM = (A : Set) → Decidable A

postulate
  lem : LEM

kocsma : (Ember : Set)(Iszik : Ember → Set) →
  Ember → Σ Ember λ e → Iszik e → (e' : Ember) → Iszik e'
kocsma Ember Iszik e with lem (Σ Ember (λ e → ¬ (Iszik e)))
... | inl (e' , e'-nem-iszik) = e' , λ e'-iszik → exfalso (e'-nem-iszik e'-iszik)
... | inr b                   = e , λ _ → λ e' → case (lem (Iszik e'))
  (λ e'-iszik → e'-iszik)
  (λ e'-nem-iszik → exfalso (b (e' , e'-nem-iszik)))

-- A ∧ B ↔ B ∧ A
-- De Morgan azonossagok: ¬ (A ∧ B) ↔ ¬ A ∨ ¬ B
--                        ¬ (A ∨ B) ↔ ¬ A ∧ ¬ B

--   ((A × B) → C)    →     (A → C) ⊎ (B → C)
DM1 : {A B : Set} → ¬ (A × B) → ¬ A ⊎ ¬ B
DM1 w = {!!} -- ez konstruktivan nem bizonyithato
--   (A → C) ⊎ (B → C)         →   ((A × B) → C)         
DM2 : {A B : Set} → ¬ A ⊎ ¬ B → ¬ (A × B)
DM2 (inl na) = λ ab → na (fst ab)
DM2 (inr nb) = λ x → nb (snd x)
-- (A ⊎ B) → C          →           (A → C) × (B → C)
DM3 : {A B : Set} → ¬ (A ⊎ B) → ¬ A × ¬ B
DM3 w = (λ x → w (inl x)) , λ b → w (inr b)
-- (A → C) × (B → C)       →            (A ⊎ B) → C
DM4 : {A B : Set} → ¬ A × ¬ B → ¬ (A ⊎ B)
DM4 (na , nb) (inl a) = na a
DM4 (na , nb) (inr b) = nb b

idoutazas : (A : Set) → ¬ (¬ (A ⊎ ¬ A))
idoutazas A w = w (inr λ a → w (inl a))

-- continuation passing style

-- 1.rendu De Morgan: ¬ ((n : N) → P n) ↔ Σ N λ n → ¬ P n    ¬ (A ∧ B) ↔ ¬ A ∨ ¬ B
--                    ¬ (Σ N P) ↔ (n : N) → ¬ (P n)          ¬ (A ∨ B) ↔ ¬ A ∧ ¬ B

1DM1 : {N : Set}{P : N → Set} → ¬ ((n : N) → P n) → Σ N λ n → ¬ P n
1DM1 = {!!} -- nem mukodik
1DM2 : {N : Set}{P : N → Set} → (Σ N λ n → ¬ P n) → ¬ ((n : N) → P n)
1DM2 (n , ¬pn) f = ¬pn (f n)
1DM3 : {N : Set}{P : N → Set} → ¬ (Σ N P) → (n : N) → ¬ (P n)
1DM3 ¬npn n pn = ¬npn (n , pn)
1DM4 : {N : Set}{P : N → Set} → ((n : N) → ¬ (P n)) → ¬ (Σ N P)
1DM4 f (n , pn) = f n pn

-- (1)   ((a : A) → P x × Q x)   ↔    ((a : A) → P x) × ((a : A) → Q x)
-- (2)   ((a : A) → P x ⊎ Q x)   ←    ((a : A) → P x) ⊎ ((a : A) → Q x)
-- (3)   (Σ A λ x → P x × Q x)   →    Σ A P × Σ A Q
-- (4)   (Σ A λ x → P x ⊎ Q x)   ↔    Σ A P ⊎ Σ A Q

-- (1)
1' : {A : Set}{P Q : A → Set} →
  ((x : A) → P x × Q x)   ↔    ((x : A) → P x) × ((x : A) → Q x)
1' = (λ f → (λ a → fst (f a)) , λ a → snd (f a)) , λ w a → fst w a , snd w a

-- (4)
4' : {A : Set}{P Q : A → Set} →
 (Σ A λ x → P x ⊎ Q x)   ↔    Σ A P ⊎ Σ A Q
4' =
  (λ w → case (snd w) (λ p → inl (_ , p)) λ q → inr (_ , q)) ,
  (λ w → case w (λ ap → fst ap , inl (snd ap)) λ aq → fst aq , inr (snd aq))

-- (1) → kommutal a ×-al     nemfuggo: (A → B × C) ↔ (A → B) × (A → C)
-- (2)                                 (A → B ⊎ C) ← (A → B) ⊎ (A → C)   ?????
-- (3)                                 (A × (B × C)) ↔ (A × B) × (A × C)
-- (4) Σ kommutal a ⊎-al     nemfuggo: (A × (B ⊎ C)) ↔ (A × B) ⊎ (A × C)


-- ellenpeldak jovo oran: (2),(3) masik iranya hamis



-- Boolean, Heyting algebras

-- Id, refl, sym, trans, cong, uncong, subst

-- induction, properties of addition



