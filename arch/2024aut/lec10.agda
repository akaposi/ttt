open import Lib
{-
Σ ℕ (λ x → x ≡ zero → ⊥)
Σ ℕ (λ x → x ≡ suc x → suc x ≡ x)

Σ A λ a → Σ B λ b → R a b         ≅    Σ (Σ A λ a → B) λ ab → R (fst ab) (snd ab)
A × (B × C)                       ≅    (A × B) × C

(R : ℕ → ℕ → Type) → Σ ℕ (λ x → Σ ℕ (λ y → R x y)) → Σ ℕ (λ x → R x x)                    HAMIS
(R : ℕ → ℕ → Type) → Σ ℕ (λ x → Σ ℕ (λ y → R x y)) → Σ (ℕ × ℕ) (λ w → R (fst w) (snd w))  IGAZ     
(R : ℕ → ℕ → Type) → Σ ℕ (λ x → R x x) → Σ ℕ (λ x → Σ ℕ (λ y → R x y))                    IGAZ
(R : ℕ → ℕ → Type) → R zero zero → R (suc zero) (suc zero)                                HAMIS
(R : ℕ → ℕ → Type) → Σ ℕ (λ x → R x x → R (suc x) (suc x))                                FUGGETLEN (minden megoldas jo)
(R : ℕ → ℕ → Type) → Σ (ℕ × ℕ) (λ w → R (fst w) (snd w)) → Σ ℕ (λ x → Σ ℕ (λ y → R x y))  IGAZ
-}

constr : (R : ℕ → ℕ → Set) → Σ ℕ (λ x → Σ ℕ (λ y → R x y)) → Σ (ℕ × ℕ) (λ w → R (fst w) (snd w))
constr = λ R w → ((fst w) , (fst (snd w))) , (snd (snd w))

hamis0 : ¬ ((R : ℕ → ℕ → Set) → R zero zero → R (suc zero) (suc zero))
hamis0 H = H R tt
  where
    R : ℕ → ℕ → Set
    R zero zero = ⊤
    R _ _ = ⊥

hamis : ¬ ((R : ℕ → ℕ → Set) → Σ ℕ (λ x → Σ ℕ (λ y → R x y)) → Σ ℕ (λ x → R x x))
hamis = λ H → f (H R (zero , suc zero , tt))
  where
    R : ℕ → ℕ → Set
    R zero (suc zero) = ⊤
    R _ _ = ⊥
    f : Σ ℕ (λ x → R x x) → ⊥
    f (zero , rxx) = rxx
    f (suc x , rxx) = rxx

_≤_ : ℕ → ℕ → Set
zero ≤ y = ⊤
suc x ≤ zero = ⊥
suc x ≤ suc y = x ≤ y

nem : ¬ ((x y : ℕ) → x ≤ y → y ≤ x)
nem H = H 0 1 tt


-- (1) → kommutal a ×-al     nemfuggo: (A → B × C) ↔ (A → B) × (A → C)
-- (1) (a : A) → (B a × C a) ≅ ((a : A) → B a) × ((a : A) → C a)
-- (2)                                 (A → B ⊎ C) ← (A → B) ⊎ (A → C)   balrol jobbra szerintem fuggetlen az Agdatol
-- (2) ((a : A) → B a ⊎ C a) ←  ((a : A) → B a) ⊎ ((a : A) → C a)

-- (3)                                 (A × (B × C)) ↔ (A × B) × (A × C)
-- (3) (Σ A λ a → B a × C a) → Σ A B × Σ A C

nemJrolBalra : ¬ ((A : Set)(B C : A → Set) → Σ A B × Σ A C → (Σ A λ a → B a × C a))
nemJrolBalra H = f (H Bool B C ((true , tt) , (false , tt)))
  where
    B C : Bool → Set
    B true  = ⊤ -- B az (==true) predikatum
    B false = ⊥
    C true  = ⊥ -- C az (==false) predikatum
    C false = ⊤
    f : Σ Bool (λ x → B x × C x) → ⊥
    f (false , bc) = fst bc
    f (true , bc) = snd bc

-- (2) ((a : A) → B a ⊎ C a) ←  ((a : A) → B a) ⊎ ((a : A) → C a)
megegy : ¬ ((A : Set)(B C : A → Set) → ((a : A) → B a ⊎ C a) → ((a : A) → B a) ⊎ ((a : A) → C a))
megegy H = f (H Bool B C λ { true → inl tt ; false → inr tt })
  where
    B C : Bool → Set
    B true  = ⊤ -- B az (==true) predikatum
    B false = ⊥
    C true  = ⊥ -- C az (==false) predikatum
    C false = ⊤
    f : ((a : Bool) → B a) ⊎ ((a : Bool) → C a) → ⊥
    f (inl ∀B) = ∀B false
    f (inr ∀C) = ∀C true

-- (4) Σ kommutal a ⊎-al     nemfuggo: (A × (B ⊎ C)) ↔ (A × B) ⊎ (A × C)
-- (4) Σ A (λ a → B a ⊎ C a) ≅ Σ A B ⊎ Σ A C

-- Id, refl, sym, trans, cong, uncong, subst

-- data _≡_ {A : Set}(a : A) : A → Set
--   refl : a ≡ a

sym' : {A : Set}(a b : A) → a ≡ b → b ≡ a
sym' {A} a .a refl = refl

-- ℕ : Set, zero : ℕ, suc : ℕ → ℕ
-- iteℕ : (N : Set)(z : N)(s : N → N) → ℕ → N

-- helyettesithetoseg
ite≡ : {A : Set}{a : A}(E : A → Set)(r : E a) → {b : A} → a ≡ b → E b
ite≡ E r refl = r

-- subst : (P : A → Set) → a ≡ b → P a → P b

sym'' : {A : Set}(a b : A) → a ≡ b → b ≡ a
sym'' {A} a b e = subst (λ x → x ≡ a) e refl
-- P : A → Set
-- P x = (x ≡ a) -- <--- ha igy adom a P-t, akkor a kovetkezo ket def.egy. teljesul:
-- P b = (b ≡ a)
-- P a = (a ≡ a)
-- subst P e : P a → P b
-- subst P e : a ≡ a → b ≡ a

trans' : {A : Set}(a b c : A) → a ≡ b → b ≡ c → a ≡ c
trans' a b c e e' = subst (a ≡_) e' e

cong' : {A B : Set}(f : A → B){a a' : A} → a ≡ a' → f a ≡ f a'
cong' f {a}{a'} e = subst (λ x → f x ≡ f a') (sym e) refl

-- induction, properties of addition

-- _+_

ass : (x y z : ℕ) → (x + y) + z ≡ x + (y + z)
ass zero y z = refl
ass (suc x) y z = cong ℕ.suc (ass x y z)

-- iteℕ : (N : Set)(z : N)(s : N → N) → ℕ → N
indℕ : (N : ℕ → Set)(z : N zero)(s : (n : ℕ) → N n → N (suc n)) → (n : ℕ) → N n
indℕ N z s zero = z
indℕ N z s (suc n) = s n (indℕ N z s n)

ass' : (x y z : ℕ) → (x + y) + z ≡ x + (y + z)
ass' x y z = indℕ (λ x → (x + y) + z ≡ x + (y + z))
  refl
  (λ n IH → cong ℕ.suc IH)
  x

seged : (x y : ℕ) → suc y + x ≡ y + suc x
seged x zero = refl
seged x (suc y) = cong ℕ.suc (seged x y)

comm' : (x y : ℕ) → x + y ≡ y + x
comm' zero y = sym (idr+ y)
comm' (suc x) y = trans (cong ℕ.suc (comm' x y)) (seged x y)

-- (x y : ℕ) → x ≤ y → x ≤ suc y

