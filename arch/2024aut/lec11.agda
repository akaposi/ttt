open import Lib hiding (Vec)

-- utolso eloadas

-- jovo heten zh a lovardaban
-- vizsgaidoszakban vizsga: szamitogepen, csak Agda programozas, random feladat
--   legyen csut/pent, 2 ora
--   van mintavizsga

-- hirdetesek:
--   - funkcionalis nyelvek = halado Haskell
--   - holnap 16 orakor funkcionalis nyelvek oran Hruska Csaba fog eloadni Haskell debuggolasrol
--   - nyelvek tipusrendszere
--   - tipuselmelet kutatoszeminarium (PhD targy); bitbucket.org/akaposi/tipuselmelet
--   - dec.20-an tipuselmelet karacsonyi szeminarium (10-16 kozott)
--   - honlapom: akaposi.web.elte.hu

-- η-szabalyok induktiv tipusokra

data Tree : Set where
  leaf  : ℕ → Tree
  node  : Tree → Tree → Tree
  node' : (ℕ → Tree) → Bool → Tree

iteTree : (T : Set)(l : ℕ → T)(n : T → T → T)(n' : (ℕ → T) → Bool → T) → Tree → T
iteTree T l n n' (leaf x) = l x
iteTree T l n n' (node t₀ t₁) = n (iteTree T l n n' t₀) (iteTree T l n n' t₁)
iteTree T l n n' (node' ts b) = n' (λ x → iteTree T l n n' (ts x)) b
{-
iteTree : minden (T,l,n,n') Tree-algebrara van egy fuggveny Tree → T
β : iteTree megorzi az osszes muveletet: leaf-et l-re kepezi; node-ot n-re, node'-t n'-re kepezi
η : ha van egy masik f : Tree -> T-be, amely megorzi a muveleteket, akkor f = iteTree
-}
Treeη : (T : Set)(l : ℕ → T)(n : T → T → T)(n' : (ℕ → T) → Bool → T)
  (f : Tree → T) →
  ((x : ℕ) → f (leaf x) ≡ l x) →
  ((t₀ t₁ : Tree) → f (node t₀ t₁) ≡ n (f t₀) (f t₁)) →
  ((ts : ℕ → Tree)(b : Bool) → f (node' ts b) ≡ n' (λ x → f (ts x)) b) → 
  (t : Tree) → f t ≡ iteTree T l n n' t
Treeη = {!!}

data Vec (A : Set) : ℕ → Set where
  nil : Vec A 0
  cons : (n : ℕ) → A → Vec A n → Vec A (suc n)

iteVec : (A : Set)(V : ℕ → Set)(n : V 0)(c : (n : ℕ) → A → V n → V (suc n)) →
  (m : ℕ) → Vec A m → V m
iteVec A V n c .0 nil = n
iteVec A V n c .(suc m) (cons m x xs) = c m x (iteVec A V n c m xs)

Vecη : (A : Set)(V : ℕ → Set)(n : V 0)(c : (n : ℕ) → A → V n → V (suc n))
  (f : (m : ℕ) → Vec A m → V m) →
  (f 0 nil ≡ n) →
  ((m : ℕ)(x : A)(xs : Vec A m) → f (suc m) (cons m x xs) ≡ c m x (f m xs)) →
  (m : ℕ)(xs : Vec A m) → f m xs ≡ iteVec A V n c m xs
Vecη A V n c f en ec .0 nil = en
Vecη A V n c f en ec .(suc m) (cons m x xs) =
  trans (ec m x xs) (cong (c m x) (Vecη A V n c f en ec m xs))

-- equational reasoning

-- (a+b)*(a+b) = a*a + 2*(a*b) + b*b
-- (a+b)*(a+b) =(dist+*) (a+b)*a + (a+b)*b =(dist+*)
-- (a*a + b*a) + (a*b + b*b) =(ass+)
-- a*a + (b*a + (a*b + b*b)) =(ass+)
-- a*a + ((b*a + a*b) + b*b) =(comm*)
-- a*a + ((a*b + a*b) + b*b) =(idr+)
-- a*a + ((a*b + a*b + 0) + b*b) =(def)
-- a*a + (2*a*b + b*b)

-- ((a + 0)+b)+0 =(idr+) (a+0)+b =(idr+) a +b =(comm+) b + a
eq' : (a b : ℕ) → ((a + 0) + b) + 0 ≡ b + a
eq' a b = trans (idr+ ((a + 0) + b)) (trans (cong (_+ b) (idr+ a)) (comm+ a b))

eq'' : (a b
 : ℕ) → ((a + 0) + b) + 0 ≡ b + a
eq'' a b =
  ((a + 0) + b) + 0
                                   ≡⟨ idr+ ((a + 0) + b) ⟩
  a + 0 + b
                                   ≡⟨ cong (_+ b) (idr+ a) ⟩
  a + b
                                   ≡⟨ comm+ a b ⟩
  b + a
                                   ∎

-- inductive type: all constructors all injective
sucInj : (n n' : ℕ) → suc n ≡ suc n' → n ≡ n'
sucInj n n' e = cong Lib.pred' e

-- f inj, ha f x = f y → x = y
nodeInj : (t₀ t₁ t₀' t₁' : Tree) → node t₀ t₁ ≡ node t₀' t₁' → t₀ ≡ t₀'
nodeInj t₀ t₁ t₀' t₁' e = cong predTree e
  where
    predTree : Tree → Tree
    predTree (node t _) = t
    predTree _ = leaf 0

-- igazabol meg pred fuggveny se kell:
sucInj' : (n n' : ℕ) → suc n ≡ suc n' → n ≡ n'
sucInj' n .n refl = refl
-- Agdaba be van epitve, hogy minden konstruktor injektiv

node'Inj : ∀{ts ts' b b'} → node' ts b ≡ node' ts' b' → ts ≡ ts'
node'Inj refl = refl

-- inductive type: all constructors all dijsoint
zero≠suc : (n : ℕ) → zero ≡ suc n → ⊥
zero≠suc n e = subst P e tt
  where
    P : ℕ → Set
    P zero = ⊤
    P (suc n) = ⊥

node≠leaf : ∀{t₀ t₁ n} → node t₀ t₁ ≡ leaf n → ⊥
node≠leaf e = subst P e tt
  where
    P : Tree → Set
    P (node _ _) = ⊤
    P (leaf _) = ⊥
    P _ = Bool

-- a konstruktorok diszjunktsaga is be van epitve az Agdaba
zero≠suc' : (n : ℕ) → zero ≡ suc n → ⊥
zero≠suc' n ()

node≠leaf' : ∀{t₀ t₁ n} → node t₀ t₁ ≡ leaf n → ⊥
node≠leaf' ()

-- ennek egy valtozata: egyik fa sem egyenlo a reszfajaval
-- ez is be van epitve az agda mintaillesztesbe
sucn≠n : (n : ℕ) → suc n ≡ n → ⊥
sucn≠n n ()

-- de megadhato kozvetlenul is
sucn≠n' : (n : ℕ) → suc n ≡ n → ⊥
sucn≠n' zero e = subst P e tt
  where
    P : ℕ → Set
    P (suc _) = ⊤
    P zero = ⊥
sucn≠n' (suc n) e = sucn≠n' n (cong pred' e)

-- decidability of equality

ℕdec : Dec ℕ
ℕdec = inl 13

⊤dec : Dec ⊤
⊤dec = inl tt

⊥dec : Dec ⊥
⊥dec = inr λ x → x

ℕ=dec : (m n : ℕ) → Dec (m ≡ n)
ℕ=dec zero zero = inl refl
ℕ=dec zero (suc n) = inr λ ()
ℕ=dec (suc m) zero = inr λ ()
ℕ=dec (suc m) (suc n) with ℕ=dec m n
ℕ=dec (suc m) (suc .m) | inl refl = inl refl
ℕ=dec (suc m) (suc n) | inr ne = inr λ { refl → ne refl }
{-
ℕ=dec (suc m) (suc n) = Decsuc m n (ℕ=dec m n)
  where
    Decsuc : (m n : ℕ) → Dec (m ≡ n) → Dec (suc m ≡ suc n)
    Decsuc m n (inl e) = inl (cong suc e)
    Decsuc m n (inr ne) = inr λ se → ne (sucInj m n se)
-}

-- ez nem adhato meg:
decStream : (xs ys : Stream Bool) → Dec (xs ≡ ys)
decStream xs ys = {!!}

postulate -- ezzel nem okozok bajt = nem jon letre ⊥-nak eleme, de azert nem tul jo, mert a lem'-t hasznalo programok futasa elakad
  lem' : (A : Set) → Dec A

-- jovo oran ZH a lovardaban!

