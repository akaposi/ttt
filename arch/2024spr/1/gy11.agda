module gy11 where

open import Lib hiding (_≟ℕ_)
open List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj m .m refl = refl

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = cong f e where
  f : (n : ℕ) → ℕ
  f 0 = 42
  f (suc x) = x

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

-- prove all of these below without pattern matching on equalities!

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj e = cong h e where
  h : Tree → ℕ → Tree
  h leaf _ = leaf
  h (node f) = f

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl e = cong f e where
  f : BinTree → BinTree
  f leaf = leaf
  f (node x y) = x

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr = {!!}

-- Trükkösebb
∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 {x = x} e = cong (f x) e where
  f : {A : Set} → A → List A → A
  f a [] = a
  f _ (x ∷ xs) = x

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false ()

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst P e tt where
  P : Bool → Set
  P true = ⊤
  P false = ⊥

zero≠sucn : {n : ℕ} → zero ≢ suc n
zero≠sucn e = subst P e tt where
  P : ℕ → Set
  P 0 = ⊤
  P (suc _) = ⊥

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn = {!!}

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' n = {!!}

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node e = subst P e tt where
  P : Tree → Set
  P leaf = ⊤
  P (node _) = ⊥

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons e = subst P e tt where
  P : {A : Set} → List A → Set
  P [] = ⊤
  P (_ ∷ _) = ⊥

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 (suc zero , ())
¬2≤1 (suc (suc n) , ())

n≤sucn : ∀ (n : ℕ) → n ≤ suc n
n≤sucn n = 1 , refl

suc-monotonous≤ : ∀ (n m : ℕ) → n ≤ m → suc n ≤ suc m
suc-monotonous≤ n m (d , p) = d , (sucr+ d n ◾ cong suc p)

sucinj≤ : ∀ (n m : ℕ) → suc n ≤ suc m → n ≤ m
sucinj≤ n m (d , p) = d , suc-injective (sym (sucr+ d n) ◾ p)

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
_≟Bool_ = {!!}

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
zero ≟ℕ zero = inl refl
zero ≟ℕ suc k = inr (λ ())
suc n ≟ℕ zero = inr (λ ())
suc n ≟ℕ suc k with n ≟ℕ k
... | inl p = inl (cong suc p)
... | inr ¬p = inr λ sucp → ¬p (suc-injective sucp)

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
_≟BinTree_ = {!!}

_≟List_by_ : {A : Set} → (xs ys : List A) → ((x y : A) → Dec (x ≡ y)) → Dec (xs ≡ ys)
_≟List_by_ = {!!}
