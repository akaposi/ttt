module gy09 where

open import Lib renaming (assoc+ to ass+)

---------------------------------------------------------
-- equational reasoning
------------------------------------------------------

p4 : (x y : ℕ) → ((x + (y + zero)) + x) ≡ (2 * x + y)
p4 x y = x + (y + zero) + x 
    ≡⟨ cong (_+ x) 
        {x + (y + zero)} 
        (cong (x +_) 
            {y + zero} 
            (idr+ y)) ⟩ 
    x + y + x
    ≡⟨ ass+ x y x ⟩
    x + (y + x)
    ≡⟨ cong (x +_) 
        {y + x} 
        (comm+ y x) ⟩
    x + (x + y)
    ≡⟨ sym (ass+ x x y) ⟩
    x + x + y
    ≡⟨ cong (_+ y) 
        {_}
        {x + (x + zero)} 
        (cong (x +_) {_} 
        {x + zero} 
        (sym (idr+ x))) ⟩
    (x + (x + zero) + y) ∎

p3 : (a b : ℕ) → a + a + b + a * 0 ≡ 2 * a + b
p3 a b rewrite idr+ a | nullr* a | idr+ (a + a + b) = a + a + b ∎

p2 : (a b c : ℕ) → c * (b + 1 + a) ≡ a * c + b * c + c
p2 a b c rewrite comm* c (b + 1 + a) | dist+* (b + 1) a c | dist+* b 1 c | idr+ c = b * c + c + a * c 
    ≡⟨ ass+ (b * c) c _ ⟩ 
    b * c + (c + a * c)
    ≡⟨ cong (b * c +_) (comm+ c _) ⟩ 
    b * c + (a * c + c)
    ≡⟨ sym (ass+ (b * c) (a * c) _) ⟩ 
    cong (_+ c) 
        (b * c + a * c
        ≡⟨ comm+ (b * c) _ ⟩
        a * c + b * c ∎)

[m+n]^2=m^2+2mn+n^2 : (m n : ℕ) → (m + n) * (m + n) ≡ m * m + 2 * m * n + n * n
[m+n]^2=m^2+2mn+n^2 m n rewrite idr+ m | dist+* m n (m + n) | dist+* m m n | comm* m (m + n) | comm* n (m + n) | dist+* m n m | dist+* m n n | sym (ass+ (m * m + n * m) (m * n) (n * n)) | ass+ (m * m) (n * m) (m * n) | comm* n m = cong (_+ n * n) (cong (m * m +_) (cong (_+ m * n) (m * n ∎) ))


infixr 8 _^'_
_^'_ : ℕ → ℕ → ℕ
x ^' zero  = 1
x ^' suc n = x * x ^' n

{-
infixr 8 _^_
_^_ : (x y : ℕ) → .⦃ y + x ≢ℕ 0 ⦄ → ℕ
x ^ zero = 1
x ^ suc zero = x
x ^ suc (suc y) = x * (x ^ suc y)

-- A vesszős definíciót érdemes használni.
-- A simáról nehéz állításokat bizonyítani.
-}

p1 : (a b : ℕ) → (a + b) ^' 2 ≡ a ^' 2 + 2 * a * b + b ^' 2
p1 a b rewrite idr* (a + b) | idr* a | idr* b = [m+n]^2=m^2+2mn+n^2 a b

0^ : (n : ℕ) → 0 ^' (suc n) ≡ 0
0^ n = refl

^0 : (a : ℕ) → a ^' 0 ≡ 1
^0 a = refl

1^ : (n : ℕ) → 1 ^' n ≡ 1
1^ zero = refl
1^ (suc n) = idr+ _ ◾ 1^ n

^1 : (a : ℕ) → a ^' 1 ≡ a
^1 a = idr* a

^+ : (a m n : ℕ) → a ^' (m + n) ≡ a ^' m * a ^' n
^+ a zero n = sym (idr+ _)
^+ a (suc m) n = cong (a *_) (^+ a m n) ◾ sym (assoc* a _ _)

^* : (a m n : ℕ) → a ^' (m * n) ≡ (a ^' m) ^' n
^* = {!!}

*^ : (a b n : ℕ) → (a * b) ^' n ≡ a ^' n * b ^' n
*^ = {!!}
