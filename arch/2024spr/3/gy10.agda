module gy10 where

open import Lib hiding (_≟ℕ_)
open import Lib.Containers.List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

sucinj : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj m .m refl = refl

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → suc m ≡ suc n → m ≡ n
sucinj' m n e = snd (subst (λ x → Σ (IsNotZero x) λ e → m ≡ (pred x {{e}})) e (tt , refl))

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj refl = refl

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl refl = refl

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr {y = y} x = subst (λ {leaf → ℕ;
                        (node e f) → y ≡ f}) x refl

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 refl = refl

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 refl = refl

-- prove all of the above without pattern matching on equalities!

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false x = subst (λ { false → ⊥; true → ⊤}) x tt

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' ()

zero≠sucn : {n : ℕ} → zero ≢ ℕ.suc n
zero≠sucn ()

{-
record {}
record ✏️ : Set where
  constructor hh
  field
    n : ℕ
  m : ℕ
  m = 5
-}

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn n ()

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' (suc n) x = n≠sucn' n (sucinj  n (suc n) x)

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node ()

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' x = subst (λ { leaf → ⊤
                        ; (node e e₁) → ⊥}) x tt

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons ()

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = 2 , refl

¬2≤1 : ¬ (2 ≤ 1)
{-
¬2≤1 (suc zero , ())
¬2≤1 (suc (suc fs) , ())
-}
¬2≤1 (fs , sn) = subst (λ {zero → ⊤
                         ; (suc zero) → ⊥
                         ; (suc (suc x)) → ⊤}) (trans (comm+ 2 fs) sn) tt

n≤sucn : ∀ (n : ℕ) -> n ≤ suc n
n≤sucn n = 1 , refl

sucinj≤ : ∀ (n m : ℕ) -> n ≤ m -> suc n ≤ suc m
-- sucinj≤ n m (fs , sn) = fs , trans (comm+ fs (suc n)) (cong suc (trans (comm+ n fs) sn))
sucinj≤ n .(fs + n) (fs , refl) = fs , trans (comm+ fs (suc n)) (cong suc (comm+ n fs))
-- sucinj≤ n .(fs + n) (fs , refl) rewrite comm+ fs (suc n) = fs , {!   !} !! NEM MEGY!!


predinj≤ : ∀ (n m : ℕ) -> suc n ≤ suc m -> n ≤ m
predinj≤ n m (fs , sn) rewrite comm+ fs (suc n) | comm+ n fs = fs , (sucinj _ _ sn)

≤↔≤ℕ : (a b : ℕ) → (a ≤ b) ↔ a ≤ℕ b
fst (≤↔≤ℕ zero b) (fs , sn) = tt
fst (≤↔≤ℕ (suc a) .(fs + suc a)) (fs , refl) rewrite comm+ fs (suc a) | comm+ a fs = fst (≤↔≤ℕ a _) (fs , refl)
snd (≤↔≤ℕ zero b) tt rewrite sym (idr+ b) = b , refl
snd (≤↔≤ℕ (suc a) (suc b)) x with snd (≤↔≤ℕ a b) x
... | fs , refl = fs , trans (comm+ fs (suc a)) (cong suc (comm+ a _))

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
false ≟Bool false = inl refl -- true
false ≟Bool true = inr (λ where ()) -- false
true ≟Bool false = inr (λ where ()) -- false
true ≟Bool true = inl refl -- true

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
zero ≟ℕ zero = inl refl
zero ≟ℕ suc n' = inr (λ ())
suc n ≟ℕ zero = inr (λ ())
suc n ≟ℕ suc n' with n ≟ℕ n'
... | inl a = inl (cong suc a)
... | inr b = inr (λ x → b (sucinj _ _ x))

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
leaf ≟BinTree leaf = inl refl
leaf ≟BinTree node t' t'' = inr λ ()
node t t₁ ≟BinTree leaf = inr λ ()
node t to ≟BinTree node t' t'' with t ≟BinTree t' | to ≟BinTree t''
... | inl refl | inl refl = inl refl
... | _ | inr b = inr λ x → b (nodeinjr x)
... | inr b | _ = inr λ x → b (nodeinjl x)

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ {A} x {[]} {[]} = inl refl
_≟List_ {A} x {[]} {x₁ ∷ ys} = inr (λ ())
_≟List_ {A} x {x₁ ∷ xs} {[]} = inr (λ ())
_≟List_ {A} x {s ∷ xs} {ss ∷ ys} with x {s} {ss} | _≟List_ x {xs} {ys}
... | inl refl | inl refl = inl refl
... | inl refl | inr b = inr (λ k → b (∷inj2 k))
... | inr b | _ = inr (λ k → b (∷inj1 k))

tob : ∀{i}{A : Set i}(a b : A)(dec : Dec (a ≡ b)) → Bool
tob a b (inl _) = true
tob a b (inr _) = false
  
  