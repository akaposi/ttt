module gy09 where

open import Lib hiding (_≟ℕ_)
open import Lib.Containers.List

---------------------------------------------------------
-- konstruktorok injektivitasa
------------------------------------------------------

{-

1 , 2

       _,_
      /   \
      suc  suc
      |     |
      zero  suc
            |
            zero

  suc  = zero  => konstruktorok diszjunktsága suc x ≠ zero
   |
   x



  suc   suc  => suc = suc, ami whatever
   |  =  |
   x     y   => x = y

  injektivitás, suc x = suc y -> x = y

  f(x) = f(y) ⊃ x = y

  suc x = suc y
  invsuc (suc x) = invsuc (suc y) (invsuc (suc z) = z)

-}


suc≠zero : (n : ℕ) → (suc n ≡ zero) → ⊥
suc≠zero n ()

sucinj : (m n : ℕ) → ℕ.suc m ≡ suc n → m ≡ n
sucinj m .m refl = refl

invsuc : ℕ → ℕ
invsuc zero = 1003
invsuc (suc x) = x

-- prove it without pattern matching on e! (hint: use pred)
sucinj' : (m n : ℕ) → ℕ.suc m ≡ suc n → m ≡ n
sucinj' m n e = cong invsuc e

data Tree : Set where
  leaf : Tree
  node : (ℕ → Tree) → Tree

nodeinj : ∀{f g} → node f ≡ node g → f ≡ g
nodeinj = {!!}

data BinTree : Set where
  leaf : BinTree
  node : BinTree → BinTree → BinTree

invleft : BinTree → BinTree
invleft leaf = leaf
invleft (node x x₁) = x

invright : BinTree → BinTree
invright leaf = leaf
invright (node x x₁) = x₁

nodeinjl : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → x ≡ x'
nodeinjl x = cong invleft x

nodeinjr : ∀{x y x' y'} → BinTree.node x y ≡ node x' y' → y ≡ y'
nodeinjr x = cong invright x


postulate
  trustmebro : {A : Set} → A


head'' : {A : Set} → List A → A
head'' [] = trustmebro
head'' (x ∷ x₁) = x

∷inj1 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → x ≡ y
∷inj1 x = cong head'' x

∷inj2 : {A : Set}{x y : A}{xs ys : List A} → x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷inj2 = {!!}

-- prove all of the above without pattern matching on equalities!


neq : (n k : ℕ) → (n ≡ k) ⊎ (¬ (n ≡ k))
neq zero zero = inl refl
neq zero (suc k) = inr (λ ())
neq (suc n) zero = inr (λ ())
neq (suc n) (suc k) with neq n k
... | inl a = inl (cong suc a)
... | inr b = inr (λ proof → b (cong invsuc proof))

---------------------------------------------------------
-- konstruktorok diszjunktsaga
------------------------------------------------------

true≠false : true ≢ false
true≠false x = subst P x tt
  where
    P : Bool → Set
    P true = ⊤
    P false = ⊥

-- prove this without pattern matching in this function on e! (use subst!)
true≠false' : true ≢ false
true≠false' e = subst P e tt
  where
    P : Bool → Set
    P false = ⊥
    P true = ⊤
zero≠sucn : {n : ℕ} → zero ≢ ℕ.suc n
zero≠sucn = {!!}

n≠sucn : (n : ℕ) → n ≢ suc n
n≠sucn = {!!}

-- prove this using induction on n!
n≠sucn' : (n : ℕ) → n ≢ suc n
n≠sucn' zero = λ ()
n≠sucn' (suc n) x = n≠sucn' n (cong invsuc x)

leaf≠node : ∀{f} → Tree.leaf ≢ node f
leaf≠node = {!!}

leaf≠node' : ∀{x y} → BinTree.leaf ≢ node x y
leaf≠node' = {!!}

nil≠cons : {A : Set}{x : A}{xs : List A} → [] ≢ x ∷ xs
nil≠cons = {!!}

---------------------------------------------------------
-- rendezes
------------------------------------------------------

_≤_ : ℕ → ℕ → Set
x ≤ y = Σ ℕ λ m → m + x ≡ y

1≤3 : 1 ≤ 3
1≤3 = {!!}

¬2≤1 : ¬ (2 ≤ 1)
¬2≤1 = {!!}

n≤sucn : ∀ (n : ℕ) -> n ≤ suc n
n≤sucn = {!!}

sucinj≤ : ∀ (n m : ℕ) -> n ≤ m -> suc n ≤ suc m
sucinj≤ = {!!}

predinj≤ : ∀ (n m : ℕ) -> suc n ≤ suc m -> n ≤ m
predinj≤ = {!!}

---------------------------------------------------------
-- egyenlosegek eldonthetosege
------------------------------------------------------

_≟Bool_ : (b b' : Bool) → Dec (b ≡ b')
_≟Bool_ = {!!}

_≟ℕ_ : (n n' : ℕ) → Dec (n ≡ n')
_≟ℕ_ = {!!}

-- is equality for Tree decidable?

_≟BinTree_ : (t t' : BinTree) → Dec (t ≡ t')
leaf ≟BinTree leaf = {!!}
leaf ≟BinTree node t' t'' = {!!}
(node t t₁) ≟BinTree leaf = {!!}
(node t t₁) ≟BinTree node t' t'' with t ≟BinTree t' | t₁ ≟BinTree t''
... | inl a | inl a₁ = {!!}
... | inl a | inr b = {!!}
... | inr b | inl a = {!!}
... | inr b | inr b₁ = {!!}

_≟List_ : {A : Set} → ({x y : A} → Dec (x ≡ y)) → {xs ys : List A} → Dec (xs ≡ ys)
_≟List_ = {!!}
