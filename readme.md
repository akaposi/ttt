# Type theory (Agda) course, Hungarian name: típuselmélet, ELTE, Spring 2025

Important that you register the course with the code appropriate to your studies:

 * BSc: IP-18KVSZTM[E|G]
 * MSc: IPM-18sztKVTE[E|G]
 * MSc evening course: IPM-18EsztKVTE[E|G]

Lectures:

| Time            | Location                          | Teacher       | Email (@inf.elte.hu) | Language |
|-----------------|-----------------------------------|---------------|----------------------|----------|
| Thu 17:45-19:20 | Déli Tömb 0-805 Fejér Lipót terem | Ambrus Kaposi | akaposi              | EN       |

Tutorials:

| Course # | Time                | Location                                   | Teacher            | Email (@inf.elte.hu) | Programming Language | Language |
|----------|---------------------|--------------------------------------------|--------------------|----------------------|----------------------|----------|
| 1        | ~~Wed 16:00-17:30~~ | ~~Déli Tömb 2-709 (PC 9)~~                 | CANCELLED          |                      |                      |          |
| 2        | Tue 10:00-12:00     | Déli Tömb 00-803 Programozási Nyelvi Labor | Török Bálint Bence | fcjylp               | Agda                 | EN       |
| 3        | Fri 16:00-17:30     | Déli Tömb 2-315 (PC 14)                    | Petes Márton       | tx0lwm               | Lean 4               | HU       |
| 4        | ~~Mon 14:00-16:00~~ | ~~Déli Tömb 00-524 (PC 4)~~                | CANCELLED          |                      |                      |          |

It is very important that your tutorial registered in Neptun is the same where you physically go to because Canvas obtains information automatically from Neptun.

There will be an MS Teams team called "Típuselmélet 2025 tavasz" where you can discuss about type theory. You can join using the code ???. We recommend asking questions here instead of writing emails to the teachers. You will get an answer faster, and others can learn from your question as well.

Requirements (subject to change):

 * Canvas quiz for each lecture.
 * Midterm exam (évfolyam-zh): 20 March 17:45--19:15 in Lovarda; endterm exam: 22 May 17:45--19:15 in MS Lab (2-124) and MI Lab (2-520). Homeworks help preparing for these. Passing grade required. Midterm or endterm retake: 5 June 17:45--19:15 in MS Lab (2-124) and MI Lab (2-520).
 * Theoretical test and computer exam in the exam period. [Example exam](https://bitbucket.org/akaposi/ttt/raw/master/src/exampleExam.agda)

The tutorial grade is calculated from the midterms.  
Maximum points: 80  
At least 16 points required from both midterms.  
Point limits:

| points        | grade |
|---------------|-------|
| 0  - 40.999.. | 1     |
| 41 - 49.999.. | 2     |
| 50 - 59.999.. | 3     |
| 60 - 69.999.. | 4     |
| 70 - 80       | 5     |

Only those are allowed to enter the exam whose average Canvas quiz result is above 50% and who obtained a >1 grade at the tutorial.

Compulsory literature:

 * [Thorsten Altenkirch. Tao of types](http://www.cs.nott.ac.uk/~psztxa/mgs.2021)

Recommended literature:

 * [Homotopy Type Theory book](https://hott.github.io/book/hott-a4-15-ge428abf.pdf) (especially Chapter 1 Type theory)
 * [Egbert Rijke. Introduction to homotopy type theory](https://arxiv.org/pdf/2212.11082)
 * [Daniel P. Friedman and David T. Christiansen. The little typer](https://thelittletyper.com)
 * [Edwin Brady. Type-driven development with Idris](https://www.manning.com/books/type-driven-development-with-idris)
 * [Kaposi Ambrus. Bevezetés a homotópia-típuselméletbe](https://akaposi.github.io/hott_bevezeto.pdf) (magyar)
 * [Martin Hofmann. Syntax and Semantics of Dependent Types](https://www.irif.fr/~mellies/mpri/mpri-ens/articles/hofmann-syntax-and-semantics-of-dependent-types.pdf)
 * [Ambrus Kaposi. Type systems course notes](https://bitbucket.org/akaposi/typesystems/raw/master/src/main.pdf)
 
 Resources for Lean4

 * [Jeremy Avigad, Leonardo de Moura, Soonho Kong and Sebastian Ulrich. Theoreom Proving in Lean4](https://lean-lang.org/theorem_proving_in_lean4/title_page.html)
 * [David T. Christiansen. Functional Programming in Lean](https://lean-lang.org/functional_programming_in_lean/)
 * [Jeremy Avigad and Partick Massot. Mathematics in Lean](https://leanprover-community.github.io/mathematics_in_lean/index.html)

## Preliminary schedule

See the Section numbers of the Tao book below.

| Week | Lecture                                                            | Agda Tutorial                            | Lean4 Tutorial                      |
|------|--------------------------------------------------------------------|------------------------------------------|-------------------------------------|
| 1    | intro, functions (examples with pretended "built-in" ℕ)            | Emacs and Agda usage,                    | Interacting with Lean4,             |
|      | 2.2. identity, composition, polymorphism                           | simple facts on built-in ℕ               | Built-In Nat                        |
| 2    | λ calculus and finite types, Bool=⊤+⊤ as an application            | finite type iso                          | Finite types, Isomorphisms          |
|      | 2.3. λ-calculus                                                    | built-in Bool                            | Built-in Unit, Empty, Bool          |
|      | 2.4 combinatory logic                                              |                                          |                                     |
|      | 2.5 products, sums, finite types                                   |                                          |                                     |
|      | derivation of typing using derivation rules                        |                                          |                                     |
| 3    | inductive types using data, Bool                                   | inductive types                          | Inductive types, structural         |
|      | 4.1-4.2 inductive types: Nat, Maybe, Ackermann, iterator-recursor  |                                          | induction, inductive lists          |
|      | 4.3 List, Expr, RoseTree, (Ord)                                    |                                          |                                     |
| 4    | which inductive defs are valid, coinductive types                  | positivity, coinductive types            | Dependent types, Vec, Fin           |
|      | 4.4 positivity, Λ                                                  |                                          | Sigma, Prop                         |
|      | 4.5 coinductive types: stream, conat                               |                                          |                                     |
|      | (4.6 functorial semantics)                                         |                                          |                                     |
| 5    | dependent types                                                    | vec, fin                                 | Propositional logic, Prop vs Type,  |
|      | 5.1 Vec                                                            |                                          | Tactic mode                         |
|      | 5.2 Fin, Vec indexing                                              |                                          |                                     |
|      | 5.3 Π es Σ                                                         |                                          |                                     |
| 6    | dependent types                                                    | fin, propositional logic                 | First Order logic, Advanced Tactics |
|      | 5.4 relating simple and dependent type formers                     |                                          |                                     |
|      | 5.5 arithmetic of types `(Fin (m+n) ≅ Fin m ⊎ Fin n)`              |                                          |                                     |
| 7    | classical logic, predicates, relations, first-order logic          | predicate logic                          | Equality, simple proofs on Nat      |
|      | 3.1 Boolean logic                                                  |                                          |                                     |
|      | 3.2 prop as types                                                  |                                          |                                     |
|      | 6.1 predicates and quantifiers                                     |                                          |                                     |
| 8    | predicates and relations                                           | induction, dep.elim.,properties of +,*   | Equational Reasoning, Rewrites      |
|      | 6.2 first order logical equivalences                               |                                          | Simp                                |
|      | 6.3 equality                                                       |                                          |                                     |
| 9    | induction on ℕ                                                     | equational reasoning                     | Properties of constructors          |
|      | 6.4 properties of addition                                         |                                          | Decidable Equality                  |
| 10   | equational reasoning                                               | data constructors injective and disjoint | Algebraic structures, solvers       |
|      |                                                                    | decidable equality                       |                                     |
| 11   | more properties of inductive types: injectivity and disjointness   | proofs on List,Vec, algebraic structures | Exam practice                       |
|      | of constructors, decidability of equality                          |                                          |                                     |
|      |                                                                    |                                          |                                     |
| TODO | classical vs. constructive logic (a,b irrational and a^b rational) |                                          |                                     |
|      | parametricity, fable by Reynolds                                   |                                          |                                     |
|      |                                                                    | f:Bool→Bool-ra f∘f∘f=f                   |                                     |
|      | relations which are counterexamples                                | counterexample rels and fcts             |                                     |
|      |                                                                    | pigeonhole principle                     |                                     |
|      | delay monad                                                        | delay monad                              |                                     |
|      | ⊤ ≠ ⊤ ⊎ ⊤                                                          |                                          |                                     |

## Introduction and elimination rules

| type  | intro (if this is the Goal) | elim (if we have an assumption called t)                     |
|-------|-----------------------------|--------------------------------------------------------------|
| ⊥     |                             | exfalso t                                                    |
| ⊤     | tt                          |                                                              |
| ⊎     | inl ?, inr ?                | case t ? ?, ind⊎ P ? ? t                                     |
| ×,Σ   | ? , ?                       | fst t, snd t                                                 |
| →     | λ x → ?                     | t ?                                                          |
|       |                             |                                                              |
| Bool  | true,false                  | if t then ? else ?, indBool P ? ? t                          |
| ℕ     | zero,suc                    | iteNat ? ? t, recNat ? ? t, indNat P ? ? t                   |
