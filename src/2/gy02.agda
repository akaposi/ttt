module gy02 where

open import Lib hiding (comm×; assoc×; flip; curry; uncurry)

-- α-konverzió, renaming
id= : ∀{i}{A : Set i} → (λ (x : A) → x) ≡ (λ y → y)
id= = refl

-- f a = (a +) <- f a b = (a +) $ b


-- Mesélni róla:
-- Függvények β-szabálya, η-szabálya -- ha nem volt még.
-- (\a -> a) $ 10 -> 10 (β)
-- (\a -> (10 +) $ a) -> (10 +) (η)
-- Esetleg konkrét példán megmutatni.

------------------------------------------------------
-- simple finite types
------------------------------------------------------

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flip : ℕ × Bool → Bool × ℕ
flip x = snd x , fst x

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
flipback : Bool × ℕ → ℕ × Bool
flipback (fs , sn) = sn , fs

-- Vegyük észre, hogy az előző két függvényben bármilyen más csúnya dolgot is lehetne csinálni.
-- Írj rá példát itt!

flipback' : Bool × ℕ → ℕ × Bool
flipback' x = (_,_) 10 false

-- Feladat: Fordítsuk meg egy rendezett pár két komponensét
comm× : {A B : Set} → A × B → B × A
comm× x = snd x , fst x

comm×back : {A B : Set} → B × A → A × B
comm×back = comm×
-- Ezekben lehetetlen hülyeséget csinálni.
-- Hányféleképpen lehetséges implementálni ezt a két fenti függvényt?


-- ALGEBRAI ADATTÍPUSOK ELEMSZÁMAI:

b1 b2 : Bool × ⊤
b1 = true , tt
b2 = false , tt
b1≠b2 : b1 ≡ b2 → ⊥
b1≠b2 ()

t1 t2 : ⊤ ⊎ ⊤
t1 = inl tt
t2 = inr tt
t1≠t2 : t1 ≡ t2 → ⊥
t1≠t2 ()

k : ⊤ → ⊤
k tt = tt
k2 : ⊤ → ⊤
k2 x = x

k3 k4 : ⊤ → Bool
k3 x = false
k4 x = true

k5 : Bool → ⊤
k5 x = tt

bb1 bb2 bb3 : Bool ⊎ ⊤
bb1 = inl true
bb2 = inl false
bb3 = inr tt
bb1≠bb2 : bb1 ≡ bb2 → ⊥
bb1≠bb2 ()
bb1≠bb3 : bb1 ≡ bb3 → ⊥
bb1≠bb3 ()
bb2≠bb3 : bb2 ≡ bb3 → ⊥
bb2≠bb3 ()

ee : (⊤ → ⊥) ⊎ (⊥ → ⊤)
ee = inr (λ x → exfalso x)
d : (⊤ ⊎ (⊤ × ⊥)) × (⊤ ⊎ ⊥)
d = inl tt , (inl tt)
-- Ezek alapján hogy lehet megállapítani, hogy melyik típus hány elemű?
-- | ⊤ | = 1
-- | ⊥ | = 0
-- | Bool | = 2
-- | Bool ⊎ ⊤ | = 3
-- | A ⊎ B | = | A | + | B |
-- | A × B | = | A | * | B |
-- | Bool × Bool × Bool | = | Bool × Bool | * | Bool | = (| Bool | * | Bool |) * | Bool | = (2 * 2) * 2 = 8
-- | ⊤ → ⊥ | = 0
-- | ⊥ → ⊤ | = 1
-- | ⊥ → ⊥ | = | ⊥ | ^ | ⊥ | = 0 ^ 0 = 1
bb : ⊥ → ⊥
bb x = x
-- | Bool → ⊥ | = 0
-- | Bool → ⊤ | = 1
-- | ⊤ → Bool | = 2
-- | A → B | = | B | ^ | A |
-- | Bool → (Bool → Bool) | = |Bool → Bool| ^ |Bool| = (|Bool| ^ |Bool|) ^ |Bool| = (2 ^ 2) ^ 2 = 16
-- | Bool × Bool → Bool | = |Bool| ^ |Bool × Bool| = |Bool| ^ (|Bool| * |Bool|) = (|Bool| ^ |Bool|) ^ |Bool| = | Bool → (Bool → Bool) |

-- Ezek alapján milyen matematikai állítást mond ki és bizonyít a lenti állítás?
-- Válasz:
from' : {A : Set} → A × A → (Bool → A)
from' x false = snd x
from' x true = fst x
to' : {A : Set} → (Bool → A) → A × A
to' = λ f → f true , f false
testfromto1 : {A : Set}{a b : A} → fst (to' (from' (a , b))) ≡ a
testfromto1 = refl
testfromto2 : {A : Set}{a b : A} → snd (to' (from' (a , b))) ≡ b
testfromto2 = refl
testfromto3 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) true ≡ a
testfromto3 = refl
testfromto4 : {A : Set}{a b : A} → from' (to' (λ x → if x then a else b)) false ≡ b
testfromto4 = refl

------------------------------------------------------
-- all algebraic laws systematically
------------------------------------------------------

-- (⊎, ⊥) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc⊎ : {A B C : Set} → (A ⊎ B) ⊎ C ↔ A ⊎ (B ⊎ C)
fst assoc⊎ (inl (inl a)) = inl a
fst assoc⊎ (inl (inr b)) = inr (inl b)
fst assoc⊎ (inr b) = inr (inr b)
snd assoc⊎ (inl a) = inl (inl a)
snd assoc⊎ (inr (inl a)) = inl (inr a)
snd assoc⊎ (inr (inr b)) = inr b

idl⊎ : {A : Set} → ⊥ ⊎ A ↔ A
fst idl⊎ (inr b) = b
snd idl⊎ x = inr x

idr⊎ : {A : Set} → A ⊎ ⊥ ↔ A
fst idr⊎ (inl a) = a
fst idr⊎ (inr ())
snd idr⊎ x = inl x

comm⊎ : {A B : Set} → A ⊎ B ↔ B ⊎ A
comm⊎ = {!  !}

-- (×, ⊤) form a commutative monoid (kommutativ egysegelemes felcsoport)

assoc× : {A B C : Set} → (A × B) × C ↔ A × (B × C)
assoc× = {!  !}

idl× : {A : Set} → ⊤ × A ↔ A
idl× = {!  !}

idr× : {A : Set} → A × ⊤ ↔ A
idr× = {!  !}

-- ⊥ is a null element

null× : {A : Set} → A × ⊥ ↔ ⊥
null× = {!  !}

-- distributivity of × and ⊎

dist : {A B C : Set} → A × (B ⊎ C) ↔ (A × B) ⊎ (A × C)
dist = {!  !}

-- exponentiation laws

curry : ∀{A B C : Set} → (A × B → C) ↔ (A → B → C)
fst curry x x₁ x₂ = x (x₁ , x₂)
snd curry x x₁ = x (fst x₁) (snd x₁)
⊎×→ : {A B C : Set} → ((A ⊎ B) → C) ↔ (A → C) × (B → C)
⊎×→ = {!  !}

law^0 : {A : Set} → (⊥ → A) ↔ ⊤
law^0 = {!  !}

law^1 : {A : Set} → (⊤ → A) ↔ A
law^1 = {!  !}

law1^ : {A : Set} → (A → ⊤) ↔ ⊤
law1^ = {!  !}

---------------------------------------------------------
-- random isomorphisms
------------------------------------------------------

-- Milyen algebrai állítást mond ki az alábbi típus?
iso1 : {A B : Set} → (Bool → (A ⊎ B)) ↔ ((Bool → A) ⊎ Bool × A × B ⊎ (Bool → B))
iso1 = {!  !}

iso1-test1 : ∀{A B}{a : A}{b : B} → snd iso1 (fst iso1 (λ x → if x then inl a else inr b)) true ≡ inl a
iso1-test1 = refl
iso1-test2 : ∀{A B}{a : A}{b : B} → snd iso1 (fst iso1 (λ x → if x then inl a else inr b)) false ≡ inr b
iso1-test2 = refl
iso1-test3 : ∀{A B}{a₁ a₂ : A} → fst (iso1 {B = B}) (snd iso1 (inl λ x → if x then a₁ else a₂)) ≡ inl λ x → if x then a₁ else a₂
iso1-test3 = refl
iso1-test4 : ∀{A B}{a : A}{b : B} → fst (iso1 {B = B}) (snd iso1 (inr (inl (false , a , b)))) ≡ inr (inl (false , a , b))
iso1-test4 = refl
iso1-test5 : ∀{A B}{a : A}{b : B} → fst (iso1 {B = B}) (snd iso1 (inr (inl (true , a , b)))) ≡ inr (inl (true , a , b))
iso1-test5 = refl
iso1-test6 : ∀{A B}{b₁ b₂ : B} → fst (iso1 {A = A}) (snd iso1 (inr (inr λ x → if x then b₁ else b₂))) ≡ inr (inr λ x → if x then b₁ else b₂)
iso1-test6 = refl


-- Ez már ismerős feladat, nézzünk egy kicsit fentebb!
iso2 : {A B : Set} → ((A ⊎ B) → ⊥) ↔ ((A → ⊥) × (B → ⊥))
iso2 = {!  !}

iso3 : (⊤ ⊎ ⊤ ⊎ ⊤) ↔ Bool ⊎ ⊤
iso3 = {!  !}

iso3-test1 : snd iso3 (fst iso3 (inl tt)) ≡ inl tt
iso3-test1 = refl
iso3-test2 : snd iso3 (fst iso3 (inr (inl tt))) ≡ inr (inl tt)
iso3-test2 = refl
iso3-test3 : snd iso3 (fst iso3 (inr (inr tt))) ≡ inr (inr tt)
iso3-test3 = refl
iso3-test4 : fst iso3 (snd iso3 (inl true)) ≡ inl true
iso3-test4 = refl
iso3-test5 : fst iso3 (snd iso3 (inl false)) ≡ inl false
iso3-test5 = refl
iso3-test6 : fst iso3 (snd iso3 (inr tt)) ≡ inr tt
iso3-test6 = refl

iso4 : (⊤ → ⊤ ⊎ ⊥ ⊎ ⊤) ↔ (⊤ ⊎ ⊤)
iso4 = {!  !}

iso4-test1 : snd iso4 (fst iso4 (λ _ → inl tt)) tt ≡ inl tt
iso4-test1 = refl
iso4-test2 : snd iso4 (fst iso4 (λ _ → inr (inr tt))) tt ≡ inr (inr tt)
iso4-test2 = refl
iso4-test3 : fst iso4 (snd iso4 (inl tt)) ≡ inl tt
iso4-test3 = refl
iso4-test4 : fst iso4 (snd iso4 (inr tt)) ≡ inr tt
iso4-test4 = refl
