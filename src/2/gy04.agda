module gy04 where

open import Lib hiding (_+∞_; coite-ℕ∞; ⊤η)

open List hiding (zipWith; head; tail)
open Stream hiding (zipWith)

---------------------------------------------------------
-- positivity
---------------------------------------------------------

-- Miért nem enged agda bizonyos típusokat definiálni? Pl. alapesetben az alábbit sem.


-- ℕ pos? ℕ = +0
-- ℕ → Bool pos? Bool = +
-- ℕ → Bool pos? ℕ = -
-- (String → Integer) → Bool pos? Integer = +

{-# NO_POSITIVITY_CHECK #-}
data Tm : Set where
  lam : (Tm → Tm) → Tm

-- FELADAT: Tm-ből adjuk vissza a lam értékét.
app : Tm → (Tm → Tm)
app (lam x) x₁ = x x₁

self-apply : Tm
self-apply = lam (λ t → app t t)

-- C-c C-n this:
Ω : Tm
Ω = app self-apply self-apply

{-# NO_POSITIVITY_CHECK #-}
data Weird : Set where
  foo : (Weird → ⊥) → Weird
  -- Hogy kell elolvasni magyarul a "foo" konstruktort?

unweird : Weird → ⊥
unweird k@(foo x) = x k

-- ⊥ típusú értéknek TILOS léteznie, ellenkező esetben a rendszer inkonzisztens, állítások bizonyítására alkalmatlan.
bad : ⊥
bad = unweird (foo unweird)
---------------------------------------------------------
-- coinductive types
---------------------------------------------------------

{-
record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A
open Stream
-}

-- C-c C-d
-- check that the type of head : Stream A → A
--                        tail : Stream A → Stream A

-- Ez a típus lényegében a végtelen listákat kódolja el.
-- Ebben véges lista nincs benne, csak végtelen!


-- Copattern matching!
-- FELADAT: Add meg azt a végtelen listát, amely csak 0-kból áll.
zeroes : Stream ℕ
--zeroes = 0 ∷ zeroes ROOOOOOOOOOSZ
head zeroes = 0
tail zeroes = zeroes
-- Honnan tudja agda, hogy ez totális?
-- Termination checker nem tud futni, hiszen a lista végtelen.
-- Productivity checker

-- by pattern match on n
-- FELADAT: Add meg azt a listát, amely n-től 0-ig számol vissza egyesével.
countDownFrom : ℕ → List ℕ
countDownFrom zero = zero ∷ []
countDownFrom k@(suc n) = k ∷ countDownFrom n

countDownFrom-test1 : countDownFrom 5 ≡ 5 ∷ 4 ∷ 3 ∷ 2 ∷ 1 ∷ 0 ∷ []
countDownFrom-test1 = refl
countDownFrom-test2 : countDownFrom 0 ≡ 0 ∷ []
countDownFrom-test2 = refl
countDownFrom-test3 : countDownFrom 3 ≡ 3 ∷ 2 ∷ 1 ∷ 0 ∷ []
countDownFrom-test3 = refl
countDownFrom-test4 : countDownFrom 9 ≡ 9 ∷ 8 ∷ 7 ∷ 6 ∷ 5 ∷ 4 ∷ 3 ∷ 2 ∷ 1 ∷ 0 ∷ []
countDownFrom-test4 = refl
countDownFrom-test5 : countDownFrom 15 ≡ 15 ∷ 14 ∷ 13 ∷ 12 ∷ 11 ∷ 10 ∷ 9 ∷ 8 ∷ 7 ∷ 6 ∷ 5 ∷ 4 ∷ 3 ∷ 2 ∷ 1 ∷ 0 ∷ []
countDownFrom-test5 = refl

-- from n is not by pattern match on n
-- copattern match on Stream
-- FELADAT: Adjuk meg azt a végtelen listát, amely n-től 1-esével felfelé számol!
-- Haskellben: [n..]
from : ℕ → Stream ℕ
head (from n) = n
tail (from n) = from (suc n)

from-test1 : head (from 4) ≡ 4
from-test1 = refl
from-test2 : head (tail (from 4)) ≡ 5
from-test2 = refl
from-test3 : head (tail (tail (from 4))) ≡ 6
from-test3 = refl

-- pointwise addition
zipWith : {A B C : Set} → (A → B → C) → Stream A → Stream B → Stream C
head (zipWith x x₁ x₂) = x (head x₁) (head x₂)
tail (zipWith x x₁ x₂) = zipWith x (tail x₁) (tail x₂)

-- Definiálható-e a filter sima listákon?
filterL : {A : Set} → (A → Bool) → List A → List A
filterL x [] = []
filterL x (x₁ ∷ x₂) with x (x₁) 
... | false = filterL x x₂
... | true = x₁ ∷ filterL x x₂

-- Definiálható-e a filter Stream-eken?
--filterS : {A : Set} → (A → Bool) → Stream A → Stream A
{-
head (filterS P xs) with P (head xs) 
... | false = head (filterS P (tail xs))
... | true = head xs
tail (filterS P xs) = filterS P (tail xs)
-}
-- filterS P xs = if P (head xs) then (head xs) ∷ filterS P (tail xs) else filterS P (tail xs)

-- Válasz: NEM!

-- one element from the first stream, then from the second stream, then from the first, and so on
interleave : {A : Set} → Stream A → Stream A → Stream A
head (interleave x x₁) = head x
head (tail (interleave x x₁)) = head x₁
tail (tail (interleave x x₁)) = interleave (tail x) (tail x₁)
-- Többszörös co-pattern match

interleave-test1 : Stream.List.take 10 (interleave (from 1) zeroes) ≡ 1 ∷ 0 ∷ 2 ∷ 0 ∷ 3 ∷ 0 ∷ 4 ∷ 0 ∷ 5 ∷ 0 ∷ []
interleave-test1 = refl

-- get the n^th element of the stream
get : {A : Set} → ℕ → Stream A → A
get zero x₁ = head x₁
get (suc x) x₁ = get x (tail x₁)

get-test1 : get 4 (from 10) ≡ 14
get-test1 = refl
get-test2 : get 100 zeroes ≡ 0
get-test2 = refl
get-test3 : get 10 (from 100) ≡ 110
get-test3 = refl

-- byIndices [0,2,3,2,...] [1,2,3,4,5,...] = [1,3,4,3,...]
byIndices : {A : Set} → Stream ℕ → Stream A → Stream A
head (byIndices x x₁) = get (head x) x₁
tail (byIndices x x₁) = byIndices (tail x) x₁

byIndices-test1 : Stream.List.take 10 (byIndices (0 ∷ 2 ∷ 3 ∷ 2 ∷ [] Stream.List.++ from 5) (from 1)) ≡ 1 ∷ 3 ∷ 4 ∷ 3 ∷ 6 ∷ 7 ∷ 8 ∷ 9 ∷ 10 ∷ 11 ∷ []
byIndices-test1 = refl

-- iteℕ : (A : Set) → A → (A → A)  → ℕ → A
--        \______________________/
--         ℕ - algebra

{-
record Stream (A : Set) : Set where
  coinductive
  field
    head : A
    tail : Stream A
open Stream
-}

-- Mi lesz a Stream konstruktora (koiterátora)?
coiteStream : {A B : Set} → (B → A) → (B → B) → B → Stream A
--               \_______________________________/
--                        Stream A - coalgebra
--                             ^ (head : Stream A → A) ~> (B → A)
--                                      ^ (tail : Stream A → Stream A) ~> (B → B)
--                                              ^ B, a kezdeti állapot
--                                                     ^ Stream A, mert ezt akaromm létrehozni
head (coiteStream cast ite start) = cast start
tail (coiteStream cast ite start) = coiteStream cast ite (ite start)

-- ex: redefine the above functions using coiteStream

-- ex: look at conatural numbers in Thorsten's book and do the exercises about them

-- simple calculator (internally a number, you can ask for the number, add to that number, multiply that number, make it zero (reset))
record Machine : Set where
  coinductive
  field
    getNumber : ℕ
    add       : ℕ → Machine
    mul       : ℕ → Machine
    reset     : Machine
open Machine

calculatorFrom : ℕ → Machine
getNumber (calculatorFrom n) = n
add (calculatorFrom n) x = calculatorFrom (n + x)
mul (calculatorFrom n) x = calculatorFrom (n * x)
reset (calculatorFrom n) = calculatorFrom 0

c0 c1 c2 c3 c4 c5 : Machine
c0 = calculatorFrom 0
c1 = add c0 3
c2 = add c1 5
c3 = mul c2 2
c4 = reset c3
c5 = add c4 2

{-
c6 : Machine
getNumber c6 = 0
add c6 x = add {!   !} (getNumber c6 + x)
mul c6 = {!   !}
reset c6 = {!   !}
-}

c0-test : getNumber c0 ≡ 0
c0-test = refl

c1-test : getNumber c1 ≡ 3
c1-test = refl

c2-test : getNumber c2 ≡ 8
c2-test = refl

c3-test : getNumber c3 ≡ 16
c3-test = refl

c4-test : getNumber c4 ≡ 0
c4-test = refl

c5-test : getNumber c5 ≡ 2
c5-test = refl

-- FELADAT: Készítsünk egy csokiautomatát.
-- A géptől el lehet kérni, hogy mennyi pénz van bedobva (mennyi a kredit). Ez kezdetben legyen 0. (getCredit)
-- A gépbe tudunk pénzt dobálni (ez legyen ℕ, ennyit adjunk hozzá a jelenlegi kreditünhöz). (addCredit)
-- A tranzakciót meg tudjuk szakítani, a kredit 0 lesz és visszaadja a pénzünket. (cancel)
-- Legyen 3 termékünk, ezek egyenként kerülnek valamennyibe és van belőlük a gépben valamennyi. (twixStock, croissantStock, snickersStock)
-- + Twix: 350-be kerül, kezdetben van a gépben 2 darab.
-- + Croissant: 400-ba kerül, kezdetben van 1 darab.
-- + Snickers: 375-be kerül, kezdetben van 1 darab.
-- Tudunk 1 terméket vásárolni, ha van elég bedobott pénzünk és van az adott termékből a gépben,
  -- akkor a darabszámból vonjunk le egyet és adjuk vissza a visszajárót, a kreditet nullázzuk le.
  -- Ha nincs elég kreditünk vagy nincs a termékből a gépben, akkor ne változtassunk a gép állapotán. (buy)
-- A gép tartalmát újra tudjuk tölteni, ekkor twix-ből legyen újra 50 darab, croissant-ból 75, snickers-ből pedig 60. (restock)

-- Szükség van egy termékeket ábrázoló datára; a konstruktorok nevei legyenek a termékek nevei: (twix, croissant, snickers)

record ChocolateMachine : Set where

buildMachine : {!  !}
buildMachine = {!  !}

startingState : {!  !}
startingState = {!  !}

{-
-- Interakció:
m1 m2 m4 m5 m6 m9 m11 : ChocolateMachine
m3 m7 m8 m10 m12 : ChocolateMachine × ℕ

m1 = addCredit startingState 100
m2 = addCredit m1 500
m3 = buy m2 croissant
---------------
m4 = addCredit (fst m3) 200
m5 = addCredit m4 100
m6 = addCredit m5 100
m7 = buy m6 croissant
m8 = buy (fst m7) snickers
---------------
m9 = addCredit (fst m8) 500
m10 = buy m9 snickers
m11 {- maintainer arrives -} = restock (fst m10)
m12 = buy m11 snickers

chocolateMachine-test1 : getCredit startingState ≡ 0
chocolateMachine-test1 = refl
chocolateMachine-test2 : fst (twixStock startingState) ≡ 2
chocolateMachine-test2 = refl
chocolateMachine-test3 : fst (snickersStock startingState) ≡ 1
chocolateMachine-test3 = refl
chocolateMachine-test4 : fst (croissantStock startingState) ≡ 1
chocolateMachine-test4 = refl
----
chocolateMachine-test5 : getCredit m1 ≡ 100
chocolateMachine-test5 = refl
chocolateMachine-test6 : fst (twixStock m1) ≡ 2
chocolateMachine-test6 = refl
chocolateMachine-test7 : fst (snickersStock m1) ≡ 1
chocolateMachine-test7 = refl
chocolateMachine-test8 : fst (croissantStock m1) ≡ 1
chocolateMachine-test8 = refl
----
chocolateMachine-test9 : getCredit m2 ≡ 600
chocolateMachine-test9 = refl
----
chocolateMachine-test10 : getCredit (fst m3) ≡ 0
chocolateMachine-test10 = refl
chocolateMachine-test11 : snd m3 ≡ 200
chocolateMachine-test11 = refl
chocolateMachine-test12 : fst (croissantStock (fst m3)) ≡ 0
chocolateMachine-test12 = refl
----
chocolateMachine-test13 : getCredit m4 ≡ 200
chocolateMachine-test13 = refl
----
chocolateMachine-test14 : getCredit m5 ≡ 300
chocolateMachine-test14 = refl
----
chocolateMachine-test15 : getCredit m6 ≡ 400
chocolateMachine-test15 = refl
----
chocolateMachine-test16 : getCredit (fst m7) ≡ 400
chocolateMachine-test16 = refl
----
chocolateMachine-test17 : getCredit (fst m8) ≡ 0
chocolateMachine-test17 = refl
chocolateMachine-test18 : snd m8 ≡ 25
chocolateMachine-test18 = refl
chocolateMachine-test19 : fst (snickersStock (fst m8)) ≡ 0
chocolateMachine-test19 = refl
----
chocolateMachine-test20 : getCredit m9 ≡ 500
chocolateMachine-test20 = refl
chocolateMachine-test21 : fst (snickersStock m9) ≡ 0
chocolateMachine-test21 = refl
----
chocolateMachine-test22 : getCredit (fst m10) ≡ 500
chocolateMachine-test22 = refl
chocolateMachine-test23 : snd m10 ≡ 0
chocolateMachine-test23 = refl
----
chocolateMachine-test24 : getCredit m11 ≡ 500
chocolateMachine-test24 = refl
chocolateMachine-test25 : fst (twixStock m11) ≡ 50
chocolateMachine-test25 = refl
chocolateMachine-test26 : fst (croissantStock m11) ≡ 75
chocolateMachine-test26 = refl
chocolateMachine-test27 : fst (snickersStock m11) ≡ 60
chocolateMachine-test27 = refl
----
chocolateMachine-test28 : fst (snickersStock (fst m12)) ≡ 59
chocolateMachine-test28 = refl
chocolateMachine-test29 : getCredit (fst m12) ≡ 0
chocolateMachine-test29 = refl
chocolateMachine-test30 : snd m12 ≡ 125
chocolateMachine-test30 = refl
-}
-- conatural numbers
{-
record ℕ∞ : Set where
  coinductive
  field
    pred∞ : Maybe ℕ∞
open ℕ∞
-}

_+∞_ : ℕ∞ → ℕ∞ → ℕ∞
_+∞_ = {!  !}

-- Ez a függvény létezik, ezzel lehet megnézni
-- egy conat tényleges értékét.
-- Az első paraméter a fuel, maximum ezt a természetes számot tudja visszaadni.
-- Második paraméter a conat, amire kíváncsiak vagyunk.
-- Értelemszerűen ∞-re mindig nothing az eredmény.
{-
ℕ∞→ℕ : ℕ → ℕ∞ → Maybe ℕ
ℕ∞→ℕ zero _ = nothing
ℕ∞→ℕ (suc n) c with pred∞ c
... | zero∞ = just 0
... | suc∞ b with ℕ∞→ℕ n b
... | nothing = nothing
... | just x = just (suc x)
-}

coiteℕ∞ : {B : Set} → (B → Maybe B) → B → ℕ∞
coiteℕ∞ = {!  !}

-- TODO, further exercises: network protocols, some Turing machines, animations, IO, repl, shell
