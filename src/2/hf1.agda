module hf1 where

open import Lib


-- Lássuk be, hogy a következő bijekciók fentállnak.

b1 : ⊤ ⊎ ⊤ ↔ Bool
b1 = {!   !}

b2 : ⊥ × ⊤ ⊎ (⊥ → ⊥) ⊎ (⊥ → ℕ) ⊎ ((List ⊤ × ⊥) → (⊥ × ⊥)) ↔ Maybe Bool
b2 = {!   !}

-- Some black magic for the brave
Σℕ : ℕ → Set
Σℕ zero = ⊥
Σℕ (suc x) = ⊤ ⊎ Σℕ x

Πℕ : ℕ → Set → Set
Πℕ zero _ = ⊤
Πℕ (suc x) A = A × (Πℕ x A)

b3 : Σℕ 4 ↔ Bool × Bool
b3 = {!   !}

b4 : Σℕ 4 ↔ Maybe (Maybe (Maybe (Maybe ⊥)))
b4 = {!   !}

b5 : Πℕ 10 ⊥ ↔ ⊥
b5 = {!   !}

b6 : Πℕ 10 ⊤ ↔ ⊤
b6 = {!   !}

b7 : Πℕ 3 Bool ↔ Bool ⊎ (Maybe Bool) × Bool
b7 = {!   !}

------------
 
--- Adott a következő típus

data Crazy (A : Set) (B : Set) : Set where
  c1 : A → A → Crazy A B
  c2 : A ⊎ B → Crazy A B
  c3 : Crazy A B
  c4 : B → Crazy A B → Crazy A B
  c5 : (B → A) → Crazy A B

-- Add meg a következő függvényeket:

iteCrazy : {!   !}
iteCrazy = {!   !}

recCrazy : {!   !}
recCrazy = {!   !}

-- Add meg az előző függvényeket úgy, hogy a rec-et ite-vel, az ite-t rec-el adod meg!

iteCrazy' : {!   !}
iteCrazy' = {!   !}

recCrazy' : {!   !}
recCrazy' = {!   !}

-----

-- Adott a következő koinduktív típus

record 𝔾 (A : Set) : Set where
  coinductive
  constructor next
  field
    prev : A ⊎ 𝔾 A

-- Add meg a fenti típus co-iterátorát!

coite𝔾 : {!   !}
coite𝔾 = {!   !}

concat : {A B : Set} → 𝔾 A → 𝔾 B → 𝔾 (A ⊎ B)
-- ötlet: `with a | b`
𝔾.prev (concat x x₁) = {!   !}

ga : 𝔾 ℕ
𝔾.prev ga = inl 10

gb gb1 : 𝔾 Bool
𝔾.prev gb = inl false
𝔾.prev gb1 = inr gb

ct0 : 𝔾.prev (concat ga gb1) ≡ inl (inl 10)
ct0 = refl
ct1 : 𝔾.prev (concat gb1 gb) ≡ inl (inr false)
ct1 = refl
ct2 : case (𝔾.prev (concat {Bool} {ℕ} gb1 (next (inr (next (inl 2)))))) (λ x → inl x) (λ x → 𝔾.prev x) ≡ inl (inl false)
ct2 = refl

-- A végtelenbe, és tovább
𝔾∞ : {A : Set} → 𝔾 A
𝔾.prev 𝔾∞ = {!   !}

∞t : {A : Set} → case (𝔾.prev (𝔾∞ {A})) (λ x → next (inl x)) id ≡ 𝔾∞
∞t = refl