/-

Lean4-Mode controls:

C-c TAB = Show messages buffer (in Emacs)
\ <stuff> = Make unicode symbols (ex.: \a = α)



Some unicode symbols
\r = →
\l = ←
\lr = ↔
\N = ℕ
\la = λ
\a = α
\b = β
\and or \an = ∧
\or = ∨
\x = ×
\S = Σ
\. = ·


-/

def add1' : Nat → Nat := fun n => n + 1

-- In Lean4, you define functions
-- using the def keyword
--         v types of the parameters
def add1 (n : Nat) : Nat := n + 1
--                   ^ return type

-- Due to currying, you can also define functions like this using lambdas
def add1curried : Nat → Nat := fun n => n + 1

-- The two definitions are the same
example (n : Nat) : add1curried n = add1 n := rfl


-- Exercise: define two functions that multiply the given Nat by 2
-- Define it both curried and uncurried

def multiply2 (n : Nat) : Nat := n * 2
def multiply2' : Nat → Nat := fun n => n * 2

#eval add1 3

-- You can evaluate expressions by using the #reduce or #eval commands
-- Using #reduce is preferred, as it works with lambdas

#reduce add1 2
#eval add1 2

#reduce fun x => x
-- #eval fun x => x
-- doesn't work

-- Other usefull commands
-- #check <T> = returns Ts type
-- #print <T> = prints the definition of T

#print add1
#print add1curried

/-

How does reduction happen?
add1 2 ≝
(λ n → n + 1) 2 ≝ (β-reduction)
(n + 1)[n := 2] ≝
2 + 1 ≝
3

-/

-- Lean supports higher order functions
def applyToThree (f : Nat → Nat) : Nat := f 3

#eval applyToThree add1
-- Exercise: Define f2 and f3 higher order functions of type (Nat → Nat) → Nat
-- such that f2 add1 ≠ f3 add1

def f2 (f : Nat → Nat) : Nat := f 1
def f3 (f : Nat → Nat) : Nat := f 2


-- Lean supports polymorphism via implicit parameters
def identity {α : Type}(a : α) : α := a

-- You can group parameters of the same type
def apply {α β : Type}(f : α → β)(a : α) : β := f a

def add1implicit {n : Nat} : Nat := n + 1
def identityExplicit (α : Type)(a : α) : α := a

def checkIfEqual {α : Type}[BEq α](a b : α) : Bool := a == b


-- Define a function tw which applies a function of type α → α twice
-- to a parameter of type α

def tw {α : Type}(f : α → α)(a : α) : α := f (f a)
def tw' {α : Type}(f : α → α) : α → α := f ∘ f



-- What's the type of tw tw add1 3? Ask Lean4 with #check
-- What's the value of tw tw add1 3? Ask Lean4 with #reduce or #eval

#print Bool
#print String
#print Char
#print List
