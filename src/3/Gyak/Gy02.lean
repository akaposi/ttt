-- Simple finite types

open Prod
open Unit
open Empty
open Sum

-- Product types / tuples
-- You can also use Prod instead of ×

-- You can use pattern matching
example (p : Nat × Bool) : Bool × Nat := match p with
  | ( first , second ) => ( second , first)

-- Or a pattern matching lambda
example : Nat × Bool → Bool × Nat
| ( first , second ) => ( second , first )

-- Or using fst and snd projections
#check fst
#check snd
-- can be used like 'fst x' or 'x.fst'

example (p : Nat × Bool) : Bool × Nat := ( snd p , p.fst )

-- You can use the print command to print the definition of a type
-- ( , ) is syntactic sugar for mk
#print Prod


-- It's the same forwards and backwards
-- Lets define a generic version of it with polymorphism
def commProd {α β : Type}(self : α × β) : β × α := match self with
| ( a , b ) => ( b , a )


#print Sum

def swapSum {α β : Type}(self : α ⊕ β) : β ⊕ α := match self with
| inl a => inr a
| inr b => inl b

-- Unit and Empty types
-- Unit has one constructor with zero parameters called unit
#check unit
-- Empty has zero constructors
#print Empty
#check Empty.elim


def exfalso {α : Type}(b : Empty) : α := Empty.elim b
-- You can also use nomatch on types with no elements
example {α : Type}(b : Empty) : α := nomatch b

-- Sum types
-- ⊕ = \o+
-- You can also use Sum instead of ⊕
#print Sum

-- This has two constructors
example {α β : Type} : α ⊕ β → β ⊕ α := _


-- Define elements of the following types
example : Unit ⊕ Empty := inl unit

example : (Empty → Empty) × Bool := ( (fun k => nomatch k) , false )
example : (Empty → Empty) × Bool := ( (fun k => nomatch k) , true )

example : Bool ⊕ Unit := inl true
example : Bool ⊕ Unit := inl false
example : Bool ⊕ Unit := inr unit

-- Based on these, how many unique elements do each of these types have?
-- | Unit | = 1
-- | Empty | = 0
-- | Bool | = 2
-- | Bool ⊕ Unit | = 3
-- | A ⊕ B | = | A | + | B |
-- | A × B | = | A | * | B |
-- | Bool × Bool × Bool | = 8
-- | Unit → Empty | = 0
-- | Empty → Unit | = 1
-- | Empty → Empty | = 1
-- | Bool → Empty | = 0
-- | Bool → Unit | = 1
-- | Unit → Bool | = 2
-- | A → B | = | B | ^ | A |
-- | Bool → Bool → Bool | = 16
-- | Bool → Bool → Bool → Bool | = 256

-- Type of bijections
def Bij.{u, v}(α : Type u)(β : Type v) : Type (max u v) := (α → β) × (β → α)
infix:20 " ↔ " => Bij

-- Define a bijection between the following types
-- Example:
def idlSum {α : Type} : Empty ⊕ α ↔ α := (fun val => match val with
  | inl a => exfalso a
  | inr b => b
  , inr)


-- Instead of pattern matching, you can use Sum.casesOn
example {α : Type} : Empty ⊕ α ↔ α := (fun v => Sum.casesOn v exfalso id , inr)

def idrSum {α : Type} : α ⊕ Empty ↔ α := (fun val => match val with
  | inr a => exfalso a
  | inl b => b
  , inl)

def assocSum {α β γ : Type} : (α ⊕ β) ⊕ γ ↔ α ⊕ (β ⊕ γ) := _
def commSum {α β : Type} : α ⊕ β ↔ β ⊕ α := _

def idlProd {α : Type} : Unit × α ↔ α := (fun val => match val with
  | ( _, a ) => a
  , ( unit, · ))
def idrProd {α : Type} : α × Unit ↔ α := _
def assocProd {α β γ : Type} : (α × β) × γ ↔ α × (β × γ) := (fun val => match val with
| (( a , b ) , c) => (a , ⟨ b , c ⟩)
, fun val => match val with
| (a , (b , c)) => ((a , b) , c))

def nullProd {α : Type} : α × Empty ↔ Empty := (snd , exfalso)
def distProdSum {α β γ : Type} : α × (β ⊕ γ) ↔ (α × β) ⊕ (α × γ) := _

def curry {α β γ : Type} : (α × β → γ) ↔ (α → β → γ) := _
def distFunSum {α β γ : Type} : (β ⊕ γ → α) ↔ (β → α) × (γ → α) := _
def lawExp0 {α : Type} : (Empty → α) ↔ Unit := _
def lawExp1 {α : Type} : (Unit → α) ↔ α := _
def law1Exp {α : Type} : (α → Unit) ↔ Unit := _

-- Bijections which require proofs

def iso1 : (Unit ⊕ Unit) ↔ Bool :=
  (fun v =>
    Sum.casesOn v
    (fun _ => true)
    (fun _ => false)
  , fun b =>
    if b
      then inl unit
      else inr unit
  )

-- Tests to make sure that they are actually bijections
example : fst iso1 (inl unit) ≠ fst iso1 (inr unit) := nofun
example : snd iso1 true ≠ snd iso1 false := nofun

def iso2 : (Unit → Unit ⊕ Empty ⊕ Unit) ↔ (Unit ⊕ Unit) := _

example : fst iso2 (fun _ => inl unit) ≠ fst iso2 (fun _ => inr $ inr unit) := nofun
example : snd iso2 (inl unit) unit ≠ snd iso2 (inr unit) unit := nofun
