-- The definition of natural numbers
set_option maxRecDepth 10000

inductive ℕ : Type
| zero : ℕ
| suc : ℕ → ℕ

open ℕ

-- Magic ignore me
instance {n : Nat} : OfNat ℕ n where
  ofNat := let rec loop : Nat → ℕ
    | .zero => zero
    | .succ k => suc (loop k)
  loop n


#eval (3 : ℕ)


-- Recursion and Termintation checking
-- Define the double function for Nats

def double : ℕ → ℕ
| zero => zero
| suc n => suc (suc (double n))


-- Some tests
example : double 2 = 4 := rfl
example : double 0 = 0 := rfl
example : double 10 = 20 := rfl

-- What's the half of odd numbers?
-- Executive descision: We round down odd numbers

def half : ℕ → ℕ
| zero => zero
| suc zero => zero
| suc (suc n) => suc (half n)

example : half 10 = 5 := rfl
example : half 11 = 5 := rfl
example : half 12 = 6 := rfl

-- Define addition
def add : ℕ → ℕ → ℕ
| n, zero => n
| n, suc k => suc (add n k)

def f (n : ℕ) : ℕ := match n with
| zero => zero
| suc k => f k

def tadd (n k : ℕ) : ℕ := match n with
| zero => k
| suc n => tadd n (suc k)


instance : Add ℕ where
  add := add

example : 3 + 5 = (8 : ℕ) := rfl
example : 0 + 2 = (2 : ℕ) := rfl
example : 2 + 0 = (2 : ℕ) := rfl

-- Define multiplication
def mul : ℕ → ℕ → ℕ
| _, 0 => 0
| k, suc n => k + (mul k n)

instance : Mul ℕ where
  mul := mul

example : 3 * 5 = (15 : ℕ) := rfl
example : 0 * 2 = (0 : ℕ) := rfl
example : 2 * 0 = (0 : ℕ) := rfl

-- Define the power function
-- 0 ^ 0 = 1
def pow : ℕ → ℕ → ℕ
| _, 0 => 1
| n, suc k => n * pow n k

instance : Pow ℕ ℕ where
  pow := pow

example : 3 ^ (4 : ℕ) = (81 : ℕ) := rfl
example : 3 ^ (0 : ℕ) = (1 : ℕ) := rfl
example : 0 ^ (3 : ℕ) = (0 : ℕ) := rfl
example : 1 ^ (3 : ℕ) = (1 : ℕ) := rfl
example : 0 ^ (0 : ℕ) = (1 : ℕ) := rfl

-- Define factorials
def fact : ℕ → ℕ
| 0 => 1
| 1 => 1
| suc n => suc n * fact n

notation:max e " ! " => fact e

example : 3 ! = 6 := rfl
example : 1 ! = 1 := rfl
example : 0 ! = 1 := rfl
example : 6 ! = 720 := rfl


-- Define less than and less than equal functions
def lt : ℕ → ℕ → Bool := _
def lte : ℕ → ℕ → Bool := _

infix:51 " < " => lt
infix:51 " ≤ " => lte

example : (0 < (1 : ℕ)) = true := rfl
example : (1 < (1 : ℕ)) = false := rfl
example : (0 ≤ (1 : ℕ)) = true := rfl
example : (1 ≤ (1 : ℕ)) = true := rfl
example : (1 < (0 : ℕ)) = false := rfl
example : (1 ≤ (0 : ℕ)) = false := rfl

-- With the help of the functions above, define equality
def eqℕ (n k : ℕ) : Bool := _

infix:51 " ≡ " => eqℕ

example : (1 ≡ 1) = true := rfl
example : (1 ≡ 2) = false := rfl
example : (2 ≡ 1) = false := rfl

-- Define a function which is the smaller of the two parameters
-- Try doing it with the previously defined functions
def min : ℕ → ℕ → ℕ := _

example : min (1 : ℕ) 2 = 1 := rfl
example : min (1 : ℕ) 1 = 1 := rfl
example : min (1 : ℕ) 0 = 0 := rfl

def fib : ℕ → ℕ := _

example : fib 10 = 89 := rfl
example : fib 15 = 987 := rfl

-- Bonus, harder exercise
--               n    0   n / 1 = n
--               n    k   n / (k + 1)
def Nat.mydiv : Nat → Nat → Nat
| n, zero => n
| n, k => if n < k + 1 then 0 else (Nat.mydiv (n - (k + 1)) k) + 1


-- Lists
-- Lists are defined inductively, like natural numbers
#print List

open List

-- Example list
def someNumbers : List ℕ := [1, 2, 3]

-- You can also use cons and nil
-- same as cons 1 ( cons 2 (cons 3 nil ) )
-- $ also works, like in Haskell
def someOtherNumbers : List ℕ := cons 1 <| cons 2 <| cons 3 <| nil

-- f <| g <| x is syntactic sugar for f (g x)
-- The above expressions are the same
example : someNumbers = someOtherNumbers := rfl


#print Option
-- Define indexing for lists
def index {α : Type} : List α → ℕ → Option α
| [], _ => none
| x :: xs, zero => some x
| x :: xs, suc n => index xs n

-- Define concatenation for lists
def concat {α : Type} : List α → List α → List α := _

-- Define length for lists
def length {α : Type} : List α → ℕ := _

-- Define sum for lists
def sum : List ℕ → ℕ := _
