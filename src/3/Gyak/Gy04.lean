-- Dependent types

import Mathlib.Tactic.Common

open Nat
open Nat.le

universe u v

-- In Lean, some function definitions are a bit problematic, like subtraction
#reduce pred 0
#reduce 0 - 3 -- <-- n - k, n ≥ k
#reduce 0 ^ 0
-- These shoudln't evaluate to anything in the first place, these could be unrepresentable
-- In the case of subtract, we would need a type level witness stating that n ≥ k
-- Or in the case of pred, we would need a type level witness stating that n ≥ 0

-- Dependent Types: Types that depend of terms
-- Predicate: A proposition constructed from a term

-- Usually we'd construct a boolean predicate like this
def ge_boolean : Nat → Nat → Bool
| _, zero => true
| zero, succ _ => false
| succ n, succ k => ge_boolean n k

-- Instead we are going to include a condition being true, with a type having an element and false with a type not having one
-- The most simple types for this behaviour are Unit and Empty
def ge_type : Nat → Nat → Type
| _, zero => Unit
| zero, succ _ => Empty
| succ n, succ k => ge_type n k

-- An alternative to Types are Props (propositions). We prefer using Props for predicates, but we'll learn about this later
def ge_prop : Nat → Nat → Prop
| _, zero => True
| zero, succ _ => False
| succ n, succ k => ge_prop n k
-- A dependent function type is also known as a Pi type (or Π-type)
--                V V               V
def better_sub : (n k : Nat) → ge_prop n k → Nat
| n, zero, p => n
-- | zero, succ k, p => nomatch p
| succ n, succ k, p => better_sub n k p
-- You can check what a parameter reduces to by typing "by reduce at <param>" in the place of a hole. We'll learn what this means next lesson

-- Define better_pred using the help of ge_prop
def better_pred : (n : Nat) → ge_prop n 1 → Nat
-- | zero, p => nomatch p
| succ n, _ => n

-- Vectors
-- Similarly to how lists are defined, we are going to define length indexed vectors

-- We add a type level Nat to always know how long a vector is
inductive Vec (α : Type u) : Nat → Type u
| nil : Vec α 0
| cons : {n : Nat} → α → Vec α n → Vec α (succ n)

-- Ex.: What is the length of a vector which is guaranteed to have a head element and a tail?
def Vec.head {α : Type u}{n : Nat} : Vec α (succ n) → α
| cons a _ => a

def Vec.tail {α : Type u}{n : Nat} : Vec α (succ n) → Vec α n
| cons _ as => as

-- Define a function which generates numbers from n to zero. How long will this list vector be?
-- Vec.countdown 3 == cons 3 (cons 2 (cons 1 (cons 0 nil)))
def Vec.countdown : (n : Nat) → Vec Nat (succ n)
| 0 => cons 0 nil
| succ n => cons (succ n) (Vec.countdown n)

-- Ex.: What is the length of a vector that has been mapped over?
def Vec.map {α : Type u}{β : Type v}{n : Nat}(f : α → β) : Vec α n → Vec β n
| nil => nil
| cons a n => cons (f a) (Vec.map f n)

-- What is the length of the concatenation of two vectors?
def Vec.add {α : Type u}{n k : Nat} : Vec α n → Vec α k → Vec α (k + n)
| nil, xs => xs
| cons y ys, xs => cons y (Vec.add ys xs)

-- We can also leverage these type level guarantees to safely index into vectors
-- What kind of constraint do we need on k to safely index?

def g_prop : Nat → Nat → Prop
| zero, _ => False
| succ _, zero => True
| succ n, succ k => g_prop n k

def Vec.index {α : Type u}{n : Nat} : Vec α n → (k : Nat) → g_prop n k → α
-- | nil, k, p => nomatch p
| cons x _, zero, p => x
| cons _ xs, succ k, p => Vec.index xs k p

-- We can combine the Nat and the condition into a single type called Fin n
/-

inductive Fin (n : Nat) : Type where
| mk : (k : Nat) → k < n → Fin n

-/
#print Fin
-- Fin n represents the type of natural numbers less than n

-- How many elements does Fin 0, Fin 1 and Fin 2 have?

example (n : Fin 0) : False := nomatch n
example : Fin 1 := 0
example : Fin 2 := 0
example : Fin 2 := 1
-- |Fin n| = n
-- |Vec α n| = |α|^n

-- When using more complex and dependent types like Fin, we may may need to occasionally prove certain aspects of our programs
-- Try writing the following function:
def Vec.indexWithFin {α : Type u}{n : Nat} : Vec α n → Fin n → α
-- | nil, f => _
| cons x xs, 0 => x --\<
| cons x xs, ⟨ succ k, p ⟩ => Vec.indexWithFin xs ⟨ k, Nat.lt_of_succ_lt_succ p ⟩

-- A problem arises, where we'd need to obtain k < n, but we only have succ k < succ n available.
-- We could prove it by hand, but the Lean stdlib provides proofs for simple concepts like this
-- You can search for proofs and functions at https://loogle.lean-lang.org/
-- Note: Loogle prefers n.succ over succ n, because that way it automatically infers that you mean Nat.succ
-- Or you can use the #find command to search locally or the #loogle command to search on loogle
#find (n k : Nat) → n.succ < k.succ → n < k

-- Define a function for taking some number of elements from a list
def Vec.take {α : Type u}{n : Nat} : Vec α n → (k : Fin n) → Vec α _ := _

-- Hard exercise: Define a function for finding the index of a given element
-- BEq allows for the usage of binary equality
def Vec.find {α : Type u}{n : Nat} [BEq α] : Vec α n → α → Option (Fin n) := _

-- Harder exercise: Define the inverse of indexing
def Vec.tabulate {α : Type u}: {n : Nat} → (Fin n → α) → Vec α n := _


-- Sigma types (Σ-types)
-- Π-types are the type theoretic equivalent of universal quantification
-- Ex.: (n : Nat) → Vec n Nat can be read as *for every* natural number n, we can make a vector of that length, consisting of nats
-- Sigma types are the type theoretic equivalent of existential quantification.
-- Fin is actually a special case of sigma types, we say that *we have / there exists* a natural number val, such that it's less than a given nat n.

-- Following this pattern we can define Sigma types the following way

/-
inductive Sigma (α : Type u)(β : α → Type v) : Type (max u v) where
| mk : (a : α) → β a → Sigma α β
*there exists* a value a of tyep α, which satisfies β
-/

-- Define a bijection between Fin n and its equivalent defined with a sigma type
-- Note since < is a proposition, we have to use PSigma or Σ'
-- The syntax Σ (a : α), β a is the same as Sigma α (fun a => β a)
example {n : Nat} : (Fin n → Σ' k : Nat, k < n) × ((Σ' k : Nat, k < n) → Fin n) := _

-- Define the filter function for vectors
def Vec.filter {α : Type u}{n : Nat} : (α → Bool) → Vec α n → Σ (k : Nat), Vec α k := _
