-- Propositional Logic

import Mathlib.Tactic.Common

open Decidable

-- The Curry-Howard Isomorphism: Converting logical statements to types or propositions
-- We'll consider a type "True" if it has an element and "False" if it doesn't
/-
+---------------------+--------------------+--------------------+
| Logical Symbol      | Type Equivalent    | Prop Equivalent    |
+---------------------+--------------------+--------------------+
| Conjunction (A ∧ B) | Prod (A × B)       | And (A ∧ B)        |
+---------------------+--------------------+--------------------+
| Disjunction (A ∨ B) | Sum (A ⊕ B)        | Or (A ∨ B)         |
+---------------------+--------------------+--------------------+
| Implication (A → B) | A → B              | A → B              |
+---------------------+--------------------+--------------------+
| True                | Unit               | True               |
+---------------------+--------------------+--------------------+
| False               | Empty              | False              |
+---------------------+--------------------+--------------------+
| Negation (¬ A)      | A → Empty          | A → False (¬ A)    |
+---------------------+--------------------+--------------------+
-/

-- Throughout the course we'll mostly formalise into Prop rather than Type

-- Sections are just block scopes which don't leak internal definitions and variables
section Formalisation

  -- We can declare logical variables with the variable keyword
  -- axiom A : Type
  -- axiom B : Prop

  -- EXERCISE: Formalise the following sentences both as Prop and Type
  -- Declare any logical variables with the variable keyword

  -- The sun is not shining
  def F1T : Type := _
  def F1P : Prop := _

  -- It's raining and the sun is shining
  def F2T : Type := _
  def F2P : Prop := _

  -- You don't need an umbrella or it's raining
  def F3T : Type := _
  def F3P : Prop := _

  -- If it's raining and the sun is shining, then rainbows appear
  def F4T : Type := _
  def F4P : Prop := _

  -- Rainbows appear
  def RT : Type := _
  def RP : Prop := _

  -- We prove that a logical statement is true if we can write a Lean4 expression of that type
  -- Prove that if F1-4P are true, then so is RP
  -- This can be proven in two seperate ways
  example (f1p : F1P)(f2p : F2P)(f3p : F3P)(f4p : F4P) : RP := _
  example (f1p : F1P)(f2p : F2P)(f3p : F3P)(f4p : F4P) : RP := _

  -- Note: to help, it may be worth investigating what each type is capable of
  -- Each type has some form of Intro function and an elim function
  #print And
  #print Or
  #check False.elim

end Formalisation


-- Try proving the following logical statements
theorem substAnd {α β γ δ : Prop} : (α → γ) → (β → δ) → α ∧ β → γ ∧ δ := _

theorem substFun {α β γ : Prop} : (α → β) → (β → γ) → (α → γ) := _

theorem contradiction {α β : Prop} : ¬ α ∧ α → β := _

theorem wk {α : Prop} : α → ¬¬ α := _

theorem func {α β : Prop} : ¬ α ∨ β → (α → β) := _

-- Why don't any of the following work?
theorem lem {α : Prop} : ¬ α ∨ α := _
theorem str {α : Prop} : ¬¬ α → α := _

-- De Morgan laws

-- Prove this manually using pattern matching and functions
theorem dm1 {α β : Prop} : ¬ (α ∨ β) ↔ ¬ α ∧ ¬ β := _


-- It's much easier to prove this using *tactics*
-- Tactics make proofs simpler and more concise, but can be hard to read to someone
-- who isn't accustomed to them

-- We'll learn about the following tactics (for now), but there is pdf in this directory
-- with further detail on potentially useful tactics

theorem dm1WithTactics {α β : Prop} : ¬ (α ∨ β) ↔ ¬ α ∧ ¬ β := _

-- The simple, most barebones tactics
-- intros <list of names>: introduces hypothesis with the given names (any unnamed hypothesis will be shadowed)
-- obtain <pat> := <exp>: pattern match on <exp>. Can be used to split
-- assumption: closes a goal if the goal appears among the hypotheses
-- exact <exp>: closes the goal by providing a solution
-- apply <exp>: solve the goal by applying a function
-- have <name> : <type> := <value>: introduces new hypothesis equal to value
-- constructor: if the type only has one constructor, this introduces it
-- left/right: if a type has 2 constructors, these introduce those

-- We can also check what kind of proofs these generate:
#print dm1WithTactics

theorem dm2 {α β : Prop} : ¬ α ∨ ¬ β → ¬ (α ∧ β) := _

-- Hard!
theorem dm2b {α β : Prop} : ¬¬ (¬ (α ∧ β) → α ∨ ¬ β) := _

theorem noncontra {α : Prop} : ¬ (α ↔ ¬ α) := _

theorem qinvol {α : Prop} : ¬¬¬¬ α ↔ ¬¬ α := _

theorem tinvol {α : Prop} : ¬¬¬ α ↔ ¬ α := _

theorem wklem {α : Prop} : ¬¬ (α ∨ ¬ α) := _

theorem wkstr {α : Prop} : ¬¬ (¬¬ α → α) := _

theorem lem2str {α : Prop} : α ∨ ¬ α → ¬¬ α → α := _

-- Decide if the following logical statements are true or not

#print Decidable

example {α β : Prop} : Decidable (α ∨ β → ¬¬ (β ∨ α)) := _

example {α : Prop} : Decidable (¬ (α ∨ ¬ α)) := _

example {α : Prop} : Decidable (¬ (α → ¬ α → α)) := _

example {α : Prop} : Decidable (False → α ∨ ¬ α) := _

example {α : Prop} : Decidable (α ∧ ¬ α → α ∨ ¬ α) := _

example {α : Prop} : Decidable ((α → α) → False) := _

-- There is a difference between
-- example {α : Prop} : Decidable (... α)
-- and
-- example : Decidable ((α : Prop) → ... α)
-- For the first one we have to universally prove if it's true or not
-- For the second one we have to universally prove if it's true or find a counterexample

example : Decidable ((α β : Prop) → α ∨ β → β) := _

example : Decidable ((α β γ : Prop) → (α → γ) ∨ (β → γ) → α ∨ β → γ) := _

example : Decidable ((α β γ : Prop) → (α → γ) ∧ (β → γ) → α ∧ β → γ) := _

example : Decidable ((α β γ : Prop) → (α ∧ β → γ) → (α → γ) ∧ (β → γ)) := _

example : Decidable ((α β γ : Prop) → α ∨ β ∧ γ → (α ∨ β) ∧ (β ∨ γ)) := _
