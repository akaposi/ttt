-- Predicate / First Order Logic

import Mathlib.Tactic.Common

open Decidable

-- In first order logic, we introduce universal and existential quantifiers over some set
-- The primary difference, is that we quantify over some type instead of some set
-- According to the Curry-Howard Isomorphism we encode these quantifiers in the following way:
/-
+---------------------------------------------+------------------------------------------+-------------------------------------------+
| Logical Symbol                              | Type Equivalent                          | Prop Equivalent                           |
+---------------------------------------------+------------------------------------------+-------------------------------------------+
| Universal Quantification (∀ x ∈ A: P(x))    | (x : A) → P x                            | (x : A) → P x                             |
+---------------------------------------------+------------------------------------------+-------------------------------------------+
| Existential Quantification (∃ x ∈ A: P(x))  | Sigma A (fun x => P x) [Σ (x : A), P x]  | Exists A (fun x => P x) [∃ (x : A), P x]  |
+---------------------------------------------+------------------------------------------+-------------------------------------------+
-/

-- Let's formalise some sentences in First Order Logic
section Formalisation

  -- We have our "universe" aka the Type that we quantify over
  axiom Person : Type
  -- We also have some known elements of the universe
  axiom Ann : Person
  axiom Kate : Person
  axiom Peter : Person
  -- We also have two binary predicates (relations)
  axiom childOf : Person → Person → Prop
  -- childOf x y = x is the child of y
  -- sameAs x y = x and y are the same people

  -- Define a predicate which states that a given person has a child
  def hasChild (p : Person) : Prop := _

  -- Ann is the child of Kate
  def ANK : Prop := _

  -- Define a predicate which states that a given person is the parent of another
  def parentOf (parent child : Person) : Prop := _

  -- Noone is the parent of everyone
  def NONE : Prop := _

  -- Hard: There is someone with exactly one child.
  def ONE : Prop := _


  -- Prove that if Ann has no children, then Kate is not the child of Ann
  theorem AK : (¬ ∃ (p : Person), childOf p Ann) → ¬ childOf Kate Ann := _

  #check parentOf

  -- Prove that is noone is their own parent, then noone is the parent of everyone
  theorem NOI : (¬ ∃ (p : Person), parentOf p p) → NONE := _


end Formalisation

-- We introduce two new tactics
-- specialze <nam> <list of params> : applies the given params in the given hypothesis
-- use <expr> : if the goal is existential, use the given expression as the value we presume that exists

theorem forallAndDistr {α : Type}{P Q : α → Prop} : ((a : α) → P a ∧ Q a) ↔ ((a : α) → P a) ∧ ((a : α) → Q a) := _

theorem forallOrDistr {α : Type}{P Q : α → Prop} : ((a : α) → P a) ∨ ((a : α) → Q a) → (a : α) → P a ∨ Q a := _

theorem existsAndDistr {α : Type}{P Q : α → Prop} : (∃ (a : α), P a ∧ Q a) → (∃ (a : α), P a) ∧ (∃ (a : α), Q a) := _

theorem existsOrDistr {α : Type}{P Q : α → Prop} : (∃ (a : α), P a ∨ Q a) ↔ (∃ (a : α), P a) ∨ (∃ (a : α), Q a) := _

theorem forallNegPush {α : Type}{P : α → Prop} : (∃ (a : α), ¬ (P a)) → ¬ ((a : α) → P a) := _

theorem existsNegPush {α : Type}{P : α → Prop} : ((a : α) → ¬ P a) → ¬ ∃ (a : α), P a := _

theorem forallOrDistrN : ¬ ((α : Type) → (P Q : α → Prop) → ((a : α) → P a ∨ Q a) → ((a : α) → P a) ∨ ((a : α) → Q a)) := _

theorem existsAndDistrN : ¬ ((α : Type) → (P Q : α → Prop) → (∃ (a : α), P a) ∧ (∃ (a : α), Q a) → ∃ (a : α), P a ∧ Q a) := _

theorem existsForall {α β : Type}{R : α → β → Prop} : (∃ (a : α), (b : β) → R a b) → (b : β) → ∃ (a : α), R a b := _

theorem effectivelyAOC {α β : Type}{R : α → β → Prop} : ((a : α) → ∃ (b : β), R a b) → ∃ (f : α → β), (a : α) → R a (f a) := _
