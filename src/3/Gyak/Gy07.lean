-- Equality
import Mathlib.Tactic.Common

universe u v

open Nat
open Eq

macro "dreduce" : tactic => `(tactic | reduce <;> dsimp)

-- We define we equality in the following way
#print Eq

-- Meaning: the only way for two things to be equal is if they are the same

-- Let's prove that Eq is an equivalence relation

-- This is called rfl in the standard library
theorem reflexive {α : Type u}{a : α} : a = a := _

-- This is called symm in the standard library
theorem symmetric {α : Type u}{a b : α} : a = b → b = a := _

-- This is called trans in the standard library
theorem transitive {α : Type u}{a b c : α} : a = b → b = c → a = c := _

-- We also prove a property known as "congruence"
-- This is called congrArg in the standard library
theorem congruent {α β : Type u}(f : α → β){l r : α} : l = r → f l = f r := _

-- We also write a substitution function, which substitutes the LHS of the equality to the RHS in any context
-- This is called subst in the standard library
def substitute {α : Type u}(P : α → Sort v){a b : α} : a = b → P a → P b := _

-- Identities for natural numbers, addition and multiplication
theorem Nat.add_right_identity (n : Nat) : n + 0 = n := _

-- This will be harder to solve due to definitional equalities
theorem Nat.add_left_identity (n : Nat) : 0 + n = n := _

theorem Nat.succ_right_shift (m n : Nat) : m + (n + 1) = (m + n) + 1 := _

theorem Nat.succ_left_shift (m n : Nat) : (m + 1) + n = (m + n) + 1 := _

theorem Nat.add_associative (l m n : Nat) : (l + m) + n = l + (m + n) := _

theorem Nat.succ_move (m n : Nat) : (n + 1) + m = n + (m + 1) := by
  _

theorem Nat.add_commutative (m n : Nat) : m + n = n + m := _


-- Let's try solving this previous one again with tactics!
-- New tactic:
-- induction <expr> with
-- | <pat> => ...
-- Performs structural induction on <expr>
-- rw <lemmas>: rewrites the goal according to the given lemmas
-- rfl: Same as exact rfl
-- dreduce: Applies definitional reductions, while preserving notations

theorem Nat.add_mul_distr (m n o : Nat) : m * (n + o) = m * n + m * o := _

theorem Nat.mul_left_null (n : Nat) : 0 * n = n := _

theorem Nat.mul_right_identity (n : Nat) : n * 1 = n := _

theorem Nat.mul_left_identity (n : Nat) : 1 * n = n := _

theorem Nat.mul_succ_expand (n m : Nat) : (n + 1) * m = n * m + m := _

theorem Nat.mul_associative (m n o : Nat) : (m * n) * o = m * (n * o) := _

theorem Nat.mul_commutative (n m : Nat) : n * m = m * n := _
