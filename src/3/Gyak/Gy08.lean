-- Equational reasoning

import Mathlib.Tactic.Common
import Mathlib.Tactic.Find


macro "dreduce" : tactic => `(tactic | reduce <;> dsimp)

open Nat
open Eq

-- Prove these without induction
-- These are a bit easier to solve with tactics
-- You can use the #find command instead of loogle

theorem p1 {x y : Nat} : x + (0 + y + x) = (2 * x + y) := _

theorem p2 {x y : Nat} : x + x + y + 0 * x = 2 * x + y := _

theorem p3 {a b c : Nat} : c * (b + 1 + a) = a * c + b * c + c := _


-- Inequalities
-- Defined as a ≠ b := ¬ (a = b)
-- Useful tactics:
-- contradiction: Closed the goal if the hypotheses contradict
-- exfalso: Replaces the goal with False
-- nomatch <expr>: Closes the goal if expr can't have any elements

example : 0 ≠ 1 := _

theorem p4 : ¬ (∀ (n : Nat), 2 + n = 1 + n) := _

-- This one is harder
theorem p5 (n : Nat) : 2 + n ≠ 1 + n := _

-- This is Viktor's favourite lemma, it's decently hard
theorem p6 : ¬ ∃ (n : Nat), 3 + n = 1 + n := _

theorem p7 : ¬ ∃ (n : Nat), n + n = 3 := _

theorem p8 {m n : Nat} : (m + n) * (m + n) = m * m + 2 * m * n + n * n := _

theorem p9 {m n : Nat} : (m + n) ^ 2 = m * m + 2 * m * n + n * n := _

theorem p10 {n : Nat} : 0 ^ (n + 1) = 0 := _

theorem p11 {n : Nat} : n ^ 0 = 1 := _

theorem p12 {n : Nat} : 1 ^ n = 1 := _

theorem p13 {n : Nat} : n ^ 1 = n := _

theorem p14 {l m n : Nat} : l ^ (m + n) = l ^ m * l ^ n := _

theorem p15 {l m n : Nat} : l ^ (m * n) = (l ^ m) ^ n := _

theorem p16 {l m n : Nat} : (l * m) ^ n = l ^ n * m ^ n := _
