-- Injectivity and Disjunction

import Mathlib.Tactic.Common
import Mathlib.Tactic.Find

universe u

open Nat
open List

-- Prove it with and without pattern matching!
theorem sucinj (m n : Nat) : succ m = succ n → m = n := _

-- Constructors in Lean4 and Agda are always injective. You can access injectivity lemmas at <Type>.<Constructor>.inj
-- Note that only constructors with parameters have a notion of injectivit, ex zero does not
#check Nat.succ.inj

-- A constructor with `k` parameters could have `k` different injectivity lemmas, but in Lean4, these are grouped into one:
#check List.cons.inj

inductive Tree : Type
  | leaf : Tree
  | node : (Nat → Tree) → Tree


-- If we wish to prove injectivity for all constructors' parameters of Tree, how many injectivity proofs do we need?
-- leaf has 0 parameters, node has 1, so we need 1

theorem Tree.nodeinj {f g : Nat → Tree} : node f = node g → f = g := _

-- What about this type?
inductive BinTree : Type
  | leaf : BinTree
  | node : BinTree → BinTree → BinTree

-- Prove injectivity for all parameters!

-- Disjunction of constructors

-- Prove it with and without pattern matching!
theorem disjunct_true_false : true ≠ false := _

-- Constructors in Lean4 and Agda are always disjunct.
-- A type with `k` constructors has `k·(k-1)/2` disjunction lemmas, one for every pair of constructors.

-- Prove disjunction of constructors for Nat, Tree and BinTree!

theorem disjunct_succ_zero : _ := _

theorem Tree.disjunct_leaf_node : _ := _

theorem BinTree.disjunct_leaf_node : _ := _

-- Harder exercise
theorem Nat.n_ne_succn (n : Nat) : n ≠ n + 1 := _

-- Decidable equality

def dec_eq_bool : DecidableEq Bool := _

def dec_eq_nat : DecidableEq Nat := _

def dec_eq_bintree : DecidableEq BinTree := _
