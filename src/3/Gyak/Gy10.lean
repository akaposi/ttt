-- Algebraic structures

import Mathlib.Tactic.Common
import Mathlib.Tactic.Find
import Mathlib.Tactic.Ring
import Mathlib.Tactic.Abel
import Mathlib.Tactic.Group
import Mathlib.Logic.Function.Defs
import Mathlib.Data.Fintype.Basic

open List
universe u

-- Mathematical properties can commonly be found on functions we've written.
-- A binary operator ⊕ is/has a(n)...
-- Associative if ∀ a, b, c        : (a ⊕ b) ⊕ c = a ⊕ (b ⊕ c)
-- Commutative if ∀ a, b           : a ⊕ b = b ⊕ a
-- Identity element (e) if ∀ a     : a ⊕ e = e ⊕ a = a
-- Inverse function (-) if ∀ a     : a ⊕ (- a) = (- a) ⊕ a = e
-- Distributive over ⊗ if ∀ a, b, c: (a ⊕ b) ⊗ c = a ⊗ c ⊕ a ⊗ c (and the other way around)

-- Grouping together properties like these into a hierarchy, we get the backbone of Group Theory
-- Here is the hierarchy which we'll look at today
/-

      via ⊗ and 1     Ring (⊕, 0, 1, ⊝, ⊗)       via ⊕, 0 and ⊝
  X-------------------[Distributive (⊗, ⊕)]-----------------------X
  |                                                               |
  |                                                               |
  |  Commutative Monoid (⊕, e)            via ⊕, e and ⊝          V
  |  [Commutative (⊕)] <------------------------X---------- Abelian Group (⊕, e, ⊝)
  |      |                                      |
  |      | via ⊕ and e                          |
  |      V                                      V
  |  Monoid (⊕, e)       via ⊕ and e   Group (⊕, e, ⊝)
  X->[Identity (⊕, e)] <-------------- [Inverse function (⊕, ⊝)]
         |
         | via ⊕
         V
     Semigroup (⊕)
     [Associative (⊕)]
-/

-- Let's prove that List forms a Monoid with ++

instance {α : Type u} : Monoid (List α) where
  mul := (. ++ .)
  -- Associativity proof
  mul_assoc := _
  -- Identity element
  one := _
  -- Left and right identity proofs
  one_mul := _
  mul_one := _

-- The definition of Monoid could have been done differently:
-- Ex.: The type could have been indexed by the `mul` function
-- Would make instance search harder though

open Function

-- Prove that injective functions form a group over composition
-- Some help:
#check Fintype.bijInv

instance {α : Type u}[DecidableEq α][Fintype α] : Group (Σ' (f : α → α), Bijective f) where
  mul := _
  mul_assoc := _
  one := _
  one_mul := _
  mul_one := _
  inv := _
  inv_mul_cancel := _


/- This proof is a bit too complicated
    | ⟨ f, pf ⟩ => by
      simp [(. * .)]
      have leftinv := Fintype.leftInverse_bijInv pf
      have aux : PSigma.snd ⟨ f , pf ⟩ = pf := rfl
      unfold LeftInverse at leftinv
      conv =>
        lhs
        congr
        intro a
        rw [aux]
        simp [(. ∘ .)]
        rw [leftinv]
      rfl
-/
-- For grouplike structures, simple equations are automatically solveable
-- We have a few tactics for that (which can't be used in the midterm)
-- group, abel, ring

example {n k : Nat} : (n + k) ^ 2 = n ^ 2 + 2 * n * k + k ^ 2 := _
