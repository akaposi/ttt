{-# OPTIONS --safe --guardedness --without-K --no-postfix-projection #-}

module Lib where

-- There are only container's types imported.
-- They have to be opened manually,
-- because there are a lot of functions that have the same name.

open import Lib.Class public
open import Lib.Level public
open import Lib.Function public
open import Lib.Inspect public

module Reflection where
  open import Lib.Reflection public
  {-
    renaming ( irrelevant to irrelevant-in-reflection
             ; absurd to absurd-in-reflection
             )
  -}

open import Lib.Unit.Type hiding (module ⊤) public
module ⊤ where
  open import Lib.Unit hiding (module ⊤) public
open ⊤ public
  renaming ( _≟_ to _≟⊤_ )

open import Lib.Empty.Type public
module ⊥ where
  open import Lib.Empty public
open ⊥ public
  renaming ( _≟_ to _≟⊥_ )

open import Lib.Nat.Type hiding (module ℕ) public
module ℕ where
  open import Lib.Nat hiding (module ⊤; module ℕ) public
open ℕ public
  renaming ( _≟_ to _≟ℕ_
           ; case to case-ℕ
           ; case′ to case′-ℕ
           ; case-proof to case-ℕ-proof
           ; iteᵣ to iteᵣ-ℕ
           ; ite to ite-ℕ
           ; iteₗ to iteₗ-ℕ
           ; recᵣ to recᵣ-ℕ
           ; rec to rec-ℕ
           ; recₗ to recₗ-ℕ
           ; elimᵣ to elimᵣ-ℕ
           ; elim to elim-ℕ
           ; elimₗ to elimₗ-ℕ
           ; iteᵣ′ to iteᵣ′-ℕ
           ; ite′ to ite′-ℕ
           ; iteₗ′ to iteₗ′-ℕ
           ; recᵣ′ to recᵣ′-ℕ
           ; rec′ to rec′-ℕ
           ; recₗ′ to recₗ′-ℕ
           ; elimᵣ′ to elimᵣ′-ℕ
           ; elim′ to elim′-ℕ
           ; elimₗ′ to elimₗ′-ℕ
           ; compare to compare-ℕ
           ; _⊔_ to _⊔ℕ_
           ; _⊓_ to _⊓ℕ_
           )
{-
open import Lib.Integer.Type hiding (module ℤ) public
module ℤ where
  open import Lib.Integer hiding (module ⊤; module ℤ) public
open ℤ public
  renaming ( case to case-ℤ
           ; case′ to case′-ℤ
           ; ite to ite-ℤ
           ; elim to elim-ℤ
           ; ite′ to ite′-ℤ
           ; elim′ to elim′-ℤ
           ; _+_ to _+ℤ_
           ; _*_ to _*ℤ_
           ; _^_ to _^ℤ_
           ; _-_ to _-ℤ_
           ; pred to pred-ℤ
           ; suc to suc-ℤ
           )
-}
open import Lib.Bool.Type hiding (module Bool) public
module Bool where
  open import Lib.Bool hiding (module Bool) public
open Bool public
  renaming ( contradiction to contradictionᵇ
           ; contraposition to contrapositionᵇ
           ; _≟_ to _≟ᵇ_
           ; elim to elim-Bool
           ; elim′ to elim′-Bool
           ; ind to ind-Bool
           ; ite to ite-Bool
           ; ite′ to ite′-Bool
           )

open import Lib.CoNat.Type hiding (module ℕ∞) public
module ℕ∞ where
  open import Lib.CoNat hiding (module ⊤; module ℕ∞) public
open ℕ∞ public
  renaming ( coite to coite-ℕ∞
           ; _+_ to _+∞_
           ; add to add∞
           ; add' to add∞'
           ; _*_ to _*∞_
           ; idr+ to idr+∞
           ; idl+ to idl+∞
           ; _^_ to _^∞_
           ; embed to embed-ℕ∞
           ; cong-+ᵣ to cong-+∞ᵣ
           ; cong-+ₗ to cong-+∞ₗ
          -- ; *-injectiveᵣ to *∞-injectiveᵣ
          -- ; *-injectiveₗ to *∞-injectiveₗ
           )

open import Lib.Sum.Type public
module ⊎ where
  open import Lib.Sum public
open ⊎ public
  renaming ( map to map-⊎
           ; mapᵣ to mapᵣ-⊎
           ; mapₗ to mapₗ-⊎
           ; elim to elim-⊎
           ; elim′ to elim′-⊎
           ; ite to ite-⊎
           ; ite′ to ite′-⊎
           ; ind to ind-⊎
           ; swap to swap-⊎
           )

open import Lib.Fin.Type hiding (module Fin) public
module Fin where
  open import Lib.Fin hiding (module ⊤; module Fin) public
open Fin public
  renaming ( cast to cast-Fin
           ; iteᵣ to iteᵣ-Fin
           ; ite to ite-Fin
           ; iteₗ to iteₗ-Fin
           ; recᵣ to recᵣ-Fin
           ; rec to rec-Fin
           ; recₗ to recₗ-Fin
           ; elimᵣ to elimᵣ-Fin
           ; elim to elim-Fin
           ; elimₗ to elimₗ-Fin
           ; iteᵣ′ to iteᵣ′-Fin
           ; ite′ to ite′-Fin
           ; iteₗ′ to iteₗ′-Fin
           ; recᵣ′ to recᵣ′-Fin
           ; rec′ to rec′-Fin
           ; recₗ′ to recₗ′-Fin
           ; elimᵣ′ to elimᵣ′-Fin
           ; elim′ to elim′-Fin
           ; elimₗ′ to elimₗ′-Fin
           ; _≟_ to _≟Fin_
           )

open import Lib.CoFin.Type hiding (module Fin∞) public
module Fin∞ where
  open import Lib.CoFin hiding (module Fin∞) public
open Fin∞ public
  renaming ( coite to coite-CoFin
      ; coiteΣ to coite-CoFinΣ
      ; coiteιΣ to coite-CoFinιΣ
      )

open import Lib.IndCoFin.Type hiding (module Fin∞ᵢ) public
module Fin∞ᵢ where
  open import Lib.IndCoFin hiding (module Fin∞ᵢ) public
open Fin∞ᵢ public
  renaming ( embed to embed-IndCoFin
           ; cast to cast-IndCoFin
           )

open import Lib.Equality.Type public
module ≡ where
  open import Lib.Equality public
open ≡ public

open import Lib.Dec.Type hiding (module Dec) public
module Dec where
  open import Lib.Dec hiding (module Dec) public
open Dec public
  renaming ( ite to ite-Dec
           ; ite′ to ite′-Dec
           ; elim to elim-Dec
           ; elim′ to elim′-Dec
           ; ind to ind-Dec
           )

open import Lib.Maybe.Type hiding (module Maybe) public
module Maybe where
  open import Lib.Maybe hiding (module Maybe) public
open Maybe public
  renaming ( elim to elim-Maybe
           ; elim′ to elim′-Maybe
           ; ite to ite-Maybe
           ; ite′ to ite′-Maybe
           ; ind to ind-Maybe
           ; map to map-Maybe
           )

open import Lib.Ordering.Type hiding (module Ordering) public
module Ordering where
  open import Lib.Ordering hiding (module Ordering) public
open Ordering public
  renaming ( _≟_ to _≟Ordering_
           ; case to case-Ordering
           ; ite to ite-Ordering
           ; elim to elim-Ordering
           ; ind to ind-Ordering
           )

open import Lib.UnitOrEmpty public

open import Lib.Pointed public

open import Lib.Isomorphism.Type public
module ≅ where
  open import Lib.Isomorphism public
open ≅ public

open import Lib.Lazy.Type hiding (module Lazy) public
module Lazy where
  open import Lib.Lazy hiding (module Lazy) public -- Functor, Applicative, Monad class needed and rename stuff in Base.
open Lazy public
  renaming ( ite to ite-Lazy )

open import Lib.Relation public

open import Lib.TruncLevel.Type hiding (module TruncLevel) public
module TruncLevel where
  open import Lib.TruncLevel hiding (module ⊤; module TruncLevel) public
open TruncLevel public

open import Lib.Char.Type public
module Char where
  open import Lib.Char public
open Char public
  hiding (module String)
  renaming ( _==_ to _==Char_ )

open import Lib.String.Type public
module String where
  open import Lib.String public
open String public
  hiding (module Char)
  renaming ( _==_ to _==String_
           ; _++_ to _++String_
           ; uncons to unconsString
           )

------------------------------------------------------------
-- Change when needed
-- open import Lib.Product public
  -- renaming (map to map-×)

open import Lib.Sigma.Type hiding (module Σ) public
module Σ where
  open import Lib.Sigma hiding (module Σ) public
open Σ public
  renaming ( map to map-Σ
           ; swap to swap-×
           ; elim to elim-Σ
           ; elim′ to elim′-Σ
           ; ite to ite-×
           ; ite′ to ite′-×
           ; ind to ind-Σ
           ; ind' to ind-×
           )

------------------------------------------------------------
-- Containers

open import Lib.Containers.List.Type hiding (module List) public
module List where
  open import Lib.Containers.List hiding (module List) public

open import Lib.Containers.Stream.Type hiding (module Stream) public
module Stream where
  open import Lib.Containers.Stream hiding (module Stream) public

open import Lib.Containers.CoList.Type hiding (module List∞; _∷_; []; head; tail) public
module List∞ where
  open import Lib.Containers.CoList hiding (module List∞) public

open import Lib.Containers.Vec.Type hiding (module Vec) public
module Vec where
  open import Lib.Containers.Vec hiding (module Vec) public

open import Lib.Containers.CoVec.Type hiding (module Vec∞; []) public
module Vec∞ where
  open import Lib.Containers.CoVec hiding (module Vec∞) public

open import Lib.Containers.HList.Type hiding (module HList) public
module HList where
  open import Lib.Containers.HList hiding (module HList) public
