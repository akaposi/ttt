{-# OPTIONS --safe --without-K #-}

module Lib.Algebra.Definitions where

open import Lib.Equality.Type

open import Lib.Sigma.Type

open import Lib.Sum.Type

Associative : ∀{i}{A : Set i} → (A → A → A) → Set i
Associative _∙_ = ∀ x y z → ((x ∙ y) ∙ z) ≡ (x ∙ (y ∙ z))

Commutative : ∀{i}{A : Set i} → (A → A → A) → Set i
Commutative _∙_ = ∀ x y → (x ∙ y) ≡ (y ∙ x)

LeftIdentity : ∀{i}{A : Set i} → (A → A → A) → A → Set i
LeftIdentity _∙_ e = ∀ x → (e ∙ x) ≡ x

RightIdentity : ∀{i}{A : Set i} → (A → A → A) → A → Set i
RightIdentity _∙_ e = ∀ x → (x ∙ e) ≡ x

Identity : ∀{i}{A : Set i} → (A → A → A) → A → Set i
Identity ∙ e = (LeftIdentity ∙ e) × (RightIdentity ∙ e)

LeftZero : ∀{i}{A : Set i} → (A → A → A) → A → Set i
LeftZero _∙_ z = ∀ x → (z ∙ x) ≡ z

RightZero : ∀{i}{A : Set i} → (A → A → A) → A → Set i
RightZero _∙_ z = ∀ x → (x ∙ z) ≡ z

Zero : ∀{i}{A : Set i} → (A → A → A) → A → Set i
Zero ∙ z = (LeftZero ∙ z) × (RightZero ∙ z)

LeftInverse : ∀{i}{A : Set i} → (A → A) → (A → A → A) → A → Set i
LeftInverse _⁻¹ _∙_ e = ∀ x → ((x ⁻¹) ∙ x) ≡ e

RightInverse : ∀{i}{A : Set i} → (A → A) → (A → A → A) → A → Set i
RightInverse _⁻¹ _∙_ e = ∀ x → (x ∙ (x ⁻¹)) ≡ e

Inverse : ∀{i}{A : Set i} → (A → A) → (A → A → A) → A → Set i
Inverse ⁻¹ ∙ e = LeftInverse ⁻¹ ∙ e × RightInverse ⁻¹ ∙ e

LeftInvertible : ∀{i}{A : Set i} → (A → A → A) → A → A → Set i
LeftInvertible _∙_ x e = ∃[ x⁻¹ ] (x⁻¹ ∙ x) ≡ e

RightInvertible : ∀{i}{A : Set i} → (A → A → A) → A → A → Set i
RightInvertible _∙_ x e = ∃[ x⁻¹ ] (x ∙ x⁻¹) ≡ e

Invertible : ∀{i}{A : Set i} → (A → A → A) → A → A → Set i
Invertible _∙_ x e = ∃[ x⁻¹ ] (x⁻¹ ∙ x) ≡ e × (x ∙ x⁻¹) ≡ e

LeftConical : ∀{i}{A : Set i} → (A → A → A) → A → Set i
LeftConical _∙_ e = ∀ x y → (x ∙ y) ≡ e → x ≡ e

RightConical : ∀{i}{A : Set i} → (A → A → A) → A → Set i
RightConical _∙_ e = ∀ x y → (x ∙ y) ≡ e → y ≡ e

Conical : ∀{i}{A : Set i} → (A → A → A) → A → Set i
Conical ∙ e = LeftConical ∙ e × RightConical ∙ e

infix 4 _DistributesOverˡ_ _DistributesOverʳ_ _DistributesOver_

_DistributesOverˡ_ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
_*_ DistributesOverˡ _+_ = ∀ x y z → (x * (y + z)) ≡ ((x * y) + (x * z))

_DistributesOverʳ_ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
_*_ DistributesOverʳ _+_ =
  ∀ x y z → ((y + z) * x) ≡ ((y * x) + (z * x))

_DistributesOver_ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
* DistributesOver + = (* DistributesOverˡ +) × (* DistributesOverʳ +)

infix 4 _MiddleFourExchange_ _IdempotentOn_ _Absorbs_

_MiddleFourExchange_ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
_*_ MiddleFourExchange _+_ = ∀ w x y z → ((w + x) * (y + z)) ≡ ((w + y) * (x + z))

_IdempotentOn_ : ∀{i}{A : Set i} → (A → A → A) → A → Set i
_∙_ IdempotentOn x = (x ∙ x) ≡ x

Idempotent : ∀{i}{A : Set i} → (A → A → A) → Set i
Idempotent ∙ = ∀ x → ∙ IdempotentOn x

IdempotentFun : ∀{i}{A : Set i} → (A → A) → Set i
IdempotentFun f = ∀ x → f (f x) ≡ f x

Selective : ∀{i}{A : Set i} → (A → A → A) → Set i
Selective _∙_ = ∀ x y → (x ∙ y) ≡ x ⊎ (x ∙ y) ≡ y

_Absorbs_ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
_∙_ Absorbs _∘_ = ∀ x y → (x ∙ (x ∘ y)) ≡ x

Absorptive : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
Absorptive ∙ ∘ = (∙ Absorbs ∘) × (∘ Absorbs ∙)

SelfInverse : ∀{i}{A : Set i} → (A → A) → Set i
SelfInverse f = ∀ {x y} → f x ≡ y → f y ≡ x

Involutive : ∀{i}{A : Set i} → (A → A) → Set i
Involutive f = ∀ x → f (f x) ≡ x

LeftCancellative : ∀{i}{A : Set i} → (A → A → A) → Set i
LeftCancellative _•_ = ∀ x y z → (x • y) ≡ (x • z) → y ≡ z

RightCancellative : ∀{i}{A : Set i} → (A → A → A) → Set i
RightCancellative _•_ = ∀ x y z → (y • x) ≡ (z • x) → y ≡ z

Cancellative : ∀{i}{A : Set i} → (A → A → A) → Set i
Cancellative _•_ = (LeftCancellative _•_) × (RightCancellative _•_)

AlmostLeftCancellative : ∀{i}{A : Set i} → (A → A → A) → A → Set i
AlmostLeftCancellative _•_ e = ∀ x y z → x ≢ e → (x • y) ≡ (x • z) → y ≡ z

AlmostRightCancellative : ∀{i}{A : Set i} → (A → A → A) → A → Set i
AlmostRightCancellative _•_ e = ∀ x y z → x ≢ e → (y • x) ≡ (z • x) → y ≡ z

AlmostCancellative : ∀{i}{A : Set i} → (A → A → A) → A → Set i
AlmostCancellative _•_ e = AlmostLeftCancellative _•_ e × AlmostRightCancellative _•_ e

Interchangable : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
Interchangable _∘_ _∙_ = ∀ w x y z → ((w ∙ x) ∘ (y ∙ z)) ≡ ((w ∘ y) ∙ (x ∘ z))

LeftDividesˡ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
LeftDividesˡ _∙_  _\\_ = ∀ x y → (x ∙ (x \\ y)) ≡ y

LeftDividesʳ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
LeftDividesʳ _∙_ _\\_ = ∀ x y → (x \\ (x ∙ y)) ≡ y

RightDividesˡ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
RightDividesˡ _∙_ _//_ = ∀ x y → ((y // x) ∙ x) ≡ y

RightDividesʳ : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
RightDividesʳ _∙_ _//_ = ∀ x y → ((y ∙ x) // x) ≡ y

LeftDivides : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
LeftDivides ∙ \\ = (LeftDividesˡ ∙ \\) × (LeftDividesʳ ∙ \\)

RightDivides : ∀{i}{A : Set i} → (A → A → A) → (A → A → A) → Set i
RightDivides ∙ // = (RightDividesˡ ∙ //) × (RightDividesʳ ∙ //)

LeftAlternative : ∀{i}{A : Set i} → (A → A → A) → Set i
LeftAlternative _∙_ = ∀ x y  →  ((x ∙ x) ∙ y) ≡ (x ∙ (x ∙ y))

RightAlternative : ∀{i}{A : Set i} → (A → A → A) → Set i
RightAlternative _∙_ = ∀ x y → (x ∙ (y ∙ y)) ≡ ((x ∙ y) ∙ y)

Alternative : ∀{i}{A : Set i} → (A → A → A) → Set i
Alternative _∙_ = (LeftAlternative _∙_ ) × (RightAlternative _∙_)

Flexible : ∀{i}{A : Set i} → (A → A → A) → Set i
Flexible _∙_ = ∀ x y → ((x ∙ y) ∙ x) ≡ (x ∙ (y ∙ x))

Medial : ∀{i}{A : Set i} → (A → A → A) → Set i
Medial _∙_ = ∀ x y u z → ((x ∙ y) ∙ (u ∙ z)) ≡ ((x ∙ u) ∙ (y ∙ z))

LeftSemimedial : ∀{i}{A : Set i} → (A → A → A) → Set i
LeftSemimedial _∙_ = ∀ x y z → ((x ∙ x) ∙ (y ∙ z)) ≡ ((x ∙ y) ∙ (x ∙ z))

RightSemimedial : ∀{i}{A : Set i} → (A → A → A) → Set i
RightSemimedial _∙_ = ∀ x y z → ((y ∙ z) ∙ (x ∙ x)) ≡ ((y ∙ x) ∙ (z ∙ x))

Semimedial : ∀{i}{A : Set i} → (A → A → A) → Set i
Semimedial _∙_ = (LeftSemimedial _∙_) × (RightSemimedial _∙_)

LeftBol : ∀{i}{A : Set i} → (A → A → A) → Set i
LeftBol _∙_ = ∀ x y z → (x ∙ (y ∙ (x ∙ z))) ≡ ((x ∙ (y ∙ x)) ∙ z )

RightBol : ∀{i}{A : Set i} → (A → A → A) → Set i
RightBol _∙_ = ∀ x y z → (((z ∙ x) ∙ y) ∙ x) ≡ (z ∙ ((x ∙ y) ∙ x))

Identical : ∀{i}{A : Set i} → (A → A → A) → Set i
Identical _∙_ = ∀ x y z → ((z ∙ x) ∙ (y ∙ z)) ≡ (z ∙ ((x ∙ y) ∙ z))
