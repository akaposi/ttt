{-# OPTIONS --safe --without-K #-}

module Lib.Bool.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Bool.Type
open import Lib.Bool.Instances.Eq

open import Lib.Equality.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Dec.Type

open import Lib.Sum.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdBool : Ord Bool
  eq OrdBool = EqBool

  (OrdBool ≤ false) _ = ⊤
  (OrdBool ≤ true) false = ⊥
  (OrdBool ≤ true) true = ⊤

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdBool)))) {false} = tt
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdBool)))) {true} = tt

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdBool)))) {false} {b} {c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdBool)))) {true} {false} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdBool)))) {true} {true} {c} = λ _ e → e

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdBool))) {false} {false} _ _ = refl
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdBool))) {false} {true} _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdBool))) {true} {false} ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdBool))) {true} {true} _ _ = refl

  connected (totalOrder (decidableTotalOrder OrdBool)) {false} {b} = inl tt
  connected (totalOrder (decidableTotalOrder OrdBool)) {true} {false} = inr tt
  connected (totalOrder (decidableTotalOrder OrdBool)) {true} {true} = inl tt

  Decidable₂.decide (decidable (decidableTotalOrder OrdBool)) false y = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdBool)) true false = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder OrdBool)) true true = yes tt
