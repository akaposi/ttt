{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Char where

open import Lib.Char.Type public
open import Lib.Char.Base public
open import Lib.Char.Properties public
open import Lib.Char.Instances public
