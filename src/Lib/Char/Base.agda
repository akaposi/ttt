{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Base where

open import Lib.Bool.Type

open import Lib.Nat.Type

open import Agda.Builtin.Char

isLower isDigit isAlpha isSpace isAscii isLatin1 isPrint isHexDigit : Char → Bool
isLower = primIsLower
{-# DISPLAY primIsLower = isLower #-}

isDigit = primIsDigit
{-# DISPLAY primIsDigit = isDigit #-}

isAlpha = primIsAlpha
{-# DISPLAY primIsAlpha = isAlpha #-}

isSpace = primIsSpace
{-# DISPLAY primIsSpace = isSpace #-}

isAscii = primIsAscii
{-# DISPLAY primIsAscii = isAscii #-}

isLatin1 = primIsLatin1
{-# DISPLAY primIsLatin1 = isLatin1 #-}

isPrint = primIsPrint
{-# DISPLAY primIsPrint = isPrint #-}

isHexDigit = primIsHexDigit
{-# DISPLAY primIsHexDigit = isHexDigit #-}

toUpper toLower : Char → Char
toUpper = primToUpper
{-# DISPLAY primToUpper = toUpper #-}

toLower = primToLower
{-# DISPLAY primToLower = toLower #-}

chr : ℕ → Char
chr = primNatToChar
{-# DISPLAY primNatToChar = chr #-}

ord : Char → ℕ
ord = primCharToNat
{-# DISPLAY primCharToNat = ord #-}

infix 4 _==_
_==_ : Char → Char → Bool
_==_ = primCharEquality
{-# DISPLAY primCharEquality = _==_ #-}

module String where
  open import Lib.Containers.List.Type

  open import Agda.Builtin.String
  open import Agda.Builtin.String.Properties

  open import Lib.Equality.Type

  convert : List Char → String
  convert = primStringFromList

  convert-injective : ∀{a b} → convert a ≡ convert b → a ≡ b
  convert-injective {a} {b} = primStringFromListInjective a b
