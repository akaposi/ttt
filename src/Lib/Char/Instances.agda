{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Char.Instances where

open import Lib.Char.Instances.Eq public
open import Lib.Char.Instances.Ord public
open import Lib.Char.Instances.DecidableEquality public
open import Lib.Char.Instances.IsSet public
