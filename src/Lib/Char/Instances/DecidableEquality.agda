{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Instances.DecidableEquality where

open import Lib.Char.Type
open import Lib.Char.Base
open import Lib.Char.Properties

open import Lib.Equality.Base

open import Lib.Dec.Type
open Decidable₂

open import Lib.Nat.Instances.DecidableEquality

instance
  DecEqChar : DecidableEquality Char
  decide DecEqChar x y with decide DecEqℕ (ord x) (ord y)
  ... | yes a = yes (ord-injective a)
  ... | no ¬a = no λ e → ¬a (cong ord e)
