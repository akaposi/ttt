{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Instances.Eq where

open import Lib.Class.Eq
open Eq

open import Lib.Char.Type

open import Lib.Char.Instances.DecidableEquality

instance
  EqChar : Eq Char
  EqChar = EqInstance
