{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Instances.IsSet where

open import Lib.Class.IsSet

open import Lib.Dec.InstanceGenerators
open import Lib.Char.Instances.DecidableEquality

open import Lib.Char.Type

instance
  IsSetChar : IsSet Char
  IsSetChar = DecidableEquality→IsSet DecEqChar
