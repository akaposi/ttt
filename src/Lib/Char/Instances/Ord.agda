{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Class.Eq

open import Lib.Char.Type
open import Lib.Char.Base
open import Lib.Char.Properties
open import Lib.Char.Instances.Eq

open import Lib.Nat.Instances.Eq
open import Lib.Nat.Instances.Ord

open import Lib.Equality.Base

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Unit.Type

open import Lib.Sum.Type

open import Lib.Dec.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdChar : Ord Char
  eq OrdChar = EqChar

  _≤_ OrdChar c1 c2 = (Ordℕ ≤ ord c1) (ord c2)

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdChar)))) {a} = reflexive (decidableTotalOrder Ordℕ) {ord a}

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdChar)))) {a} {b} {c} = transitive (decidableTotalOrder Ordℕ) {ord a} {ord b} {ord c}

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdChar))) {a} {b} e1 e2 = ord-injective (antisymmetric (decidableTotalOrder Ordℕ) {ord a} {ord b} e1 e2)

  connected (totalOrder (decidableTotalOrder OrdChar)) {a} {b} = connected (decidableTotalOrder Ordℕ) {ord a} {ord b}

  Decidable₂.decide (decidable (decidableTotalOrder OrdChar)) a b = (decidableTotalOrder Ordℕ ≤? ord a) (ord b)
