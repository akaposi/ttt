{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Properties where

open import Lib.Char.Base
open import Lib.String.Base

open import Lib.Containers.List.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Agda.Builtin.Char.Properties

ord-injective : ∀{a b} → ord a ≡ ord b → a ≡ b
ord-injective {a} {b} = primCharToNatInjective a b
