{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Char.Type where

open import Agda.Builtin.Char using (Char) public
