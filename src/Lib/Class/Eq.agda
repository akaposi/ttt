{-# OPTIONS --safe --without-K #-}

module Lib.Class.Eq where

open import Agda.Primitive

open import Lib.Bool.Type

open import Lib.Sigma.Type

open import Lib.Equality.Type

open import Lib.Dec.Type
open import Lib.Dec.Base

open import Lib.Empty.Base

open import Lib.Unit.Type

record Eq {i} (A : Set i) : Set (lsuc i) where
  constructor EqInstance
  inductive
  field
    ⦃ decEqA ⦄ : DecidableEquality A

  _≟_ = decide decEqA

  ≡↔yes : ∀{a b} → a ≡ b ↔ IsYes (a ≟ b)
  fst (≡↔yes {a} {b}) e with a ≟ b
  ... | yes p = prove-yes
  ... | no ¬p = exfalso (¬p e)
  snd (≡↔yes {a} {b}) n with a ≟ b
  ... | yes e = e

  ≢↔no : ∀{a b} → a ≢ b ↔ IsNo (a ≟ b)
  fst (≢↔no {a} {b}) e with a ≟ b
  ... | yes p = exfalso (e p)
  ... | no ¬p = prove-no
  snd (≢↔no {a} {b}) n e with a ≟ b
  ... | no ¬a = ¬a e

  _==_ : (a b : A) → Bool
  a == b with a ≟ b
  ... | yes _ = true
  ... | no  _ = false

  _/=_ : (a b : A) → Bool
  a /= b with a ≟ b
  ... | yes _ = false
  ... | no  _ = true

  infix 4 _==_ _/=_ _≟_

open Eq {{...}} public
