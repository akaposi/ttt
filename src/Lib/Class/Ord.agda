{-# OPTIONS --safe --without-K #-}

module Lib.Class.Ord where

open import Lib.Level

open import Lib.Bool.Type

open import Lib.Class.Eq

open import Lib.Ordering.Type
open import Lib.Ordering.Properties hiding (_≟_)

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Sum.Type
open import Lib.Sum.Base

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Relation.Structures

open import Lib.Dec.Type

record Ord {i}(A : Set i) : Set (lsuc i) where
  constructor OrdInstance
  infix 4 _≤_
  field
    overlap ⦃ eq ⦄ : Eq A
    _≤_ : A → A → Set -- Make sure this is indeed less than or equals, otherwise it can also behave in a flipped manner.
    instance decidableTotalOrder : DecidableTotalOrder _≤_

  open DecidableTotalOrder {{...}}

  infix 4 compare _<_ _>_ _≥_ _≤ᵇ_ _<ᵇ_ _≥ᵇ_ _>ᵇ_

  _≥_ = λ x y → y ≤ x
  _<_ = λ x y → x ≤ y × ¬ (y ≤ x)
  _>_ = λ x y → y < x

  compare : A → A → Ordering
  compare x y with x ≟ y
  ... | yes _ = EQ
  ... | no  _ with x ≤? y
  ... | yes _ = LT
  ... | no  _ = GT

  _≤ᵇ_ : A → A → Bool
  x ≤ᵇ y with compare x y
  ... | LT = true
  ... | EQ = true
  ... | GT = false

  _<ᵇ_ : A → A → Bool
  x <ᵇ y with compare x y
  ... | LT = true
  ... | EQ = false
  ... | GT = false

  _≥ᵇ_ : A → A → Bool
  x ≥ᵇ y = y ≤ᵇ x

  _>ᵇ_ : A → A → Bool
  x >ᵇ y = y <ᵇ x
{-
  sameEQ : ∀{x} → compare x x ≡ EQ
  sameEQ {x} with compare x x in eq1
  ... | LT = exfalso (GT≢LT (sym (fst flippable eq1) ◾ eq1))
  ... | EQ = refl
  ... | GT = exfalso (GT≢LT (sym eq1 ◾ snd flippable eq1))
-}
open Ord {{...}} public
