{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Class.Show where

open import Lib.String.Type

record Show {i}(A : Set i) : Set i where
  constructor ShowInstance
  field
    show : A → String

open Show {{...}} public
