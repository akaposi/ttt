{-# OPTIONS --safe --without-K #-}

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Agda.Primitive

module Lib.Class.Uninhabited where

record Uninhabited {i} (A : Set i) : Set i where
  constructor UninhabitedInstance
  field
    uninhabited : A → ⊥

  absurd : ∀{j}{B : Set j} → A → B
  absurd t = exfalso (uninhabited t)

  absurd-irrelevant : ∀{j}{B : Set j} → .A → B
  absurd-irrelevant t = exfalso-irrelevant (uninhabited t)

open Uninhabited {{...}} public

record Uninhabitedω (A : Setω) : Setω where
  constructor UninhabitedωInstance
  field
    uninhabitedω : A → ⊥

  absurdω : ∀{j}{B : Set j} → A → B
  absurdω t = exfalso (uninhabitedω t)

  absurdω-irrelevant : ∀{j}{B : Set j} → .A → B
  absurdω-irrelevant t = exfalso-irrelevant (uninhabitedω t)

open Uninhabitedω {{...}} public
