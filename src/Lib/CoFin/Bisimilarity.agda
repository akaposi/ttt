{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.CoFin.Bisimilarity where

open import Lib.CoNat.Type
open import Lib.CoNat.Base

open import Lib.Maybe.Type

open import Lib.CoFin.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

infix 4 _≈Fin∞_ _≈Fin∞′_
record _≈Fin∞_ {n : ℕ∞} (k l : Fin∞ n) : Set

data _≈Fin∞′_ {n : ℕ∞} .⦃ e : IsNotZero∞ n ⦄ : Maybe (Fin∞ (predℕ∞ n)) → Maybe (Fin∞ (predℕ∞ n)) → Set where
  instance nothing-refl : nothing ≈Fin∞′ nothing
  cong-just             : ∀{x y} → x ≈Fin∞ y → just x ≈Fin∞′ just y

record _≈Fin∞_ {n} k l where
  coinductive
  field
    prove : _≈Fin∞′_ {n} ⦃ inz k ⦄ (fpred∞ k) (fpred∞ l)

open _≈Fin∞_ public
