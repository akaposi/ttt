{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.CoFin.Literals where

open import Agda.Builtin.FromNat public

open import Lib.CoFin.Type

open import Lib.CoNat.Type
open import Lib.CoNat.Base
open import Lib.CoNat.Properties

open import Lib.Nat.Type

open import Lib.Maybe.Type

instance
  open Number
  NumFin∞ : {n : ℕ∞} → .⦃ IsNotZero∞ n ⦄ → Number (Fin∞ n)
  Constraint (NumFin∞ {n}) k = k ℕ<ℕ∞ n
  fromNat (NumFin∞ {n} ⦃ e ⦄) zero ⦃ inst ⦄ = fin∞ ⦃ recomputeIsNotZero∞ {n} ⦄ nothing
  inz (fromNat (NumFin∞ {n} ⦃ e ⦄) (suc k) ⦃ inst ⦄) = recomputeIsNotZero∞ {n}
  fpred∞ (fromNat (NumFin∞ {n} ⦃ e ⦄) (suc k) ⦃ inst ⦄) with .(isNotZero∞ e)
  ... | _ with pred∞ n
  ... | just x = just (fromNat (NumFin∞ {x} ⦃ <→IsNotZero∞ {k} {x} ⦄) k ⦃ inst ⦄ )
