{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.CoFin.Type where

open import Lib.CoNat.Base
open import Lib.CoNat.Literals
open import Lib.CoNat.Type

open import Lib.Maybe.Type

open import Lib.Empty.Type

open import Lib.Equality.Type

record Fin∞ (n : ℕ∞) : Set where
  coinductive
  constructor fin∞
  field
    ⦃ inz ⦄ : IsNotZero∞ n
    fpred∞ : Maybe (Fin∞ (predℕ∞ n))

open Fin∞ public

coz : Fin∞ 0 → ⊥
coz c = isNotZero∞ (inz c)
{-
proba : (n : ℕ∞) → .⦃ p1 p2 : IsNotZero∞ n ⦄ → Fin∞ n
proba n = cofin ⦃ recomputeIsNotZero∞ {n} ⦄ {- Az instance paramétert még valahogy el kéne tűntetni. -} nothing
-}
