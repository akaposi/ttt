{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.Containers.CoList.Base where

open import Lib.Containers.CoList.Type
open import Lib.Containers.CoList.PatternSynonym

open import Lib.Sum.Type

open import Lib.Sigma.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Nat.Type

open import Lib.CoNat.Type
open import Lib.CoNat.PatternSynonym

open import Lib.Level

open import Lib.Maybe.Type

open import Lib.Equality.Type

Null∞ᵗ : ∀{i}{A : Set i} → List∞ A → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
Null∞ᵗ xs with uncons xs
... | _ ∷∞ _ = ⊥ , inr refl
... | []∞ = ⊤ , inl refl

record Null∞ {i}{A : Set i}(xs : List∞ A) : Set where
  instance constructor prove-null∞
  field
    ⦃ null∞ ⦄ : fst (Null∞ᵗ xs)

open Null∞ {{...}} public

NotNull∞ᵗ : ∀{i}{A : Set i} → List∞ A → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
NotNull∞ᵗ xs with uncons xs
... | []∞ = ⊥ , inr refl
... | _ ∷∞ _ = ⊤ , inl refl

record NotNull∞ {i}{A : Set i}(xs : List∞ A) : Set where
  instance constructor prove-notNull∞
  field
    ⦃ notNull∞ ⦄ : fst (NotNull∞ᵗ xs)

open NotNull∞ {{...}} public

replicate : ∀{i}{A : Set i} → ℕ → A → List∞ A
replicate zero a = []
replicate (suc n) a = a ∷ replicate n a

replicate∞ : ∀{i}{A : Set i} → ℕ∞ → A → List∞ A
uncons (replicate∞ n a) with pred∞ n
uncons (replicate∞ n a) | zero∞ = []∞
uncons (replicate∞ n a) | suc∞ n' = a ∷∞ replicate∞ n' a

repeat : ∀{i}{A : Set i} → A → List∞ A
repeat = replicate∞ ∞

take∞ : ∀{i}{A : Set i} → ℕ∞ → List∞ A → List∞ A
uncons (take∞ n xs) with pred∞ n
uncons (take∞ n xs) | zero∞   = []∞
uncons (take∞ n xs) | suc∞ n' with uncons xs
uncons (take∞ n xs) | suc∞ n' | []∞     = []∞
uncons (take∞ n xs) | suc∞ n' | y ∷∞ ys = y ∷∞ take∞ n' ys

module List where
  import Lib.Containers.List.Type as L
  open L using (List)

  splitAt : ∀{i}{A : Set i} → ℕ → List∞ A → List A × List∞ A
  splitAt zero xs = L.[] , xs
  splitAt (suc n) xs with uncons xs
  ... | []∞     = L.[] , []
  ... | x ∷∞ xs = let (a , b) = splitAt n xs in x L.∷ a , b

  take : ∀{i}{A : Set i} → ℕ → List∞ A → List A
  take n xs = fst (splitAt n xs)

  drop : ∀{i}{A : Set i} → ℕ → List∞ A → List∞ A
  drop n xs = snd (splitAt n xs)

length : ∀{i}{A : Set i} → List∞ A → ℕ∞
pred∞ (length xs) with uncons xs
... | []∞ = zero∞
... | _ ∷∞ ys = suc∞ (length ys)

Unwrap-uncons : ∀{a}{A : Set a} → Maybe (A × List∞ A) → Set a
Unwrap-uncons {a} {A} nothing = Lift a ⊤
Unwrap-uncons {A = A} (just _) = A × List∞ A

unwrap-uncons : ∀{a}{A : Set a} → (l : Maybe (A × List∞ A)) → Unwrap-uncons l
unwrap-uncons nothing = _
unwrap-uncons (just x) = x
