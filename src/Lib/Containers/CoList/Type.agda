{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.Containers.CoList.Type where

open import Lib.Maybe.Type

open import Lib.Sigma.Type

open import Lib.Containers.CoList.TypeBase hiding (module List∞) public
module List∞ where
  instance
    [] : ∀{a}{A : Set a} → List∞ A
    uncons [] = nothing

  infixr 5 _∷_
  _∷_ : ∀{a}{A : Set a} → A → List∞ A → List∞ A
  uncons (x ∷ xs) = just (x , xs)
open List∞ public
