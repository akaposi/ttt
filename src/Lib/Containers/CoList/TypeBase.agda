{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.Containers.CoList.TypeBase where

open import Lib.Maybe.Type

open import Lib.Sigma.Type

record List∞ {a}(A : Set a) : Set a where
  coinductive
  constructor mkList∞
  field
    uncons : Maybe (A × List∞ A)

  head : Maybe A
  head with uncons
  ... | nothing = nothing
  ... | just (x , _) = just x

  tail : Maybe (List∞ A)
  tail with uncons
  ... | nothing = nothing
  ... | just (_ , xs) = just xs

open List∞ public
