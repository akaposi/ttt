{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.Containers.CoVec where

open import Lib.Containers.CoVec.Type public
open import Lib.Containers.CoVec.Base public
open import Lib.Containers.CoVec.Bisimilarity public
open import Lib.Containers.CoVec.Properties public
