{-# OPTIONS --safe --guardedness --without-K --no-postfix-projection #-}

module Lib.Containers.CoVec.Base where

open import Lib.Containers.CoVec.Type

open import Lib.CoNat.Type
open import Lib.CoNat.Base
  renaming (_+_ to _+∞_)
open import Lib.CoNat.Properties
open import Lib.CoNat.PatternSynonym
open import Lib.CoNat.Bisimilarity

open import Lib.Nat.Type

open import Lib.Unit.Type

open import Lib.Maybe.Type

open import Lib.Equality.Base hiding (cast)

open import Lib.Containers.Vec.Type
  renaming ([] to []ᵥ ; _∷_ to _∷ᵥ_)

open import Lib.Fin.Type

open import Lib.CoFin.Type

open import Lib.Lazy.Type

replicate : ∀{i}{A : Set i} → (n : ℕ∞) → A → Vec∞ A n
head (replicate n a) = a
tail (replicate n a) = replicate (predℕ∞ n) a

repeat : ∀{i}{A : Set i} → A → Vec∞ A ∞
repeat = replicate ∞

map : ∀{i j}{A : Set i}{B : Set j}{n : ℕ∞} → 
            (A → B) → Vec∞ A n → Vec∞ B n
head (map f xs) = f (head xs)
tail (map f xs) = map f (tail xs)

cast : ∀{i}{A : Set i}{n m : ℕ∞} → .(n ≈ℕ∞ m) → Vec∞ A n → Vec∞ A m
head (cast {n = n} {m} e xs) ⦃ i ⦄ with .(isNotZero∞ i)
... | _ with .(prove e)
... | _ with pred∞ n | pred∞ m
... | suc∞ n' | suc∞ m' = head xs ⦃ subst-IsNotZero∞ (symℕ∞ e) i ⦄
tail (cast {n = n} {m} e xs) ⦃ i ⦄ with .(isNotZero∞ i)
... | _ with .(prove e) 
... | t with pred∞ n in eq1 | pred∞ m
... | suc∞ n' | suc∞ m' = cast (transℕ∞ (pred∞≈predℕ∞ {n} {n'} (≡→≈ℕ∞′ eq1)) (just-injectiveℕ∞ t)) (tail xs ⦃ subst-IsNotZero∞ (symℕ∞ e) i ⦄)

infixr 5 _++_
_++_ : ∀{i}{A : Set i}{n m : ℕ∞} → Vec∞ A n → Vec∞ A m → Vec∞ A (n +∞ m)
head (_++_ {n = n} {m} xs ys) ⦃ i ⦄ with .(isNotZero∞ i)
... | _ with pred∞ n in eq1
... | suc∞ n' = head xs ⦃ JustIsNotZero∞ ⦃ eq1 ⦄ ⦄
... | zero∞ with pred∞ m in eq2
... | suc∞ m' = head ys ⦃ JustIsNotZero∞ ⦃ eq2 ⦄ ⦄
tail (_++_ {n = n} {m} xs ys) ⦃ i ⦄ with .(isNotZero∞ i)
... | _ with pred∞ n in eq1
... | suc∞ n' = cast (≡→≈ℕ∞ (pred∞≡predℕ∞ {n} eq1)) (tail xs ⦃ JustIsNotZero∞ ⦃ eq1 ⦄ ⦄) ++ ys
... | zero∞ with pred∞ m in eq2
... | suc∞ m' = cast (transℕ∞ (pred∞≈predℕ∞ {m} {m'} (≡→≈ℕ∞′ eq2)) (symℕ∞ (+-aux-inr m m'))) (tail ys ⦃ JustIsNotZero∞ ⦃ eq2 ⦄ ⦄)

take∞ : ∀{i}{A : Set i}(n : ℕ∞){m : ℕ∞} → Vec∞ A (n +∞ m) → Vec∞ A n
head (take∞ n {m} xs) ⦃ e ⦄ with .(isNotZero∞ e)
... | _ with pred∞ n
... | suc∞ n' = head xs ⦃ weakenℕ∞ n m e ⦄
tail (take∞ n {m} xs) ⦃ e ⦄ with .(isNotZero∞ e)
... | _ with pred∞ n in eq1
... | suc∞ n' = let txs = tail xs ⦃ weakenℕ∞ n m e ⦄ in take∞ n' {m} (cast (transℕ∞ (symℕ∞ (predℕ∞l+ n m)) (≡→≈ℕ∞ (cong (_+∞ m) (pred∞≡predℕ∞ {n} eq1)))) txs)

take : ∀{i}{A : Set i}(n : ℕ){m : ℕ∞} → .⦃ n ℕ≤ℕ∞ m ⦄ → Vec∞ A m → Vec A n
take zero v = []ᵥ
take (suc n) {m} ⦃ e ⦄ v with pred∞ m in eq1
... | suc∞ x = head v ⦃ JustIsNotZero∞ ⦃ eq1 ⦄ ⦄ ∷ᵥ take n {x} ⦃ e ⦄ (cast (pred∞≈predℕ∞ {m} (≡→≈ℕ∞′ eq1)) (tail v ⦃ JustIsNotZero∞ ⦃ eq1 ⦄ ⦄))

infixl 10 _‼∞_
_‼∞_ : ∀{ℓ}{A : Set ℓ}{n : ℕ∞} → Vec∞ A n → Fin∞ n → Lazy A
cs ‼∞ f with fpred∞ f
... | just x = later (indx (tail cs ⦃ inz f ⦄) x)
  where
    indx : ∀{ℓ}{A : Set ℓ}{n : ℕ∞} → Vec∞ A n → Fin∞ n → Lazy∞ A
    force (indx cs f) = cs ‼∞ f
... | nothing = now (head cs ⦃ inz f ⦄)

{-
infixl 10 _‼_
_‼_ : ∀{ℓ}{A : Set ℓ}{n : ℕ∞}{m : ℕ} → .⦃ m ℕ≤ℕ∞ n ⦄ → Vec∞ A n → Fin m → A
_‼_ {n = n} {suc m} ⦃ t ⦄ xs fzero = head xs ⦃ <→IsNotZero∞ ⦃ t ⦄ ⦄
_‼_ {n = n} {suc m} ⦃ t ⦄ xs (fsuc i) = tail xs ⦃ <→IsNotZero∞ ⦃ t ⦄ ⦄ ‼ {!!}
-}
