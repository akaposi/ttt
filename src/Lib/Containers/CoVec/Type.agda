{-# OPTIONS --safe --guardedness --without-K #-}

module Lib.Containers.CoVec.Type where

open import Lib.CoNat.Type
open import Lib.CoNat.Base
open import Lib.CoNat.Literals

infixr 5 _∷_
record Vec∞ {ℓ}(A : Set ℓ) (n : ℕ∞) : Set ℓ where
  coinductive
  constructor _∷_
  field
    head : .⦃ p : IsNotZero∞ n ⦄ → A
    tail : .⦃ p : IsNotZero∞ n ⦄ → Vec∞ A (predℕ∞ n)

open Vec∞ public

instance
  [] : ∀{i}{A : Set i} → Vec∞ A 0
  head [] ⦃ () ⦄
  tail [] ⦃ () ⦄
