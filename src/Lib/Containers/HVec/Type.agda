{-# OPTIONS --safe --without-K #-}

module Lib.Containers.HVec.Type where

open import Lib.Nat.Type
open import Lib.Containers.Vec.Type
open import Lib.Level

infixr 5 _∷_
data HVec {i} : {n : ℕ} → Vec (Set i) n → Set (lsuc i) where
  instance [] : HVec []
  _∷_ : ∀{A : Set i}{n}{As : Vec (Set i) n} → A → HVec As → HVec (A ∷ As)
