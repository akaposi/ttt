{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.List.Instances where

open import Lib.Containers.List.Instances.DecidableEquality public
open import Lib.Containers.List.Instances.Eq public
open import Lib.Containers.List.Instances.Ord public
open import Lib.Containers.List.Instances.IsSet public
