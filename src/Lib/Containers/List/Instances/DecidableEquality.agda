{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.List.Instances.DecidableEquality where

open import Lib.Dec.Type

open import Lib.Equality.Type

open import Lib.Containers.List.Type
open import Lib.Containers.List.Properties

≡-dec-List : ∀{i}{A : Set i} → ((a b : A) → Dec (a ≡ b)) → (a b : List A) → Dec (a ≡ b)
≡-dec-List _≟_ []       []       = yes refl
≡-dec-List _≟_ (x ∷ xs) []       = no λ ()
≡-dec-List _≟_ []       (y ∷ ys) = no λ ()
≡-dec-List _≟_ (x ∷ xs) (y ∷ ys) = ∷-dec (x ≟ y) (≡-dec-List _≟_ xs ys)

instance
  DecEqList : ∀{i}{A : Set i} → ⦃ DecidableEquality A ⦄ → DecidableEquality (List A)
  DecEqList ⦃ i1 ⦄ = DecProof (≡-dec-List (decide i1))
