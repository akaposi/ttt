{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.List.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Containers.List.Type
open import Lib.Containers.List.Instances.DecidableEquality

instance
  EqList : ∀{i}{A : Set i} → ⦃ Eq A ⦄ → Eq (List A)
  EqList = EqInstance
