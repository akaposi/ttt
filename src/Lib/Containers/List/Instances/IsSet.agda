{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.List.Instances.IsSet where

open import Lib.Class.IsTrunc
open import Lib.Class.IsSet

open import Lib.Nat.Type

open import Lib.Containers.List.Type
open import Lib.Containers.List.Properties

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type
open import Lib.Sigma.Properties

isSetList : ∀{i}{A : Set i}⦃ I : IsSet A ⦄(xs ys : List A)(p q : xs ≡ ys) → p ≡ q
isSetList ⦃ I = isSetA ⦄ [] [] refl refl = refl
isSetList ⦃ I = isSetA ⦄ (x ∷ xs) .(x ∷ xs) refl q =
  let p1 = isSet' ⦃ isSetA ⦄ x x refl (fst (∷-injective q))
      p2 = isSetList ⦃ isSetA ⦄ xs xs refl (snd (∷-injective q))
  in ∷-injective-injective refl q (snd ×-inj (p1 , p2))

instance
  IsSetList : ∀{i}{A : Set i} → ⦃ I : IsSet A ⦄ → IsSet (List A)
  IsSetList ⦃ I ⦄ = isSet'→IsSet (isSetList ⦃ I ⦄)
