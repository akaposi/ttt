{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.List.Instances.Ord where

open import Agda.Primitive

open import Lib.Class.Eq
open import Lib.Class.Ord
open Ord

open import Lib.Containers.List.Type
open import Lib.Containers.List.Instances.Eq

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Unit.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Dec.Type

open import Lib.Sum.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

infix 4 _≤List_
_≤List_ : ∀{i}{A : Set i} → ⦃ Ord A ⦄ → (xs ys : List A) → Set
[] ≤List ys = ⊤
x ∷ xs ≤List [] = ⊥
_≤List_ ⦃ ordA ⦄ (x ∷ xs) (y ∷ ys) with x ≟ y
... | yes _ = xs ≤List ys
... | no  _ = (ordA ≤ x) y

refl≤List : ∀{i}{A : Set i}⦃ _ : Ord A ⦄{xs : List A} → xs ≤List xs
refl≤List {xs = []} = tt
refl≤List {xs = x ∷ xs} with x ≟ x
... | yes a = refl≤List {xs = xs}
... | no ¬a = exfalso (¬a refl)

trans≤List : ∀{i}{A : Set i}⦃ _ : Ord A ⦄{xs ys zs : List A} → xs ≤List ys → ys ≤List zs → xs ≤List zs
trans≤List {A = _} ⦃ ordA ⦄ {[]} {ys} {zs} e1 e2 = tt
trans≤List {A = _} ⦃ ordA ⦄ {x ∷ xs} {y ∷ ys} {z ∷ zs} e1 e2 with x ≟ y | y ≟ z | x ≟ z
... | yes  _ | yes  _ | yes  _ = trans≤List {xs = xs} {ys} {zs} e1 e2
... | yes xy | yes yz | no ¬xz = exfalso (¬xz (xy ◾ yz))
... | yes xy | no ¬yz | yes xz = exfalso (¬yz (sym xy ◾ xz))
... | yes xy | no   _ | no   _ = subst (λ t → (ordA ≤ t) z) (sym xy) e2
... | no ¬xy | yes yz | yes xz = exfalso (¬xy (xz ◾ sym yz))
... | no   _ | yes yz | no   _ = subst (ordA ≤ x) yz e1
... | no ¬xy | no   _ | yes xz = exfalso (¬xy (antisymmetric (decidableTotalOrder ordA) e1 (subst (ordA ≤ y) (sym xz) e2)))
... | no   _ | no   _ | no   _ = transitive (decidableTotalOrder ordA) e1 e2

antisym≤List : ∀{i}{A : Set i}⦃ _ : Ord A ⦄{xs ys : List A} → xs ≤List ys → ys ≤List xs → xs ≡ ys
antisym≤List {A = _} ⦃ ordA ⦄ {[]} {[]} e1 e2 = refl
antisym≤List {A = _} ⦃ ordA ⦄ {x ∷ xs} {y ∷ ys} e1 e2 with x ≟ y | y ≟ x
... | yes xy | yes _ = cong₂ _∷_ xy (antisym≤List {xs = xs} {ys} e1 e2)
... | yes  a | no ¬a = exfalso (¬a (sym a))
... | no  ¬a | yes a = exfalso (¬a (sym a))
... | no ¬xy | no  _ = exfalso (¬xy (antisymmetric (decidableTotalOrder ordA) e1 e2))

stronglyConnected≤List : ∀{i}{A : Set i}⦃ _ : Ord A ⦄{xs ys : List A} → xs ≤List ys ⊎ ys ≤List xs
stronglyConnected≤List {A = _} ⦃ ordA ⦄ {[]} {ys} = inl tt
stronglyConnected≤List {A = _} ⦃ ordA ⦄ {x ∷ xs} {[]} = inr tt
stronglyConnected≤List {A = _} ⦃ ordA ⦄ {x ∷ xs} {y ∷ ys} with x ≟ y | y ≟ x
... | yes  _ | yes _ = stronglyConnected≤List {xs = xs} {ys}
... | yes  a | no ¬a = exfalso (¬a (sym a))
... | no  ¬a | yes a = exfalso (¬a (sym a))
... | no ¬xy | no  _ = connected (decidableTotalOrder ordA)

decidable≤List : ∀{i}{A : Set i}⦃ _ : Ord A ⦄(xs ys : List A) → Dec (xs ≤List ys)
decidable≤List [] ys = yes tt
decidable≤List (x ∷ xs) [] = no λ ()
decidable≤List ⦃ ordA ⦄ (x ∷ xs) (y ∷ ys) with x ≟ y
... | yes _ = decidable≤List xs ys
... | no  _ = (decidableTotalOrder ordA ≤? x) y

instance
  OrdList : ∀{i}{A : Set i} → ⦃ Ord A ⦄ → Ord (List A)
  eq (OrdList {A = _} ⦃ ordA ⦄) = EqList ⦃ eq ordA ⦄

  _≤_ (OrdList ⦃ ordA ⦄) = _≤List_

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdList {A = _} ⦃ ordA ⦄))))) {xs} = refl≤List {xs = xs}

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdList {A = _} ⦃ ordA ⦄))))) {xs} = trans≤List {xs = xs}

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (OrdList {A = _} ⦃ ordA ⦄)))) = antisym≤List

  connected (totalOrder (decidableTotalOrder (OrdList {A = _} ⦃ ordA ⦄))) {xs} = stronglyConnected≤List {xs = xs}

  Decidable₂.decide (decidable (decidableTotalOrder (OrdList {A = _} ⦃ ordA ⦄))) = decidable≤List
