{-# OPTIONS --safe --without-K #-}

module Lib.Containers.Vec where

open import Lib.Containers.Vec.Type public
open import Lib.Containers.Vec.Base public
open import Lib.Containers.Vec.Properties public
open import Lib.Containers.Vec.Instances public
