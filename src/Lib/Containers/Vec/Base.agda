{-# OPTIONS --safe --without-K #-}

module Lib.Containers.Vec.Base where

open import Lib.Unit.Type

open import Lib.Containers.Vec.Type

open import Lib.Sigma.Type

open import Lib.Fin.Type

open import Lib.Nat.Type
open import Lib.Nat.Base using (pred' ; _+_ ; _*_)
open import Lib.Nat.Literals

open import Lib.Level

open import Lib.Bool.Type

open import Lib.Equality.Type
open import Lib.Equality.Base using (cong)

infixl 6 _[_]%=_
_[_]%=_ : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Fin n → (A → A) → Vec A n
(x ∷ xs) [ fzero ]%= f = f x ∷ xs
(x ∷ xs) [ fsuc i ]%= f = x ∷ xs [ i ]%= f

infixl 6 _[_]≔_
_[_]≔_ : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Fin n → A → Vec A n
xs [ i ]≔ y = xs [ i ]%= (λ _ → y)

cast : ∀{i}{A : Set i}{m n : ℕ} → .(eq : m ≡ n) → Vec A m → Vec A n
cast {n = zero}  eq []       = []
cast {n = suc _} eq (x ∷ xs) = x ∷ cast (cong pred' eq) xs

map : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → (A → B) → Vec A n → Vec B n
map f []       = []
map f (x ∷ xs) = f x ∷ map f xs

infixr 5 _++_
_++_ : ∀{i}{A : Set i}{m n : ℕ} → Vec A m → Vec A n → Vec A (m + n)
[]       ++ ys = ys
(x ∷ xs) ++ ys = x ∷ (xs ++ ys)

concat : ∀{i}{A : Set i}{m n : ℕ} → Vec (Vec A m) n → Vec A (n * m)
concat []         = []
concat (xs ∷ xss) = xs ++ concat xss

lengthᵗ : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Σ ℕ (n ≡_)
lengthᵗ {n = .0} [] = 0 , refl
lengthᵗ {n = .(suc _)} (x ∷ xs) = let (n , p) = lengthᵗ xs in suc n , cong suc p

length : ∀{i}{A : Set i}{n : ℕ} → Vec A n → ℕ
length xs = fst (lengthᵗ xs)

head : ∀{i}{A : Set i}{n : ℕ} → Vec A (suc n) → A
head (x ∷ xs) = x

tail : ∀{i}{A : Set i}{n : ℕ} → Vec A (suc n) → Vec A n
tail (x ∷ xs) = xs

last : ∀{i}{A : Set i}{n : ℕ} → Vec A (suc n) → A
last (x ∷ []) = x
last (_ ∷ xs@(_ ∷ _)) = last xs

init : ∀{i}{A : Set i}{n : ℕ} → Vec A (suc n) → Vec A n
init (_ ∷ []) = []
init (x ∷ xs@(_ ∷ _)) = x ∷ init xs

---------------------------------------
-- Eliminators (first parameter)
---------------------------------------

elimᵣ : ∀{i j}{A : Set i}{B : {n : ℕ} → Vec A n → Set j}{n : ℕ} → (xs : Vec A n) → B [] → ((x : A){n : ℕ}{xs : Vec A n} → B xs → B (x ∷ xs)) → B xs
elimᵣ [] nil cons = nil
elimᵣ {B = B} (x ∷ xs) nil cons = cons x (elimᵣ {B = B} xs nil cons)

elim : ∀{i j}{A : Set i}{B : {n : ℕ} → Vec A n → Set j}{n : ℕ} → (xs : Vec A n) → B [] → ((x : A){n : ℕ}{xs : Vec A n} → B xs → B (x ∷ xs)) → B xs
elim = elimᵣ

-- NO elimₗ

elim-lengthᵣ : ∀{i j}{A : Set i}{B : ℕ → Set j}{n : ℕ}(xs : Vec A n) → B zero → (A → {n : ℕ}{xs : Vec A n} → B n → B (suc n)) → B n
elim-lengthᵣ {B = B} xs nil cons = elimᵣ {B = λ {n} _ → B n} xs nil cons

elim-length : ∀{i j}{A : Set i}{B : ℕ → Set j}{n : ℕ}(xs : Vec A n) → B zero → (A → {n : ℕ}{xs : Vec A n} → B n → B (suc n)) → B n
elim-length = elim-lengthᵣ

elim-lengthₗ : ∀{i j}{A : Set i}{B : ℕ → Set j}{n : ℕ}(xs : Vec A n) → B zero → ({n : ℕ}{xs : Vec A n} → B n → A → B (suc n)) → B n
elim-lengthₗ [] nil snoc = nil
elim-lengthₗ {B = B} (x ∷ xs) nil snoc = elim-lengthₗ {B = λ n → B (suc n)} xs (snoc {xs = []} nil x) λ {n} {ys} r y → snoc {xs = y ∷ ys} r y

recᵣ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (A → ∀{k} → Vec A k → B → B) → B
recᵣ xs nil cons = elimᵣ xs nil λ x {_} {ys} → cons x ys

rec : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (A → ∀{k} → Vec A k → B → B) → B
rec = recᵣ

recₗ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (∀{k} → Vec A k → B → A → B) → B
recₗ {n = n} xs nil snoc = elim-lengthₗ xs nil λ {k} {xs} → snoc {k} xs

iteᵣ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (A → B → B) → B
iteᵣ xs nil cons = elimᵣ xs nil λ x → cons x

ite : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (A → B → B) → B
ite = iteᵣ

iteₗ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → (B → A → B) → B
iteₗ xs nil snoc = recₗ xs nil λ _ → snoc

case : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → Vec A n → B → B → B
case xs b₁ b₂ = iteᵣ xs b₁ λ _ _ → b₂

---------------------------------------
-- Eliminators (last parameter)
---------------------------------------

elimᵣ′ : ∀{i j}{A : Set i}{B : {n : ℕ} → Vec A n → Set j}{n : ℕ} → B [] → ((x : A){n : ℕ}{xs : Vec A n} → B xs → B (x ∷ xs)) → (xs : Vec A n) → B xs
elimᵣ′ nil cons xs = elimᵣ xs nil cons

elim′ : ∀{i j}{A : Set i}{B : {n : ℕ} → Vec A n → Set j}{n : ℕ} → B [] → ((x : A){n : ℕ}{xs : Vec A n} → B xs → B (x ∷ xs)) → (xs : Vec A n) → B xs
elim′ = elimᵣ′

recᵣ′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (A → ∀{k} → Vec A k → B → B) → Vec A n → B
recᵣ′ nil cons xs = recᵣ xs nil cons

rec′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (A → ∀{k} → Vec A k → B → B) → Vec A n → B
rec′ = recᵣ′

recₗ′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (∀{k} → Vec A k → B → A → B) → Vec A n → B
recₗ′ nil snoc xs = recₗ xs nil snoc

iteᵣ′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (A → B → B) → Vec A n → B
iteᵣ′ nil cons xs = iteᵣ xs nil cons

ite′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (A → B → B) → Vec A n → B
ite′ = iteᵣ′

iteₗ′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → (B → A → B) → Vec A n → B
iteₗ′ nil snoc xs = iteₗ xs nil snoc

case′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → B → B → Vec A n → B
case′ b₁ b₂ = iteᵣ′ b₁ λ _ _ → b₂

---------------------------------------

infixl 10 _‼_
_‼_ : ∀{i}{A : Set i}{n : ℕ} → Vec A n → Fin n → A
_‼_ (x ∷ xs) fzero    = x
_‼_ (x ∷ xs) (fsuc i) = _‼_ xs i

iterate : ∀{i}{A : Set i} → (A → A) → A → ∀ {n} → Vec A n
iterate s z {zero}  = []
iterate s z {suc n} = z ∷ iterate s (s z)

replicate : ∀{i}{A : Set i}(n : ℕ) → A → Vec A n
replicate zero a = []
replicate (suc n) a = a ∷ replicate n a

foldr : ∀{i j}{A : Set i}(B : ℕ → Set j){n : ℕ} → (∀{n} → A → B n → B (suc n)) → B zero → Vec A n → B n
foldr B _⊕_ e []       = e
foldr B _⊕_ e (x ∷ xs) = x ⊕ foldr B _⊕_ e xs

foldl : ∀{i j}{A : Set i}(B : ℕ → Set j){n : ℕ} → (∀ {n} → B n → A → B (suc n)) → B zero → Vec A n → B n
foldl B _⊕_ e []       = e
foldl B _⊕_ e (x ∷ xs) = foldl (λ x → B (suc x)) _⊕_ (e ⊕ x) xs

foldr′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → (A → B → B) → B → Vec A n → B
foldr′ _⊕_ = foldr _ _⊕_

foldl′ : ∀{i j}{A : Set i}{B : Set j}{n : ℕ} → (B → A → B) → B → Vec A n → B
foldl′ _⊕_ = foldl _ _⊕_

foldr₁ : ∀{i}{A : Set i}{n : ℕ} → (A → A → A) → Vec A (suc n) → A
foldr₁ _⊕_ (x ∷ xs) = foldr _ _⊕_ x xs

foldl₁ : ∀{i}{A : Set i}{n : ℕ} → (A → A → A) → Vec A (suc n) → A
foldl₁ _⊕_ (x ∷ xs) = foldl _ _⊕_ x xs

sumℕ : {n : ℕ} → Vec ℕ n → ℕ
sumℕ = foldr _ _+_ 0

splitAt : ∀{i}{A : Set i} → ∀ m {n} (xs : Vec A (m + n)) →
          Σ (Vec A m) (λ ys → Σ (Vec A n) (λ zs → xs ≡ ys ++ zs))
splitAt zero    xs                = ([] , xs , refl)
splitAt (suc m) (x ∷ xs)          with splitAt m xs
splitAt (suc m) (x ∷ .(ys ++ zs)) | (ys , zs , refl) =
  ((x ∷ ys) , zs , refl)

take : ∀{i}{A : Set i} → ∀ m {n} → Vec A (m + n) → Vec A m
take m xs = fst (splitAt m xs)

drop : ∀{i}{A : Set i} → ∀ m {n} → Vec A (m + n) → Vec A n
drop m xs = fst (snd (splitAt m xs))

module List where
  open import Lib.Containers.List.Type

  convert : ∀{i}{A : Set i}{n}(xs : Vec A n) → List A
  convert [] = []
  convert (x ∷ xs) = x ∷ convert xs
