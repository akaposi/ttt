{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances where

open import Lib.Containers.Vec.Instances.DecidableEquality public
open import Lib.Containers.Vec.Instances.Eq public
open import Lib.Containers.Vec.Instances.Ord public
open import Lib.Containers.Vec.Instances.Uninhabited public
open import Lib.Containers.Vec.Instances.IsSet public
