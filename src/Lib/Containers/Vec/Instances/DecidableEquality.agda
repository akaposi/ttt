{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances.DecidableEquality where

open import Lib.Dec.Type

open import Lib.Nat.Type

open import Lib.Equality.Type

open import Lib.Containers.Vec.Type
open import Lib.Containers.Vec.Properties

≡-dec-Vec : ∀{i}{A : Set i}{n : ℕ} → ((a b : A) → Dec (a ≡ b)) → (a b : Vec A n) → Dec (a ≡ b)
≡-dec-Vec _≟_ []       []       = yes refl
≡-dec-Vec _≟_ (x ∷ xs) (y ∷ ys) = ∷-dec (x ≟ y) (≡-dec-Vec _≟_ xs ys)

instance
  DecEqVec : ∀{i}{A : Set i}{n} → ⦃ DecidableEquality A ⦄ → DecidableEquality (Vec A n)
  DecEqVec ⦃ i1 ⦄ = DecProof (≡-dec-Vec (decide i1))
