{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Nat.Type

open import Lib.Containers.Vec.Type
open import Lib.Containers.Vec.Instances.DecidableEquality

instance
  EqVec : ∀{i}{A : Set i}{n : ℕ} → ⦃ Eq A ⦄ → Eq (Vec A n)
  EqVec = EqInstance
