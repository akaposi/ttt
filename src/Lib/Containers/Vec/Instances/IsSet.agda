{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances.IsSet where

open import Lib.Class.IsTrunc
open import Lib.Class.IsSet

open import Lib.Nat.Type

open import Lib.Containers.Vec.Type
open import Lib.Containers.Vec.Properties

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type
open import Lib.Sigma.Properties

isSetVec : ∀{i}{A : Set i}{n : ℕ}⦃ I : IsSet A ⦄(xs ys : Vec A n)(p q : xs ≡ ys) → p ≡ q
isSetVec ⦃ I = isSetA ⦄ [] [] refl refl = refl
isSetVec {n = suc n} ⦃ I = isSetA ⦄ (x ∷ xs) .(x ∷ xs) refl q =
  let p1 = isSet' ⦃ isSetA ⦄ x x refl (fst (∷-injective q))
      p2 = isSetVec {n = n} ⦃ isSetA ⦄ xs xs refl (snd (∷-injective q))
  in ∷-injective-injective refl q (snd ×-inj (p1 , p2))

instance
  IsSetVec : ∀{i}{A : Set i}{n : ℕ} → ⦃ I : IsSet A ⦄ → IsSet (Vec A n)
  IsSetVec ⦃ I ⦄ = isSet'→IsSet (isSetVec ⦃ I ⦄)
