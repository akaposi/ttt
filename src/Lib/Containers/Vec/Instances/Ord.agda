{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances.Ord where

open import Lib.Class.Eq
open import Lib.Class.Ord

open import Lib.Ordering.Type

open import Lib.Containers.Vec.Instances.Eq
open import Lib.Containers.Vec.Type
open import Lib.Containers.Vec.Properties

open import Lib.Unit.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Nat.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type

open import Lib.Sum.Type
open import Lib.Sum.Base

open import Lib.Dec.Type
import Lib.Dec.Base as Dec

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Maybe.Base

open import Agda.Primitive


open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdVec : ∀{i}{A : Set i}{n : ℕ} → ⦃ Ord A ⦄ → Ord (Vec A n)
  Ord.eq OrdVec = EqVec

  (OrdVec Ord.≤ []) ys = ⊤
  ((OrdVec ⦃ ordA ⦄) Ord.≤ x ∷ xs) (y ∷ ys) with x ≟ y
  ... | yes _ = ((OrdVec ⦃ ordA ⦄) Ord.≤ xs) ys
  ... | no  _ = x ≤ y

  reflexive (preorder (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))))) {[]} = tt
  reflexive (preorder (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))))) {x ∷ xs} with x ≟ x
  ... | yes a = reflexive (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)) {xs}
  ... | no ¬a = exfalso (¬a refl)

  transitive (preorder (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))))) {[]} {ys} {zs} _ _ = tt
  transitive (preorder (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))))) {x ∷ xs} {y ∷ ys} {z ∷ zs} with x ≟ y | y ≟ z | x ≟ z
  ... | yes a | yes a₁ | yes a₂ = transitive (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)) {xs} {ys} {zs}
  ... | yes a | yes a₁ | no ¬a  = exfalso (¬a (a ◾ a₁))
  ... | yes a | no  ¬a | yes a₁ = exfalso (¬a (sym a ◾ a₁))
  ... | yes a | no  ¬a | no ¬a₁ = λ _ y≤z → subst (_≤ z) (sym a) y≤z
  ... | no ¬a | yes  a | yes a₁ = exfalso (¬a (a₁ ◾ sym a))
  ... | no ¬a | yes  a | no ¬a₁ = λ x≤y _ → subst (x ≤_) a x≤y
  ... | no ¬a | no ¬a₁ | yes a  = λ x≤y y≤z → exfalso (¬a₁ (sym (antisymmetric (Ord.decidableTotalOrder ordA) (subst (_≤ y) a x≤y) y≤z)))
  ... | no ¬a | no ¬a₁ | no ¬a₂ = transitive (Ord.decidableTotalOrder ordA)

  antisymmetric (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)))) {[]} {[]} e1 e2 = refl
  antisymmetric (partialOrder (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)))) {x ∷ xs} {y ∷ ys} e1 e2 with x ≟ y | y ≟ x
  ... | yes a | yes a₁ = cong₂ _∷_ a (antisymmetric (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)) e1 e2)
  ... | yes a | no  ¬a = exfalso (¬a (sym a))
  ... | no ¬a | yes  a = exfalso (¬a (sym a))
  ... | no ¬a | no ¬a₁ = exfalso (¬a (antisymmetric (Ord.decidableTotalOrder ordA) e1 e2))

  connected (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))) {[]} {[]} = inl tt
  connected (totalOrder (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))) {x ∷ xs} {y ∷ ys} with x ≟ y | y ≟ x
  ... | yes a | yes a₁ = connected (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)) {xs} {ys}
  ... | yes a | no  ¬a = exfalso (¬a (sym a))
  ... | no ¬a | yes  a = exfalso (¬a (sym a))
  ... | no ¬a | no ¬a₁ = connected (Ord.decidableTotalOrder ordA) {x} {y}

  Decidable₂.decide (decidable (Ord.decidableTotalOrder OrdVec)) [] [] = yes tt
  Decidable₂.decide (decidable (Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄))) (x ∷ xs) (y ∷ ys) with x ≟ y
  ... | yes a = ((Ord.decidableTotalOrder (OrdVec ⦃ ordA ⦄)) ≤? xs) ys
  ... | no ¬a = ((Ord.decidableTotalOrder ordA) ≤? x) y
