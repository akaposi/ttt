{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Instances.Uninhabited where

open import Lib.Class.Uninhabited
open Uninhabited

open import Lib.Containers.Vec.Type

open import Lib.Nat.Type

instance
  NonEmptyVecUninhabited : ∀{i}{A : Set i}{n : ℕ}⦃ a : Uninhabited A ⦄ → Uninhabited (Vec A (suc n))
  uninhabited (NonEmptyVecUninhabited {n = n} ⦃ a ⦄) (x ∷ xs) = uninhabited a x
