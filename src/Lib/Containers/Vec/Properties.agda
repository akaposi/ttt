{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Containers.Vec.Properties where

open import Lib.Containers.Vec.Type
open import Lib.Containers.Vec.Base

open import Lib.Sigma.Type

open import Lib.Equality.Type
open import Lib.Equality.Base
open import Lib.Equality.Properties

open import Lib.Dec.Type

open import Lib.Nat.Type
open import Lib.Nat.Base
open import Lib.Nat.Properties

open import Lib.Unit.Type

open import Lib.Empty.Type

∷-η : ∀{i}{A : Set i}{n : ℕ}{xs : Vec A (suc n)} → head xs ∷ tail xs ≡ xs
∷-η {xs = x ∷ xs} = refl

∷-injective : ∀{i}{A : Set i}{n : ℕ}{x y : A}{xs ys : Vec A n} → 
  (x ∷ xs) ≡ (y ∷ ys) → x ≡ y × xs ≡ ys
∷-injective e = cong head e , cong tail e

∷-injective-injective : ∀{i}{A : Set i}{n : ℕ}{x y : A}{xs ys : Vec A n} →
  (e1 e2 : x ∷ xs ≡ y ∷ ys) → ∷-injective e1 ≡ ∷-injective e2 → e1 ≡ e2
∷-injective-injective {x = x} {y} {xs} {ys} refl e2 inj = cong (λ (a , b) → cong₂ _∷_ a b) inj ◾ cong₂∘ _∷_ head tail e2 e2 ◾ cong₂↓ (λ as bs → head as ∷ tail bs) e2 ◾ J (λ a e → cong (λ as → head as ∷ tail as) e ≡ (e ◾ sym ∷-η)) refl e2

∷-injectiveˡ : ∀{i}{A : Set i}{n : ℕ}{x y : A}{xs ys : Vec A n} →
  x ∷ xs ≡ y ∷ ys → x ≡ y
∷-injectiveˡ e = fst (∷-injective e)

∷-injectiveʳ : ∀{i}{A : Set i}{n : ℕ}{x y : A}{xs ys : Vec A n} →
  x ∷ xs ≡ y ∷ ys → xs ≡ ys
∷-injectiveʳ e = snd (∷-injective e)

∷-dec : ∀{i}{A : Set i}{n : ℕ}{x y : A}{xs ys : Vec A n} → Dec (x ≡ y) → Dec (xs ≡ ys) → Dec (x ∷ xs ≡ y ∷ ys)
∷-dec (no p) _ = no λ e → p (∷-injectiveˡ e)
∷-dec (yes p) (no q) = no λ e → q (∷-injectiveʳ e)
∷-dec (yes p) (yes q) = yes (cong₂ _∷_ p q)

substVec-suc : ∀{i}{A : Set i}{n}(x : A)(xs : Vec A n){m}(e : n ≡ m) → subst (λ k → Vec A (suc k)) e (x ∷ xs) ≡ x ∷ subst (Vec A) e xs
substVec-suc {n = n} x xs {.n} refl = refl
