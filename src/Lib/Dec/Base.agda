{-# OPTIONS --safe --without-K #-}

module Lib.Dec.Base where

open import Lib.Dec.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Unit.Type

open import Lib.Sum.Type

open import Lib.Sigma.Type

open import Lib.Equality.Type

True : ∀{i}{A : Set i} → Dec A → Set
True (yes _) = ⊤
True (no _) = ⊥

False : ∀{i}{A : Set i} → Dec A → Set
False (yes _) = ⊥
False (no _) = ⊤

extractTrue : ∀{i}{A : Set i}(da : Dec A) → .⦃ True da ⦄ → A
extractTrue (yes a) = a

extractFalse : ∀{i}{A : Set i}(da : Dec A) → .⦃ False da ⦄ → A → ⊥
extractFalse (no a) = a

elim : ∀{i j}{A : Set i}{C : Dec A → Set j} → (decA : Dec A) → ((a : A) → C (yes a)) → ((¬a : ¬ A) → C (no ¬a)) → C decA
elim (yes a) f g = f a
elim (no ¬a) f g = g ¬a

ite : ∀{i j}{A : Set i}{C : Set j} → Dec A → (A → C) → (¬ A → C) → C
ite = elim

ite′ : ∀{i j}{A : Set i}{C : Set j} → (A → C) → (¬ A → C) → Dec A → C
ite′ f g x = ite x f g

elim′ : ∀{i j}{A : Set i}{C : Dec A → Set j} → ((a : A) → C (yes a)) → ((¬a : ¬ A) → C (no ¬a)) → (decA : Dec A) → C decA
elim′ {C = C} f g x = elim {C = C} x f g

ind : ∀{i j}{A : Set i}{C : Dec A → Set j} → (decA : Dec A) → ((a : A) → yes a ≡ decA → C decA) → ((¬a : ¬ A) → no ¬a ≡ decA → C decA) → C decA
ind (yes a) f g = f a refl
ind (no ¬a) f g = g ¬a refl

-----------------------------

recompute-const : ∀{i}{A : Set i}(r : Recomputable A) (p q : A) → r p ≡ r q
recompute-const r p q = refl

⊥-recompute : Recomputable ⊥
⊥-recompute ()

_×-recompute_ : ∀{i j}{A : Set i}{B : Set j} → Recomputable A → Recomputable B → Recomputable (A × B)
(rA ×-recompute rB) p = rA (fst p) , rB (snd p)

_→-recompute_ : ∀{i j}(A : Set i){B : Set j} → Recomputable B → Recomputable (A → B)
(A →-recompute rB) f a = rB (f a)

Π-recompute : ∀{i j}{A : Set i}(B : A → Set j) → (∀ x → Recomputable (B x)) → Recomputable (∀ x → B x)
Π-recompute B rB f a = rB a (f a)

∀-recompute : ∀{i j}{A : Set i}(B : A → Set j) → (∀ {x} → Recomputable (B x)) → Recomputable (∀ {x} → B x)
∀-recompute B rB f = rB f

¬-recompute : ∀{i}{A : Set i} → Recomputable (¬ A)
¬-recompute {A = A} = A →-recompute ⊥-recompute

------------------------------------

recompute : ∀{i}{A : Set i} → Dec A → Recomputable A
recompute (yes a) aᵢ = a
recompute (no ¬a) aᵢ = exfalso-irrelevant (¬a aᵢ)

recompute-constant : ∀{i}{A : Set i}(a? : Dec A) (p q : A) → recompute a? p ≡ recompute a? q
recompute-constant a? p q = refl
