{-# OPTIONS --safe --without-K #-}

module Lib.Dec.Type where

open import Lib.Level

open import Lib.Equality.Type

open import Lib.Unit.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

{-
infix 2 _because_
record Dec {p} (P : Set p) : Set p where
  constructor _because_
  field
    does  : Bool
    proof : Reflects P does
  
  pattern yes p = record { does = true ; proof = ofʸ p }
  pattern no ¬p = record { does = false ; proof = ofⁿ ¬p }

open Dec public
-}

data Dec {i}(A : Set i) : Set i where
  yes : (a : A)    → Dec A
  no  : (¬a : ¬ A) → Dec A
{-
Dec : ∀{i}(A : Set i) → Set i
Dec A = A ⊎ (A → ⊥)
-}

IsYesᵗ : ∀{i}{A : Set i} → Dec A → Set
IsYesᵗ (yes _) = ⊤
IsYesᵗ (no  _) = ⊥

record IsYes {i}{A : Set i}(d : Dec A) : Set where
  constructor prove-yes
  field
    ⦃ isYes ⦄ : IsYesᵗ d

open IsYes {{...}} public

IsNoᵗ : ∀{i}{A : Set i} → Dec A → Set
IsNoᵗ (yes _) = ⊥
IsNoᵗ (no  _) = ⊤

record IsNo {i}{A : Set i}(d : Dec A) : Set where
  constructor prove-no
  field
    ⦃ isNo ⦄ : IsNoᵗ d

open IsNo {{...}} public

record Decidable {i}{j}{A : Set i}(P : A → Set j) : Set (i ⊔ j) where
  constructor DecProof
  field
    decide : ∀ x → Dec (P x)

open Decidable {{...}} public

record Decidable₂ {i}{j}{k}{A : Set i}{B : A → Set j}(P : (a : A) → B a → Set k) : Set (i ⊔ j ⊔ k) where
  constructor DecProof
  field
    decide : ∀ x y → Dec (P x y)

open Decidable₂ {{...}} public

DecidableEquality : ∀{i}(A : Set i) → Set _
DecidableEquality A = Decidable₂ {A = A} _≡_

Recomputable : ∀{i}(A : Set i) → Set i
Recomputable A = .A → A
