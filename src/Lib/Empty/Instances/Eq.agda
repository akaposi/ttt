{-# OPTIONS --safe --without-K #-}

module Lib.Empty.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Empty.Type
open import Lib.Empty.Instances.DecidableEquality

instance
  Eq⊥ : Eq ⊥
  Eq⊥ = EqInstance
