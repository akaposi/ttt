{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Empty.Instances.Uninhabited where

open import Lib.Class.Uninhabited
open Uninhabited

open import Lib.Empty.Type

instance
  ⊥Empty : Uninhabited ⊥
  uninhabited ⊥Empty x = x
