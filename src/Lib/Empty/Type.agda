{-# OPTIONS --safe --without-K #-}

module Lib.Empty.Type where

open import Lib.Irrelevant

private
  data 𝟘 : Set where

⊥ : Set
⊥ = Irrelevant 𝟘

{-# DISPLAY Irrelevant 𝟘 = ⊥ #-}
