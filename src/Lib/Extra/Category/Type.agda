{-# OPTIONS --safe #-}
module Lib.Extra.Category.Type where

open import Agda.Primitive using (Level; _⊔_)
open import Lib.Extra.Semigroupoid.Type public
open import Lib.Extra.Class.Identity public

record Category {i} {adj : Level → Level} (A : Set i) (P : A → Set ( adj i )) (_<>_ : (x y : A){{m : P x}}{{n : P y}} → A) : Set (i ⊔ adj i) where
    field
        ε : A
        overlap {{groupoid}} : Semigroupoid {adj = adj} A P _<>_
        overlap {{id}} : Identity A {P} _<>_ ε
    module groupoid = Semigroupoid groupoid
    open groupoid public
    module id = Identity id 
    open id public