{-# OPTIONS --safe #-}
module Lib.Extra.Class.Associtivity where

open import Lib.Equality using (_≡_)
open import Agda.Primitive using (_⊔_)
open import Lib.Unit using (⊤)

record Assoc {i j} (A : Set i){P : A → Set j}(_∘_ : (a b : A){{m : P a}}{{n : P b}} → A) : Set (i ⊔ j) where
    field
        ass∘ : (a b c : A){{m : P a}}{{n : P b}}{{o : P c}}{{p : P (a ∘ b)}}{{q : P (b ∘ c)}} →  ((a ∘ b) ∘ c) ≡ (a ∘ (b ∘ c))

record AssocT {i} (A : Set i)(_∘_ : (a b : A) → A) : Set i where
    field
       overlaping {{raw}} : Assoc A {λ _ → ⊤} λ a b → a ∘ b