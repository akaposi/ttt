{-# OPTIONS --safe #-}
module Lib.Extra.Class.Commutativity where

open import Lib.Equality using (_≡_)
open import Agda.Primitive using (_⊔_)
open import Lib.Unit using (⊤)

record Commutativity {i} {j} (A : Set i) {P : A → Set j}(_∘_ : (a b : A){{m : P a}}{{n : P b}} → A) : Set (i ⊔ j) where
    field
        comm∘ : (a b : A) {{m : P a}}{{n : P b}} → a ∘ b ≡ b ∘ a

record CommutativityT {i} (A : Set i) (_∘_ : (a b : A) → A) : Set i where
    field
       overlap {{raw}} : Commutativity A {λ x → ⊤} λ a b → a ∘ b