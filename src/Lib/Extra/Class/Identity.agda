{-# OPTIONS --safe #-}
module Lib.Extra.Class.Identity where

open import Lib.Equality using (_≡_)
open import Agda.Primitive using (_⊔_)
open import Lib.Unit using (⊤)

record LeftIdent {i j} (A : Set i) {P : A → Set j}(_∘_ : (a b : A){{m : P a}}{{n : P b}} → A) (ε : A) : Set (i ⊔ j) where
    field
      overlap {{Pidl}} : P ε
      idl : (a : A){{n : P a}} → (ε ∘ a) ≡ a

record RightIdent {i j} (A : Set i) {P : A → Set j}(_∘_ : (a b : A){{m : P a}}{{n : P b}} → A) (ε : A) : Set (i ⊔ j) where
    field
      overlap {{Pidr}} : P ε
      idr : (a : A){{n : P a}} → a ∘ ε ≡ a

record Identity {i j} (A : Set i) {P : A → Set j}(_∘_ : (a b : A){{m : P a}}{{n : P b}} → A) (ε : A) : Set (i ⊔ j) where
    field
      overlap {{right}} : RightIdent A _∘_ ε
      overlap {{left}} : LeftIdent A _∘_ ε
    module right = RightIdent right
    module left = LeftIdent left
    open right public
    open left public

record IdentityT {i} (A : Set i) (_∘_ : (a b : A) → A) (ε : A) : Set i where
  field
    overlap {{raw}} : Identity A {λ _ → ⊤} (λ a b → a ∘ b) ε
  module identity = Identity raw
  open identity
  
