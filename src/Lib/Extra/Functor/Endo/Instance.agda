module Lib.Extra.Functor.Endo.Instance where

open import Lib.Extra.Functor.Endo.Type
open import Lib.Extra.Functor.Functions
open import Lib.Extra.Category.Type
open import Lib.Extra.Functor.Type
open import Agda.Primitive using (Level)
open import Lib.Equality

instance identf : ∀{i}{adj : Level → Level}{A : Set i}{P : A → Set (adj i)}{C : Category {adj = adj} A P λ a _ → a} → EndoFunctor {adj = adj} P C
Functor.[ identf .EndoFunctor.functor ↑] = λ x → x
Functor.[↓ identf .EndoFunctor.functor ] = λ x → x
identf .EndoFunctor.functor .Functor.idp = refl
identf .EndoFunctor.functor .Functor.∘p = refl