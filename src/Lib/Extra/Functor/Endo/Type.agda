{-# OPTIONS --safe #-}
module Lib.Extra.Functor.Endo.Type where

open import Agda.Primitive using (Level)
open import Lib.Extra.Category.Type
open import Lib.Extra.Functor.Type

record EndoFunctor {i : Level}{adj : Level → Level}{A : Set i} (P : A → Set (adj i)) {_⇛_ : (a b : A){{n : P a}}{{m : P b}} → A } (C : Category {adj = adj} A P _⇛_) : Set (i) where
    field
        overlap {{functor}} : Functor {i} {i} {adj} {A} {A} P P {_⇛_} {_⇛_} C C
    module category = Category C
    module functor = Functor functor
    open functor public