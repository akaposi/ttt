module Lib.Extra.Functor.Functions where

open import Lib.Extra.Functor.Type
open import Lib.Extra.Category.Type
open import Lib.Unit.Type
open import Lib.Equality
open import Agda.Primitive using (Level)

postulate
    funext : ∀{i}{j}{A : Set i}{B : Set j} → (f : ( A → B )) → (g : (A → B)) → ((x : A) → f x ≡ g x) → f ≡ g

comp : ∀{i}{A : Set i}{adj : Level → Level}{P Q R : A → Set (adj i)}{r : (a b : A){{n : P a}}{{m : P b}} → A}{p : (a b : A){{n : Q a}}{{m : Q b}} → A}{q : (a b : A){{n : R a}}{{m : R b}} → A}{C : Category {adj = adj} A P r}{D : Category A Q p}{E : Category A R q} → (Functor P Q C D) → Functor Q R D E → Functor P R C E
Functor.[ comp x x₁ ↑] a = Functor.[ x ↑] (Functor.[ x₁ ↑] a)
Functor.[↓ comp x x₁ ] fa = Functor.[↓ x ] (Functor.[↓ x₁ ] fa)
Functor.idp (comp x x₁) = trans (cong Functor.[↓ x ] (Functor.idp x₁)) (Functor.idp x)
Functor.∘p (comp x x₁) {a} {b} {f} {g} = trans (cong (λ x₂ → Functor.[↓ x ] x₂ a b) (funext _ _ λ x₂ → funext _ _ λ x₃ → Functor.∘p x₁ {x₂} {x₃} {f} {g})) (Functor.∘p x {a} {b} {Functor.[↓ x₁ ] f} {Functor.[↓ x₁ ] g})    