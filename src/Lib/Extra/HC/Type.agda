{-# OPTIONS --safe #-}
module Lib.Extra.HC.Type where


open import Lib using (_≡_; refl; lsuc; _∘_; id; Lift; lzero)
open import Lib.Extra.Partial.Type
open import Lib.Extra.Semigroupoid.Type
open import Lib.Extra.Category.Type
open import Lib.Extra.Class
open import Lib.Unit using (⊤)

instance HCp : ∀{i} → Partial {adj = λ x → lzero} ((Set i) → (Set i)) (λ x → ⊤) λ x y → x ∘ y
HCp = record {}

instance HCa : ∀{i} → Assoc { lsuc i } {lzero} (Set i → Set i) {λ x → ⊤} λ a b → a ∘ b
Assoc.ass∘ HCa a b c = refl

instance HCs : ∀{i} → Semigroupoid {adj = λ x → lzero} ((Set i) → (Set i)) (λ x → ⊤) λ x y → x ∘ y
HCs .Semigroupoid.magma = HCp
(HCs {i}) .Semigroupoid.assoc = HCa {i}

instance HCil : ∀{i} → LeftIdent {lsuc i} {lzero} (Set i → Set i) {λ x → ⊤} (λ a b → a ∘ b) id
HCil .LeftIdent.Pidl = Lib.tt
HCil .LeftIdent.idl a = refl

instance HCir : ∀{i} → RightIdent {lsuc i} {lzero} (Set i → Set i) {λ x → ⊤} (λ a b → a ∘ b) id
HCir .RightIdent.Pidr = Lib.tt
HCir .RightIdent.idr a = refl

instance HCi : ∀{i} → Identity {lsuc i} { lzero } (Set i → Set i) {λ x → ⊤} (λ a b → a ∘ b) id
HCi .Identity.right = HCir
HCi .Identity.left = HCil

HC : ∀{i} → Category {adj = λ x → lzero} ((Set i) → (Set i)) (λ x → ⊤) λ x y → x ∘ y
HC .Category.ε = id
HC .Category.groupoid = HCs
HC .Category.id = HCi 