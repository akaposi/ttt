{-# OPTIONS --safe #-}
module Lib.Extra.Partial.Type where

open import Agda.Primitive using (Level )

record Partial {i} {adj : Level → Level} (A : Set i) (P : A → Set (adj i)) (_<>_ : (x y : A){{m : P x }}{{n : P y}} → A) : Set i where
    _∘_ = _<>_