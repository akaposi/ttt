{-# OPTIONS --safe #-}
module Lib.Extra.Semigroupoid.Type where

open import Agda.Primitive using (Level; _⊔_)
open import Lib.Extra.Partial.Type public
open import Lib.Extra.Class.Associtivity public

record Semigroupoid {i} {adj : Level → Level} (A : Set i) (P : A → Set (adj i)) (_<>_ : (x y : A){{ m : P x}}{{n : P y}} → A) : Set (i ⊔ adj i) where
    field
        overlap {{magma}} : Partial {adj = adj} A P _<>_
        overlap {{assoc}} : Assoc A {P} _<>_
    module magma = Partial magma
    module assoc = Assoc assoc
    open magma public
    open assoc public