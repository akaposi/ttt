{-# OPTIONS --safe --without-K #-}

module Lib.Fin.Base where

open import Lib.Equality.Type
open import Lib.Equality.Base using (cong) -- has cast in it.

open import Lib.Nat.Type hiding (module ℕ)
import Lib.Nat.Base as ℕ
open ℕ
  using (_+_ ; pred')

open import Lib.Fin.Type

toℕ : {n : ℕ} → Fin n → ℕ
toℕ fzero    = 0
toℕ (fsuc i) = suc (toℕ i)

Fin' : {n : ℕ} → Fin n → Set
Fin' i = Fin (toℕ i)

cast : ∀{m n} → .(m ≡ n) → Fin m → Fin n
cast {zero}  {zero}  eq k        = k
cast {suc m} {suc n} eq fzero    = fzero
cast {suc m} {suc n} eq (fsuc k) = fsuc (cast (cong pred' eq) k)

fromℕ : (n : ℕ) → Fin (suc n)
fromℕ zero    = fzero
fromℕ (suc n) = fsuc (fromℕ n)

raiseᵣ : {m : ℕ}(n : ℕ) → Fin m → Fin (n + m)
raiseᵣ zero m' = m'
raiseᵣ (suc n) m' = fsuc (raiseᵣ n m')

raiseₗ : {m : ℕ} → Fin m → (n : ℕ) → Fin (m + n)
raiseₗ {suc m} fzero n = fzero
raiseₗ {suc m} (fsuc m') n = fsuc (raiseₗ {m} m' n)

elimᵣ : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
      → (f : Fin k)
      → (pzero : {n : ℕ} → P (fzero {n}))
      → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
      → P f
elimᵣ fzero pzero psuc = pzero
elimᵣ {P = P} (fsuc f) pzero psuc = psuc (elimᵣ {P = P} f pzero psuc)

elim : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
     → (f : Fin k)
     → (pzero : {n : ℕ} → P (fzero {n}))
     → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
     → P f
elim = elimᵣ

elimₗ : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
      → (f : Fin k)
      → (pzero : {n : ℕ} → P (fzero {n}))
      → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
      → P f
elimₗ fzero pzero psuc = pzero
elimₗ {P = P} (fsuc f) pzero psuc = elimₗ {P = λ m → P (fsuc m)} f (psuc pzero) psuc

recᵣ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → ({n : ℕ} → Fin n → A → A) → A
recᵣ f z s = elimᵣ f z λ {_}{m} → s m

rec : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → ({n : ℕ} → Fin n → A → A) → A
rec = recᵣ

recₗ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → ({n : ℕ} → Fin n → A → A) → A
recₗ f z s = elimₗ f z λ {_}{m} → s m

iteᵣ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → (A → A) → A
iteᵣ f z s = elimᵣ f z s

ite : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → (A → A) → A
ite = iteᵣ

iteₗ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → Fin k → A → (A → A) → A
iteₗ f z s = elimₗ f z s

elimᵣ′ : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
       → (pzero : {n : ℕ} → P (fzero {n}))
       → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
       → (f : Fin k)
       → P f
elimᵣ′ {P = P} pzero psuc f = elimᵣ {P = P} f pzero psuc

elim′ : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
      → (pzero : {n : ℕ} → P (fzero {n}))
      → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
      → (f : Fin k)
      → P f
elim′ = elimᵣ′

elimₗ′ : ∀{ℓ}{P : {n : ℕ} → Fin n → Set ℓ}{k : ℕ}
       → (pzero : {n : ℕ} → P (fzero {n}))
       → (psuc : {n : ℕ}{f : Fin n} → P f → P (fsuc f))
       → (f : Fin k)
       → P f
elimₗ′ {P = P} pzero psuc f = elimₗ {P = P} f pzero psuc

recᵣ′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → ({n : ℕ} → Fin n → A → A) → Fin k → A
recᵣ′ z s f = recᵣ f z s

rec′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → ({n : ℕ} → Fin n → A → A) → Fin k → A
rec′ = recᵣ′

recₗ′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → ({n : ℕ} → Fin n → A → A) → Fin k → A
recₗ′ z s f = recₗ f z s

iteᵣ′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → (A → A) → Fin k → A
iteᵣ′ z s f = iteᵣ f z s

ite′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → (A → A) → Fin k → A
ite′ = iteᵣ′

iteₗ′ : ∀{ℓ}{A : Set ℓ}{k : ℕ} → A → (A → A) → Fin k → A
iteₗ′ z s f = iteₗ f z s
