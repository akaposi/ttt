{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Fin.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Fin.Type
open import Lib.Fin.Instances.DecidableEquality

instance
  EqFin : ∀{n} → Eq (Fin n)
  EqFin = EqInstance
