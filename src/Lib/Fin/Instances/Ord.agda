{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Fin.Instances.Ord where

open import Lib.Fin.Type

open import Agda.Primitive

open import Lib.Class.Ord
open Ord

open import Lib.Sum.Type

open import Lib.Fin.Instances.Eq

open import Lib.Dec.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdFin : ∀{n} → Ord (Fin n)
  eq OrdFin = EqFin

  (OrdFin ≤ fzero) _ = ⊤
  (OrdFin ≤ fsuc m) fzero = ⊥
  (OrdFin ≤ fsuc m) (fsuc k) = (OrdFin ≤ m) k

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fzero} = tt
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fsuc a} = reflexive (decidableTotalOrder OrdFin) {a}

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fzero} {b} {c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fsuc a} {fzero} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fsuc a} {fsuc b} {fzero} = λ _ x → x
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdFin)))) {fsuc a} {fsuc b} {fsuc c} = transitive (decidableTotalOrder OrdFin) {a} {b} {c}

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdFin))) {fzero} {fzero} = λ _ _ → refl
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdFin))) {fzero} {fsuc b} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdFin))) {fsuc a} {fzero} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdFin))) {fsuc a} {fsuc b} = λ a≤b b≤a → cong fsuc (antisymmetric (decidableTotalOrder OrdFin) {a} {b} a≤b b≤a)

  connected (totalOrder (decidableTotalOrder OrdFin)) {fzero} {fzero} = inl tt
  connected (totalOrder (decidableTotalOrder OrdFin)) {fzero} {fsuc b} = inl tt
  connected (totalOrder (decidableTotalOrder OrdFin)) {fsuc a} {fzero} = inr tt
  connected (totalOrder (decidableTotalOrder OrdFin)) {fsuc a} {fsuc b} = connected (decidableTotalOrder OrdFin) {a} {b}

  Decidable₂.decide (decidable (decidableTotalOrder OrdFin)) fzero fzero = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdFin)) fzero (fsuc b) = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdFin)) (fsuc a) fzero = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder OrdFin)) (fsuc a) (fsuc b) = (decidableTotalOrder OrdFin ≤? a) b
