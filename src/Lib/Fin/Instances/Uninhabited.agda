{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Fin.Instances.Uninhabited where

open import Lib.Class.Uninhabited
open Uninhabited

open import Lib.Fin.Type

instance
  Fin0Empty : Uninhabited (Fin 0)
  uninhabited Fin0Empty ()
