{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Function.Instances.Uninhabited where

open import Lib.Class.Uninhabited
open Uninhabitedω

open import Lib.Empty.Type
open import Lib.Unit.Type

instance
  A→BEmpty : Uninhabitedω (∀{i j}{A : Set i}{B : Set j} → A → B)
  uninhabitedω A→BEmpty f = f {A = ⊤} {⊥} tt

  AEmpty : Uninhabitedω (∀{i}{A : Set i} → A)
  uninhabitedω AEmpty f = f {A = ⊥}
