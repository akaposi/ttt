{-# OPTIONS --safe --without-K #-}

module Lib.IndCoFin.Type where

open import Lib.CoNat.Type
open import Lib.CoNat.Base

data Fin∞ᵢ : ℕ∞ → Set where
  izero : {n : ℕ∞} → .⦃ IsNotZero∞ n ⦄ → Fin∞ᵢ n
  isuc  : {n : ℕ∞} → .⦃ p : IsNotZero∞ n ⦄ → Fin∞ᵢ (predℕ∞ n ⦃ p ⦄) → Fin∞ᵢ n
