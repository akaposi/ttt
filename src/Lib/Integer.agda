{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer where

open import Lib.Integer.Type public
open import Lib.Integer.Base public
open import Lib.Integer.Literals public
open import Lib.Integer.Properties public
open import Lib.Integer.Instances public
