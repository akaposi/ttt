{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Base where

open import Lib.Integer.Type

open import Lib.Equality.Type

open import Lib.Nat.Type hiding (suc)
import Lib.Nat.Base as ℕ

open import Lib.Sigma.Type

open import Lib.Sum.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

ind : ∀{i}{A : ℤ → Set i}(z : ℤ)(pos : (n : ℕ) → z ≡ + n → A z)(negsuc : (n : ℕ) → z ≡ -suc n → A z) → A z
ind (+ n) pos negsuc = pos n refl
ind (-suc n) pos negsuc = negsuc n refl

elim : ∀{i}{A : ℤ → Set i}(z : ℤ)(pos : (n : ℕ) → A (+ n))(negsuc : (n : ℕ) → A (-suc n)) → A z
elim (+ n) pos negsuc = pos n
elim (-suc n) pos negsuc = negsuc n

elim′ : ∀{i}{A : ℤ → Set i}(pos : (n : ℕ) → A (+ n))(negsuc : (n : ℕ) → A (-suc n))(z : ℤ) → A z
elim′ {A = A} pos negsuc z = elim {A = A} z pos negsuc

ite : ∀{i}{A : Set i}(z : ℤ)(pos : ℕ → A)(negsuc : ℕ → A) → A
ite = elim

ite′ : ∀{i}{A : Set i}(pos : ℕ → A)(negsuc : ℕ → A)(z : ℤ) → A
ite′ = elim′

case : ∀{i}{A : Set i}(z : ℤ)(pos : A)(negsuc : A) → A
case z pos negsuc = ite z (λ _ → pos) λ _ → negsuc

case′ : ∀{i}{A : Set i}(pos : A)(negsuc : A)(z : ℤ) → A
case′ pos negsuc z = case z pos negsuc

suc : ℤ → ℤ
suc (+ n)            = + (ℕ.suc n)
suc (-suc zero)      = + zero
suc (-suc (ℕ.suc n)) = -suc n

pred : ℤ → ℤ
pred (+ zero)      = -suc zero
pred (+ (ℕ.suc n)) = + n
pred (-suc n)      = -suc (ℕ.suc n)

infixl 6 _⊝_
_⊝_ : ℕ → ℕ → ℤ
n       ⊝ zero    = + n
zero    ⊝ ℕ.suc k = -suc k
ℕ.suc n ⊝ ℕ.suc k = n ⊝ k

infixl 6 _+_ _-_
_+_ : ℤ → ℤ → ℤ
+ zero         + k = k
+ (ℕ.suc n)    + k = suc (+ n + k)
-suc zero      + k = pred k
-suc (ℕ.suc n) + k = pred (-suc n + k)

_-_ : ℤ → ℤ → ℤ
z - k = z + - k

infixl 7 _*_
_*_ : ℤ → ℤ → ℤ
+ zero         * k = + 0
+ (ℕ.suc n)    * k = k + (+ n) * k
-suc zero      * k = - k
-suc (ℕ.suc n) * k = - k + -suc n * k

infixr 8 _^_
_^_ : ℤ → ℕ → ℤ
z ^ zero    = + 1
z ^ ℕ.suc k = z * z ^ k

infixl 1000 ∣_∣
∣_∣ : ℤ → ℕ
∣ + n ∣    = n
∣ -suc n ∣ = ℕ.suc n

IsNotZeroᵗ : ℤ → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
IsNotZeroᵗ (+ zero) = ⊥ , inr refl
IsNotZeroᵗ (+ (ℕ.suc n)) = ⊤ , inl refl
IsNotZeroᵗ (-suc n) = ⊤ , inl refl

record IsNotZero (z : ℤ) : Set where
  constructor prove-isNotZero
  field
    isNotZero : fst (IsNotZeroᵗ z)

open IsNotZero {{...}} public

IsZeroᵗ : ℤ → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
IsZeroᵗ (+ zero) = ⊤ , inl refl
IsZeroᵗ (+ (ℕ.suc n)) = ⊥ , inr refl
IsZeroᵗ (-suc n) = ⊥ , inr refl

record IsZero (z : ℤ) : Set where
  constructor prove-isZero
  field
    isZero : fst (IsZeroᵗ z)

open IsZero {{...}} public
