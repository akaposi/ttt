{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Instances where

open import Lib.Integer.Instances.DecidableEquality public
open import Lib.Integer.Instances.Eq public
open import Lib.Integer.Instances.Ord public
open import Lib.Integer.Instances.IsSet public
