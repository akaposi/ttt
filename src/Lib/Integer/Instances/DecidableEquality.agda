{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Instances.DecidableEquality where

open import Lib.Dec.Type
open Decidable₂

open import Lib.Nat.Instances.DecidableEquality

open import Lib.Integer.Type
open import Lib.Integer.Properties

open import Lib.Equality.Base

instance
  DecEqℤ : DecidableEquality ℤ
  decide DecEqℤ (+ n) (+ k) with decide DecEqℕ n k
  ... | yes a = yes (cong +_ a)
  ... | no ¬a = no λ e → ¬a (+-injective e)
  decide DecEqℤ (+ _) (-suc _) = no λ ()
  decide DecEqℤ (-suc _) (+ _) = no λ ()
  decide DecEqℤ (-suc n) (-suc k) with decide DecEqℕ n k
  ... | yes a = yes (cong -suc a)
  ... | no ¬a = no λ e → ¬a (-suc-injective e)
