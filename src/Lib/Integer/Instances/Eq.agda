{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Integer.Type
open import Lib.Integer.Instances.DecidableEquality

instance
  Eqℤ : Eq ℤ
  Eqℤ = EqInstance
