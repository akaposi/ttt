{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Instances.IsSet where

open import Lib.Class.IsSet

open import Lib.Integer.Type
open import Lib.Integer.Instances.DecidableEquality

open import Lib.Dec.InstanceGenerators.IsSet

instance
  IsSetℤ : IsSet ℤ
  IsSetℤ = DecidableEquality→IsSet DecEqℤ 
