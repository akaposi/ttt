{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Integer.Type
open import Lib.Integer.Instances.Eq

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Sum.Type

open import Lib.Dec.Type

open import Lib.Equality.Base

open import Lib.Nat.Equality.Base
open import Lib.Nat.Instances.Ord

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder
open Decidable₂

instance
  Ordℤ : Ord ℤ
  eq Ordℤ = Eqℤ

  (Ordℤ ≤ + n) (+ k) = (Ordℕ ≤ n) k
  (Ordℤ ≤ + n) (-suc k) = ⊥
  (Ordℤ ≤ -suc n) (+ k) = ⊤
  (Ordℤ ≤ -suc n) (-suc k) = (Ordℕ ≤ k) n

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) {+ n} = refl≤ℕ n
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) { -suc n} = refl≤ℕ n

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) {+ n} {+ m} {+ k} e1 e2 = trans≤ℕ n m k e1 e2
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) { -suc n} {+ m} {+ k} e1 e2 = tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) { -suc n} { -suc m} {+ k} e1 e2 = tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℤ)))) { -suc n} { -suc m} { -suc k} e1 e2 = trans≤ℕ k m n e2 e1

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ordℤ))) {+ n} {+ k} e1 e2 = cong +_ (antisymmetric (decidableTotalOrder Ordℕ) e1 e2)
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ordℤ))) {+ n} { -suc k} () e2
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ordℤ))) { -suc n} {+ k} e1 ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ordℤ))) { -suc n} { -suc k} e1 e2 = cong -suc ((antisymmetric (decidableTotalOrder Ordℕ) e2 e1))

  connected (totalOrder (decidableTotalOrder Ordℤ)) {+ n} {+ k} = connected (decidableTotalOrder Ordℕ) {n}
  connected (totalOrder (decidableTotalOrder Ordℤ)) {+ n} { -suc k} = inr tt
  connected (totalOrder (decidableTotalOrder Ordℤ)) { -suc n} {+ k} = inl tt
  connected (totalOrder (decidableTotalOrder Ordℤ)) { -suc n} { -suc k} = connected (decidableTotalOrder Ordℕ) {k}

  decide (decidable (decidableTotalOrder Ordℤ)) (+ n) (+ k) = (decidableTotalOrder Ordℕ ≤? n) k
  decide (decidable (decidableTotalOrder Ordℤ)) (+ n) (-suc k) = no λ ()
  decide (decidable (decidableTotalOrder Ordℤ)) (-suc n) (+ k) = yes tt
  decide (decidable (decidableTotalOrder Ordℤ)) (-suc n) (-suc k) = (decidableTotalOrder Ordℕ ≤? k) n
