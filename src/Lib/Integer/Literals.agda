{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Literals where

open import Lib.Integer.Type

open import Agda.Builtin.FromNat public
open import Agda.Builtin.FromNeg public

open import Lib.Unit.Type public

open import Lib.Integer.Base using (suc)

instance
  open Number
  Numℤ : Number ℤ
  Constraint Numℤ _ = ⊤
  fromNat Numℤ n = + n

  open Negative
  Negℤ : Negative ℤ
  Constraint Negℤ _ = ⊤
  fromNeg Negℤ n = suc (-suc n)
