{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.Integer.Properties where

open import Lib.Integer.Type
open import Lib.Integer.Base
open import Lib.Integer.Literals

open import Lib.Nat.Type hiding (suc)
import Lib.Nat.Base as ℕ
import Lib.Nat.Properties as ℕ
open import Lib.Nat.Literals

open import Lib.Equality.Type
open import Lib.Equality.Base

--------------------------------
open import Lib.Unit.Type

open import Lib.Empty.Type

{-# WARNING_ON_IMPORT "ℤ.Properties not yet implemented.\n\
                      \TODO:\n\
                      \  ∙ Actual properties of operations \n\
                      \Implemented:\n\
                      \  ∙ +-injective\n\
                      \  ∙ -suc-injective" #-}

+-injective : ∀{n k} → + n ≡ + k → n ≡ k
+-injective = cong (λ x → ite x (λ y → y) (λ y → y))

-suc-injective : ∀{n k} → -suc n ≡ -suc k → n ≡ k
-suc-injective = cong (λ x → ite x (λ y → y) (λ y → y))

sucpred : (z : ℤ) → suc (pred z) ≡ z
sucpred z = {!!}

predsuc : (z : ℤ) → pred (suc z) ≡ z
predsuc z = {!!}

suc-injective : {a b : ℤ} → suc a ≡ suc b → a ≡ b
suc-injective e = {!!}

pred-injective : {a b : ℤ} → pred a ≡ pred b → a ≡ b
pred-injective e = {!!}

sucn≢n : ∀ {n} → suc n ≢ n
sucn≢n {n} e = {!!}

n≢sucn : ∀ {n} → n ≢ suc n
n≢sucn {n} e = {!!}

predn≢n : ∀ {n} → pred n ≢ n
predn≢n {n} e = {!!}

n≢predn : ∀ {n} → n ≢ pred n
n≢predn {n} e = {!!}

idr+ : (z : ℤ) → z + 0 ≡ z
idr+ z = {!!}

sucl+ : (a b : ℤ) → suc a + b ≡ suc (a + b)
sucl+ a b = {!!}

sucr+ : (a b : ℤ) → a + suc b ≡ suc (a + b)
sucr+ a b = {!!}

predl+ : (a b : ℤ) → pred a + b ≡ pred (a + b)
predl+ a b = {!!}

predr+ : (a b : ℤ) → a + pred b ≡ pred (a + b)
predr+ a b = {!!}

assoc+ : (a b c : ℤ) → (a + b) + c ≡ a + (b + c)
assoc+ a b c = {!!}

comm+ : (a b : ℤ) → a + b ≡ b + a
comm+ a b = {!!}

involutive- : (z : ℤ) → - - z ≡ z
involutive- z = {!!}

invl+ : (z : ℤ) → - z + z ≡ 0
invl+ z = {!!}

invr+ : (z : ℤ) → z + - z ≡ 0
invr+ z = {!!}

+-injectiveₗ : (a b c : ℤ) → a + b ≡ a + c → b ≡ c
+-injectiveₗ a b c e = {!!}

+-injectiveᵣ : (a b c : ℤ) → a + b ≡ c + b → a ≡ c
+-injectiveᵣ a b c e = {!!}

*-injectiveᵣ : (a b c : ℤ) → .⦃ IsNotZero b ⦄ → a * b ≡ c * b → a ≡ c
*-injectiveᵣ a b c = {!!}

*-injectiveₗ : (a b c : ℤ) → .⦃ IsNotZero a ⦄ → a * b ≡ a * c → b ≡ c
*-injectiveₗ a b c = {!!}

idl* : (z : ℤ) → 1 * z ≡ z
idl* z = {!!}

idr* : (z : ℤ) → z * 1 ≡ z
idr* z = {!!}

dist+* : (a b c : ℤ) → (a + b) * c ≡ a * c + b * c
dist+* a b c = {!!}

dist*+ : (a b c : ℤ) → a * (b + c) ≡ a * b + a * c
dist*+ a b c = {!!}

nullr* : (z : ℤ) → z * 0 ≡ 0
nullr* z = {!!}

sucr* : (a b : ℤ) → a * suc b ≡ a + a * b
sucr* a b = {!!}

assoc* : (a b c : ℤ) → (a * b) * c ≡ a * (b * c)
assoc* a b c = {!!}

comm* : (a b : ℤ) → a * b ≡ b * a
comm* a b = {!!}

nulll^ : (n : ℕ) → 1 ^ n ≡ 1
nulll^ n = {!!}

idr^ : (a : ℤ) → a ^ 1 ≡ a
idr^ = idr*

dist^+ : (m : ℤ)(n o : ℕ) → m ^ (n ℕ.+ o) ≡ m ^ n * m ^ o
dist^+ m zero o = sym (idr+ (m ^ o))
dist^+ m (ℕ.suc n) o = trans (cong (m *_) (dist^+ m n o)) (sym (assoc* m (m ^ n) (m ^ o)))

dist^* : (a : ℤ)(m n : ℕ) → a ^ (m ℕ.* n) ≡ (a ^ m) ^ n
dist^* a m n = {!!}

dist*^ : (a b : ℤ)(n : ℕ) → (a * b) ^ n ≡ a ^ n * b ^ n
dist*^ a b n = {!!}
