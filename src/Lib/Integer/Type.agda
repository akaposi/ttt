{-# OPTIONS --safe --without-K #-}

module Lib.Integer.Type where

open import Agda.Builtin.Int
  renaming ( Int to ℤ
           ; pos    to +_
           ; negsuc to -suc
           )
  public

open import Lib.Nat.Type

infix 7.5 -_
-_ : ℤ → ℤ
- + zero      = + zero
- + (ℕ.suc n) = -suc n
- -suc n      = + ℕ.suc n

-- {-# DISPLAY +_ x = x #-}
{-# DISPLAY -suc x = - (suc x) #-}
