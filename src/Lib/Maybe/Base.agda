{-# OPTIONS --safe --without-K #-}

module Lib.Maybe.Base where

open import Lib.Maybe.Type

open import Lib.Sigma.Type

open import Lib.Sum.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Equality.Type

IsJustᵗ : ∀{i}{A : Set i} → Maybe A → Σ Set (λ B → B ≡ ⊤ ⊎ B ≡ ⊥)
IsJustᵗ (just x) = ⊤ , inl refl
IsJustᵗ nothing = ⊥ , inr refl

{-
IsJust : ∀{i}{A : Set i} → Maybe A → Set
IsJust n = fst (IsJustᵗ n)
-}

record IsJust {i}{A : Set i}(n : Maybe A) : Set where
  instance constructor prove-just
  field
    ⦃ isJust ⦄ : fst (IsJustᵗ n)

open IsJust public

IsNothingᵗ : ∀{i}{A : Set i} → Maybe A → Σ Set (λ B → B ≡ ⊤ ⊎ B ≡ ⊥)
IsNothingᵗ (just x) = ⊥ , inr refl
IsNothingᵗ nothing = ⊤ , inl refl

{-
IsNothing : ∀{i}{A : Set i} → Maybe A → Set
IsNothing n = fst (IsNothingᵗ n)
-}

record IsNothing {i}{A : Set i}(n : Maybe A) : Set where
  instance constructor prove-nothing
  field
    ⦃ isNothing ⦄ : fst (IsNothingᵗ n)

open IsNothing public

elim : ∀{i j}{A : Set i}{B : Maybe A → Set j} → (x : Maybe A)
       → ((a : A) → B (just a)) → B nothing → B x
elim (just x) j n = j x
elim nothing j n  = n

elim′ : ∀{i j}{A : Set i}{B : Maybe A → Set j}
       → ((a : A) → B (just a)) → B nothing → (x : Maybe A) → B x
elim′ {B = B} j n x = elim {B = B} x j n

ite : ∀{a b}{A : Set a}{B : Set b} → Maybe A → (A → B) → B → B
ite = elim

ite′ : ∀{a b}{A : Set a}{B : Set b} → (A → B) → B → Maybe A → B
ite′ = elim′

fromMaybe : ∀{a}{A : Set a} → A → Maybe A → A
fromMaybe = ite′ (λ x → x)

ind : ∀{a b}{A : Set a}{B : Maybe A → Set b} → (a : Maybe A) →
    ({x : A} → a ≡ just x → B a) →
    (a ≡ nothing → B a) → B a
ind (just x) j n = j refl
ind nothing  j n = n refl

map : ∀{i j}{A : Set i}{B : Set j} → (A → B) → Maybe A → Maybe B
map _ nothing = nothing
map f (just x) = just (f x)

fromJust : ∀{i}{A : Set i}(m : Maybe A) → .⦃ IsJust m ⦄ → A
fromJust (just a) = a
