{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Maybe.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Maybe.Type
open import Lib.Maybe.Instances.DecidableEquality

instance
  EqMaybe : ∀{i}{A : Set i} → ⦃ Eq A ⦄ → Eq (Maybe A)
  EqMaybe = EqInstance
