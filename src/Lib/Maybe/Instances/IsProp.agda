{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Maybe.Instances.IsProp where

open import Lib.Maybe.Type
open import Lib.Maybe.Properties

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type

open import Lib.Class.IsProp

IsPropMaybe : ∀{i}{A : Set i} → IsProp (Maybe A) → IsProp A
IsPropMaybe (isProp-proof isPropA) = isProp-proof λ x y → let (p1 , p2) = isPropA (just x) (just y) in just-injective p1 , λ {refl → cong just-injective (p2 refl)}

open import Lib.Empty.Type
open import Lib.Empty.Base

bot : ∀{i}{A : Set i} → IsProp (Maybe A) → Σ (A → ⊥) (λ f → Σ (⊥ → A) λ g → (∀ x → f (g x) ≡ x) × (∀ x → g (f x) ≡ x))
bot (isProp-proof isPropA) = (λ a → just≢nothing (fst (isPropA (just a) nothing))) , exfalso , (λ ()) , λ a → let (p1 , p2) = isPropA (just a) nothing in exfalso (just≢nothing p1)
