{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Maybe.Instances.IsSet where

open import Lib.Maybe.Type
open import Lib.Maybe.Properties
open import Lib.Maybe.Equality

open import Lib.Class.IsSet

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sigma.Type

instance
  IsSetMaybe : ∀{i}{A : Set i} → ⦃ IsSet A ⦄ → IsSet (Maybe A)
  IsSetMaybe {A = A} ⦃ isSet-proof isSetA ⦄ = isSet'→IsSet λ { nothing nothing refl refl → refl ; (just x) .(just x) refl q → cong (cong just) (fst (isSetA x x refl (just-injective q))) ◾ ≡→≡Maybe→≡-id (just x) (just x) q}
