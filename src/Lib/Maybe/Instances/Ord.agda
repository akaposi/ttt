{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Maybe.Instances.Ord where

open import Agda.Primitive

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Class.Ord
open Ord

open import Lib.Maybe.Instances.Eq

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Unit.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Dec.Type

open import Lib.Sum.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdMaybe : ∀{i}{A : Set i} → ⦃ Ord A ⦄ → Ord (Maybe A)
  eq OrdMaybe = EqMaybe

  (OrdMaybe ⦃ ordA ⦄ ≤ nothing) _        = ⊤
  (OrdMaybe ⦃ ordA ⦄ ≤ just x)  nothing  = ⊥
  (OrdMaybe ⦃ ordA ⦄ ≤ just x)  (just y) = (ordA ≤ x) y

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {just x} = reflexive (decidableTotalOrder ordA) {x}
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {nothing} = tt

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {nothing} {b} {c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {just x} {nothing} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {just x} {just y} {nothing} = λ _ z → z
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))))) {just x} {just y} {just z} = transitive (decidableTotalOrder ordA)

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄)))) {just x} {just y} = λ x≤y y≤x → cong just (antisymmetric (decidableTotalOrder ordA) {x} {y} x≤y y≤x)
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄)))) {just x} {nothing} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄)))) {nothing} {just x} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄)))) {nothing} {nothing} = λ _ _ → refl

  connected (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) {just x} {just y} = connected (decidableTotalOrder ordA)
  connected (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) {just x} {nothing} = inr tt
  connected (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) {nothing} {just x} = inl tt
  connected (totalOrder (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) {nothing} {nothing} = inl tt

  Decidable₂.decide (decidable (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) nothing  _        = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) (just x) nothing  = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder (OrdMaybe ⦃ ordA ⦄))) (just x) (just y) = (decidableTotalOrder ordA ≤? x) y
