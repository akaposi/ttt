{-# OPTIONS --safe --without-K #-}

module Lib.Maybe.Type where

open import Agda.Builtin.Maybe public

infixl 900 _？
_？ : ∀{i}(A : Set i) → Set i
_？ = Maybe
