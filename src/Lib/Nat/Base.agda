{-# OPTIONS --safe --without-K #-}

module Lib.Nat.Base where

open import Lib.Nat.Literals
open import Lib.Nat.Type
open import Lib.Nat.Equality.Type
open import Lib.Nat.Equality.Base

open import Lib.Empty.Type

open import Lib.Unit.Type

open import Lib.Sigma.Type

open import Lib.Sum.Type

open import Lib.Bool.Type

import Lib.Sum.Base as Sum

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Containers.List.Type

open import Lib.UnitOrEmpty.Type

open import Agda.Builtin.Nat public
  hiding (Nat ; suc ; zero)
  renaming (_<_ to _<ℕᵇ_ ; _==_ to _==ℕᵇ_ ; _-_ to _-'_)

pred' : ℕ → ℕ
pred' 0 = 0
pred' (suc n) = n

infixr 6 _-′_
_-′_ : (x y : ℕ) → ℕ
x -′ zero = x
zero -′ suc _ = zero
suc x -′ suc y = x -′ y

infixr 8 _^_
_^_ : ℕ → ℕ → ℕ
x ^ zero  = 1
x ^ suc n = x * x ^ n

infix 8 _⁰ _¹ _² _³ _⁴ _⁵ _⁶ _⁷ _⁸ _⁹

_⁰ : ℕ → ℕ
_⁰ = _^ 0

_¹ : ℕ → ℕ
_¹ = _^ 1

_² : ℕ → ℕ
_² = _^ 2

_³ : ℕ → ℕ
_³ = _^ 3

_⁴ : ℕ → ℕ
_⁴ = _^ 4

_⁵ : ℕ → ℕ
_⁵ = _^ 5

_⁶ : ℕ → ℕ
_⁶ = _^ 6

_⁷ : ℕ → ℕ
_⁷ = _^ 7

_⁸ : ℕ → ℕ
_⁸ = _^ 8

_⁹ : ℕ → ℕ
_⁹ = _^ 9

infixl 50 _!
_! : ℕ → ℕ
zero  ! = 1
suc n ! = suc n * n !

infixl 50 _!′
_!′ : ℕ → ℕ
zero  !′ = 1
suc n !′ = n !′ * suc n

even : ℕ → Bool
even zero = true
even (suc zero) = false
even (suc (suc n)) = even n

odd : ℕ → Bool
odd zero = false
odd (suc zero) = true
odd (suc (suc n)) = odd n

data Even : ℕ → Set where
  instance Even0 : Even 0
  instance Even+2 : {n : ℕ} → .⦃ Even n ⦄ → Even (suc (suc n))

data Odd : ℕ → Set where
  instance Odd1 : Odd 1
  instance Odd+2 : {n : ℕ} → .⦃ Odd n ⦄ → Odd (suc (suc n))

case-proof : ∀{i}{A : ℕ → Set i}(n : ℕ) → (n ≡ 0 → A 0) → ({k : ℕ} → n ≡ suc k → A (suc k)) → A n
case-proof zero a0 ak = a0 refl
case-proof (suc n) a0 ak = ak refl

elimᵣ : ∀{i}{A : ℕ → Set i} → (n : ℕ) → A 0 → ({k : ℕ} → A k → A (suc k)) → A n
elimᵣ         zero    a0 f = a0
elimᵣ {A = A} (suc n) a0 f = f (elimᵣ {A = A} n a0 f)

elim : ∀{i}{A : ℕ → Set i} → (n : ℕ) → A 0 → ({k : ℕ} → A k → A (suc k)) → A n
elim = elimᵣ

elimₗ : ∀{i}{A : ℕ → Set i} → (n : ℕ) → A 0 → ({k : ℕ} → A k → A (suc k)) → A n
elimₗ zero z s = z
elimₗ {A = A} (suc n) z s = elimₗ {A = λ k → A (suc k)} n (s z) s

recᵣ : ∀{i}{A : Set i} → ℕ → A → (ℕ → A → A) → A
recᵣ n z s = elim n z λ {k} → s k

rec : ∀{i}{A : Set i} → ℕ → A → (ℕ → A → A) → A
rec = recᵣ

recₗ : ∀{i}{A : Set i} → ℕ → A → (ℕ → A → A) → A
recₗ n z s = elimₗ n z λ {k} → s k

iteᵣ : ∀{i}{A : Set i} → ℕ → A → (A → A) → A
iteᵣ n z s = elim n z s -- No η-reduce; implicit vs explicit parameters

ite : ∀{i}{A : Set i} → ℕ → A → (A → A) → A
ite = iteᵣ

iteₗ : ∀{i}{A : Set i} → ℕ → A → (A → A) → A
iteₗ n z s = elimₗ n z s -- No η-reduce; implicit vs explicit parameters

elimᵣ′ : ∀{i}{A : ℕ → Set i} → A 0 → ({k : ℕ} → A k → A (suc k)) → (n : ℕ) → A n
elimᵣ′ {A = A} z s n = elimᵣ {A = A} n z s

elim′ : ∀{i}{A : ℕ → Set i} → A 0 → ({k : ℕ} → A k → A (suc k)) → (n : ℕ) → A n
elim′ = elimᵣ′

elimₗ′ : ∀{i}{A : ℕ → Set i} → A 0 → ({k : ℕ} → A k → A (suc k)) → (n : ℕ) → A n
elimₗ′ {A = A} z s n = elimₗ {A = A} n z s

recᵣ′ : ∀{i}{A : Set i} → A → (ℕ → A → A) → ℕ → A
recᵣ′ z s = elim′ z (λ {k} → s k)

rec′ : ∀{i}{A : Set i} → A → (ℕ → A → A) → ℕ → A
rec′ = recᵣ′

recₗ′ : ∀{i}{A : Set i} → A → (ℕ → A → A) → ℕ → A
recₗ′ z s = elimₗ′ z λ {k} → s k

iteᵣ′ : ∀{i}{A : Set i} → A → (A → A) → ℕ → A
iteᵣ′ z s = elimᵣ′ z s

ite′ : ∀{i}{A : Set i} → A → (A → A) → ℕ → A
ite′ = iteᵣ′

iteₗ′ : ∀{i}{A : Set i} → A → (A → A) → ℕ → A
iteₗ′ z s = elimₗ′ z s

case : ∀{i}{A : Set i} → ℕ → A → A → A
case n z s = ite n z (λ _ → s)

case′ : ∀{i}{A : Set i} → A → A → ℕ → A
case′ z s n = case n z s

IsZeroᵗ : ℕ → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
IsZeroᵗ n = case n (⊤ , inl refl) (⊥ , inr refl)

record IsZero (n : ℕ) : Set where
  instance constructor prove-isZero
  field
    ⦃ isZero ⦄ : fst (IsZeroᵗ n)

open IsZero public

IsNotZeroᵗ : ℕ → Σ Set (λ A → A ≡ ⊤ ⊎ A ≡ ⊥)
IsNotZeroᵗ n = case n (⊥ , inr refl) (⊤ , inl refl)

record IsNotZero (n : ℕ) : Set where
  instance constructor prove-isNotZero
  field
    ⦃ isNotZero ⦄ : fst (IsNotZeroᵗ n)

open IsNotZero public

pred : (n : ℕ) → .⦃ nonZero : IsNotZero n ⦄ → ℕ
pred (suc n) = n

infixr 6 _-_
_-_ : (x y : ℕ) → .⦃ nonZero : x ≥ℕ y ⦄ → ℕ
x - zero = x
suc x - suc y = x - y

infixl 7 _div-suc_ div _`div`_ _/_
_div-suc_ : ℕ → ℕ → ℕ
n div-suc 1+m = div-helper 0 1+m n 1+m

div : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
div n (suc k) = n div-suc k

_`div`_ : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
_`div`_ = div

_/_ : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
_/_ = div

infixl 7 _mod-suc_ mod _`mod`_ _%_
_mod-suc_ : ℕ → ℕ → ℕ
n mod-suc 1+m = mod-helper 0 1+m n 1+m

mod : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
mod n (suc k) = n mod-suc k

_`mod`_ : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
_`mod`_ = mod

_%_ : (n k : ℕ) → .⦃ IsNotZero k ⦄ → ℕ
_%_ = mod

minMax : (n k : ℕ) → Σ (ℕ × ℕ) (λ {(a , b) → (n ≤ℕ k × n ≡ℕ a × k ≡ℕ b) ⊎ (k ≤ℕ n × k ≡ℕ a × n ≡ℕ b)})
minMax zero k = (zero , k) , inl (tt , tt , reflℕ k)
minMax (suc n) zero = (zero , suc n) , inr (tt , tt , reflℕ n)
minMax (suc n) (suc k) = let ((a , b) , c) = minMax n k in (suc a , suc b) , c

minWithProof : (n k : ℕ) → Σ ℕ (λ a → (n ≤ℕ k × n ≡ℕ a) ⊎ (k ≤ℕ n × k ≡ℕ a))
minWithProof n k = let ((a , b) , e) = minMax n k in Sum.case e (λ (e1 , e2 , e3) → a , inl (e1 , e2)) (λ (e1 , e2 , e3) → a , inr (e1 , e2))

maxWithProof : (n k : ℕ) → Σ ℕ (λ a → (n ≤ℕ k × k ≡ℕ a) ⊎ (k ≤ℕ n × n ≡ℕ a))
maxWithProof n k = let ((a , b) , e) = minMax n k in Sum.case e (λ (e1 , e2 , e3) → b , inl (e1 , e3)) (λ (e1 , e2 , e3) → b , inr (e1 , e3))

min : ℕ → ℕ → ℕ
min n m = fst (fst (minMax n m))

max : ℕ → ℕ → ℕ
max n m = snd (fst (minMax n m))

infixl 6 _⊔_
_⊔_ : ℕ → ℕ → ℕ
_⊔_ = max

infixl 7 _⊓_
_⊓_ : ℕ → ℕ → ℕ
_⊓_ = min

compare : ∀{i}{A : Set i} → ℕ → ℕ → A → A → A → A
compare zero    zero    n<k n=k n>k = n=k
compare zero    (suc k) n<k n=k n>k = n<k
compare (suc n) zero    n<k n=k n>k = n>k
compare (suc n) (suc k) n<k n=k n>k = compare n k n<k n=k n>k

squareRoot : ℕ → ℕ → ℕ
squareRoot 0 _ = 0
squareRoot n@(suc _) fuel = babylon n n fuel where
  babylon : ℕ → ℕ → ℕ → ℕ
  babylon orig-n 0 _ = 0
  babylon orig-n (suc _) 0 = 0
  babylon orig-n a'@(suc a) (suc fuel) = let b = (a' + orig-n `div` a') `div` 2 in compare b a' (babylon orig-n b fuel) a' a'

infixr 30 ⌊√_⌋
⌊√_⌋ : ℕ → ℕ
⌊√ n ⌋ = squareRoot n n
