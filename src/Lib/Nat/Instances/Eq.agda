{-# OPTIONS --safe --without-K #-}

module Lib.Nat.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Nat.Type
open import Lib.Nat.Properties
open import Lib.Nat.Instances.DecidableEquality

open import Lib.Sigma.Type

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Unit.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

instance
  Eqℕ : Eq ℕ
  Eqℕ = EqInstance
