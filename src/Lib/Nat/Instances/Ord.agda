{-# OPTIONS --safe --without-K #-}

module Lib.Nat.Instances.Ord where

open import Lib.Nat.Type
open import Lib.Nat.Equality.Type
open import Lib.Nat.Equality.Base
open import Lib.Nat.Equality.RelationToEqualityType
open import Lib.Nat.Properties hiding (_≟_)
open import Lib.Nat.Instances.Eq

open import Lib.Class.Ord
open Ord

open import Lib.Sigma.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Sum.Type
open import Lib.Sum.Base

open import Lib.Unit.Type

open import Lib.Empty.Base

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Dec.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  Ordℕ : Ord ℕ
  eq Ordℕ = Eqℕ
  _≤_ Ordℕ = _≤ℕ_
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℕ)))) {n} = refl≤ℕ n
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ordℕ)))) {n} {m} {k} e1 e2 = trans≤ℕ n m k e1 e2
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ordℕ))) {n} {k} e1 e2 = ≡ℕ→≡ (antisym≤ℕ n k e1 e2)
  connected (totalOrder (decidableTotalOrder Ordℕ)) {n} {k} = stronglyConnected≤ℕ n k
  decidable (decidableTotalOrder Ordℕ) = decidable≤ℕ
