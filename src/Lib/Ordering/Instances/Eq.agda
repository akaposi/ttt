{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Ordering.Instances.Eq where

open import Lib.Ordering.Type
open import Lib.Ordering.Instances.DecidableEquality

open import Lib.Class.Eq

instance
  EqOrdering : Eq Ordering
  EqOrdering = EqInstance
