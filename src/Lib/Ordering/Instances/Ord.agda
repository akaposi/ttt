{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Ordering.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Ordering.Instances.Eq
open import Lib.Ordering.Type

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Dec.Type

open import Lib.Unit.Type

open import Lib.Empty.Type

open import Lib.Sum.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  OrdOrdering : Ord Ordering
  eq OrdOrdering = EqOrdering

  (OrdOrdering ≤ LT) b = ⊤
  (OrdOrdering ≤ EQ) LT = ⊥
  (OrdOrdering ≤ EQ) EQ = ⊤
  (OrdOrdering ≤ EQ) GT = ⊤
  (OrdOrdering ≤ GT) LT = ⊥
  (OrdOrdering ≤ GT) EQ = ⊥
  (OrdOrdering ≤ GT) GT = ⊤

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {LT} = tt
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} = tt
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {GT} = tt

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {LT} {b} {c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} {LT} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} {EQ} {c} = λ _ x → x
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} {GT} {LT} = λ _ x → x
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} {GT} {EQ} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {EQ} {GT} {GT} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {GT} {LT} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {GT} {EQ} {c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdOrdering)))) {GT} {GT} {c} = λ _ x → x

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {LT} {LT} = λ _ _ → refl
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {LT} {EQ} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {LT} {GT} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {EQ} {LT} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {EQ} {EQ} = λ _ _ → refl
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {EQ} {GT} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {GT} {LT} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {GT} {EQ} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdOrdering))) {GT} {GT} = λ _ _ → refl

  connected (totalOrder (decidableTotalOrder OrdOrdering)) {LT} {_}  = inl tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {EQ} {LT} = inr tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {EQ} {EQ} = inl tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {EQ} {GT} = inl tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {GT} {LT} = inr tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {GT} {EQ} = inr tt
  connected (totalOrder (decidableTotalOrder OrdOrdering)) {GT} {GT} = inl tt

  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) LT b = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) EQ LT = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) EQ EQ = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) EQ GT = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) GT LT = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) GT EQ = no λ ()
  Decidable₂.decide (decidable (decidableTotalOrder OrdOrdering)) GT GT = yes tt
