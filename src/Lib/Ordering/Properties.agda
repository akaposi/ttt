{-# OPTIONS --safe --without-K #-}

module Lib.Ordering.Properties where

open import Lib.Ordering.Type

open import Lib.Dec.Type

open import Lib.Equality.Type

open import Lib.Sum.Type

LT≢GT : LT ≢ GT
LT≢GT ()

GT≢LT : GT ≢ LT
GT≢LT ()

EQ≢GT : EQ ≢ GT
EQ≢GT ()

EQ≢LT : EQ ≢ LT
EQ≢LT ()

GT≢EQ : GT ≢ EQ
GT≢EQ ()

LT≢EQ : LT ≢ EQ
LT≢EQ ()

infix 4 _≟_
_≟_ : (a b : Ordering) → Dec (a ≡ b)
LT ≟ LT = yes refl
LT ≟ EQ = no (λ ())
LT ≟ GT = no (λ ())
EQ ≟ LT = no (λ ())
EQ ≟ EQ = yes refl
EQ ≟ GT = no (λ ())
GT ≟ LT = no (λ ())
GT ≟ EQ = no (λ ())
GT ≟ GT = yes refl
