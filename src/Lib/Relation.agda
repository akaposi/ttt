{-# OPTIONS --safe --without-K #-}

module Lib.Relation where

open import Lib.Relation.Definitions public
open import Lib.Relation.Consequences public
open import Lib.Relation.Structures public

-- open import Lib.Equality.Base
-- open import Lib.Equality.Type
-- open import Lib.Relation.Notation (_≡_) (trans) (refl) public
