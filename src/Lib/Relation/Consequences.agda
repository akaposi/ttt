{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Relation.Consequences where

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Sum.Base

open import Lib.Dec.Type

open import Lib.Function.Base

open import Lib.Equality.Type

open import Lib.Relation.Definitions

sym⇒¬-sym : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Symmetric R → Symmetric (λ x y → ¬ R x y)
sym⇒¬-sym sym≁ x≁y y∼x = x≁y (sym≁ y∼x)

cotrans⇒¬-trans : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Cotransitive R → Transitive (λ x y → ¬ R x y)
cotrans⇒¬-trans cotrans ¬Rxz ¬Rzy Rxy = case (cotrans Rxy) ¬Rxz ¬Rzy

irrefl⇒¬-refl : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Irreflexive R → Reflexive (λ x y → ¬ R x y)
irrefl⇒¬-refl {A = A} irr re = irr re

trans∧irr⇒asym : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Transitive R →
                 Irreflexive R → Asymmetric R
trans∧irr⇒asym trans irrefl x<y y<x = irrefl (trans x<y y<x)

irr∧antisym⇒asym : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Irreflexive R → AntiSymmetric R → Asymmetric R
irr∧antisym⇒asym irrefl antisym x<y y<x with antisym x<y y<x
... | refl = irrefl x<y

asym⇒antisym : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Asymmetric R → AntiSymmetric R
asym⇒antisym asym x<y y<x = exfalso (asym x<y y<x)

asym⇒irr : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Asymmetric R → Irreflexive R
asym⇒irr asym {x} Rxx = asym Rxx Rxx

open Decidable₂

antisym∧scon⇒dec : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → AntiSymmetric R → StronglyConnected R → DecidableEquality A → Decidable₂ R
decide (antisym∧scon⇒dec antisym scon decEqA) x y with decide decEqA x y
... | yes refl = yes (reduceIdem (scon {x} {x}))
... | no ¬a    = case (scon {x} {y}) yes λ Ryx → no λ Rxy → ¬a (antisym Rxy Ryx)

tri⇒asym : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Trichotomous R → Asymmetric R
tri⇒asym tri {x} {y} x<y x>y with tri x y
... | tri< _   _ x≯y = x≯y x>y
... | tri≈ _   _ x≯y = x≯y x>y
... | tri> x≮y _ _   = x≮y x<y

tri⇒irr : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Trichotomous R → Irreflexive R
tri⇒irr {R = R} compare {x} Rxx = asym⇒irr {R = R} (tri⇒asym compare) Rxx

tri⇒dec : ∀{ℓ κ}{A : Set ℓ}{R : A → A → Set κ} → Trichotomous R → Decidable₂ R
decide (tri⇒dec compare) x y with compare x y
... | tri< x<y _ _ = yes x<y
... | tri≈ x≮y _ _ = no  x≮y
... | tri> x≮y _ _ = no  x≮y

-- Without Loss of Generality
wlog : ∀{ℓ κ ν}{A : Set ℓ}{R : A → A → Set κ}{Q : A → A → Set ν} → StronglyConnected R → Symmetric Q →
       (∀ a b → R a b → Q a b) → ∀ a b → Q a b
wlog r-connected q-sym prf a b = case (r-connected {a} {b}) (prf a b) λ Rba → q-sym (prf b a Rba)
