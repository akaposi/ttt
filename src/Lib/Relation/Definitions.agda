{-# OPTIONS --safe --without-K #-}

module Lib.Relation.Definitions where

open import Agda.Primitive

open import Lib.Empty.Base

open import Lib.Sum.Type

open import Lib.Sigma.Type

open import Lib.Equality.Type

open import Lib.Function.Base

Reflexive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Reflexive R = ∀{a} → R a a

Irreflexive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Irreflexive R = ∀{a} → ¬ (R a a)

Coreflexive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Coreflexive R = ∀{x y} → R x y → x ≡ y

QuasiReflexive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
QuasiReflexive R = ∀{x y} → R x y → R x x × R y y

Symmetric : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Symmetric R = ∀{a b} → R a b → R b a

AntiSymmetric : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
AntiSymmetric R = ∀{a b} → R a b → R b a → a ≡ b

Asymmetric : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Asymmetric R = ∀{a b} → R a b → ¬ (R b a)

Transitive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Transitive R = ∀{a b c} → R a b → R b c → R a c

AntiTransitive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
AntiTransitive R = ∀{a b c} → R a b → R b c → ¬ R a c

Cotransitive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Cotransitive R = ∀{x y z} → R x z → R x y ⊎ R y z

QuasiTransitive : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
QuasiTransitive R = ∀{x y z} → R x y → R y z → ¬ R y x → ¬ R z y → R x z × ¬ R z x

Substitutive : ∀{ℓ ℓ₁}{A : Set ℓ} → (A → A → Set ℓ₁) → (ℓ₂ : Level) → Set (ℓ ⊔ ℓ₁ ⊔ lsuc ℓ₂)
Substitutive {A = A} R p = ∀{x y}(P : A → Set p) → R x y → P x → P y

Connected : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Connected R = ∀{a b} → R a b ⊎ R b a ⊎ a ≡ b

StronglyConnected : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
StronglyConnected R = ∀{a b} → R a b ⊎ R b a

Universal : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Universal R = ∀{x y} → R x y

infix 4 _⊕_
data _⊕_ {a}{b}(A : Set a) (B : Set b) : Set (a ⊔ b) where
  left  : ( a :   A)(¬b : ¬ B) → A ⊕ B
  right : (¬a : ¬ A)( b :   B) → A ⊕ B

Dense : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Dense R = ∀{x y} → R x y → ∃[ z ] R x z × R z y

Dichotomous : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Dichotomous R = ∀{a b} → R a b ⊕ R b a

data Tri {a}{b}{c}(A : Set a) (B : Set b) (C : Set c) : Set (a ⊔ b ⊔ c) where
  tri< : ( a :   A)(¬b : ¬ B)(¬c : ¬ C) → Tri A B C
  tri≈ : (¬a : ¬ A)( b :   B)(¬c : ¬ C) → Tri A B C
  tri> : (¬a : ¬ A)(¬b : ¬ B)( c :   C) → Tri A B C

Trichotomous : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Trichotomous _<_ = ∀ x y → Tri (x < y) (x ≡ y) (x > y)
  where _>_ = flip _<_

Empty : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → Set (ℓ ⊔ κ)
Empty R = ∀{x y} → ¬ (R x y)

Maximum : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → A → Set (ℓ ⊔ κ)
Maximum R T = ∀{x} → R x T

Minimum : ∀{ℓ κ}{A : Set ℓ} → (A → A → Set κ) → A → Set (ℓ ⊔ κ)
Minimum R = Maximum (flip R)

LeftUnique : ∀{ℓ ℓ₂ κ}{A : Set ℓ}{B : Set ℓ₂} → (A → B → Set κ) → Set (ℓ ⊔ ℓ₂ ⊔ κ)
LeftUnique R = ∀{x y z} → R x y → R z y → x ≡ z

RightUnique : ∀{ℓ ℓ₂ κ}{A : Set ℓ}{B : Set ℓ₂} → (A → B → Set κ) → Set (ℓ ⊔ ℓ₂ ⊔ κ)
RightUnique R = ∀{x y z} → R x y → R x z → y ≡ z

Univalent : ∀{ℓ ℓ₂ κ}{A : Set ℓ}{B : Set ℓ₂} → (A → B → Set κ) → Set (ℓ ⊔ ℓ₂ ⊔ κ)
Univalent = RightUnique

Total : ∀{ℓ ℓ₂ κ}{A : Set ℓ}{B : Set ℓ₂} → (A → B → Set κ) → Set (ℓ ⊔ ℓ₂ ⊔ κ)
Total R = ∀ x → ∃[ y ] R x y

Surjective : ∀{ℓ ℓ₂ κ}{A : Set ℓ}{B : Set ℓ₂} → (A → B → Set κ) → Set (ℓ ⊔ ℓ₂ ⊔ κ)
Surjective R = ∀ y → ∃[ x ] R x y

DownwardClosed : ∀{a r}{A : Set a} → (A → A → Set r) → ∀ {ℓ} → (A → Set ℓ) → A → Set _
DownwardClosed _<_ P x = ∀ {y} → y < x → P y

record Accessible {a}{ℓ}{A : Set a} (_<_ : A → A → Set ℓ) (x : A) : Set (a ⊔ ℓ) where
  inductive
  pattern
  constructor access
  field
    accessDown : DownwardClosed _<_ (Accessible _<_) x

open Accessible public

WellFounded : ∀{a ℓ}{A : Set a} → (A → A → Set ℓ) → Set _
WellFounded _<_ = ∀ x → Accessible _<_ x
