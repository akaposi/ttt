{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.Relation.Structures where

open import Lib.Level

open import Lib.Relation.Definitions
open import Lib.Relation.Consequences

open import Lib.Dec.Type

------------------
-- Equivalences --
------------------

record PartialEquivalence {ℓ κ}{A : Set ℓ}(R : A → A → Set κ) : Set (ℓ ⊔ κ) where
  field
    transitive : Transitive R
    symmetric  : Symmetric R

record Equivalence {ℓ κ}{A : Set ℓ}(R : A → A → Set κ) : Set (ℓ ⊔ κ) where
  field
    ⦃ partialEquivalence ⦄ : PartialEquivalence R
    reflexive  : Reflexive R

  open PartialEquivalence partialEquivalence public

---------------
-- Preorders --
---------------

record Preorder {a}{ℓ}{A : Set a}(_≲_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    reflexive  : Reflexive _≲_
    transitive : Transitive _≲_

  module PreO where

    sym-equiv : Symmetric _≲_ → Equivalence _≲_
    sym-equiv sym = record { partialEquivalence = record { transitive = transitive ; symmetric = sym } ; reflexive = reflexive }

record TotalPreorder {a}{ℓ}{A : Set a}(_≲_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    ⦃ preorder ⦄ : Preorder _≲_
    connected    : StronglyConnected _≲_

  open Preorder preorder hiding (module PreO) public

--------------------
-- Partial orders --
--------------------

record PartialOrder {a}{ℓ}{A : Set a}(_≤_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    ⦃ preorder ⦄  : Preorder _≤_
    antisymmetric : AntiSymmetric _≤_

  open Preorder preorder hiding (module PreO) public

record DecidablePartialOrder {a}{ℓ}{A : Set a}(_≤_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  infix 4 decidable
  field
    ⦃ partialOrder ⦄   : PartialOrder _≤_
    instance decidable : Decidable₂ _≤_

  open PartialOrder partialOrder public

  infix 4 _≤?_

  _≤?_ = decide decidable

record StrictPartialOrder {a}{ℓ}{A : Set a}(_<_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    irreflexive : Irreflexive _<_
    transitive  : Transitive _<_

  module SPO where
    asymmetric : Asymmetric _<_
    asymmetric {x} {y} e1 sym-e1 = irreflexive (transitive e1 sym-e1)

record DecidableStrictPartialOrder {a}{ℓ}{A : Set a}(_<_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  infix 4 decidable
  field
    ⦃ strictPartialOrder ⦄ : StrictPartialOrder _<_
    instance decidable     : Decidable₂ _<_

  open StrictPartialOrder strictPartialOrder hiding (module SPO) public

  infix 4 _<?_
  _<?_ = decide decidable

------------------
-- Total orders --
------------------

record TotalOrder {a}{ℓ}{A : Set a}(_≤_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    ⦃ partialOrder ⦄ : PartialOrder _≤_
    connected        : StronglyConnected _≤_

  open PartialOrder partialOrder public

  module TO where
    instance
      totalPreorder : TotalPreorder _≤_
      totalPreorder = record
        { preorder  = preorder
        ; connected = connected
        }

record DecidableTotalOrder {a}{ℓ}{A : Set a}(_≤_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  infix 4 decidable
  field
    ⦃ totalOrder ⦄     : TotalOrder _≤_
    instance decidable : Decidable₂ _≤_

  open TotalOrder totalOrder hiding (module TO) public

  infix 4 _≤?_ _≥?_

  _≤?_ = decide decidable
  _≥?_ = λ x y → y ≤? x

  module DPO where
    instance
      decidablePartialOrder : DecidablePartialOrder _≤_
      decidablePartialOrder = record
        { partialOrder = partialOrder
        ; decidable    = decidable
        }


record StrictTotalOrder {a}{ℓ}{A : Set a}(_<_ : A → A → Set ℓ) : Set (a ⊔ ℓ) where
  field
    transitive   : Transitive _<_
    trichotomous : Trichotomous _<_

  module STO where

    instance
      decidable : Decidable₂ _<_
      decidable = tri⇒dec trichotomous


    instance
      strictPartialOrder : StrictPartialOrder _<_
      strictPartialOrder = record
        { irreflexive = tri⇒irr trichotomous
        ; transitive = transitive
        }

      decidableStrictPartialOrder : DecidableStrictPartialOrder _<_
      decidableStrictPartialOrder = record
        { strictPartialOrder = strictPartialOrder
        ; decidable          = decidable
        }

  infix 4 _<?_
  _<?_ = decide STO.decidable

{-
-----------------------------
-- open structures
-----------------------------

open PartialEquivalence {{...}} public
open Equivalence {{...}} public

open Preorder {{...}} public
open TotalPreorder {{...}} public

open PartialOrder {{...}} public
open DecidablePartialOrder {{...}} public
open StrictPartialOrder {{...}} public
open DecidableStrictPartialOrder {{...}} public

open TotalOrder {{...}} public
open DecidableTotalOrder {{...}} public
open StrictTotalOrder {{...}} public
-}
