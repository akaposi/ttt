{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Sigma.Instances.Eq where

open import Lib.Sigma.Type
open import Lib.Sigma.Instances.DecidableEquality

open import Lib.Class.Eq

open import Lib.Dec.Type

instance
  {-
  Eq× : ∀{i j}{A : Set i}{B : Set j} → ⦃ Eq A ⦄ → ⦃ Eq B ⦄ → Eq (A × B)
  Eq× = EqInstance ⦃ DecEq× ⦄
  -}
  EqΣ : ∀{i j}{A : Set i}{B : A → Set j}
      → ⦃ Eq A ⦄
      → ⦃ ∀{a} → Eq (B a) ⦄
      → Eq (Σ A B)
  EqΣ ⦃ decEq ⦄ ⦃ decEqBa ⦄ = EqInstance ⦃ DecEqΣ ⦃ decEqA ⦃ decEq ⦄ ⦄ ⦃ decEqA ⦃ decEqBa ⦄ ⦄ ⦄
