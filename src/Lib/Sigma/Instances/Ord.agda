{-# OPTIONS --safe --without-K #-}

module Lib.Sigma.Instances.Ord where

open import Agda.Primitive

open import Lib.Sigma.Type
open import Lib.Sigma.Instances.Eq

open import Lib.Class.Eq
open import Lib.Class.Ord
open Ord

open import Lib.Equality.Base

open import Lib.Empty.Base

open import Lib.Maybe.Type

open import Lib.Dec.Type

open import Lib.Sum.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  Ord× : ∀{i j}{A : Set i}{B : Set j} → ⦃ Ord A ⦄ → ⦃ Ord B ⦄ → Ord (A × B)
  eq (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄) = EqΣ ⦃ eq ordA ⦄ ⦃ eq ordB ⦄

  _≤_ (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄) (a1 , b1) (a2 , b2) with a1 ≟ a2
  ... | yes _ = (ordB ≤ b1) b2
  ... | no  _ = (ordA ≤ a1) a2

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄))))) {a , b} with a ≟ a
  ... | yes _ = reflexive (decidableTotalOrder ordB)
  ... | no  _ = reflexive (decidableTotalOrder ordA)

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄))))) {a1 , b1} {a2 , b2} {a3 , b3} e1 e2 with a1 ≟ a2 | a2 ≟ a3 | a1 ≟ a3
  ... | yes _    | yes _    | yes _    = transitive (decidableTotalOrder ordB) e1 e2
  ... | yes a1a2 | yes a2a3 | no ¬a    = exfalso (¬a (a1a2 ◾ a2a3))
  ... | yes a1a2 | no  ¬a   | yes a1a3 = exfalso (¬a (sym a1a2 ◾ a1a3))
  ... | yes a1a2 | no _     | no _     = subst (λ x → (ordA ≤ x) a3) (sym a1a2) e2
  ... | no ¬a    | yes a2a3 | yes a1a3 = exfalso (¬a (a1a3 ◾ sym a2a3))
  ... | no _     | yes a2a3 | no _     = subst (ordA ≤ a1) a2a3 e1 
  ... | no _     | no ¬a    | yes a1a3 = exfalso (¬a (antisymmetric (decidableTotalOrder ordA) e2 (subst (λ x → (ordA ≤ x) a2) a1a3 e1)))
  ... | no _     | no _     | no _     = transitive (decidableTotalOrder ordA) e1 e2

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄)))) {a1 , b1} {a2 , b2} e1 e2 with a1 ≟ a2 | a2 ≟ a1
  ... | yes a | yes _ = cong₂ _,'_ a (antisymmetric (decidableTotalOrder ordB) e1 e2)
  ... | yes a | no ¬a = exfalso (¬a (sym a))
  ... | no ¬a | yes a = exfalso (¬a (sym a))
  ... | no ¬a | no _  = exfalso (¬a (antisymmetric (decidableTotalOrder ordA) e1 e2))

  connected (totalOrder (decidableTotalOrder (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄))) {a1 , b1} {a2 , b2} with a1 ≟ a2 | a2 ≟ a1
  ... | yes _ | yes _ = connected (decidableTotalOrder ordB)
  ... | yes a | no ¬a = exfalso (¬a (sym a))
  ... | no ¬a | yes a = exfalso (¬a (sym a))
  ... | no _  | no _  = connected (decidableTotalOrder ordA)

  Decidable₂.decide (decidable (decidableTotalOrder (Ord× ⦃ ordA ⦄ ⦃ ordB ⦄))) (a1 , b1) (a2 , b2) with a1 ≟ a2
  ... | yes a = (decidableTotalOrder ordB ≤? b1) b2
  ... | no  _ = (decidableTotalOrder ordA ≤? a1) a2
