{-# OPTIONS --safe --without-K #-}

module Lib.String where

open import Lib.String.Type public
open import Lib.String.Base public
open import Lib.String.Instances public
