{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Base where

open import Lib.Bool.Type

open import Lib.Maybe.Type

open import Lib.Sigma.Type

open import Lib.Char.Type

open import Lib.String.Type
open import Agda.Builtin.String hiding (String)

{-
  primShowChar       : Char → String
  primShowString     : String → String
  primShowNat        : Nat → String
-}

uncons : String → Maybe (Char × String)
uncons = primStringUncons
{-# DISPLAY primStringUncons = uncons #-}

module Char where
  open import Agda.Builtin.String.Properties

  open import Lib.Containers.List.Type

  open import Lib.Equality.Type

  open import Lib.Char.Base

  convert : String → List Char
  convert = primStringToList
  {-# DISPLAY primStringToList = convert #-}

  convert-injective : ∀{a b} → convert a ≡ convert b → a ≡ b
  convert-injective {a} {b} = primStringToListInjective a b
  {-# DISPLAY primStringToListInjective a b = convert-injective {a} {b} #-}

infixr 5 _++_
_++_ : String → String → String
_++_ = primStringAppend
{-# DISPLAY primStringAppend = _++_ #-}

infix 4 _==_
_==_ : String → String → Bool
_==_ = primStringEquality
{-# DISPLAY primStringEquality = _==_ #-}
