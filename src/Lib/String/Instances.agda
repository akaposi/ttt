{-# OPTIONS --safe --without-K --exact-split #-}

module Lib.String.Instances where

open import Lib.String.Instances.DecidableEquality public
open import Lib.String.Instances.Eq public
open import Lib.String.Instances.Ord public
open import Lib.String.Instances.IsSet public
