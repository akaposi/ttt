{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Instances.DecidableEquality where

open import Lib.String.Type
open import Lib.String.Base

open import Lib.Char.Instances.DecidableEquality

open import Lib.Containers.List.Instances.DecidableEquality

open import Lib.Equality.Base

open import Lib.Dec.Type
open Decidable₂

instance
  DecEqString : DecidableEquality String
  decide DecEqString xs ys with decide (DecEqList ⦃ DecEqChar ⦄) (Char.convert xs) (Char.convert ys)
  ... | yes a = yes (Char.convert-injective a)
  ... | no ¬a = no λ e → ¬a (cong Char.convert e)
