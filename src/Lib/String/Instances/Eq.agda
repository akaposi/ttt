{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Instances.Eq where

open import Lib.Class.Eq

open import Lib.String.Type
open import Lib.String.Instances.DecidableEquality

instance
  EqString : Eq String
  EqString = EqInstance
