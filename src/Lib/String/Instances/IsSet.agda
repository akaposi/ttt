{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Instances.IsSet where

open import Lib.Class.IsSet

open import Lib.Dec.InstanceGenerators.IsSet

open import Lib.String.Type
open import Lib.String.Instances.DecidableEquality

instance
  IsSetString : IsSet String
  IsSetString = DecidableEquality→IsSet DecEqString
