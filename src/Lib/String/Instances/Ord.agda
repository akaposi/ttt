{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.String.Type
open import Lib.String.Base
open import Lib.String.Instances.Eq

open import Lib.Char.Instances.Ord

open import Lib.Containers.List.Instances.Ord

open import Lib.Sum.Type

open import Lib.Dec.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder
open Decidable₂

instance
  OrdString : Ord String
  eq OrdString = EqString

  _≤_ OrdString xs ys = _≤_ (OrdList ⦃ OrdChar ⦄) (Char.convert xs) (Char.convert ys)

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdString)))) {xs} = reflexive (decidableTotalOrder OrdList) {Char.convert xs}

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder OrdString)))) {xs} = transitive (decidableTotalOrder OrdList) {Char.convert xs}

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder OrdString))) e1 e2 = Char.convert-injective (antisymmetric (decidableTotalOrder OrdList) e1 e2)

  connected (totalOrder (decidableTotalOrder OrdString)) {xs} = connected (decidableTotalOrder OrdList) {Char.convert xs}

  decide (decidable (decidableTotalOrder OrdString)) xs ys = (decidableTotalOrder OrdList ≤? Char.convert xs) (Char.convert ys)
