{-# OPTIONS --safe --without-K --exact-split --no-postfix-projection #-}

module Lib.String.Type where

open import Agda.Builtin.String using (String) public
