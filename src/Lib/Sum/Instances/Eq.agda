{-# OPTIONS --safe --without-K #-}

module Lib.Sum.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Sum.Type
open import Lib.Sum.Instances.DecidableEquality

instance
  Eq⊎ : ∀{i j}{A : Set i}{B : Set j} → ⦃ Eq A ⦄ → ⦃ Eq B ⦄ → Eq (A ⊎ B)
  Eq⊎ = EqInstance
