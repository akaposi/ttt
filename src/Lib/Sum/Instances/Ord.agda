{-# OPTIONS --safe --without-K --no-postfix-projection #-}

module Lib.Sum.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

open import Lib.Sum.Type
open import Lib.Sum.Instances.Eq

open import Agda.Primitive

open import Lib.Dec.Type

open import Lib.Sigma.Type

open import Lib.Maybe.Type
open import Lib.Maybe.Base

open import Lib.Equality.Type
open import Lib.Equality.Base

open import Lib.Unit.Type

open import Lib.Empty.Type
open import Lib.Empty.Base

open import Lib.Ordering.Type

instance
  Ord⊎ : ∀{i j}{A : Set i}{B : Set j} → ⦃ Ord A ⦄ → ⦃ Ord B ⦄ → Ord (A ⊎ B)
  eq (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄) = Eq⊎ ⦃ eq ordA ⦄ ⦃ eq ordB ⦄

  ((Ord⊎ ⦃ ordA ⦄) ≤ inl a) (inl b) = (ordA ≤ a) b
  (Ord⊎ ≤ inl a) (inr b) = ⊤
  (Ord⊎ ≤ inr b) (inl a) = ⊥
  ((Ord⊎ ⦃ _ ⦄ ⦃ ordB ⦄) ≤ inr a) (inr b) = (ordB ≤ a) b

  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄))))) {inl a} = reflexive (decidableTotalOrder ordA)
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ _ ⦄ ⦃ ordB ⦄))))) {inr b} = reflexive (decidableTotalOrder ordB)

  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inl a} {inl b} {inl c} = transitive (decidableTotalOrder ordA)
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inl a} {inl b} {inr c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inl a} {inr b} {inl c} = λ _ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inl a} {inr b} {inr c} = λ _ _ → tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inr a} {inl b} {inl c} = λ x _ → x
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inr a} {inl b} {inr c} = λ ()
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inr a} {inr b} {inl c} = λ _ x → x
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))))) {inr a} {inr b} {inr c} = transitive (decidableTotalOrder ordB)

  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄)))) {inl a} {inl b} = λ a≤b b≤a → cong inl (antisymmetric (decidableTotalOrder ordA) a≤b b≤a)
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄)))) {inl a} {inr b} = λ _ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄)))) {inr a} {inl b} = λ ()
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄)))) {inr a} {inr b} = λ a≤b b≤a → cong inr (antisymmetric (decidableTotalOrder ordB) a≤b b≤a)

  connected (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) {inl a} {inl b} = connected (decidableTotalOrder ordA)
  connected (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) {inl a} {inr b} = inl tt
  connected (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) {inr a} {inl b} = inr tt
  connected (totalOrder (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) {inr a} {inr b} = connected (decidableTotalOrder ordB)

  Decidable₂.decide (decidable (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) (inl a) (inl b) = (decidableTotalOrder ordA ≤? a) b
  Decidable₂.decide (decidable (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) (inl a) (inr b) = yes tt
  Decidable₂.decide (decidable (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) (inr a) (inl b) = no (λ ())
  Decidable₂.decide (decidable (decidableTotalOrder (Ord⊎ ⦃ ordA ⦄ ⦃ ordB ⦄))) (inr a) (inr b) = (decidableTotalOrder ordB ≤? a) b
