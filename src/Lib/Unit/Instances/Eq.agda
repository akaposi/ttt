{-# OPTIONS --safe --without-K #-}

module Lib.Unit.Instances.Eq where

open import Lib.Class.Eq

open import Lib.Unit.Type
open import Lib.Unit.Instances.DecidableEquality

instance
  Eq⊤ : Eq ⊤
  Eq⊤ = EqInstance
