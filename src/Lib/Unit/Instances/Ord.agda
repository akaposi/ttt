{-# OPTIONS --safe --without-K #-}

module Lib.Unit.Instances.Ord where

open import Lib.Class.Ord
open Ord

open import Lib.Unit.Type
open import Lib.Unit.Instances.Eq

open import Lib.Sum.Type

open import Lib.Dec.Type

open import Lib.Equality.Type

open import Lib.Relation.Structures
open DecidableTotalOrder
open TotalOrder
open PartialOrder
open Preorder

instance
  Ord⊤ : Ord ⊤
  eq Ord⊤ = Eq⊤
  _≤_ Ord⊤ _ _ = ⊤
  reflexive (preorder (partialOrder (totalOrder (decidableTotalOrder Ord⊤)))) = tt
  transitive (preorder (partialOrder (totalOrder (decidableTotalOrder Ord⊤)))) = λ _ _ → tt
  antisymmetric (partialOrder (totalOrder (decidableTotalOrder Ord⊤))) = λ _ _ → refl
  connected (totalOrder (decidableTotalOrder Ord⊤)) = inl tt
  decidable (decidableTotalOrder Ord⊤) = DecProof (λ _ _ → yes tt)
