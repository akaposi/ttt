-- BEGIN FIX
{-# OPTIONS --guardedness --safe #-}

open import Lib.Class
open import Lib.Level
open import Lib.Function

import Lib.Unit as ⊤
open ⊤
  renaming ( _≟_ to _≟⊤_ )

import Lib.Empty as ⊥
open ⊥
  renaming ( _≟_ to _≟⊥_ )

import Lib.Nat as ℕ
open ℕ
  renaming ( _≟_ to _≟ℕ_
           ; case to case-ℕ
           ; case-proof to case-ℕ-proof
           ; ite to ite-ℕ
           ; rec to rec-ℕ
           ; elim to elim-ℕ
           )

import Lib.Equality as ≡
open ≡

import Lib.Sum as ⊎
open ⊎
  renaming ( map to map-⊎
           ; mapᵣ to mapᵣ-⊎
           ; mapₗ to mapₗ-⊎
           ; elim to elim-⊎
           ; ind to ind-⊎
           ; swap to swap-⊎
           )

import Lib.Fin as Fin
open Fin
  renaming ( cast to cast-Fin
           ; elim to elim-Fin
           )

import Lib.Sigma as Σ
open Σ
  renaming ( map to map-Σ
           ; swap to swap-×
           )

import Lib.Bool as Bool
open Bool
  renaming ( contradiction to contradictionᵇ
           ; contraposition to contrapositionᵇ
           ; _≟_ to _≟ᵇ_
           ; elim to elim-Bool
           ; ind to ind-Bool
           )

import Lib.CoNat as ℕ∞
open ℕ∞
  renaming ( coite to coite-ℕ∞
           ; _+_ to _+∞_
           ; add to add∞
           ; add' to add∞'
           ; _*_ to _*∞_
           ; idr+ to idr+∞
           ; idl+ to idl+∞
           ; _^_ to _^∞_
           ; embed to embed-ℕ∞
           )

open import Lib.Dec

import Lib.Maybe as Maybe
open Maybe
  renaming ( elim to elim-Maybe
           ; ite to ite-Maybe
           ; ind to ind-Maybe
           ; map to map-Maybe
           )

open import Lib.Relation

open import Lib.Containers.List.Type hiding (module List)
import Lib.Containers.List as List

open import Lib.Containers.Stream.Type hiding (module Stream)
import Lib.Containers.Stream as Stream

open import Lib.Containers.Vec.Type hiding (module Vec)
import Lib.Containers.Vec as Vec

_≤_ : ℕ → ℕ → Set
n ≤ k = Σ ℕ λ m → m + n ≡ k
infix 5 _≤_
-- END FIX

-- BEGIN FIX
-- b1 and b2 should be such that b1 ℕ 1 2 ≠ b2 ℕ 1 2
b1 b2 : (A : Set) → A → A → A
-- END FIX
b1 A a b = a
b2 A a b = b
-- BEGIN FIX
test-b1-b2 : ¬ (b1 ℕ 1 2 ≡ b2 ℕ 1 2)
test-b1-b2 ()
-- END FIX

-- BEGIN FIX
weirdLogicalEquiv : (A B C : Set) → (B → A → (⊥ ⊎ C)) ↔ (A → (B → C × A))
-- END FIX
weirdLogicalEquiv A B C = (λ f a b → case (f b a) exfalso (λ c → c) , a) , (λ f b a → inr (fst (f a b)))

-- BEGIN FIX
cocΣ : (A : Set)(B : A → Set) → Σ A B ↔ ((C : Set) → ((a : A) → B a → C) → C)
-- END FIX
fst (cocΣ A B) (a , ba) C f = f a ba
snd (cocΣ A B) f = f (Σ A B) (λ a ba → a , ba)

-- BEGIN FIX
prop : {P : Set} → P ⊎ ¬ P → (¬ ( ¬ P) → P)
-- END FIX
prop = λ x x₁ → case x (λ z → z) λ x₂ → exfalso (x₁ x₂)

-- BEGIN FIX
ref≤ : Reflexive _≤_
-- END FIX
ref≤ = zero , refl

-- BEGIN FIX
cong⁻¹ : {A B : Set}(a b : A)(f : A → B) → ¬ (f a ≡ f b) → ¬ (a ≡ b)
-- END FIX
cong⁻¹ a b f ¬f a≡b = ¬f (cong f a≡b)

-- BEGIN FIX
a+b=0→a=0 : (a b : ℕ) → (a + b) ≡ 0 → a ≡ 0
-- END FIX
a+b=0→a=0 zero b e = refl
a+b=0→a=0 (suc a) b ()

-- BEGIN FIX
noℕsqrt : ¬ ((n k : ℕ) → Σ ℕ λ m → m * m ≡ n * k)
-- END FIX
noℕsqrt x with x 1 2
... | suc (suc zero) , ()
... | suc (suc (suc m)) , pr with cong (λ x → pred' (pred' x)) pr
... | ()

-- BEGIN FIX
¬¬∃↓ : ¬ ((f : ℕ → ℕ) → Σ ℕ λ n → (k : ℕ) → suc (f n) ≤ (f k))
-- END FIX
¬¬∃↓ x with x (λ _ → 0)
... | num , proof with proof 0
... | zero , ()
... | suc num₂ , ()

-- BEGIN FIX
-- works like haskell's zip
zipStream : {A B : Set} → Stream A → Stream B → Stream (A × B)
-- END FIX
head (zipStream sa sb) = (head sa) , (head sb)
tail (zipStream sa sb) = zipStream (tail sa) (tail sb)
-- BEGIN FIX
test-s1 : Stream.Vec.take 10 (zipStream (Stream.iterate suc 0) (Stream.iterate pred' 100)) ≡
  (0 , 100) ∷ (1 , 99) ∷ (2 , 98) ∷
  (3 , 97)  ∷ (4 , 96) ∷ (5 , 95) ∷
  (6 , 94)  ∷ (7 , 93) ∷ (8 , 92) ∷
  (9 , 91) ∷ []
test-s1 = refl
test-s2 : Stream.Vec.take 10 (Stream.map (λ (a , b) → a + b) (zipStream (Stream.iterate (λ x → suc (suc x)) 0) (Stream.iterate pred' 100))) ≡
  100 ∷ 101 ∷ 102 ∷ 103 ∷ 104 ∷ 105 ∷ 106 ∷ 107 ∷ 108 ∷ 109 ∷ []
test-s2 = refl
-- END FIX

