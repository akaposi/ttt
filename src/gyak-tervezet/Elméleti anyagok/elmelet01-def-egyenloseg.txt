Definíció szerinti egyenlőség: ld. első gyakorlat anyaga!

----------------------------------------------
-- Beugró kifejtős feladat példa
----------------------------------------------

Normalizáld az alábbi kifejezést lépésenként! Egy átalakítás legyen egy lépés.
Átalakításnak minősül
- a definíció szerinti átírás (ekkor oda kell írni PONTOSAN, hogy mi alapján alakítottunk át egy kifejezést; melyik függvény melyik mintaillesztése pontosan)
- β-szabály használata (ekkor oda kell írni, hogy melyik típusnak használtuk fel a β-szabályát)
- η-szabály használata (ekkor oda kell írni, hogy melyik típusnak használtuk fel az η-szabályát)
- Ha több teljesen pontosan ugyanazon részkifejezéseket írnánk át pontosan ugyanarra pontosan ugyanazzal az egy szabállyal a fenti szabályok közül valamelyikkel,
  azt meg lehet tenni egy lépésként. (Hogy mit kell érteni ez alatt, arra a 2. példában lehet magyarázatot találni.)
Csak a végeredményre nem jár pont.

1. példa:

Adottak az alábbi definíciók:

and : Bool → Bool → Bool
and x true  = x
and x false = false

ct : Bool → Bool
ct true  = true
ct false = true

Normalizáld az alábbi kifejezést lépésenként!
λ x → ct (and x (and (ct false) true)) = -- Itt két lehetőség is van; bármelyik irányba el lehet indulni, a normalizálás a végén ugyanazt kell kihozza.

1.                                                                         2.
=⟨ ct false definíciója szerint ⟩                                          =⟨ and x true definíciója szerint (x helyére ct false-ot helyettesítve) ⟩
λ x → ct (and x (and true true))                                           λ x → ct (and x (ct false))
=⟨ and x true definíciója szerint (x helyére true-t helyettesítve) ⟩       =⟨ ct false definíciója szerint ⟩
λ x → ct (and x true)                                                      λ x → ct (and x true)
=⟨ and x true definíciója szerint ⟩                                        =⟨ and x true definíciója szerint ⟩
λ x → ct x -- ezen a ponton már maximum pont                               λ x → ct x -- ezen a ponton már maximum pont
=⟨ opcionálisan: függvény η-szabálya ⟩                                     =⟨ opcionálisan: függvény η-szabálya ⟩
ct                                                                         ct

FIGYELEM! A `ct x` kifejezés se nem `ct true`, se nem `ct false`, így a `ct x`-szel nem lehet mit csinálni, a normálformája önmaga.

2. példa:

Adottak az alábbi definíciók:

or true  = λ x → true
or false = λ x → x

cf : Bool → Bool
cf true  = false
cf false = false

Normalizáld az alábbi kifejezést lépésenként!
λ y → or (or (cf false) y) (cf false) =

1.
=⟨ cf false definíciója szerint ⟩ (Mindkettőt átírjuk a fent megfogalmazott negyedik szabály alapján.)
λ y → or (or false y) false
=⟨ or false definíciója szerint ⟩
λ y → or ((λ x → x) y) false
=⟨ függvény β-szabálya szerint ⟩
λ y → or (x[x↦y]) false = λ y → or y false
                        ^
                Itt nem szükséges magyarázni,
                elég cska a jobb oldalt felírni,
                a β-szabály része

or y se nem or true, se nem or false,
így ezt nem lehet tovább egyszerűsíteni,
ez a normálforma.

Gyakorlásért ld. házi feladatot!
