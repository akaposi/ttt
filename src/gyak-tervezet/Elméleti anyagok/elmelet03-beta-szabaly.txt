--------------------------------------------------------
-- β szabályok egyszerűen megfogalmazva
--------------------------------------------------------

Minden típushoz tartoznak konstruktor(ok) és destruktor(ok). Lásd: táblázat a tárgy honlapja alján.

Egy adott típus β-szabálya(i) azt határozzá(k) meg, hogy mit kell csinálni, ha egy konstruktorra alkalmazok egy destruktort.
Pl. függvények esetén (λ n → n + 2) 3, ebben a kifejezésben a λ a konstruktor, a függvényalkalmazás a destruktor; ekkor csak be kell helyettesíteni az értéket a megfelelő helyére,
majd ki kell számolni az értékét:

(λ n → n + 2) 3 =(függvény típus β szabálya)
(n+2)[n↦3] =(behelyettesítés)
3+2 =(általános iskola)
5

--------------------------------------------------------
-- típusok β szabályai
--------------------------------------------------------

Minden típusnak megadható a β szabálya a konstruktorok és destruktorok alapján.

β-szabályok azt mondják meg, hogy egy típus egy adott értékével mit kell csinálni, hogy megkülönböztessük a típus többi értékétől.

Egyszerűbb ezt az ötletet talán Bool-on szemléltetni:

data Bool : Set where
  false true : Bool

Hogyan lehet megkülönböztetni a false-ot a true-tól?
Kell egy függvény (destruktor), amely különböző Bool értékekre különböző eredményt ad szintaxis szerint, és CSAK és PONTOSAN a Bool értékeit kezeli,
tehát mivel a Bool-nak két értéke van, ezért a destruktornak PONTOSAN két elemet kell kezelnie, nem többet, nem kevesebbet.

Melyik ez a függvény a Bool-ok felett, ami false-ra, illetve true-ra egyértelműen két különböző dolgot ad eredményül? (Akár haskell-ből, akár más oop nyelvekből ismert konstrukció.)
Mi a destruktora?
Válasz:

Hány β-szabályra van szükség a Bool esetén?
Válasz:

Mik lesznek ezek a β-szabályok?
Válasz:
---------------------------------------------------------
Ha írunk egy 3 elemű típust (lényegében csak egy enumot):

data 𝟛 : Set where
  a1 a2 a3 : 𝟛

Mi lesz a 𝟛 típus destruktora?
Válasz:

Akkor ennek a típusnak mik lesznek a β-szabályai?
Válasz:
----
4 elemre:

data 𝟜 : Set where
  b1 b2 b3 b4 : 𝟜

Mi lesz a destruktora?
Válasz:

Mik lesznek ennek a β-szabályai?
Válasz:
----
Mi a ⊤ típus destruktora?
Válasz:

Mi lesz a ⊤ típus β-szabálya?
Válasz:
----
Mi a ⊥ destruktora?
Válasz:

Mi lesz a ⊥ típus β-szabálya?
Válasz:
----------------------------------------------------------
Mi történik abban az esetben, ha vannak a típusoknak paramétereik?

data Alma : Set where
  c1 : Alma
  c2 : Bool → Alma

Természetesen semmi különleges, pontosan ugyanaz fog a destruktorban szerepelni, mint a konstruktorok továbbra is.

Mi lesz a destruktora?
Válasz:

Mik lesznek a β-szabályai?
Válasz:
-----------------------------------------------------------
Mi történik, ha van legalább két paramétere egy konstruktornak?

Pl. rendezett pár: _,_ : A → B → A × B

Semmi, a destruktor továbbra is ugyanúgy generálható (ez természetesen nem azt jelenti, hogy csak az az egy jó van).
Mi lesz a rendezett párok egy destruktora?

Amelyik generálható az eddigiek alapján: uncurry : (A → B → C) → A × B → C

Más destruktorok is jók, pl. ezzel az eggyel ekvivalens az alábbi kettő együtt:
- fst : A × B → A
- snd : A × B → B

Ezek alapján mik a β-szabályok?
Az uncurry-vel csak egy szabály szükséges: uncurry f (a , b) ≡ f a b
Az fst, snd-vel kettő (hiszen két destruktor van egy konstruktorral, 2 ∙ 1 = 2): fst (a , b) ≡ a; snd (a , b) ≡ b
------------------------------------------------------------
data Körte : Set where
  d1 : Körte
  d2 : Bool → Körte
  d3 : Bool → 𝟛 → Körte

Mi lesz ezen típus destruktora?
Válasz:

És a β-szabályai?
Válasz:
