---------------------------------------------------------
-- típusok gyenge η-szabályai
---------------------------------------------------------

A gyenge η-szabály azt mondja meg, hogy mit tegyünk, ha destruktorra alkalmazunk konstruktort.
Pl. függvények esetén (λ a → f a) ≡ f, ahol a függvényalkalmazás a destruktor és a λ a konstruktor.

Természetesen más típusoknak is ugyanúgy van η-szabálya.

Vegyük példaként a ⊤-ot:
Destruktora: ite⊤ : A → ⊤ → A

Ez alapján az η-szabály az alábbi lesz:
ite⊤ tt x ≡ x

Ez természetesen Agdában bizonyítható is.

ite⊤ : ∀{i}{A : Set i} → A → ⊤ → A
ite⊤ x _ = x

⊤η : ∀{x} → ite⊤ tt x ≡ x
⊤η = refl

Ahogy emlékeztek rá, a ⊤ η-szabálya úgy néz ki, hogy ∀ a → a ≡ tt,
tehát itt is igaz lesz, hogy egy típusnak több egymással ekvivalens η-szabálya lehet.

Nézzük újra példaként a Bool típust. A β-szabályai a következők voltak:
if true then u else v ≡ u
if false then u else v ≡ v

Mi lehet az η-szabály? Hogy lehet "destruktorra alkalmazni konstruktort" ilyen esetben?
Az if_then_else_ esetén a "then" és az "else" ágban lévő dolgok tetszőleges értékek lehetnek;
ide akár konstruktort is be lehet írni. Tehát úgy lehet felépíteni az η-szabályokat, hogy a destruktor megfelelő
helyeire beírom az azonos típus konstruktorait.
Bool esetén ez azt jelenti, hogy az if_then_else_-ben a második és harmadik helyre kell a Bool két konstruktorát írni.
Ezen felül úgy kell beírni a két konstruktort, hogy alapvetően az "identitás" függvényt kapjuk az adott típuson.
Bool esetén tehát úgy kell az if_then_else_-et felparaméterezni, hogy a false-ra false legyen az eredmény, true-ra pedig true.

Ez alapján mi lesz a Bool-oknak egy lehetséges η-szabálya?
Válasz:

Ugyanezt az ismert 𝟛 típuson is el lehet játszani.
data 𝟛 : Set where
  a1 a2 a3 : 𝟛

Ismert a destruktor: ite𝟛 : A → A → A → 𝟛 → A

Mi lesz a 𝟛 η-szabálya?
Válasz:

Természetes számokon a helyzet szintén nem változik.
Ismert a destruktor: iteℕ : A → (A → A) → ℕ → A

Mi lesz ℕ η-szabálya?
Válasz:
