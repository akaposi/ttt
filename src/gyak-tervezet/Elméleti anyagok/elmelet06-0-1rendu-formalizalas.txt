---------------------------------------
-- Kapcsolat a logikával
---------------------------------------

Maguk a típusok egyben egy logikai állítást is elkódolnak.
(Ez a tény már korábban megfigyelhető volt, pl. pred-nél előfeltétel, hogy n ≠ 0, illetve minMax-nál adott volt az utófeltétel.)

Ezt a kapcsolatot szokás Curry-Howard izomorfizmusnak nevezni.

Elmélet:
  ∙ átalakítani logikai állításokat típusokra.
  ∙ formalizálni állításokat típusokkal.
  × = ∧ = konjunkció
  ⊎ = ∨ = diszjunkció
  ¬ = ¬ = negáció
  ⊃ = → = implikáció

Az állítások megfogalmazása lesz a függvény típusa, míg a függvény definíciója lesz az állítás bizonyítása.
Lényegében program = bizonyítás.

Ettől függetlenül a formalizálás nulladrendben nem tér semmilyen módon a többi tantárgy logikájától (dimat, logika, számításelmélet).

Példákért ld. 6-os és 7-es gyakfájlt!

----------------------------------
-- Elsőrendű formalizálás tippek
----------------------------------

Elsőrendben egy kicsit érdekesebb lesz a helyzet, mert a matek és a magyar nyelv nem igazán vannak jó barátságban,
így nagyon furcsa dolgok történnek formalizáláskor.

Mondatokat zártan formalizálunk, amely azt jelenti, hogy a formalizált mondatban, kifejezésben szabad változó nem szerepelhet!

"Vagy x vagy y" féle mondatok esetén nem kell kizárt vagyot formalizálni, elég a sima vagy.
"Vagy csak x vagy csak y" esetén kizárt vagyot kell formalizálni!

Ha egy csoport csak adott részhalmazáról szeretnénk állítást megfogalmazni, akkor figyelni kell, hogy
milyen kvantorhoz milyen művelet tartozik.

  ∙ A ∀ az implikációt szereti. Ha olyat szeretnénk formalizálni, hogy "Az a szám, amely 6-tal osztható, az osztható 3-mal is.", akkor azt úgy kell megtenni,
    hogy megadjuk az univerzumot, majd a predikátumokat (U = ℕ, |P(x)|ⁱ = x 6-tal osztható, |Q(x)|ⁱ = x 3-mal osztható), majd rájövünk, hogy ez egy olyan állítás,
    amely a számok egy bizonyos részhalmazáról szeretne valamit mondani, azon belül mindegyikről.
    Az állítást a következő módon lehet formalizálni: ∀x(P(x) ⊃ Q(x))

  ∙ A ∃ a konjunkciót szereti. "Van olyan szám, amely ha 3-mal osztható, akkor 6-tal is osztható." (Az univerzum és predikátumok ugyanazok, mint fentebb.)
    Intuitív módon az lenne a formalizáltja, hogy ∃x(Q(x) ⊃ P(x)), azonban ez helytelen. Például az 1-re az állítás igaz lesz, mert az 1 az nem osztható 3-mal,
    hamisból meg bármi következik, azonban nem ezt szeretnénk. Az állítás csoporton belüli létezést szeretne kifejezni,
    tehát a létező példát azon értékek közül szeretnénk választani, amelyekre teljesül a premissza, tehát 3-mal oszthatóak.
    A példákat csak a 3,6,9,12,15,18... számok közül szeretnék választani, ezért ezt helyesen úgy kell formalizálni, hogy ∃x(P(x) ∧ Q(x))

Egyértelműen létezik nincs direktben típuselméletben (se logikában), azonban megfogalmazható a már létező kvantorokkal, így ha olyan állítást kell formalizálni,
amely egyértelmű létezést emleget, akkor az egyértelmű létezést magát is formalizálni kell, nem jó a ∃!.
∃!xP(x) = ∃x(P(x) ∧ ∀y(P(y) ⊃ x = y)).
Magyarra fordítva az egyértelműség úgy szól, hogy "Létezik elem, amelyre az állítás teljesül és ha találok egy másikat, akkor az teljesen ugyanaz, mint amit az elején találtam."
