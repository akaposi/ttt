module gy06 where

open import Lib hiding (K)

----------------------------------------------
-- Some Sigma types
----------------------------------------------

Σ=⊎ : {A B : Set} → Σ Bool (if_then A else B) ↔ A ⊎ B
Σ=⊎ = {!!}

Σ=× : {A B : Set} → Σ A (λ _ → B) ↔ A × B
Σ=× = {!!}

-- Π A F is essentially (a : A) → F a
-- what does this mean?

                    -- Π A (λ _ → B)
Π=→ : {A B : Set} → ((a : A) → (λ _ → B) a) ↔ (A → B)
Π=→ = {!!}

                    -- Π Bool (if_then A else B)
Π=× : {A B : Set} → ((b : Bool) → if b then A else B) ↔ A × B
Π=× = {!!}

dependentCurry : {A : Set}{B : A → Set}{C : (a : A) → B a → Set} →
  ((a : A)(b : B a) → C a b) ↔ ((w : Σ A B) → C (fst w) (snd w))
dependentCurry = {!!}

∀×-distr  : {A : Set}{P : A → Set}{Q : A → Set} → ((a : A) → P a × Q a)
               ↔ ((a : A) → P a) × ((a : A) → Q a)
∀×-distr = {!!}

Bool=Fin2 : Bool ↔ Fin 2
Bool=Fin2 = {!!}

Bool=Fin2-test1 : snd Bool=Fin2 (fst Bool=Fin2 true) ≡ true
Bool=Fin2-test1 = refl
Bool=Fin2-test2 : snd Bool=Fin2 (fst Bool=Fin2 false) ≡ false
Bool=Fin2-test2 = refl
Bool=Fin2-test3 : fst Bool=Fin2 (snd Bool=Fin2 fzero) ≡ fzero
Bool=Fin2-test3 = refl
Bool=Fin2-test4 : fst Bool=Fin2 (snd Bool=Fin2 (fsuc fzero)) ≡ fsuc fzero
Bool=Fin2-test4 = refl

Fin1+3=Fin4 : Fin (1 + 3) ↔ Fin 1 ⊎ Fin 3
Fin1+3=Fin4 = {!!}

Fin1+3=Fin4-test1 : snd Fin1+3=Fin4 (fst Fin1+3=Fin4 fzero) ≡ fzero
Fin1+3=Fin4-test1 = refl
Fin1+3=Fin4-test2 : snd Fin1+3=Fin4 (fst Fin1+3=Fin4 (fsuc fzero)) ≡ fsuc fzero
Fin1+3=Fin4-test2 = refl
Fin1+3=Fin4-test3 : snd Fin1+3=Fin4 (fst Fin1+3=Fin4 (fsuc (fsuc fzero))) ≡ fsuc (fsuc fzero)
Fin1+3=Fin4-test3 = refl
Fin1+3=Fin4-test4 : snd Fin1+3=Fin4 (fst Fin1+3=Fin4 (fsuc (fsuc (fsuc fzero)))) ≡ fsuc (fsuc (fsuc fzero))
Fin1+3=Fin4-test4 = refl
Fin1+3=Fin4-test5 : fst Fin1+3=Fin4 (snd Fin1+3=Fin4 (inl fzero)) ≡ inl fzero
Fin1+3=Fin4-test5 = refl
Fin1+3=Fin4-test6 : fst Fin1+3=Fin4 (snd Fin1+3=Fin4 (inr fzero)) ≡ inr fzero
Fin1+3=Fin4-test6 = refl
Fin1+3=Fin4-test7 : fst Fin1+3=Fin4 (snd Fin1+3=Fin4 (inr (fsuc fzero))) ≡ inr (fsuc fzero)
Fin1+3=Fin4-test7 = refl
Fin1+3=Fin4-test8 : fst Fin1+3=Fin4 (snd Fin1+3=Fin4 (inr (fsuc (fsuc fzero)))) ≡ inr (fsuc (fsuc fzero))
Fin1+3=Fin4-test8 = refl

-- relating Fin m ⊎ Fin n and Fin (m + n)

inj₁f : {m n : ℕ} → Fin m → Fin (m + n)
inj₁f = {!!}

test-inj₁f : inj₁f {3}{4} (fsuc (fsuc fzero)) ≡ fsuc (fsuc fzero)
test-inj₁f = refl

inj₂f : {m n : ℕ} → Fin n → Fin (m + n)
inj₂f {m} i = {!!}

test-inj₂f : inj₂f {3}{4} (fsuc (fsuc fzero)) ≡ fsuc (fsuc (fsuc (fsuc (fsuc fzero))))
test-inj₂f = refl

f : {m n : ℕ} → Fin m ⊎ Fin n → Fin (m + n)
f i = {!!}

-- now backwards
f⁻¹ : {m n : ℕ} → Fin (m + n) → Fin m ⊎ Fin n   -- \^- \^1
f⁻¹ {m} i = {!!}

-- use f⁻¹
-- this essentially merges finite sequences
casef : {m n : ℕ}{C : Set} → (Fin m → C) → (Fin n → C) → Fin (m + n) → C
casef {m} g h i = {!!}

test-casef : casef {3}{3} (λ i → i) (λ i → i) (fsuc (fsuc fzero)) ≡ fsuc (fsuc fzero)
test-casef = refl
test-casef' : casef {3}{3} (λ i → i) (λ i → i) (fsuc (fsuc (fsuc fzero))) ≡ fzero
test-casef' = refl
test-casef'' : casef {3}{3} (λ i → i) (λ i → i) (fsuc (fsuc (fsuc (fsuc fzero)))) ≡ fsuc fzero
test-casef'' = refl

Fin+ : {m n : ℕ} → Fin (m + n) ↔ Fin m ⊎ Fin n
fst Fin+ = f⁻¹
snd Fin+ = f

-- this might be hard
Fin* : {m n : ℕ} → Fin (m * n) ↔ Fin m × Fin n
Fin* = {!!}

Fin*-test1 : snd (Fin* {3} {4}) (fst (Fin* {3} {4}) 4) ≡ 4
Fin*-test1 = refl
Fin*-test2 : snd (Fin* {10} {10}) (fst (Fin* {10} {10}) 50) ≡ 50
Fin*-test2 = refl
Fin*-test3 : snd (Fin* {6} {5}) (fst (Fin* {6} {5}) 23) ≡ 23
Fin*-test3 = refl
Fin*-test4 : fst (Fin* {6} {5}) (snd (Fin* {6} {5}) (4 , 4)) ≡ (4 , 4)
Fin*-test4 = refl
Fin*-test5 : fst (Fin* {7} {2}) (snd (Fin* {7} {2}) (3 , 1)) ≡ (3 , 1)
Fin*-test5 = refl
Fin*-test6 : fst (Fin* {2} {5}) (snd (Fin* {2} {5}) (0 , 4)) ≡ (0 , 4)
Fin*-test6 = refl

-- n-1
--  Σ  a i = a 0 + a 1 + ... + a (n-1)
-- i=0

Σℕ : (n : ℕ) → (Fin n → ℕ) → ℕ
Σℕ zero    a = 0
Σℕ (suc n) a = a fzero + Σℕ n (λ i → a (fsuc i))

-- not very easy
Σ+ : (n : ℕ)(a : Fin n → ℕ) → Σ (Fin n) (λ i → Fin (a i)) ↔ Fin (Σℕ n a)
Σ+ = {!!}

Σ+-test1 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Σ+ n f) (fst (Σ+ n f) (fzero , fzero)) ≡ (fzero , fzero)
Σ+-test1 = refl
Σ+-test2 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Σ+ n f) (fst (Σ+ n f) (1 , 1)) ≡ (1 , 1)
Σ+-test2 = refl
Σ+-test3 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Σ+ n f) (fst (Σ+ n f) (1 , 0)) ≡ (1 , 0)
Σ+-test3 = refl
Σ+-test4 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Σ+ n f) (fst (Σ+ n f) (3 , 2)) ≡ (3 , 2)
Σ+-test4 = refl
Σ+-test5 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Σ+ n f) (snd (Σ+ n f) 9) ≡ 9
Σ+-test5 = refl
Σ+-test6 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Σ+ n f) (snd (Σ+ n f) 0) ≡ 0
Σ+-test6 = refl
Σ+-test7 : let n = 4; f = λ {fzero → 1; (fsuc fzero) → 2; (fsuc (fsuc fzero)) → 3; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Σ+ n f) (snd (Σ+ n f) 5) ≡ 5
Σ+-test7 = refl

-- n-1
--  Π  a i = a 0 * a 1 * ... * a (n-1)
-- i=0

Πℕ : (n : ℕ) → (Fin n → ℕ) → ℕ
Πℕ zero    a = 1
Πℕ (suc n) a = a fzero * Πℕ n (λ i → a (fsuc i))

-- not very easy
Π* : (n : ℕ)(a : Fin n → ℕ) → ((i : Fin n) → Fin (a i)) ↔ Fin (Πℕ n a)
Π* = {!!}

Π*-test1 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Π* n f) (fst (Π* n f) (λ {fzero → 3; (fsuc fzero) → 4; (fsuc (fsuc fzero)) → 5; (fsuc (fsuc (fsuc fzero))) → 1})) 0 ≡ 3
Π*-test1 = refl
Π*-test2 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Π* n f) (fst (Π* n f) (λ {fzero → 3; (fsuc fzero) → 4; (fsuc (fsuc fzero)) → 5; (fsuc (fsuc (fsuc fzero))) → 1})) 1 ≡ 4
Π*-test2 = refl
Π*-test3 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Π* n f) (fst (Π* n f) (λ {fzero → 3; (fsuc fzero) → 4; (fsuc (fsuc fzero)) → 5; (fsuc (fsuc (fsuc fzero))) → 1})) 2 ≡ 5
Π*-test3 = refl
Π*-test4 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  snd (Π* n f) (fst (Π* n f) (λ {fzero → 3; (fsuc fzero) → 4; (fsuc (fsuc fzero)) → 5; (fsuc (fsuc (fsuc fzero))) → 1})) 3 ≡ 1
Π*-test4 = refl
Π*-test5 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Π* n f) (snd (Π* n f) 839) ≡ 839
Π*-test5 = refl
Π*-test6 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Π* n f) (snd (Π* n f) 12) ≡ 12
Π*-test6 = refl
Π*-test7 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Π* n f) (snd (Π* n f) 8) ≡ 8
Π*-test7 = refl
Π*-test8 : let n = 4; f = λ {fzero → 5; (fsuc fzero) → 7; (fsuc (fsuc fzero)) → 6; (fsuc (fsuc (fsuc fzero))) → 4} in
  fst (Π* n f) (snd (Π* n f) 230) ≡ 230
Π*-test8 = refl

---------------------------------------------------------
-- propositional logic
------------------------------------------------------

-- Curry-Howard izomorfizmus
-- Elmélet:
--   ∙ átalakítani logikai állításokat típusokra.
--   ∙ formalizálni állításokat típusokkal.
--   × = ∧ = konjunkció
--   ⊎ = ∨ = diszjunkció
--   ¬ = ¬ = negáció
--   ⊃ = → = implikáció

--------------------------------------------------
-- Formalisation
--------------------------------------------------

-- Formalizáljuk a mondatokat!

-- Az egyes formalizált alap mondatrészeket vegyük fel modul paraméterként, akkor szépen fog működni minden.
module Formalise where

  -- Nem süt a nap.
  form1 : Set
  form1 = {!!}

  -- Esik az eső és süt a nap.
  form2 : Set
  form2 = {!!}

  -- Nem kell az esernyő vagy esik az eső.
  form3 : Set
  form3 = {!!}

  -- Ha esik az eső és süt a nap, akkor van szivárvány.
  form4 : Set
  form4 = {!!}

  -- Van szivárvány.
  K : Set
  K = {!!}

---- Következményfogalom (logika tárgy 1-3. gyakorlat)
  -- Agdában legegyszerűbben szintaktikus következményekkel lehet foglalkozni.

  -- Mondd ki, és bizonyítsd be, hogy a fenti állításokból következik a K.
  -- A típusban kell kimondani az állítást; az állítás kimondásához az eldöntésprobléma tételét kell használni.
  -- Két féleképpen lehet bizonyítani.

  Köv : Set
  Köv = {!!}

  Köv1 : Köv
  Köv1 = {!!}

  Köv2 : Köv
  Köv2 = {!!}

----------------------------------------------------------------------------

subt-prod : {A A' B B' : Set} → (A → A') → (B → B') → A × B → A' × B'
subt-prod = {!!}

subt-fun : {A A' B B' : Set} → (A → A') → (B → B') → (A' → B) → (A → B')
subt-fun = {!!}

anything : {X Y : Set} → ¬ X → X → Y
anything = {!!}

ret : {X : Set} → X → ¬ ¬ X
ret = {!!}

fun : {X Y : Set} → (¬ X) ⊎ Y → (X → Y)
fun = {!!}

-- De Morgan

dm1 : {X Y : Set} →  ¬ (X ⊎ Y) ↔ ¬ X × ¬ Y
dm1 = {!!}

dm2 : {X Y : Set} → ¬ X ⊎ ¬ Y → ¬ (X × Y)
dm2 = {!!}

dm2b : {X Y : Set} → ¬ ¬ (¬ (X × Y) → ¬ X ⊎ ¬ Y)
dm2b = {!!}

-- stuff

nocontra : {X : Set} → ¬ (X ↔ ¬ X)
nocontra = {!!}

¬¬invol₁ : {X : Set} → ¬ ¬ ¬ ¬ X ↔ ¬ ¬ X
¬¬invol₁ = {!!}

¬¬invol₂ : {X : Set} → ¬ ¬ ¬ X ↔ ¬ X
¬¬invol₂ = {!!}

nnlem : {X : Set} → ¬ ¬ (X ⊎ ¬ X)
nnlem = {!!}

nndnp : {X : Set} → ¬ ¬ (¬ ¬ X → X)
nndnp = {!!}

dec2stab : {X : Set} → (X ⊎ ¬ X) → (¬ ¬ X → X)
dec2stab = {!!}

-- you have to decide:
{-
data Dec {i}(A : Set i) : Set i where
  yes : (a : A)    → Dec A
  no  : (¬a : ¬ A) → Dec A
-}

ee1 : {X Y : Set} → Dec (X ⊎ Y → ¬ ¬ (Y ⊎ X))
ee1 = {!!}

ee2 : {X : Set} → Dec (¬ (X ⊎ ¬ X))
ee2 = {!!}

e3 : {X : Set} → Dec (¬ (X → (¬ X → X)))
e3 = {!!}

e4 : Dec ℕ
e4 = {!!}

e5 : Dec ⊥
e5 = {!!}

e6 : {X : Set} → Dec (⊥ → X ⊎ ¬ X)
e6 = {!!}

e6' : {X : Set} → Dec ((⊥ → X) ⊎ ¬ X)
e6' = {!!}

e7 : {X : Set} → Dec (X × ¬ X → ¬ X ⊎ X)
e7 = {!!}

e8 : {X : Set} → Dec ((X → X) → ⊥)
e8 = {!!}

f1 : {X Y : Set} → ¬ ¬ X ⊎ ¬ ¬ Y → ¬ ¬ (X ⊎ Y)
f1 = {!!}

f2 : ({X Y : Set} → ¬ (X × Y) → ¬ X ⊎ ¬ Y) → {X Y : Set} → ¬ ¬ (X ⊎ Y) → ¬ ¬ X ⊎ ¬ ¬ Y
f2 = {!!}

----------------------------------------------------------------------
-- Not exactly first order logic but kinda is and kinda isn't.

f3 : Dec ((X Y : Set) → X ⊎ Y → Y)
f3 = {!!}

f4 : Dec ((X Y Z : Set) → (X → Z) ⊎ (Y → Z) → (X ⊎ Y → Z))
f4 = {!!}

f5 : Dec ((X Y Z : Set) → (X → Z) × (Y → Z) → (X × Y → Z))
f5 = {!!}

f6 : Dec ((X Y Z : Set) → (X × Y → Z) → (X → Z) × (Y → Z))
f6 = {!!}

f7 : Dec ((X Y Z : Set) → (X ⊎ Y × Z) → (X ⊎ Y) × (X ⊎ Z))
f7 = {!!}

f8 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → ((X ⊎ Y) × Z))
f8 = {!!}

f9 : Dec ((X Y Z : Set) → (X ⊎ Y) × (X ⊎ Z) → (X ⊎ Y × Z))
f9 = {!!}
