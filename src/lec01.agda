{-
Kaposi Ambrus
tipuselmelet
https://bitbucket.org/akaposi/ttt

tegezodunk

szunet 18.30--18.35

eloadason barmikor lehet bekiabalni

kapcsolodo targyak:
- funkcionalis programozas (Haskell)
  Agda = Haskell - rekurzio + fuggo tipusok
  Agda       sved     (Haskell)          tipuselmelesz kutatok
  Lean       amerikai (C, Lean)          Kevin Buzzard
  Coq(Rocq)  francia  (OCaml)            legregebbi, erett, matematikai bizonyitas, BoringSSL, CompCert
  Idris      skot     (Haskell, Idris)   programozas
- logika
  - elsorendu logika, halmazelmelet, elsodleges: predikatum (pl. Nat-on), fuggveny az masodlagos
  - tipuselmelet: nincs logika, elsodleges: fuggveny, prediktum megadhato Nat -> Bool fuggvenykent
  55-kor folytatjuk
  BeOS
  John Reynolds
- diszkret matek, algebra
- szamitaselmelet
  Turing-gep 1936
  λ-kalkulus (Church) 1930
  kombinator (Schönfinkel) 1926
    data Tm = App Tm Tm | S | K       Schönfinkel
    -- App (App K u) v ----> u
    -- App (App (App S u) v) w ----> App (App u w) (App v w)

    data Tm = App Tm Tm | Lam Int Tm | Var Int     Church
    -- App (Lam i t) u = t[i |-> u]

    (\x->x+1) 5 = (x+1)[x |-> 5] = 5+1 = 6

  App :: Tm -> Tm -> Tm

  típusos lambda kalkulus (Haskell):
  App :: Tm (a ~> b) -> Tm a -> Tm b

  App :: Tm ((x::A)->B) -> (a :: Tm A) -> Tm (B[x↦a])
  App :: Tm ((n::ℕ)->Mat(n,n)) -> (m:Tm ℕ) -> Tm(Mat(m,m))
  App f 3 :: Tm(Mat(3,3))
-}
