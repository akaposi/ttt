open import Lib hiding (id; _∘_)

-- 2. eloadas Agda

-- intro, functions (examples with pretended "built-in" ℕ)
-- 2.2. identity, composition, polymorphism

-- id :: forall (a :: *) . a -> a
id : ∀ {a : Set} → a → a  -- \->, \forall
id x = x
{-

    +---------+
3-->|   id    |-->3
    +---------+
-}

_∘_ : {A B C : Set} → (B → A) → (C → B) → C → A
(f ∘ g) a = f (g a)
{-
                +------+
          +-+   |      |    +-----+
          |f|-->|  _∘_ |--->| f∘_ |
          +-+   |      |    +-----+
                +------+

  +-+   +-----+   +---+
  |g|-->| f∘_ |-->|f∘g|
  +-+   +-----+   +---+

    +---+
c-->|f∘g|-->a
    +---+
-}

add2 : ℕ → ℕ
add2 x = 2 + x

f : ℕ → ℕ
f = add2 ∘ add2

-- 2.3. λ-calculus

add2' : ℕ → ℕ
add2' = λ x → 2 + x

-- currying (Schönfinkeling)

-- _+_ : ℕ → (ℕ → ℕ)   -- _+_ 2 = 2 +_

-- Haskell     Agda
-- (1+)        1 +_
-- (+1)        _+ 1
-- (1+1)       1 + 1

=→ : ℕ
=→ = 9

h : (ℕ → ℕ) → ℕ
h f = f 0 + f 1

-- h add2 =(h def) add2 0 + add2 1 =(add2 def) (2 + 0) + (2 + 1) =(alt.isk) 5

h' : (ℕ → ℕ) → ℕ
h' = λ f → f 0 + f 1

-- h' add2' =(h' def) (λ f → f 0 + f 1) add2' =(β redukcio)
-- (f 0 + f 1)[f ↦ add2'] = add2' 0 + add2' 1 =(add2' def)
-- (λ x → 2 + x) 0 + add2' 1 =(β) (2+0) + add2' 1 =(add2' def)
-- (2+0) + (λ x → 2 + x) 1 =(β) (2+0) + (2+x)[x↦1] = (2+0)+(2+1) =(alt.isk) 5

-- (x+y)^2 =(^2 def.) (x+y)*(x+y) =(distr) x*(x+y) + y*(x+y) =(distr) (x*x + x*y) + (y*x+y*y) =(...) ... = x*x + 2*x*y + y*y

-- altalanosan a β redukcio:   (λ x → t) u = t[x↦u],    1+1=2, ...

-- α: "kotott valtozo neve nem szamit"

-- 10
-- Σ    ( lim (z+1/x) )
-- z=0    x↦∞

-- (∀z y, z+y = y+z) === (∀x y, x+y = y+x)

-- int f(int y) { return (y + y); }

-- 1
-- ∫ 1/x dx
-- 0

-- kotott valtozo "nem erheto el" = neve nem szamit

-- (λ x y → x + y) = (λ x → (λ y → x + y)) 3 = (λ y → x + y)[x↦3] = λ y → 3 + y
-- (x + y)[y↦3] = x + 3
-- (λ y → x + y)[y↦3] = (λ z → x + z)[y↦3] = (λ z → x + z)
-- ((λ y → (λ y → y)) 3) 2 = ((λ y → (λ z → z)) 3) 2 =(β) ((λ z → z)[y↦3]) 2 = (λ z → z) 2 =(β) z[z↦2] = 2
-- (λ x → x + y)[y↦x] ≠ (λ x → x + x)  elkapas!!!! ROSSZ
-- (λ x → x + y)[y↦x] = (λ z → z + y)[y↦x] = (λ z → z + x)
{-

f : A → B    u : A 
------------------(1)
     f u : B


  Γ,x:A ⊢ t : B       -- turnstile, t-nek B a tipusa, ahol a t-ben lehet A tipusu x valtozo
---------------------(2)
Γ ⊢ (λ x → t) : A → B


n : ℕ    m : ℕ
--------------(3)
  n+m : ℕ

--------  ------   ....     
  1 : ℕ   2 : ℕ

-----------
true : Bool

----------------(5)
Γ,x:A,Δ ⊢ x:A


    (x : ℕ) ⊢ (x + x) : ℕ
  ------------------------
   (λ x → x + x) : ℕ → ℕ

 
-----------(5)  -----------(5)
x:ℕ,y:ℕ⊢x:ℕ     x:ℕ,y:ℕ⊢y:ℕ
---------------------------(3)
x:ℕ,y:ℕ ⊢ x+y : ℕ
------------------------------(2)
x:ℕ ⊢ (λ y → x + y) : ℕ → ℕ
---------------------------------(2)      -----
(λ x → (λ y → x + y)) : ℕ → ℕ → ℕ         3 : ℕ
-----------------------------------------------(1)
(λ x → (λ y → x + y)) 3 : ℕ → ℕ
-}

t : ℕ -> ℕ
t = (λ x → (λ y → x + y)) 3
{-
                     ?
-----------(5)   -----------
x:ℕ,y:ℕ⊢x:ℕ      x:ℕ,y:ℕ⊢z:ℕ
-------------------------------------(3)
x:ℕ,y:ℕ ⊢ x + z : ℕ
------------------------------(2)
x:ℕ ⊢ (λ y → x + z) : ℕ → ℕ
---------------------------------(2)      -----
(λ x → (λ y → x + z)) : ℕ → ℕ → ℕ         3 : ℕ
-----------------------------------------------(1)
(λ x → (λ y → x + z)) 3 : ℕ → ℕ

    ???
--------------   --------------(5)
x:Bool,y:ℕ⊢x:ℕ   x:Bool,y:ℕ⊢y:ℕ
-------------------------------(3)
x:Bool,y:ℕ⊢x+y:ℕ
------------------(2)
x:Bool⊢λy→x+y:ℕ→ℕ
--------------------------------(2)                                     
(λ x → (λ y → x + y)) : Bool→ℕ→ℕ    true : Bool
-----------------------------------------------(1)
(λ x → (λ y → x + y)) true : ℕ → ℕ
-}

-- t' : ℕ -> ℕ
-- t' = (λ x → (λ y → x + y)) true

-- 2.4 combinatory logic, kombinator kalkulus, SK kalkulus, Moses Schönfinkel
{-
f : A → B    u : A 
------------------(1)
     f u : B

-------------(2)
K : A → B → A

---------------------(3)
S : (A→B→C)→(A→B)→A→C

K a b = a
S f g u = f u (g u)

S K K a = K a (K a) = a

(λ x → x)   := S K K                              (a)
(λ x → t)   := K t  , ahol t-ben nem szerepel x   (b)
(λ x → t u) := S (λ x → t) (λ x → u)              (c)

(λ x y → y) = (λ x → (λ y → y)) =(a) (λ x → S K K) =(b) K (S K K)

(λ x y → y) a b = b
(K (S K K) a) b = S K K b = K b (K b) = b
-}
-- ℕ, 1,2,3,4,+
-- Bool, true, false, if-then-else

-- 2.5 products, sums, finite types
{-
 Haskell                         Agda            Simonyi         Haskell    Agda
 (True,3):(Bool,Int)    (true,1):Bool × Int      x_int : int     a : a      a : A

 Either Bool Bool                Bool ⊎ Bool
 Left True,Right True,           inl true, inr true
 Left False,Right False          inl false, inr false
-}
-- derivation of typing using derivation rules

-- kov. ora 3 perccel rovidebb
