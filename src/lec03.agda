open import Lib hiding (_⊎_; case; _×_; _,_; ⊤; ⊥; Bool; true; false; inl; inr; ℕ; zero; suc)

-- 2.5 products, sums, finite types

-- data-val kezdodik: induktiv tipus: konstruktorai hatarozzak meg, destuktora az ebbol kovetkezik
data _⊎_ (A B : Set) : Set where  -- data a :+: b = Inl a | Inr b
  inl : A → A ⊎ B
  inr : B → A ⊎ B

-- destruktort mintaillesztessel adjuk meg:
case : {A B C : Set} → (A → C) → (B → C) → A ⊎ B → C
case f g (inl a) = f a
case f g (inr b) = g b

-- η:   (f : (x:A⊎B)→C) → f = case (λ a → f (inl a)) (λ b → f (inr b))

{-
OrderNum = ℕ
CustRef = ℕ
Complaint = OrderNum ⊎ CustRef
-}

-- nullaris osszeg:

data ⊥ : Set where

des⊥ : {A : Set} → ⊥ → A
des⊥ ()                     -- confusion: Haskellben () az Agdaban ⊤

-- koinduktiv tipus: destruktorai
record _×_ (A B : Set) : Set where
  field
    fst : A  -- fst : A × B → A
    snd : B  -- snd : A × B → B
open _×_

-- konstruktort komintaillesztessel adjuk meg
_,_ : {A B : Set} → A → B → A × B
fst (a , b) = a -- szorzat tipusra vonatkozo β szabalyok
snd (a , b) = b

--                        η (kon(des))                                          β(des(kon))
-- fuggveny tipusra η:    ha van egy f : A → B, akkor f = (λ x → f x)           (λ x → t) u = t[x↦u]
-- szorzat  tipusra η:    ha van egy w ∶ A × B, akkor w = (fst w,snd w)         fst (a,b) = a, snd (a,b) = b

-- fuggveny extenzionalitas:
-- f x = g x ----> (λ x → f x) = (λ x → g x) ----> f = g

-- A tipusban n elem van, B tipusban m elem van, akkor A×B-ben n*m
-- A tipusban n elem van, B tipusban m elem van, akkor A⊎B-ben n+m

-- ternaris szorzat: A × (B × C), (A × B) × C
-- unaris szorzat: A
-- nullaris szorzat: un × B =    1*n = ∣⊤|*∣B∣ = |⊤ × B| = |B| = n         |A| = A tipus elemszama
-- ∣B∣ = n

record ⊤ : Set where

ttt : ⊤
ttt = record {}

-- η : minden w : ⊤ -re teljesul, hogy w = ttt; ⊤ tetszoleges ket eleme =.

-- 0,1,+,×,^  |A→B| = |B|^|A|

-- 6.43-kor folytatjuk

-- inductive types using data, Bool

Bool = ⊤ ⊎ ⊤
true false : Bool
true = inl ttt
false = inr ttt

false' : ⊥ ⊎ ⊤
false' = inr {_}{_} ttt

ifthenelse : {C : Set} → Bool → C → C → C
ifthenelse b t f = case (λ _ → t) (λ _ → f) b

-- ifthenelse true t f = case (λ _ → t) (λ _ → f) (inl ttt) = (λ _ → t) ttt = t 

{-
  ttt : ⊤
---------------
inl ttt : ⊤ ⊎ ⊤
-}

-- (f ∘ g)

-- 4.1-4.2 inductive types: Nat, Maybe, Ackermann, iterator-recursor

data ℕ : Set where
  zero : ℕ
  suc  : ℕ → ℕ

-- 3 = suc (suc (suc zero))

_*2+3 : ℕ → ℕ
zero *2+3 = suc (suc (suc zero))
suc n *2+3 = suc (suc (n *2+3))
-- suc n * 2 + 3 = (1+n)*2 + 3 = 1*2 + n*2+3 = 2 + n*2+3 = suc (suc (n*2+3))

-- suc (suc zero) *2+3 = suc (suc (suc zero *2+3)) = suc (suc (suc (suc (zero *2+3)) = 
--     \________/                      \__/
--        n                             n
--
-- = suc (suc (suc (suc (suc (suc (suc zero))))

_*2+2 : ℕ → ℕ
zero *2+2 = suc (suc zero)
suc n *2+2 = suc (suc (n *2+2))
-- suc n *2+2 = (1+n) *2+2 =

-- iterator, rekurzor, fold, eliminator, catamorphism

ite : {C : Set} → C → (C → C) → (ℕ → C)
ite z s zero = z
ite z s (suc n) = s (ite z s n)
-- ite z s n az n-ben a zero-t lecsereli z-re, a suc-okat pedig s-re

*2+2' : ℕ → ℕ
*2+2' = ite (suc (suc zero)) (λ n → suc (suc n))

-- kov. 8 perccel rovidebb

-- 4.3 List, Expr, RoseTree, (Ord)                                  
