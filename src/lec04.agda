module lec04 where

open import Lib


-- 4.4 Pozitivitás
--
{-# NO_POSITIVITY_CHECK #-}
data W : Set where
  w : (W → ⊥) → W
--     ^
--  Negatív pozícióban van

unW : W → ⊥
unW (w f) = f (w f)

bad : ⊥ -- Upsz
bad = unW (w λ t → unW t)
-- f = λ x → f x  <- Függvény η-szabálya

h : ∀{A A' B B' : Set} → (A → A') → (B → B') → (A ⊎ B)
                       → (A' ⊎ B')
h aa' bb' (inl a) = inl (aa' a)
h aa' bb' (inr b) = inr (bb' b)

g : ∀{A A' B B' : Set} → (A → A') → (B → B')
                       → (A × B) → (A' × B')
g aa' bb' (a , b) = aa' a , bb' b

f : ∀{A A' B B' : Set} → (A' → A) → (B → B')
                       → (A → B) → (A' → B')
f a'a bb' ab = λ a' → bb' (ab (a'a a'))

{-
data W : Set where
  w : ((W → ⊥) → ⊥) → W
        ^
    Pozitív pozícióban van, de ezzel még mindig inkonzisztens

Szigorúan pozitív típusok kellenek csak -> Soha nincs a típusom semmilyen nyílnak a bal oldalán.
-}
-----------------------------------------------

{-
data List (A : Set) : Set where
  []  : List A
  _∷_ : A → List A → List A
-}

_+++_ : ∀{A : Set} → List A → List A → List A
[]       +++ ys = ys
(x ∷ xs) +++ ys = x ∷ (xs +++ ys)

-- [1..] ++ [9,10,22,10,1] -- Ilyen nem megy agdában induktívan.

-- 4.5 Koinduktív típusok
-- Stream

record Stream' (A : Set) : Set where
  coinductive
  field
    head'' : A
    tail'' : Stream' A

open Stream'

-- Mintaillesztés
down : ℕ → List ℕ
down zero = 0 ∷ []
down k@(suc n) = k ∷ down n

-- Komintaillesztés
[_-∞] : ℕ → Stream ℕ
head [ n -∞] = n
tail [ n -∞] = [ suc n -∞] -- korekurzió
{-
data ℕ
  zero : ℕ
  suc : ℕ → ℕ
-}
-- Iterátor:
--               zero   suc
--                 v    vvvvv
iteℕ : {A : Set} → A → (A → A) → ℕ → A
iteℕ z s zero    = z
iteℕ z s (suc n) = s (iteℕ z s n)

-- Stream A
-- head : Stream A → A
-- tail : Stream A → Stream A

-- Koiterátor:
--                       head     tail
--                      vvvvv     vvvvv
coiteS : {A S : Set} → (S → A) → (S → S) → S → Stream A
head (coiteS h t s) = h s
tail (coiteS h t s) = coiteS h t (t s)

{-
[_-∞] : ℕ → Stream ℕ
head [ x -∞] = x
tail [ y -∞] = [ suc y -∞]
-}

[_-∞]' : ℕ → Stream ℕ
[ n -∞]' = coiteS (λ x → x) (λ y → suc y) n

-- Haskell-ben:
-- data Stream a = Cons a (Stream a)

-- kövi előadás 10 perccel rövidebb
